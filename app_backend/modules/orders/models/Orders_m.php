<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orders_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->select('b.fullname')
                        ->select('c.couponCode,c.type as coupons_type,c.discount as coupons_discount')
                        ->from('orders a')
                        ->join('users b', 'a.created_by = b.user_id', 'inner')
                        ->join('coupons c', 'a.coupon_id = c.coupon_id', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.fullname')
                        ->select('c.couponCode')
                        ->from('orders a')
                        ->join('users b', 'a.created_by = b.user_id', 'inner')
                        ->join('coupons c', 'a.coupon_id = c.coupon_id', 'left')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.fullname', $param['keyword'])
                    ->or_like('a.order_code', $param['keyword']) 
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['status']) && $param['status'] != "" ) {
            $this->db->where('a.status', $param['status']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.fullname', $param['search']['value'])
                    ->or_like('a.order_code', $param['search']['value']) 
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.order_code";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.created_at";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "b.fullname";
                //if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['order_id']) ) 
            $this->db->where('a.order_id', $param['order_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }
    
    public function insert($value) {
        $this->db->insert('orders', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('order_id', $id)
                        ->update('orders', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('order_id', $id)
                        ->update('orders', $value);
        return $query;
    } 
    public function update_course_studens_in($id, $value)
    {
       
        $query = $this->db
                        ->where_in('order_id', $id)
                        ->update('courses_students', $value);
        return $query;
    }
    
    public function get_course_studens($id)
    {
        $query = $this->db
                        ->where_in('order_id', $id)
                        ->get('courses_students');
        return $query;
    }

    public function get_courses($order_id)
    { 

       return $this->db
                ->select('c.title')
                ->from('orders a')
                ->join('courses_students b','a.order_id=b.order_id','left')
                ->join('courses c','b.course_id=c.course_id','left')
                ->where('a.order_id', $order_id)
                ->get()->result();   
    }
}
