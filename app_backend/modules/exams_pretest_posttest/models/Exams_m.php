<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Exams_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title as course_name')
                        ->from('exam a')
                        ->join('courses b', 'a.course_id = b.course_id', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title as course_name')
                        ->from('exam a')
                        ->join('courses b', 'a.course_id = b.course_id', 'left')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 5) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['course_id']) ) 
            $this->db->where('a.course_id', $param['course_id']);

         if ( isset($param['exam_id']) ) 
            $this->db->where('a.exam_id', $param['exam_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);


        $this->db->where('a.pretest_posttest', 1);

    }
    
    public function insert($value) {
        $this->db->insert('exam', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('exam_id', $id)
                        ->update('exam', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('exam_id', $id)
                        ->update('exam', $value);
        return $query;
    }    

}
