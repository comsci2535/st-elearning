<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">หัวข้อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row" style="display: none;">
                    <label class="col-sm-2 col-form-label" for="slug">Slug</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug-1" id="slug-1" >
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                </div>

                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">รายละเอียดย่อ</label>
                    <div class="col-sm-7">
                        <textarea name="excerpt" rows="3" class="form-control summernote" id="excerpt" required><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="descript">เวลาในการทำข้อสอบ</label>
                    <div class="col-sm-3">
                       <input value="<?php echo isset($info->examTime) ? $info->examTime : NULL ?>" type="number" id="input-examTime" class="form-control" name="examTime" required>
                    </div>
                    <div class="col-sm-3" style="text-align: left;">
                     (นาที)
                    </div>
                </div>  
                 <div class="form-group m-form__group row" style="display: none;">
                    <label class="col-sm-2 col-form-label" for="descript">จำนวนครั้งที่สอบ (ไม่เกิน)</label>
                    <div class="col-sm-3">
                         <input value="<?php echo isset($info->examNo) ? $info->examNo : 2 ?>" type="number" id="input-examNo" class="form-control" name="examNo" required>
                    </div>
                    <div class="col-sm-3" style="text-align: left;">
                     (ครั้ง)
                    </div>
                </div>
                 
                 <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="descript">สุ่มข้อ</label>
                    <div class="col-sm-7">
                       <label class="icheck-inline"><input type="radio" name="answerRandom" value="1" class="icheck" <?php if(isset($info->answerRandom) && $info->answerRandom=="1"){?> checked <?php } ?>/> สุ่ม</label>
                        <label class="icheck-inline"><input type="radio" name="answerRandom" value="0" class="icheck" <?php if(isset($info->answerRandom) && $info->answerRandom=="0"){?> checked <?php } ?>/> ไม่สุ่ม</label>
                    </div>
                </div>
                 <div class="form-group m-form__group row" style="display: none;">
                    <label class="col-sm-2 col-form-label" for="descript">จำนวนข้อที่ออกสอบ</label>
                    <div class="col-sm-3">
                         <input value="<?php echo isset($info->examNumber) ? $info->examNumber : NULL ?>" type="text" id="input-examNumber" class="form-control" name="examNumber" >
                    </div>
                </div>
                 <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="descript">ผ่าน</label>
                    <div class="col-sm-3">
                         <input value="<?php echo isset($info->pass) ? $info->pass : NULL ?>" type="number" id="input-pass" class="form-control" name="pass" required>
                    </div>
                    <div class="col-sm-3" style="text-align: left;">
                     (%)
                    </div>
                </div>
                 <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="descript">กำหนดวันสอบ</label>
                    <div class="col-sm-3">
                        <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>" />
                        <input type="hidden" name="startDate" value="<?php echo isset($info->startDate) ? $info->startDate : NULL ?>" />
                        <input type="hidden" name="endDate" value="<?php echo isset($info->endDate) ? $info->endDate : NULL ?>" />
                    </div>
                </div>
                
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="course_id" id="input-course_id" value="<?php echo $course_id ?>">
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
           <!--   <input type="hidden" class="form-control" name="db" id="db" value="exam"> -->
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->exam_id) ? encode_id($info->exam_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=$info->exam_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->exam_id;?>';

</script>




