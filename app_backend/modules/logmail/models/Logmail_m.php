<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logmail_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('send_email_log a')
                        ->order_by("a.send_email_log_id", "desc")
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('send_email_log a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }    

        // END form filter
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.subject', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->or_like('a.email_send', $param['search']['value'])
                    ->or_like('a.email_to', $param['search']['value'])
                    ->group_end();
        }
        
        if ( isset($param['send_email_log_id']) ) 
            $this->db->where('a.send_email_log_id', $param['send_email_log_id']);

    }
    
    public function insert($value) {
        $this->db->insert('send_email_log', $value);
        return $this->db->insert_id();
    }

    public function insert_send_email($value) {
        $this->db->insert('send_email', $value);
        return $this->db->insert_id();
    }

    public function update($id, $value)
    {
        $query = $this->db
                        ->where('send_email_log_id', $id)
                        ->update('send_email_log', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('send_email_log_id', $id)
                        ->update('send_email_log', $value);
        return $query;
    }    

}
