<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        รายละเอียดอีเมล์
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>


        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
        <div class="m-portlet__body">

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">จาก</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info->email_send) ? $info->email_send : NULL ?>" type="text"
                        class="form-control m-input " name="email_send" id="email_send" required>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">ส่งถึง</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info->email_to) ? $info->email_to : NULL ?>" type="text"
                        class="form-control m-input " name="email_to" id="email_to" required>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="slug">CC </label>
                <div class="col-sm-7">
                    <select id="article_categorie_id" name="email_cc[]"
                        class="form-control m-input  select2" multiple required>
                        <option value="">เลือก</option>
                        <?php
                            if(count($email_arr) > 0):
                                foreach($email_arr as $item):
                                ?>
                        <option value="<?=$item;?>" selected><?=$item?></option>
                        <?php
                                endforeach;
                            endif;
                            ?>
                    </select>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">หัวข้อ</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info->subject) ? $info->subject : NULL ?>" type="text"
                        class="form-control m-input " name="subject" id="subject" required>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">รายละเอียด</label>
                <div class="col-sm-7">
                    <textarea name="detail" rows="3" class="form-control summernote" id="detail"
                        required><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="file">ไฟล์ </label>
                <div class="col-sm-7">
                    <div class="m-widget4" style="padding: 20px; margin: 10px 0 30px 0; border: 4px solid #efefef">
                    <?php
                        if(count($file_arr) > 0):
                            foreach($file_arr as $key => $item_file):
                                $type_file = substr($item_file,-3);

                                if($type_file === 'jpg' || $type_file === 'JPG'){
                            ?>
                                <div class="m-widget4__item" id="key_<?=$key?>">
                                    <div class="m-widget4__img m-widget4__img--icon">
                                        <img src="<?=$this->config->item('template'); ?>assets/app/media/img/files/jpg.svg" alt="">
                                    </div>
                                    <div class="m-widget4__info">
                                        <span class="m-widget4__text">
                                            <input type="hidden" name="file[]" value="<?=$item_file?>"> 
                                            <?=$item_file?>
                                        </span>
                                    </div>
                                    <div class="m-widget4__ext">
                                        <a href="javascript:void(0)" class="m-widget4__icon btn_delete" data-key="<?=$key?>">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php }else if($type_file === 'pdf' || $type_file === 'PDF'){ ?>
                                <div class="m-widget4__item" id="key_<?=$key?>">
                                    <div class="m-widget4__img m-widget4__img--icon">
                                        <img src="<?=$this->config->item('template'); ?>assets/app/media/img/files/pdf.svg" alt="">
                                    </div>
                                    <div class="m-widget4__info">
                                        <span class="m-widget4__text">
                                            <input type="hidden" name="file[]" value="<?=$item_file?>">
                                            <?=$item_file?>
                                        </span>
                                    </div>
                                    <div class="m-widget4__ext">
                                        <a href="javascript:void(0)" class="m-widget4__icon btn_delete" data-key="<?=$key?>">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php }else if($type_file === 'zip' || $type_file === 'ZIP'){ ?>
                                <div class="m-widget4__item" id="key_<?=$key?>">
                                    <div class="m-widget4__img m-widget4__img--icon">
                                        <img src="<?=$this->config->item('template'); ?>assets/app/media/img/files/zip.svg" alt="">
                                    </div>
                                    <div class="m-widget4__info">
                                        <span class="m-widget4__text">
                                            <input type="hidden" name="file[]" value="<?=$item_file?>">
                                            <?=$item_file?>
                                        </span>
                                    </div>
                                    <div class="m-widget4__ext">
                                        <a href="javascript:void(0)" class="m-widget4__icon btn_delete" data-key="<?=$key?>">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </div>
                                </div>
                            <?php }else if($type_file === 'doc' || $type_file === 'DOC'){ ?>
                                <div class="m-widget4__item" id="key_<?=$key?>">
                                    <div class="m-widget4__img m-widget4__img--icon">
                                        <img src="<?=$this->config->item('template');?>assets/app/media/img/files/doc.svg" alt="">
                                    </div>
                                    <div class="m-widget4__info">
                                        <span class="m-widget4__text">
                                            <input type="hidden" name="file[]" value="<?=$item_file?>">
                                            <?=$item_file?>
                                        </span>
                                    </div>
                                    <div class="m-widget4__ext">
                                        <a href="javascript:void(0)" class="m-widget4__icon btn_delete" data-key="<?=$key?>">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </div>
                                </div>
                        <?php
                                }
                                endforeach;
                        else :
                        ?>
                            <p class="text-center">ไม่มีไฟล์</p>
                        <?php        
                        endif;
                            ?>
                    </div>

                </div>
            </div>

        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">

            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button type="submit" class="btn btn-success m-btn--wide">ส่งอีเมล์ใหม่</button>
                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                    </div>
                </div>

            </div>
        </div>
        <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
        <input type="hidden" class="form-control" name="db" id="db" value="articles">
        <input type="hidden" name="id" id="input-id"
            value="<?php echo isset($info->article_id) ? encode_id($info->article_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>



</div>

<script>
    //set par fileinput;
    var required_icon = true;
    var file_image = '<?=(isset($info->file)) ? $this->config->item('
    root_url ').$info->file : '
    '; ?>';
    var file_id = '<?=$info->article_id;?>';
    var deleteUrl = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->article_id;?>';
</script>