<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Logmail extends MX_Controller {

    private $_title = "ประวัติการส่งอีเมล์";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ ประวัติการส่งอีเมล์";
    private $_grpContent = "logmail";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("logmail_m");

        $this->status = array('รอส่ง','ส่งแล้ว');
    }
    
    public function index(){
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf' => site_url("{$this->router->class}/pdf"),
        );
        // $action[1][] = action_refresh(site_url("{$this->router->class}"));
        // $action[1][] = action_filter();
        // $action[1][] = action_add(site_url("{$this->router->class}/create"));
        //$action[2][] = action_export_group($export);
        // $action[3][] = action_trash_multi("{$this->router->class}/destroy");
        // $action[3][] = action_trash_view(site_url("{$this->router->class}/trash"));
        // $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index(){
        $input = $this->input->post();
        $info = $this->logmail_m->get_rows($input);
        $infoCount = $this->logmail_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->send_email_log_id);
            $action = array();
            $action[1][] = table_sendNews(site_url("{$this->router->class}/tblsendnews/{$id}"));
            $action[1][] = table_view(site_url("{$this->router->class}/edit/{$id}"));

            if($rs->status > 0){
                $color = 'm-badge--success';
            }else{
                $color = 'm-badge--warning';
            }
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['subject'] = $rs->subject;
            $column[$key]['email_send'] = $rs->email_send;
            $column[$key]['email_to'] = $rs->email_to;
            $column[$key]['status'] = '<p class="m-badge '.$color.' m-badge--wide">'.$this->status[$rs->status].'</p>';
            $column[$key]['send_date'] = datetime_table($rs->send_date);
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function edit($id=""){
        $this->load->module('template');
        
        $id = decode_id($id);
        $input['send_email_log_id'] = $id;
        $info = $this->logmail_m->get_rows($input);

        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/storage");
        
        //email_arr
        $email_arr = array();
        if(!empty($info->email_cc))
            $email_arr = explode(",",$info->email_cc);

        $data['email_arr'] = $email_arr; 

        //file_arr
        $file_arr = array();
        if(!empty($info->file))
            $file_arr = explode(",",$info->file);

        $data['file_arr'] = $file_arr; 

        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('รายละเอียดอีเมล์', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function tblsendnews($id=""){
        
        $id = decode_id($id);
        $input['send_email_log_id'] = $id;
        $info = $this->logmail_m->get_rows($input);

        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();

        $param = array();
        $param['subject']       = $info->subject;
        $param['detail']        = $info->detail;
        $param['email_send']    = $info->email_send;
        $param['email_to']      = $info->email_to;
        $param['email_cc']      = $info->email_cc;
        $param['send_date']     = db_datetime_now();;
        $param['file']          = $info->file;

        $result = $this->logmail_m->insert_send_email($param);

        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'ส่งอีเมล์ใหม่เรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'ส่งอีเมล์ใหม่ไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$course_lesson_id}"));
    }

    public function storage(){
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->logmail_m->insert_send_email($value);

        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'ส่งอีเมล์ใหม่เรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'ส่งอีเมล์ใหม่ไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$course_lesson_id}"));
    }
    
    private function _build_data($input){
        
        $value['subject'] = $input['subject'];
        $value['detail'] = $input['detail'];
        $value['email_send'] = $input['email_send'];
        $value['email_to'] = $input['email_to'];
        $value['send_date'] = db_datetime_now();

        if(count($input['email_cc']) > 0){
            $email_cc_arr;
            foreach($input['email_cc'] as $item_email){
                $email_cc_arr .= $item_email.',';
            }
        }
        
        $value['email_cc'] = $email_cc_arr;

        if(count($input['file']) > 0){
            $file_arr;
            foreach($input['file'] as $item_file){
                $file_arr .= $item_file.',';
            }
        }
        $value['file'] = $file_arr;

        return $value;
    }


    // ----------------------------------------------
    public function excel(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->logmail_m->get_rows($input);
        $fileName = "logmail";
        $sheetName = "Sheet name";
        $sheetTitle = "Sheet title";
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'created_at',            
            'D' => 'updated_at',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->logmail_m->get_rows($input);
        $data['info'] = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font' => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "logmail.pdf";
        $pathFile = "uploads/pdf/logmail/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash(){
        $this->load->module('template');
        
        // toobar
        $action[1][] = action_list_view(site_url("{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash(){
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->logmail_m->get_rows($input);
        $infoCount = $this->logmail_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->repoId);
            $action = array();
            $action[1][] = table_restore("{$this->router->class}/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    } 

    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->logmail_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->logmail_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->logmail_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }      
    
    public function action($type=""){
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->logmail_m->update_in($input['id'], $value);
            }
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids){

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
	}
    
}
