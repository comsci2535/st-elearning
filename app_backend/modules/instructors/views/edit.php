<div class="col-md-12">
    <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
        <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                <i class="flaticon-share m--hide"></i>
                                ข้อมูลทั่วไป
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                เปลี่ยนรหัสผ่าน
                            </a>
                        </li>
                        
                    </ul>
                </div>
        </div>

       
       
        <div class="tab-content">
            <div class="tab-pane active" id="m_user_profile_tab_1">
                <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-edit', 'method' => 'post')) ?>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title"></label>
                    <div class="col-sm-7">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php if(!empty($info->recommend) &&  $info->recommend=='1'){ echo "checked";}?> type="checkbox" name="recommend" valus="1"> รายการแนะนำ
                            <input type="hidden" name="approve" value="<?php echo isset($info->approve) ? $info->approve : 0 ?>">
                            <span></span>
                        </label>
                    </div>
                </div>
                 <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label" for="file">โปรไฟล์ <span class="text-danger"> *</span></label>
                        <div class="col-sm-7">
                            <input id="file" name="file" type="file" data-preview-file-type="text">
                             <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : ''; ?>">
                        </div>
                    </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อ <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->fname) ? $info->fname : NULL ?>" type="text" id="input-fname" class="form-control" name="fname" required>
                    </div>
                </div>  

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >นามสกุล <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->lname) ? $info->lname : NULL ?>" type="text" id="input-lname" class="form-control" name="lname">
                    </div>
                </div> 
               
                
               <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >อีเมล <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->username) ? $info->username : NULL ?>" type="email" id="input-username" class="form-control" name="username" readonly>
                    </div>
                </div>  
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title"></label>
                    <div class="col-sm-7">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php if(!empty($info->private) &&  $info->private=='1'){ echo "checked";}?> type="checkbox" name="private" valus="1" id="checkview"> ผู้สอน Private
                            <span></span>
                        </label>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for=""></label>
                    <div class="col-sm-7">
                        <ul class="nav nav-tabs  m-tabs-line" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#m_tabs_1_4" role="tab"
                                    aria-selected="true">ความชำนาญ 
                                    <!-- <span class="text-danger"> *</span> -->
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_1_3" role="tab"
                                    aria-selected="false">ประสบการณ์</a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_1_1" role="tab"
                                    aria-selected="false">ข้อมูลติดต่อ</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="m_tabs_1_1" role="tabpanel">
                                <div class="form-group m-form__group row">
                                    <!-- <div class="col-sm-6">
                                        <label class="col-form-label" for="email">อีเมล</label>
                                        <input name="email" value="<?php echo isset($info->email) ? $info->email : NULL ?>"
                                            type="text" class="form-control" >
                                    </div> -->
                                    <div class="col-sm-6">
                                        <label class="col-form-label" for="phone">เบอร์โทรศัพท์</label>
                                        <input name="phone" value="<?php echo isset($info->phone) ? $info->phone : NULL ?>"
                                            type="text" class="form-control format-phone">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label" for="lineID">LineID</label>
                                        <input name="lineID" value="<?php echo isset($info->lineID) ? $info->lineID : NULL ?>"
                                            type="text" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-sm-6">
                                        <label class="col-form-label" for="facebook">Facebook</label>
                                        <input name="facebook" value="<?php echo isset($info->facebook) ? $info->facebook : NULL ?>"
                                            type="text" class="form-control" >
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label" for="instagram">Instagram</label>
                                        <input name="instagram" value="<?php echo isset($info->instagram) ? $info->instagram : NULL ?>"
                                            type="text" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-sm-6">
                                        <label class="col-form-label" for="linkedin">Linkedin</label>
                                        <input name="linkedin" value="<?php echo isset($info->linkedin) ? $info->linkedin : NULL ?>"
                                            type="text" class="form-control" >
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label" for="twitter">Twitter</label>
                                        <input name="twitter" value="<?php echo isset($info->twitter) ? $info->twitter : NULL ?>"
                                            type="text" class="form-control" >
                                    </div>
                                </div>
                                
                            </div>
                            <div class="tab-pane" id="m_tabs_1_3" role="tabpanel">
                                <div>
                                    <textarea name="experience" rows="3" class="form-control summernote" id="experience"
                                        required><?php echo isset($info->experience) ? $info->experience : NULL ?></textarea>
                                </div>
                            </div>
                            <div class="tab-pane active show" id="m_tabs_1_4" role="tabpanel">
                                <div>
                                    <textarea name="skill_long" rows="3" class="form-control summernote" id="skill_long"
                                        required><?php echo isset($info->skill_long) ? $info->skill_long : NULL ?></textarea>
                                </div>

                                <!-- <div class="form-group m-form__group row d-none">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <select class="form-control m-select2" id="m_select2_11" multiple name="skill[]" style="width: 100%">
                                                <option></option>
                                                <?php if(!empty($instructors_skill)){ foreach ($instructors_skill as $key => $value) { ?>
                                                <option value="<?=$value->skill_id?>" <?php if(in_array($value->skill_id, $skill)){ echo "selected"; } ?>><?=$value->skill_name?></option>
                                                <?php }} ?>
                                                
                                        </select>
                                    </div>
                                </div> -->

                            </div>
                        </div>
                    </div>
                </div>  
                
                <div class="m-portlet__foot m-portlet__foot--fit">

                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2">
                            </div>
                            <div class="col-10">
                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">บันทึก</button>
                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                            </div>
                        </div>
                        
                    </div>
                </div>
                 <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                 <input type="hidden" class="form-control" name="db" id="db" value="repo">
                 <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->user_id) ? encode_id($info->user_id) : 0 ?>">
                <?php echo form_close() ?>
            </div>
            <div class="tab-pane " id="m_user_profile_tab_2">
            <?php echo form_open_multipart($frmActionPassword, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-change-password', 'method' => 'post')) ?>
                <div class="m-portlet__body">
                    
                   
                    <!-- <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label"><h4 class="box-title">เปลี่ยนรหัสผ่าน</h4></label>
                        <div class="col-sm-7">
                            
                        </div>
                    </div> -->
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label">รหัสผ่าน</label>
                        <div class="col-sm-7">
                            <input value="" type="password" id="input-password" class="form-control" name="password" required>
                        </div>
                    </div>
                    
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label">ยืนยันรหัสผ่าน</label>
                        <div class="col-sm-7">
                            <input value="" type="password" id="input-repassword" class="form-control" name="rePassword">
                        </div>
                    </div>   
                     
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">

                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2">
                            </div>
                            <div class="col-10">
                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">บันทึก</button>
                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                            </div>
                        </div>
                        
                    </div>
                </div>
                 <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                 <input type="hidden" class="form-control" name="db" id="db" value="repo">
                 <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->user_id) ? encode_id($info->user_id) : 0 ?>">
            <?php echo form_close() ?>

            </div>
        </div>
    </div>


    
</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : 'not img'; ?>';
    var file_id         = '<?=$info->user_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->user_id;?>';


</script>


