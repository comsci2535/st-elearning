<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Exercise_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title as course_lesson_name')
                        ->from('exercise a')
                        ->join('courses_lesson b', 'a.course_lesson_id = b.course_lesson_id', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                       ->select('a.*')
                        ->select('b.title as course_lesson_name')
                        ->from('exercise a')
                        ->join('courses_lesson b', 'a.course_lesson_id = b.course_lesson_id', 'left')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 

          if ( isset($param['course_lesson_id']) ) 
            $this->db->where('a.course_lesson_id', $param['course_lesson_id']);
        
        if ( isset($param['exercise_id']) ) 
            $this->db->where('a.exercise_id', $param['exercise_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }
    
    public function insert($value) {
        $this->db->insert('exercise', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('exercise_id', $id)
                        ->update('exercise', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('exercise_id', $id)
                        ->update('exercise', $value);
        return $query;
    }    

    public function get_exercise_by_course_lesson_id($course_lesson_id)
    {
        $query = $this->db
                        ->from('exercise a')
                        ->where('a.course_lesson_id', $course_lesson_id)
                        ->get();
        return $query;
    } 

    public function get_exercise_by_exercise_user_id($exercise_id, $user_id)
    {
        $query = $this->db
                        ->from('quiz_exercise a')
                        ->where('a.exercise_id', $exercise_id)
                        ->where('a.user_id', $user_id)
                        ->get();
        return $query;
    } 
    

    public function get_quiz_exercise_by_user_id($user_id)
    {
        $query = $this->db
                        ->from('quiz_exercise a')
                        ->where('a.user_id', $user_id)
                        ->get();
        return $query;
    }

    public function get_exercise_by_exercise_id($id)
    {
        $query = $this->db
                        ->from('exercise a')
                        ->where('a.exercise_id', $id)
                        ->get();
        return $query;
    }

    public function get_question_quiz_exercise_detail($param)
    {
        $query = $this->db
            ->select('a.exercise_topic_id, a.exercise_topic_name, a.exercise_type_id, a.exercisePath, a.exerciseFile,a.active')
            ->from('exercise_topic a')
            ->where('a.active', 1)
            ->where('a.exercise_id', $param['exercise_id'])
            ->order_by('a.exercise_topic_id', 'ASC')
            ->get()->result();
            foreach($query as $value):
                $value->exercise_answer = $this->get_detail_by_exercise_topic_id($value->exercise_topic_id);
                if(!empty($value->exercise_answer)):
                    foreach($value->exercise_answer as $answer):
                        $answer->quiz_select = $this->get_quiz_detail_by_exercise_answer_id($answer->exercise_answer_id, $param['quiz_id']);
                    endforeach;
                endif;
                
            endforeach;
        return $query;
    }

    public function get_detail_by_exercise_topic_id($id)
    {
        $query = $this->db
                        ->from('exercise_answer a')
                        ->where('a.exercise_topic_id', $id)
                        ->get()
                        ->result();
        return $query;
    }

    public function get_quiz_detail_by_exercise_answer_id($id, $quiz_id)
    {
        $query = $this->db
                        ->from('quiz_exercise_detail a')
                        ->where('a.exercise_answer_id', $id)
                        ->where('a.quiz_id', $quiz_id)
                        ->get()
                        ->result_array();
        return $query;
    }

}
