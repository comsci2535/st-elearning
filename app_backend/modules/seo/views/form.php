<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">
                    
                

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="display_page">แสดงที่หน้า</label>
                    <div class="col-sm-7">
                        <select id="display_page" name="display_page" class="form-control m-input select2" required>
                            <option value="">เลือก</option>
                            <?php
                            if(isset($display_page)):
                                $selected = '';
                                foreach($display_page as $item):
                                    if($info->display_page == $item):
                                        $selected = 'selected';
                                    else:
                                        $selected = '';
                                    endif;
                                    
                                ?>
                                    <option value="<?=$item;?>" <?=$selected?>><?=$item;?></option>
                                <?php
                                endforeach;
                            
                            endif;
                            ?>
                            
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">หัวข้อ (Title)</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="titles" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="excerpt">คำอธิบาย(Description)</label>
                    <div class="col-sm-7">
                        <textarea name="excerpt" rows="7" class="form-control" id="excerpt" required><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail">คำหลัก (Keyword)</label>
                    <div class="col-sm-7">
                        <textarea name="detail" rows="7" class="form-control" id="detail" required><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="file">ไฟล์</label>
                    <div class="col-sm-7">
                        <input id="file" name="file" type="file" data-preview-file-type="text">
                         <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : ''; ?>">
                    </div>
                </div> 
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->seo_id) ? encode_id($info->seo_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>

<script>
    //set par fileinput;
    var required_icon   = false; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=$info->seo_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->seo_id;?>';

</script>




