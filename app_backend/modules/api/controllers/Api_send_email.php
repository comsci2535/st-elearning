<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';

class Api_send_email extends MX_Controller {

    public function __construct(){
        parent::__construct();
        $this->_set_contactus();
    }

    private function _set_contactus()
    {
       
        $query=$this->db
                ->select('a.*')
                ->from('config a')
                ->where('a.type', 'mail')
                ->get()->result_array();
       
        foreach ($query as $rs) $this->config->set_item($rs['variable'], $rs['value']);
        
        return true;
        
    }

    public function send(){

        $status              = 0;
        $mailAuthenticate	 = $this->config->item('mailAuthenticate');
        $mailDefault		 = $this->config->item('mailDefault');
        $mailMethod			 = $this->config->item('mailMethod');
        $senderEmail		 = $this->config->item('senderEmail');
        $senderName			 = $this->config->item('senderName');
        $SMTPpassword		 = $this->config->item('SMTPpassword');
        $SMTPport			 = $this->config->item('SMTPport');
        $SMTPserver			 = $this->config->item('SMTPserver');
        $SMTPusername	     = $this->config->item('SMTPusername');
        

        $query = $this->db->select('*')->get('send_email')->result();
        if(!empty($query)):
            foreach($query  as $item):
                $email['subject']    = $item->subject;
                $email['detail']     = $item->detail;

                $textEmail = $this->load->view('email/index',$email , TRUE);

                $this->load->library('m_phpmailer');
                
                try {
                    $this->m_phpmailer->mail->SMTPDebug    = 0;                                // Enable verbose debug output
                    $this->m_phpmailer->mail->isSMTP();                                        // Set mailer to use SMTP
                    $this->m_phpmailer->mail->Host         = $SMTPserver;                      // Specify main and backup SMTP servers
                    $this->m_phpmailer->mail->SMTPAuth     = true;                             // Enable SMTP authentication
                    $this->m_phpmailer->mail->Username     = $SMTPusername;                    // SMTP username
                    $this->m_phpmailer->mail->Password     = $SMTPpassword;                    // SMTP password
                    $this->m_phpmailer->mail->SMTPSecure   = 'ssl';                            // Enable TLS encryption, ssl also accepted
                    $this->m_phpmailer->mail->Port         = $SMTPport;                        // TCP port to connect to
                    $this->m_phpmailer->mail->CharSet      = 'UTF-8';
                    $this->m_phpmailer->mail->From         = $senderEmail;
                    $this->m_phpmailer->mail->FromName     = $senderName;

                    $this->m_phpmailer->mail->addAddress($item->email_to);                      // Name is optional
                    if(!empty($item->email_cc)):
                        $email_cc_arr = explode(',',$item->email_cc);
                        foreach($email_cc_arr as $email_list):
                            $this->m_phpmailer->mail->addCC($email_list);
                        endforeach;
                        
                    endif;
                    if(!empty($item->file)):
                        $this->m_phpmailer->mail->addAttachment(getcwd().'/'.$item->file);           // Add attachments
                    endif;
                    $this->m_phpmailer->mail->isHTML(true);                                    // Set email format to HTML

                    $this->m_phpmailer->mail->Subject = $item->email_to.' : '.$item->subject;
                    $this->m_phpmailer->mail->Body    = $textEmail;
                    $this->m_phpmailer->mail->AltBody = $textEmail;
                    $this->m_phpmailer->mail->send();
                    if(!empty($item->file)):
                        $this->m_phpmailer->mail->ClearAttachments();
                    endif;

                    $data_arr = array(
                        'id'           => $item->id
                       ,'subject'      => $item->subject
                       ,'detail'       => $item->detail
                       ,'email_send'   => $senderEmail
                       ,'email_to'     => $item->email_to
                       ,'email_cc'     => $item->email_cc
                       ,'status'       => 1
                       ,'send_date'    => date('Y-m-d H:i:s')
                       ,'file'         => $item->file
                   );

                    if($this->insert_mail_log($data_arr)):
                        $this->db->delete('send_email', array('id' => $item->id));
                    endif;

                    $status = 1;
                    $msg    = 'success';
                } catch (Exception $e) {
                    $status = 0;
                    $msg    = $e->ErrorInfo;
                    //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                }

                endforeach;
            endif;
       
        $data['status'] = $status;
        $data['msg']    = $msg;

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function insert_mail_log($data)
    {
        return $this->db->insert('send_email_log', $data);
    }
}
