<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';

class Api_income_instructors extends MX_Controller {

    public function __construct(){
        parent::__construct();
        $this->_set_contactus();
        $this->load->model("revenue_share/revenue_share_m");
    }

    private function _set_contactus()
    {
       
        $query=$this->db
                ->select('a.*')
                ->from('config a')
                ->where('a.type', 'settime')
                ->get()->result_array();
       
        foreach ($query as $rs) $this->config->set_item($rs['variable'], $rs['value']);
        
        return true;
        
    }

    public function income_cycle(){
        $status = 0;

        $start_date = date('Y').'-'.(date('m')-1).'-'.($this->config->item('set_time')-1);
        $end_date   = date('Y').'-'.date('m').'-'.$this->config->item('set_time');
        $this->db->where('c.status', 1);
        $this->db->where("DATE(c.created_at) BETWEEN '".$start_date."' AND '".$end_date."'");
        $this->db->select('a.course_id, a.order_id , b.user_id, c.discount, c.created_at, c.status, d.revenue_share_id');
        $this->db->join('courses_instructors b', 'a.course_id = b.course_id');
        $this->db->join('orders c', 'a.order_id = c.order_id');
        $this->db->join('courses d', 'a.course_id = d.course_id');
        $query = $this->db->get('courses_students a')->result();

        if(!empty($query)):

            foreach($query as $item):
                $revenue_share = $this->revenue_share_m->get_revenue_share_by_id($item->revenue_share_id)->row();
                if(!empty($revenue_share)):
                    $item->instructor_total = ($item->discount * $revenue_share->Instructor_price) / 100;
                    $item->instructor_price = $revenue_share->Instructor_price;
                    $item->owner_price      = $revenue_share->owner_price;
                    $item->owner_total      = ($item->discount * $revenue_share->owner_price) / 100;
                endif;
                unset($revenue_share);
            endforeach;
        endif;

        $obj_income = $this->insert_income_instructors($query);
        if($obj_income > 0):
            $status = $status;
        endif;
        $data['status'] = $status;
        $data['data']   = $this->config->item('set_time');
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function insert_income_instructors($data)
    {   
        $cycle_date  = date('y-m-16');
        $system_date = date('Y-m-d H:i:s');
        $created_by  = 'system';
        $error = 0;
        if(!empty($data)):
            foreach($data as $item):
                $cycle_no = $this->count_income_instructors_course_id($item->course_id);
                $data_arr = array(
                     'course_id'        => $item->course_id
                    ,'user_id'          => $item->user_id
                    ,'discount'         => !empty($item->instructor_total) ? $item->instructor_total : 0
                    ,'order_id'         => $item->order_id
                    ,'created_at'       => $system_date
                    ,'created_by'       => $created_by
                    ,'updated_at'       => $system_date
                    ,'updated_by'       => $created_by
                    ,'cycle_date'       => $cycle_date
                    ,'cycle_no'         => $cycle_no+1
                    ,'price'            => !empty($item->discount) ? $item->discount : 0
                    ,'instructor_price' => $item->instructor_price
                    ,'owner_price'      => $item->owner_price
                );

                $query = $this->db->insert('income_instructors', $data_arr);
                if(!$query):
                    $error++;
                endif;

            endforeach;
        endif;

        return $error;
    }

    public function count_income_instructors_course_id($course_id)
    {
        $this->db->where('course_id', $course_id);
        return $this->db->count_all_results('income_instructors');
    }
   
}
