<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <?php foreach ($sidebar as $level0) : ?>
                <?php if ($level0['type'] == 2) : ?>
                <li class="m-menu__section ">
                    <h4 class="m-menu__section-text"><span><?php echo $level0['title'] ?></h4>
                    <i class="m-menu__section-icon flaticon-more-v2"></i>
                </li>
                <?php else: ?>
                    <?php if (!isset($level0['children'])) : ?>
                        <li class="m-menu__item  m-menu__item--<?php echo $level0['active'] ? "active" : null; ?>" aria-haspopup="true">
                            <a href="<?php echo $level0['href'] ?>" <?php if($level0['target']!=""){ echo 'target="_blank"'; } ?> class="m-menu__link "><i class="m-menu__link-icon <?php echo $level0['icon'] ?>"></i><span class="m-menu__link-title"> <span class="m-menu__link-wrap"> <span class="m-menu__link-text"><?php echo $level0['title'] ?></span>
                                       <!--  <span class="m-menu__link-badge"><span class="m-badge m-badge--danger">2</span></span> </span></span> -->
                                    </a>
                        </li> 
                    <?php else : ?>
                       <li class="m-menu__item  m-menu__item--submenu <?php echo $level0['active'] ? "m-menu__item--open m-menu__item--expanded" : null; ?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon <?php echo $level0['icon'] ?>"></i><span class="m-menu__link-text"><?php echo $level0['title'] ?></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text"><?php echo $level0['title'] ?></span></span></li>
                                    <?php foreach ($level0['children'] as $level1) : ?>
                                        <?php if (!isset($level1['children'])) : ?>
                                            <li class="m-menu__item  <?php echo $level1['active'] ? "m-menu__item--active" : null; ?>" aria-haspopup="true"><a href="<?php echo $level1['href'] ?>" <?php if($level1['target']!=""){ echo 'target="_blank"'; } ?> class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"><?php echo $level1['title'] ?></span></a></li>
                                        <?php else : ?>

                                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"><?php echo $level1['title'] ?></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                                                <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                                    <ul class="m-menu__subnav">
                                                         <?php foreach ($level1['children'] as $level2) : ?>
                                                            <?php if (!isset($level2['children'])) : ?>
                                                               <li class="m-menu__item " aria-haspopup="true"><a  href="<?php echo $level2['href'] ?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"><?php echo $level2['title'] ?></span></a></li>
                                                            <?php else : ?>
                                                                <li class="m-menu__item " aria-haspopup="true"><a href="components/base/tabs/line.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"><?php echo $level2['title'] ?></span></a>
                                                                    <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                                                                     <ul class="m-menu__subnav">
                                                                    <?php foreach ($level2['children'] as $level3) : ?>
                                                                        <li class="m-menu__item " aria-haspopup="true"><a href="<?php echo $level3['href'] ?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"><?php echo $level3['title'] ?></span></a></li>
                                                                    <?php endforeach ?>
                                                                    </ul>
                                                                </div>
                                                                </li>

                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            </li>
                                            
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>

    <!-- END: Aside Menu -->
</div>