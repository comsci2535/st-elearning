<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Exam_list extends MX_Controller {

    private $_title = "รายการแบบทดสอบ";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับรายการแบบทดสอบ";
    private $_grpContent = "exam_list";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("exam_list_m");
        $this->load->model("exams/exams_m");
        $this->load->model("courses/courses_m");
    }
    
    public function index($exam_id=""){
        $this->load->module('template');
        
        $action[1][] = action_refresh(site_url("{$this->router->class}/index/{$exam_id}"));
        //$action[1][] = action_filter();
        $action[1][] = action_add(site_url("{$this->router->class}/create/{$exam_id}"));
        //$action[2][] = action_export_group($export);
        $action[3][] = action_trash_multi("{$this->router->class}/destroy");
        $action[3][] = action_trash_view(site_url("{$this->router->class}/trash/{$exam_id}"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        $data['exam_id'] = $exam_id;
        $input_['exam_id'] = $exam_id;
        $info_ = $this->exams_m->get_rows($input_);
        $info_ = $info_->row();
        $data['info_'] = $info_;
 
        // breadcrumb
       
        $data['breadcrumb'][] = array($data['info_']->course_name, site_url("courses"));
        $data['breadcrumb'][] = array($data['info_']->title, site_url("exams/index/{$data['info_']->course_id}"));
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->exam_list_m->get_rows($input);
        $infoCount = $this->exam_list_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->exam_topic_id);
            $action = array();
            $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$input['exam_id']}/{$id}"));
            $active = $rs->active ? "checked" : null;

            $answer_data="";
            $exam_answer=$this->exam_list_m->get_exam_answerID($rs->exam_topic_id);

            //print_r($exam_answer);exit();
            //$answer_data=arr($exam_answer);
            if($exam_answer){
                foreach ($exam_answer as $key2 => $answer_) {
                    if($answer_->point=="1"){
                       $answer_data.='<label class="control-label text-success" for="inputSuccess"><i class="fa fa-check"></i> '.$answer_->exam_answer_name.'</label></br>';
                    }else{
                        $answer_data.='<label class="control-label" for="inputError"><i class="fa fa-times"></i> '.$answer_->exam_answer_name.'</label></br>';
                    }
                }
            }else{
                $answer_data.='-';
            }

            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->exam_topic_name;
            $column[$key]['excerpt'] = $answer_data;
            $column[$key]['active'] = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['createDate'] = datetime_table($rs->created_at);
            $column[$key]['updateDate'] = datetime_table($rs->updated_at);
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create($exam_id=""){
        $this->load->module('template');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/storage");
        

        $data['exam_id'] = $exam_id;
        $input_['exam_id'] = $exam_id;
        $info_ = $this->exams_m->get_rows($input_);
        $info_ = $info_->row();
        $data['info_'] = $info_;
 
        // breadcrumb
       
        $data['breadcrumb'][] = array($data['info_']->course_name, site_url("courses"));
        $data['breadcrumb'][] = array($data['info_']->title, site_url("exams/index/{$data['info_']->course_id}"));
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}/index/{$exam_id}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage(){
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->exam_list_m->insert($value);
        if ( $result ) {
              $value_a = $this->_build_data_answer($result,$input);

           $this->exam_list_m->update_answer($result, $value_a);
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['exam_id']}"));
    }
    
    public function edit($exam_id="",$id=""){
        $this->load->module('template');
        
        $id = decode_id($id);
        $input['exam_topic_id'] = $id;
        $info = $this->exam_list_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/update");
         $data['item_answer']=$this->exam_list_m->get_exam_answerID($info->exam_topic_id);
        
        $data['exam_id'] = $exam_id;
        $input_['exam_id'] = $exam_id;
        $info_ = $this->exams_m->get_rows($input_);
        $info_ = $info_->row();
        $data['info_'] = $info_;
 
        // breadcrumb
        $data['breadcrumb'][] = array($data['info_']->course_name, site_url("courses"));
        $data['breadcrumb'][] = array($data['info_']->title, site_url("exams/index/{$data['info_']->course_id}"));
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}/index/{$exam_id}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update(){
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->exam_list_m->update($id, $value);
        if ( $result ) {
              $value_a = $this->_build_data_answer($id,$input);
           $this->exam_list_m->update_answer($id, $value_a);
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['exam_id']}"));
    }
    
    private function _build_data($input){
        
        $value['exam_topic_name'] = $input['exam_topic_name'];
        $value['exam_type_id'] = 2;
        $value['exam_id'] = $input['exam_id'];
        $value['publish'] = 1;
        $value['active'] = 1;
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }

    private function _build_data_answer($id,$input) {
        
        $exam_answer_name=$this->input->post('exam_answer_name');
        $exam_answer_id=$this->input->post('exam_answer_id');
        $answer_true=$this->input->post('answer_true');

        $value = array();

        if ( isset($input['exam_answer_name']) ) {
            foreach ( $input['exam_answer_name'] as $key => $rs ) {
                if($answer_true[0]==$key){  $point="1"; }else{  $point="0";  }
                if($rs!=""){
                    $value[] = array(
                        'exam_topic_id' => $id,
                        'exam_answer_name' =>$rs,
                        'point'=>$point,
                        'order_number'=>($key+1),
                        'updated_at'=>db_datetime_now(),
                        'updated_by'=>$this->session->users['user_id']
                    );
                }
                
            }
        }
       //  arr($value);exit();
        return $value;
    }
    
    public function trash($exam_id=""){
        $this->load->module('template');
        
        // toobar
        $action[1][] = action_list_view(site_url("{$this->router->class}/index/{$exam_id}"));
        $action[2][] = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        $data['exam_id'] = $exam_id;
        $input_['exam_id'] = $exam_id;
        $info_ = $this->exams_m->get_rows($input_);
        $info_ = $info_->row();
        $data['info_'] = $info_;
        
        // breadcrumb
        $data['breadcrumb'][] = array($data['info_']->course_name, site_url("courses"));
        $data['breadcrumb'][] = array($data['info_']->title, site_url("exams/index/{$data['info_']->course_id}"));
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));

        $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash(){
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->exam_list_m->get_rows($input);
        $infoCount = $this->exam_list_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->exam_topic_id);
            $action = array();
            $action[1][] = table_restore("{$this->router->class}/restore");         
            $active = $rs->active ? "checked" : null;

             $answer_data="";
            $exam_answer=$this->exam_list_m->get_exam_answerID($rs->exam_topic_id);

            if($exam_answer){
                foreach ($exam_answer as $key2 => $answer_) {
                    if($answer_->point=="1"){
                       $answer_data.='<label class="control-label text-success" for="inputSuccess"><i class="fa fa-check"></i> '.$answer_->exam_answer_name.'</label></br>';
                    }else{
                        $answer_data.='<label class="control-label" for="inputError"><i class="fa fa-times"></i> '.$answer_->exam_answer_name.'</label></br>';
                    }
                }
            }else{
                $answer_data.='-';
            }

            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->exam_topic_name;
            $column[$key]['excerpt'] = $answer_data;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    } 

    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();

            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->exam_list_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->exam_list_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->exam_list_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }      
    
    public function action($type=""){
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            //arr($input);exit();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->exam_list_m->update_in($input['id'], $value);
            }
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids){

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
	}
    
}
