<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Course_students_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
            $query = $this->db
                    ->select('
                    a.order_id, 
                    a.user_id,
                    a.course_id, 
                    b.created_at, 
                    b.updated_at, 
                    b.status, 
                    c.fullname, 
                    c.fname, 
                    c.lname, 
                    c.recycle, 
                    c.active')
                    ->from('courses_students a')
                    ->join('orders b', 'a.order_id = b.order_id')
                    ->join('users c', 'a.user_id = c.user_id')
                    ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('
                         a.order_id, 
                         a.user_id,
                         a.course_id, 
                         b.created_at, 
                         b.updated_at, 
                         b.status, 
                         c.fullname, 
                         c.fname, 
                         c.lname, 
                         c.recycle, 
                         c.active')
                        ->from('courses_students a')
                        ->join('orders b', 'a.order_id = b.order_id')
                        ->join('users c', 'a.user_id = c.user_id')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('c.fullname', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(b.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(b.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('c.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('c.fullname', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
           
            if ($param['order'][0]['column'] == 1) $columnOrder = "b.updated_at";
            if ($param['order'][0]['column'] == 2) $columnOrder = "b.created_at";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 1) $columnOrder = "b.updated_at";
                if ($param['order'][0]['column'] == 2) $columnOrder = "b.created_at";            
            } 
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        
        if ( isset($param['status']) ) 
            $this->db->where('a.status', $param['status']);
        
        if ( isset($param['course_id']) ) 
            $this->db->where('a.course_id', $param['course_id']);

        if ( isset($param['recycle']) )
            $this->db->where('c.recycle', $param['recycle']);

    }


    public function count_course_students($course_id)
    {
        $this->db->where('a.status', 1);
        $this->db->where('a.course_id', $course_id);
        return $this->db->count_all_results('courses_students a');
    }

}
