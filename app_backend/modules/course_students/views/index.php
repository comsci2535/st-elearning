
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">

            <form  role="form">
                <input type="hidden" id="course_id" value="<?php echo !empty($course_id) ? $course_id : 0;?>">
                <table id="data-list" class="table table-hover dataTable" width="100%">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>วันที่จ่ายเงิน</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>วันที่เข้าเรียน</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </form>
        </div>
    </div>
</div>