
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายละเอียด
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>

        
        <div class="m-portlet__body">
            
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-quiz-1">สอบก่อนเรียน</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-quiz-2">สอบหลังเรียน</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-quiz-3">สอบ Final</a>
                </li>
            </ul>
            
            <!-- Tab panes -->
            <div class="tab-content">
                <div id="tab-quiz-1" class="container tab-pane active"><br>
                    <div id="div-quiz-1">
                        <h5>คะแนน (<strong><?php echo !empty($info['pretest']['qty'])? $info['pretest']['qty']: 0; ?></strong> / <strong class="text-score"><?php echo !empty($info['pretest']['score'])? $info['pretest']['score']: 0; ?></strong> ) ข้อ</h5>
                        <h5>หัวข้อแบบทดสอบ : <label><?php echo !empty($info['pretest']['exam']->title)? $info['pretest']['exam']->title: ''; ?></label></h5>
                        <table id="" class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-left">คำถาม</th>
                                    <th>คำตอบ</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($info['pretest']['exam_detail'])):

                                $text_point_pretest = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                foreach($info['pretest']['exam_detail'] as $key => $item):
                                    
                                ?>
                                    <tr>
                                        <td>
                                        <p><?=$key+1?>.) <?=$item->exam_topic_name?></p>
                                        <ul>
                                            <?php
                                                
                                                foreach($item->exam_answer as $item_list):
                                                    $text_point = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                                    if($item_list->point > 0):
                                                        $text_point = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                                    endif;
                                                    
                                                    if(count($item_list->quiz_select) > 0 && $item_list->point > 0):
                                                        $text_point_pretest = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                                    elseif (count($item_list->quiz_select) > 0 && $item_list->point == 0):
                                                        $text_point_pretest = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                                    endif;
                                                    
                                                ?>
                                                    <li><?=$item_list->exam_answer_name?> ( เฉลย <?=$text_point?> )</li>
                                            <?php
                                                endforeach;
                                                ?>
                                            </ul>
                                        </td>
                                    <td class="text-center">
                                        <?=$text_point_pretest?>
                                    </td>
                                </tr>
                                <?php
                                endforeach;
                            else:
                            ?>
                               <tr>
                                    <td colspan="2">
                                        <p class="text-center">ไม่มีข้อมูล</p>
                                    <td>
                                </tr>
                            <?php
                            endif;
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="tab-quiz-2" class="container tab-pane fade"><br>
                    <div id="div-quiz-2">
                    <h5>คะแนน (<strong><?php echo !empty($info['posttest']['qty'])? $info['posttest']['qty']: 0; ?></strong> / <strong class="text-score"><?php echo !empty($info['posttest']['score'])? $info['posttest']['score']: 0; ?></strong> ) ข้อ</h5>
                    <h5>หัวข้อแบบทดสอบ : <label><?php echo !empty($info['posttest']['exam']->title)? $info['posttest']['exam']->title: ''; ?></label></h5>
                        <table id="" class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-left">คำถาม</th>
                                    <th>คำตอบ</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($info['posttest']['exam_detail'])):

                                $text_point_posttest = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                foreach($info['posttest']['exam_detail'] as $key => $item):
                                    
                                ?>
                                    <tr>
                                        <td>
                                        <p><?=$key+1?>.) <?=$item->exam_topic_name?></p>
                                        <ul>
                                            <?php
                                                
                                                foreach($item->exam_answer as $item_list):
                                                    $text_pointposttest = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                                    if($item_list->point > 0):
                                                        $text_pointposttest = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                                    endif;

                                                    if(count($item_list->quiz_select) > 0 && $item_list->point > 0):
                                                        $text_point_posttest = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                                    elseif (count($item_list->quiz_select) > 0 && $item_list->point == 0):
                                                        $text_point_posttest = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                                    endif;
                                                ?>
                                                        <li><?=$item_list->exam_answer_name?> ( เฉลย <?=$text_pointposttest?> )</li>
                                            <?php
                                                endforeach;
                                                ?>
                                            </ul>
                                        </td>
                                        <td class="text-center">
                                            <?=$text_point_posttest?>
                                        </td>
                                    </tr>
                                <?php
                                endforeach;
                            else:
                            ?>
                               <tr>
                                    <td colspan="2">
                                        <p class="text-center">ไม่มีข้อมูล</p>
                                    <td>
                                </tr>
                            <?php
                            endif;
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="tab-quiz-3" class="container tab-pane fade"><br>
                    <div id="div-quiz-3">
                    <h5>คะแนน (<strong><?php echo !empty($info['final']['qty'])? $info['final']['qty']: 0; ?></strong> / <strong class="text-score"><?php echo !empty($info['final']['score'])? $info['final']['score']: 0; ?></strong> ) ข้อ</h5>
                    <h5>หัวข้อแบบทดสอบ : <label><?php echo !empty($info['final']['exam']->title)? $info['final']['exam']->title: ''; ?></label></h5>
                        <table id="" class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-left">คำถาม</th>
                                    <th>คำตอบ</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(!empty($info['final']['exam_detail'])):
                                $text_point_final   = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                foreach($info['final']['exam_detail'] as $key => $item):
                                    
                                ?>
                                    <tr>
                                        <td>
                                        <p><?=$key+1?>.) <?=$item->exam_topic_name?></p>
                                        <ul>
                                            <?php
                                                
                                                foreach($item->exam_answer as $item_list):
                                                    $text_pointfinal = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                                    if($item_list->point > 0):
                                                        $text_pointfinal = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                                    endif;
                                                    if(count($item_list->quiz_select) > 0 && $item_list->point > 0):
                                                        $text_point_final = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                                    elseif (count($item_list->quiz_select) > 0 && $item_list->point == 0):
                                                        $text_point_final = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                                    endif;
                                                ?>
                                                        <li><?=$item_list->exam_answer_name?> ( เฉลย <?=$text_pointfinal?> )</li>
                                            <?php
                                                endforeach;
                                                ?>
                                            </ul>
                                        </td>
                                    <td class="text-center">
                                        <?=$text_point_final?>
                                    </td>
                                </tr>
                                <?php
                                endforeach;
                            else:
                            ?>
                            <tr>
                                <td colspan="2">
                                    <p class="text-center">ไม่มีข้อมูล</p>
                                <td>
                            </tr>
                            <?php
                            endif;
                            ?>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       เกี่ยวกับการเรียน
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-1">แบบฝึกหัด</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-2">การบ้าน</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-3">การเรียน</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="container tab-pane active">
                    <?php
                    
                    if(!empty($info_exercises)):
                        foreach($info_exercises as $list):
                    ?>
                    <p><strong> บทเรียน</strong> : <?=$list['title']?></p>

                    <?php
                    
                    if(!empty($list['exercises'])):
                        foreach($list['exercises'] as $exercises):
                    ?>
                    <table id="" class="table table-hover">
                        <thead>
                            <tr style="background-color: #f2f3f8;">
                                <th colspan="6" class="text-left">หัวข้อ : <?=$exercises->title?></th>
                            </tr>
                            <tr>
                                <th style="text-align: center;">ครั้งที่</th>
                                <th style="text-align: center;">วันและเวลาเริ่ม</th>
                                <th style="text-align: center;">วันและเวลาสิ้นสุด</th>
                                <th style="text-align: center;">สถานะ</th>
                                <th style="text-align: center;">คะแนนที่ได้</th>
                                <th style="text-align: center;">ผลการสอบ</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            if(!empty($exercises->exercises_history)):
                                foreach ($exercises->exercises_history as $key => $rs):
                         ?>
                            <tr>
                                <td style="text-align: center;"><?php echo $key+1 ?></td>
                                <td style="text-align: center;"><?php if($rs->start != ""){ echo date_language($rs->start,true,'th'); } ?></td>
                                <td style="text-align: center;"><?php if($rs->end != ""){ echo date_language($rs->end,true,'th'); }else{ echo "ไม่ได้ส่งแบบฝึกหัด"; } ?></td>
                                <td style="text-align: center;"><?php if($rs->status == "2"){ echo "ส่งแบบฝึกหัด"; }else{  echo "ไม่ได้ส่งแบบฝึกหัด"; } ?></td>
                                <td style="text-align: center;"><?php echo $rs->score ?></td>
                                <td style="text-align: center;"><?php if($rs->result == "pass"){ echo "ผ่าน"; }else{  echo "ไม่ผ่าน"; } ?></td>
                            </tr>
                            <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>

                    <?php
                        endforeach;
                    else:
                        echo '<p><strong>( ไม่พบข้อมูล )</strong></p>';
                    endif;
                    ?>

                <?php
                    endforeach;
                endif;
                ?>
                
                </div>
                <div id="tab-2" class="container tab-pane fade">
                    <?php
                    
                     if(!empty($info_homeworks)):
                        foreach($info_homeworks as $keys => $item):
                    ?>
                    <p><strong> บทเรียน</strong> : <?=$item['title']?></p>
                    
                    <table id="" class="table table-hover" width="100%">
                        <thead>
                            <tr>
                                <th style="width:5%">ลำดับ</th>
                                <th class="text-left" style="width:50%">คำถาม</th>
                                <th class="text-left" style="width:30%">คำตอบ</th>
                                <th class="text-left" style="width:15%">ไฟล์</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if(!empty($item['courses_homeworks'])):
                                foreach($item['courses_homeworks'] as $key => $homeworks):

                            ?>
                            <tr>
                                <td class="text-center"><?=$key+1?></td>
                                <td><?=$homeworks->title?></td>
                                <td><?php echo !empty($homeworks->info_homeworks->detail) ? $homeworks->info_homeworks->detail : 'ไม่มีคำตอบ'?></td>
                                <td>
                                    <?php 
                                    if(!empty($homeworks->info_homeworks->file)):
                                    ?>
                                        <a href="<?=$this->config->item('root_url').$homeworks->info_homeworks->file?>" target="_blank" download="<?=$homeworks->title?>"><i class="fa fa-download" aria-hidden="true"></i> ดาวน์โหลดไฟล์</a> 
                                    <?php
                                    else:
                                        echo 'ไม่มีไฟล์';
                                    endif;
                                    ?>
                                    
                                </td>
                            </tr>
                            <?php
                                endforeach;
                            endif;
                            ?>
                            
                        </tbody>
                    </table>
                    <?php
                        endforeach;
                    endif;
                    ?>
                </div>
                <div id="tab-3" class="container tab-pane fade">
                    <table id="" class="table" width="100%">
                        <thead>
                            <tr>
                                <th style="width:5%">ลำดับ</th>
                                <th class="text-left" style="width:50%">หัวข้อ</th>
                                <th style="width:15%">สถานะการดูวีดีโอ</th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php
                        if(!empty($info_lesson)):
                            foreach($info_lesson as $key => $item):
                        ?>

                        <tr class="success">
                            <td class="text-center"><?=$key+1?></td>
                            <td><strong><?=$item->title?></strong></td>
                            <td></td>
                        </tr>
                            <?php
                            if(!empty($item->info_lesson)):
                                foreach($item->info_lesson as $lessons):
                            ?>
                            <tr>
                               <td></td>
                               <td>&nbsp&nbsp&nbsp&nbsp&nbsp<?=$lessons->title?></td>
                               <td>
                                <?php
                                    if(!empty($lessons->lesson)):
                                ?>
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: <?=number_format($lessons->lesson[0]->persen)?>%;" aria-valuenow="<?=$lessons->lesson[0]->persen?>" aria-valuemin="0" aria-valuemax="100"><?=$lessons->lesson[0]->persen?>%</div>
                                    </div>
                                <?php
                                    endif;
                                ?>
                                </td>
                            </tr>
                           <?php
                                endforeach;
                            endif;
                            ?>

                        <?php     
                            endforeach;
                        endif;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
           
        </div>
    </div>
</div>