<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Course_students extends MX_Controller {

    private $_title = "ข้อมูลผู้เรียนที่สั่งซื้อคอร์ส";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับข้อมูลผู้เรียนที่สั่งซื้อคอร์ส";
    private $_grpContent = "course_students";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("course_students_m");
        $this->load->model("courses/courses_m");
        $this->load->model("courses_lesson/courses_lesson_m");
        $this->load->model('exams/exams_m');
        $this->load->model('exam_list/exam_list_m');
        $this->load->model('homeworks/homeworks_m');
        $this->load->model('exercise/exercise_m');
        $this->load->model('certificates/certificates_m');
        $this->load->model('users/users_m');

        $this->status=array('รอจ่ายเงิน','ชำระเงินเรียบร้อยแล้ว','ยกเลิก');
    }
    
    public function index($course_id=''){
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf' => site_url("{$this->router->class}/pdf"),
        );
        // $action[0][] = action_print(site_url("{$this->router->class}"));
        $action[1][] = action_refresh(site_url("{$this->router->class}/index/{$course_id}"));
        
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $input['course_id'] = $course_id;
        $courses = $this->courses_m->get_rows($input)->row();

        // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($courses->title, base_url("courses"));
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}/index/{$course_id}"));
        $data['course_id'] = $course_id;
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index(){
        $input = $this->input->post();
        $input['recycle']       = 0;
        $input['status']        = 1;
        $info = $this->course_students_m->get_rows($input);
        $infoCount = $this->course_students_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        $i = $input['start']+1;
        foreach ($info->result() as $key => $rs) {

            $action = array();
            $action[1][] = table_view(site_url("{$this->router->class}/view/{$rs->course_id}/{$rs->user_id}"));

            $count_certifi = $this->certificates_m->count_certificates_by_courses_id($rs->course_id);
            if($count_certifi > 0):
                $action[1][] = table_print(site_url("{$this->router->class}/print/{$rs->course_id}/{$rs->user_id}"));
            endif;
            $id = encode_id($rs->order_id);
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $i;
            $column[$key]['fullname']   = $rs->fullname;
            $column[$key]['status']     = '<span class="m-badge m-badge--success m-badge--wide">'.$this->status[$rs->status].'</span>';
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action']     = Modules::run('utils/build_button_group', $action);
            $i++;
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create(){
        $this->load->module('template');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/storage");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage(){
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->course_students_m->insert($value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    public function edit($id=""){
        $this->load->module('template');
        
        $id = decode_id($id);
        $input['repoId'] = $id;
        $info = $this->course_students_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }

    public function view($course_id="", $user_id=""){
        $this->load->module('template');
        
        $id = decode_id($user_id);
        $input['user_id'] = $id;
        $info = $this->course_students_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}/index/{$course_id}"));
        $data['breadcrumb'][] = array('แสดงข้อมูล', site_url("{$this->router->class}/view/{$id}"));

        $info_arr = array();
        
        $info = $this->exams_m->get_quiz_by_user_id($user_id, $course_id)->result();
        if(!empty($info)):
            foreach($info as $item):
                $inputs['exam_id'] = $item->exam_id;
                $inputs['quiz_id'] = $item->quiz_id;
                $info_arr[$item->type] = array(
                     'score'       => $item->score
                    ,'quiz_id'     => $item->quiz_id
                    ,'exam_id'     => $item->exam_id
                    ,'status'      => $item->status
                    ,'user_id'     => $item->user_id
                    ,'qty'         => $this->exam_list_m->count_exams_list_by_all($inputs)
                    ,'exam'        => $this->exams_m->get_exam_by_exam_id($item->exam_id)->row()
                    ,'exam_detail' => $this->exams_m->get_question_quiz_detail($inputs)
                );
                
            endforeach;
        endif;

        $data['info']   = $info_arr;
        $arr_homeworks  = array();
        $arr_exercises  = array();
        
        $info_lesson = $this->courses_lesson_m->get_courses_lesson_by_course_id($course_id)->result();
        if($info_lesson):
            foreach($info_lesson as $item):
                $item->info_lesson  = $this->courses_lesson_m->get_courses_lesson_by_parent_id($item->course_lesson_id)->result();
                if(!empty($item->info_lesson)):
                    foreach($item->info_lesson as $key => $list):
                        $list->lesson  = $this->courses_lesson_m->get_courses_lesson_member_by_id($list->course_lesson_id, $input['user_id'])->result();
                    
                        $input_['course_lesson_id']  = $list->course_lesson_id;
                        $input_['recycle']           = 0;
                        $input_['active']            = 1;

                        $courses_homeworks = $this->homeworks_m->get_rows($input_)->result();
                        foreach($courses_homeworks as $homeworks):
                            $homeworks->info_homeworks = $this->homeworks_m->get_courses_homeworks_question_by_courses_homework_id($homeworks->courses_homework_id, $input['user_id'])->row();
                        endforeach;
                        array_push($arr_homeworks, array( 
                            'title'                 => $list->title
                            ,'courses_homeworks'    => $courses_homeworks
                        ));
                        unset($courses_homeworks);

                        $info_exercises = $this->exercise_m->get_exercise_by_course_lesson_id($list->course_lesson_id)->result();
                        foreach($info_exercises as $exercise):
                            $exercise->exercises_history = $this->exercise_m->get_exercise_by_exercise_user_id($exercise->exercise_id, $input['user_id'])->result();
                        endforeach;
                        array_push($arr_exercises, array( 
                            'title'         => $list->title
                            ,'exercises'    => $info_exercises
                        ));
                        unset($info_exercises);
                        
                    endforeach;
                endif;
            endforeach;
        endif;

        
        $data['info_lesson']    = $info_lesson;
        $data['info_homeworks'] = $arr_homeworks;
        $data['info_exercises'] = $arr_exercises;

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/view";
        
        $this->template->layout($data);
    }

    public function print($course_id="", $user_id=""){

        $certificates           = $this->certificates_m->get_certificates_by_coursesid($course_id)->row();
        $data['detail']         = $certificates->detail; 
        $data['file']           = $certificates->file; 
        $infoCourses            = $this->courses_m->get_courses_by_id($course_id)->row();
        $data['course_title']   = $infoCourses->title;
        $infoUsers              = $this->users_m->get_user_by_id($user_id)->row();
        $data['fullname']       =  $infoUsers->fullname; 
        $data['dateTh']           = DateThai(date('d-m-Y'),'full','th'); //set date , get full name , digit
        $data['dateEn']           = DateThai(date('d-m-Y')); //set date , get full name , digit

		$html = $this->load->view("certificate",$data, true);
        //this the the PDF filename that user will get to download
		$pdfFilePath = getcwd()."/uploads/".$course_id."-".$user_id.".pdf";
		$this->load->library('m_pdf');
		$this->m_pdf->pdf->WriteHTML($html);
		$this->m_pdf->pdf->Output($pdfFilePath, "I");
    }

    public function get_cer_html($course_id="", $user_id=""){
        $courses = $this->certificates_m->get_certificates_by_coursesid($course_id)->row();
        $data['info'] = $courses; 

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/certificate";

        $this->load->view("{$this->router->class}/certificate", $data);
        
      
    }
    
    public function update(){
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->course_students_m->update($id, $value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input){
        
        $value['title'] = $input['title'];
        $value['excerpt'] = $input['excerpt'];
        $value['detail'] = $input['detail'];
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }
    
    public function excel(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->course_students_m->get_rows($input);
        $fileName = "course_students";
        $sheetName = "Sheet name";
        $sheetTitle = "Sheet title";
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'created_at',            
            'D' => 'updated_at',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->course_students_m->get_rows($input);
        $data['info'] = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font' => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "course_students.pdf";
        $pathFile = "uploads/pdf/course_students/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash(){
        $this->load->module('template');
        
        // toobar
        $action[1][] = action_list_view(site_url("{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash()
    {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->course_students_m->get_rows($input);
        $infoCount = $this->course_students_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->repoId);
            $action = array();
            $action[1][] = table_restore("{$this->router->class}/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    } 

    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->course_students_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }

    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->course_students_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->course_students_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }      
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->course_students_m->update_in($input['id'], $value);
            }
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function sumernote_img_upload()
    {
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids)
    {

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
	}
    
}
