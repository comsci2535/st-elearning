<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">เนื้อหาหลัก <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <?php echo $parentModule ?>
                    </div>
                </div>   
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">หัวข้อ <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">Slug <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug"  required>
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="excerpt">อธิบาย</label>
                    <div class="col-sm-7">
                        <textarea id="excerpt" name="excerpt" rows="3" class="form-control m-input "><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="file">ไฟล์</label>
                    <div class="col-sm-7">
                        <input id="file" name="file" type="file" data-preview-file-type="text">
                         <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : ''; ?>">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="file-icon">ไอคอนไฟล์</label>
                    <div class="col-sm-7">
                        <input id="file-icon" name="file_icon" type="file" data-preview-file-type="text">
                         <input type="hidden" name="outfile_icon" value="<?=(isset($info->file_icon)) ? $info->file_icon : ''; ?>">
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="courses_categories">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->course_categorie_id) ? encode_id($info->course_categorie_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>

<script type="text/javascript">
    //set par fileinput;
    var required_icon   = false; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=(isset($info->course_categorie_id)) ? $info->course_categorie_id : '0'; ?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/'+file_id;
    var file_icon       = '<?=(isset($info->file_icon)) ? $this->config->item('root_url').$info->file_icon : ''; ?>';
    
</script>


