<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Courses_categories_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        
        $this->db->order_by('order', 'asc')
                ->order_by('parent_id', 'asc');   
        
        $query = $this->db
                        ->select('a.*')
                        ->from('courses_categories a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('courses_categories a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        // if ( isset($param['order']) ){
        //     if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
        //     if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
        //     if ( $this->router->method =="data_index" ) {
        //         if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
        //         if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
        //     } else if ( $this->router->method =="data_trash" ) {
        //         if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
        //     }
        //     $this->db
        //             ->order_by($columnOrder, $param['order'][0]['dir']);
        // } 
        
        if ( isset($param['course_categorie_id']) ) 
            $this->db->where('a.course_categorie_id', $param['course_categorie_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }

    public function get_courses_categorie()
    {
        $this->db
                ->order_by('parent_id', 'asc')
                ->order_by('order', 'asc')
                ->order_by('course_categorie_id', 'asc'); 
        $query = $this->db
                        ->from('courses_categories a')
                        ->select('a.course_categorie_id, a.title, a.parent_id, a.order')
                        ->where('a.recycle', 0)
                        ->get()
                        ->result_array();
        return $query;
    }
    
    public function insert($value) {
        $this->db->insert('courses_categories', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('course_categorie_id', $id)
                        ->update('courses_categories', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('course_categorie_id', $id)
                        ->update('courses_categories', $value);
        return $query;
    }   

    public function get_courses_categories_parent_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('parent_id', 0);
        $this->db->order_by('order', 'asc');
        $this->db->select('*');
        $query = $this->db->get('courses_categories')->result();
        foreach ($query as $key => $rs) {
           $rs->parent = $this->db->where('recycle', 0)
                                ->where('active', 1)
                                ->where('parent_id', $rs->course_categorie_id)
                                ->order_by('order', 'asc')
                                ->select('*')
                                ->get('courses_categories')
                                ->result();
        }
        return $query;
    } 

}
