<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Courses extends MX_Controller {

    private $_title = "ข้อมูลคอร์สเรียน";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับคอร์สเรียน";
    private $_grpContent = "courses";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("courses_m");
        $this->load->model("courses_categories/courses_categories_m");
        $this->load->model("instructors/instructor_m");
        $this->load->model("revenue_share/revenue_share_m");
        $this->load->model("course_students/course_students_m");
        $this->load->library('uploadfile_library');
        $this->load->library('notification/notif_library');
        $this->approve=array('รอการอนุมติ','อนุมัติเรียบร้อย','ไม่อนุมัติ');
    }
    
    public function index() {
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf' => site_url("{$this->router->class}/pdf"),
        );
        $action[1][] = action_refresh(site_url("{$this->router->class}"));
       // $action[1][] = action_filter();
        $action[1][] = action_add(site_url("{$this->router->class}/create"));
        //$action[2][] = action_export_group($export);
        $action[3][] = action_trash_multi("{$this->router->class}/destroy");
        $action[3][] = action_trash_view(site_url("{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->courses_m->get_rows($input);
        $infoCount = $this->courses_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->course_id);

            switch ($rs->approve) {
                case 0 : $style = 'btn-warning'; break; //รอการอนุมัติ
                case 1 : $style = 'btn-success'; break; //อนุมัติเรียบร้อย
                case 2 : $style = 'btn-danger'; break; //ไม่อนุมัติ
                default: $style = 'btn-default'; break;
            }

            $statusMethod = site_url("{$this->router->class}/action/approve");
            $statusPicker = form_dropdown('statusPicker', $this->approve, $rs->approve, "class='form-control custom-select-sm  statusPicker' data-method='{$statusMethod}' data-id='{$id}' data-style='btn-flat custom-select-sm {$style}'");

            $action = array();
            $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;

            $manage = array();
            //$manage[] = action_custom(site_url("exams/index/{$rs->course_id}"),'btn-secondary','add','แบบทดสอบ','','','');
            $button1='<a class="dropdown-item" href="'.site_url("exams/index/{$rs->course_id}").'">แบบทดสอบ Final</a>';
            $button2="";
            if($rs->pretest_posttest==1){
               // $manage[] = action_custom(site_url("exams-pretest-posttest/index/{$rs->course_id}"),'btn-secondary','add','แบบทดสอบก่อนและหลังเรียน','','','');
                $button2='<a class="dropdown-item" href="'.site_url("exams-pretest-posttest/index/{$rs->course_id}").'">แบบทดสอบก่อนและหลังเรียน</a>';
            }
            $button3='<a class="dropdown-item" href="'.site_url("certificates/manage/{$rs->course_id}").'">เกียรติบัตร</a>';
            
            $manage[]='<div class="btn-group" role="group">
    <button id="btnGroupDrop1" type="button" class="btn btn-sm  btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      แบบทดสอบ
    </button>
    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">'.$button1.$button2.$button3.'</div>
  </div>';
            
            $manage[] = action_custom(site_url("reviews/index/{$rs->course_id}"),'btn-warning','add','รีวิวคอร์ส','','','');
            $manage[] = action_custom(site_url("courses_lesson/index/{$rs->course_id}"),'btn-success','add','บทเรียน','','','');
            if($rs->price!=0){
                $manage[] = action_custom(site_url("promotions/index/{$rs->course_id}"),'btn-info','add','โปรโมชั่น','','','');
            }

            $manage[] = action_custom(site_url("course_students/index/{$rs->course_id}"),'btn-primary','add','ผู้เรียน','fa-user','','');
            

            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['qty']        = $this->course_students_m->count_course_students($rs->course_id);
            $column[$key]['manage']     = implode($manage," "); 
            $column[$key]['active']     = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action']     = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create() {
        $this->load->module('template');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/storage");

        $value['active'] = 1;
        $value['recycle'] = 0;

        $infotype = $this->courses_categories_m->get_courses_categories_parent_all();
        //$infotype           = $infotype->result();
        $data['infotype']   = $infotype;


        $data['instructors'] = $this->instructor_m->get_rows($value)->result();
        $data['revenue_share']  = $this->revenue_share_m->get_revenue_share_all()->result();
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage() {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->courses_m->insert($value);
        if ( $result ) {
            $value_ = $this->_build_data_instructors($result,$input);
            $this->courses_m->insert_courses_instructors($result,$value_);

            $value_map = $this->_build_course_categorie_map($result,$input);
            $this->courses_m->insert_courses_categories_map($result,$value_map);

           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    public function edit($id="") {
        $this->load->module('template');
        
        $id = decode_id($id);
        $input['course_id'] = $id;
        $info = $this->courses_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        if($info->startDate!="0000-00-00" && $info->endDate!="0000-00-00" ){
            $data['dateRang'] = date('d-m-Y', strtotime($info->startDate)).' ถึง '.date('d-m-Y', strtotime($info->endDate));
        }
        
        $value['active'] = 1;
        $value['recycle'] = 0;

        $infotype = $this->courses_categories_m->get_courses_categories_parent_all();
        $data['infotype']   = $infotype;
        $data['revenue_share']  = $this->revenue_share_m->get_revenue_share_all()->result();
        $data['instructors'] = $this->instructor_m->get_rows($value)->result();

        $courses_instructors=$this->db
                    ->select('a.*')
                    ->from('courses_instructors a')
                    ->where('course_id',$id)
                    ->get()->result();
        $data['courses_instructors'] = array();
        if(!empty($courses_instructors)){
            foreach ($courses_instructors as $key => $value) {
                $data['courses_instructors'][]=$value->user_id;
            }
        }

        $courses_categories_map=$this->db
                    ->select('a.*')
                    ->from('courses_categories_map a')
                    ->where('course_id',$id)
                    ->get()->result();
        $data['courses_categories_map'] = array();
        if(!empty($courses_categories_map)){
            foreach ($courses_categories_map as $key => $value) {
                $data['courses_categories_map'][] = $value->course_categorie_sub_id;
            }
        }

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->courses_m->update($id, $value);
        if ( $result ) {
            $value_ = $this->_build_data_instructors($id,$input);
            $this->courses_m->insert_courses_instructors($id,$value_);

            $value_map = $this->_build_course_categorie_map($id,$input);
            $this->courses_m->insert_courses_categories_map($id,$value_map);

            $get_courses = $this->courses_m->get_courses_instructors($id)->result();
            if ($input['approve']==1) {
                $status = 'การสร้างคอร์สเรียนถูกอนุมติเรียบร้อย';
            } else {
                $status = 'การสร้างคอร์สเรียนถูกยกเลิก';
            }
            
            if(!empty($get_courses)&&$input['approve']!=0){
                foreach ($get_courses as $value) {

                    if (!empty($value->user_id)) {
                        $data_noti = array(
                            'title' => $status,
                            'url' => $this->config->item('root_url').'instructors/courses',
                            'user_id' => $value->user_id,
                            'type' => '1',
                            'date' => date('Y-m-d H:i:s'),
                        );
                        $this->notif_library->noti_insert($data_noti);
                    }
                }
            }
             
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input) {
        
        $value['title']               = $input['title'];
        $value['slug']                = $input['slug'];
        $value['excerpt']             = $input['excerpt'];
        $value['detail']              = html_escape($this->input->post('detail',false));
        $value['receipts']            = html_escape($this->input->post('receipts',false));//html_escape($input['receipts']);
        $value['recommendVideo']      = $input['recommendVideo'];
        $value['price']               = str_replace(",","",$input['price']);
        $value['publish']             = $input['publish'];
        $value['revenue_share_id']    = $input['revenue_share_id'];
        if($input['publish']=="public"){
            $value['startDate']           = "";        
            $value['endDate']             = "";
        }else{
            $value['startDate']           = $input['startDate'];        
            $value['endDate']             = $input['endDate'];
        }
        
        $value['recommend']           = ($input['recommend'] == 'on') ? 1 : 0;
        $value['video_preview']       = ($input['video_preview'] == 'on') ? 1 : 0; 
        $value['pretest_posttest']       = ($input['pretest_posttest'] == 'on') ? 1 : 0;

        $value['approve']             = $input['approve']; 
        

        if($input['metaTitle']!=""){
            $value['metaTitle'] = $input['metaTitle'];
        }else{
             $value['metaTitle'] = $input['title'];
        }
        if($input['metaDescription']!=""){
            $value['metaDescription'] = $input['metaDescription'];
        }else{
            $value['metaDescription'] = $input['excerpt'];
        }
        if($input['metaKeyword']!=""){
            $value['metaKeyword'] = $input['metaKeyword'];
        }else{
             $value['metaKeyword'] = $input['title'];
        }

        $path   = 'courses';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
        $file = '';
		if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
            $outfile = $input['outfile'];
			if(isset($outfile)){
				$this->load->helper("file");
				unlink($outfile);
            }
            $value['file'] = $file;
        }

        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['updated_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }

    private function _build_data_instructors($course_id,$input) {
        
         if ( isset($input['instructor_id']) ) {
            foreach ( $input['instructor_id'] as $key1 => $rs ) {
                $value[] = array(
                    'course_id' => $course_id,
                    'user_id' => $rs
                );
            }
        }
        return $value;
    }

    private function _build_course_categorie_map($course_id,$input) {

        if ( isset($input['course_categorie_id']) ) {
           foreach ( $input['course_categorie_id'] as $key => $rs ) {
               if(!empty($input['course_categorie_sub_id'][$key])):
                    $value[] = array(
                        'course_id'                  => $course_id,
                        'course_categorie_id'        => $rs,
                        'course_categorie_sub_id'    => $input['course_categorie_sub_id'][$key]
                    );
                endif;
           }
       }
       return $value;
    }
     
    public function excel(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->courses_m->get_rows($input);
        $fileName = "courses";
        $sheetName = "Sheet name";
        $sheetTitle = "Sheet title";
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'created_at',            
            'D' => 'updated_at',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->courses_m->get_rows($input);
        $data['info'] = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font' => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "courses.pdf";
        $pathFile = "uploads/pdf/courses/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash() {
        $this->load->module('template');
        
        // toobar
        $action[1][] = action_list_view(site_url("{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->courses_m->get_rows($input);
        $infoCount = $this->courses_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->course_id);
            $action = array();
            $action[1][] = table_restore("{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->courses_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->courses_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->courses_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }   
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->courses_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycle_at'] = $dateTime;
                $value['recycle_by'] = $this->session->users['user_id'];
                $result = $this->courses_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->courses_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->courses_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    
    public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids){

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
	}
    
}
