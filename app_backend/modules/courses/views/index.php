
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">

           
                <table id="data-list" class="table table-striped- table-bordered table-hover table-checkable" width="100%">
                    <thead>
                        <tr>
                            <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                            <th>ชื่อคอร์ส</th>
                            <th>จำนวนนักเรียน</th>
                            <th>จัดการ</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

                
           

        </div>
    </div>
</div>