<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title"></label>
                    <div class="col-sm-7">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php if(!empty($info->recommend) &&  $info->recommend=='1'){ echo "checked";}?> type="checkbox" name="recommend" valus="1"> รายการแนะนำ
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title"></label>
                    <div class="col-sm-7">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php if(!empty($info->pretest_posttest) &&  $info->pretest_posttest=='1'){ echo "checked";}?> type="checkbox" name="pretest_posttest" valus="1"> สอบก่อนเรียนและหลังเรียน
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="revenue_share_id">ส่วนแบ่งรายได้ <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <select id="revenue_share_id" name="revenue_share_id" class="form-control m-input  select2" required>
                                <option value="">เลือกส่วนแบ่งรายได้</option>
                            <?php
                            if(!empty($revenue_share)):
                                $selected = '';
                                foreach($revenue_share as $item):
                                    if($info->revenue_share_id == $item->revenue_share_id):
                                        $selected = 'selected';
                                    else:
                                        $selected = '';
                                    endif;
                                ?>
                                    <option value="<?=$item->revenue_share_id;?>" <?=$selected?>><?=$item->title.' ('.$item->Instructor_price.' ต่อ '.$item->owner_price.')';?></option>
                                <?php
                                endforeach;
                            endif;
                            ?>
                            
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row"> <!-- <font color=red>*</font> -->
                    <label class="col-sm-2 col-form-label" for="title">หัวข้อ <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">Slug <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug" required>
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                </div>

                

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="file">รูปภาพหน้าปก <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input id="file" name="file" type="file"  data-preview-file-type="text">
                         <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : ''; ?>">
                    </div>
                </div> 

                <?php
                if(!empty($infotype)):
                    foreach($infotype as $key => $item):
                ?>
                 <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="course_categorie_sub_id[]"><?=$item->title;?> <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input type="hidden" name="course_categorie_id[]" value="<?=$item->course_categorie_id?>">
                        <select id="<?=$item->course_categorie_id?>" name="course_categorie_sub_id[<?=$key?>]" class="form-control m-input  select2" required>
                            <option value="">เลือก<?=$item->title;?></option>
                            <?php
                            if(!empty($item->parent)):
                                $selected2 = '';
                                foreach($item->parent as $item2):

                                    if(!empty($courses_categories_map) && in_array($item2->course_categorie_id, $courses_categories_map)):
                                        $selected2 = 'selected';
                                    else:
                                        $selected2 = '';
                                    endif;
                            ?>
                            <option value="<?=$item2->course_categorie_id;?>" <?=$selected2?>> <?=$item2->title;?></option>
                            <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>
                </div>
                <?php
                    endforeach;
                endif;
                ?>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">รายละเอียดย่อ</label>
                    <div class="col-sm-7">
                        <textarea name="excerpt" rows="3" class="form-control" id="excerpt" ><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>
                    <div class="col-sm-7">
                        <textarea name="detail" rows="3" class="form-control summernote" id="detail" ><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                    </div>
                </div>  
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="receipts">สิ่งที่ได้รับ</label>
                    <div class="col-sm-7">
                        <textarea name="receipts" rows="3" class="form-control summernote" id="receipts" ><?php echo isset($info->receipts) ? $info->receipts : NULL ?></textarea>
                    </div>
                </div>  
                 <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">ผู้สอน <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                       <!-- multiple=Multi-->
                       <select id="instructor_id" name="instructor_id[]"  class="form-control m-input  m-select2" multiple required>
                            <option value="">เลือก</option>
                            <?php
                            if(isset($instructors)): foreach($instructors as $item): ?>
                                    <option value="<?=$item->user_id;?>" <?php if(!empty($courses_instructors) && in_array($item->user_id, $courses_instructors)){ echo "selected"; }?> ><?=$item->fullname;?></option>
                            <?php endforeach; endif; ?>
                            
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title"></label>
                    <div class="col-sm-7">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php if(!empty($info->video_preview) &&  $info->video_preview=='1'){ echo "checked";}?> type="checkbox" name="video_preview" valus="1" id="checkview"> แสดงวีดีโอตัวอย่าง
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="recommendVideo">ลิงก์วิดีโอแนะนำคอร์ส <span id="star-red" class="text-danger d-none"> *</span></label>
                    <div class="col-sm-7">
                        <div class="input-group">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2" data-toggle="modal" data-target="#contentVideo"><i class="fa fa-video"></i></span>
                            </div>
                            <input value="<?php echo isset($info->recommendVideo) ? $info->recommendVideo : NULL ?>" type="text" class="form-control m-input " name="recommendVideo" id="recommendVideo">
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="price">ราคา <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->price) ? number_format($info->price) : NULL ?>" type="text" class="form-control m-input amount" name="price" id="price" required >
                       
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">เผยแพร่ <span class="text-danger"> *</span></label>
                    <div class="col-sm-7 icheck-inline">
                        <label><input <?php if(!empty($info->publish) && $info->publish=='public'){  echo  "checked"; } ?> <?php if(empty($info->publish)){ echo "checked"; } ?> type="radio" name="publish" class="icheck" value="public" > Public</label>
                        <label><input <?php if(!empty($info->publish) && $info->publish=='private'){  echo  "checked"; } ?> type="radio" name="publish" class="icheck" value="private"> Private</label>
                    </div>
                </div>
                <div class="form-group m-form__group row" id="dateRang1"   <?php if(!empty($info->publish) && $info->publish=='public'){ ?> style="display:none"  <?php } ?>>
                    <label class="col-sm-2 col-form-label">ช่วงวันที่แสดงผล</label>
                    <div class="col-sm-7">
                       <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>"/>
                        <input type="hidden" name="startDate"  value="<?php echo isset($info->startDate) ? $info->startDate : NULL ?>"/>
                        <input type="hidden" name="endDate"  value="<?php echo isset($info->endDate) ? $info->endDate : NULL ?>"/>
                    </div>
                </div>
                 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">อนุมัติคอร์สเรียน <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <select id="approve" name="approve" class="form-control m-input  select2" required>
                            <option value="0" <?php if(!empty($info) && $info->approve == 0 ){ echo "selected"; }?>>รออนุมัติ</option>
                            <option value="1" <?php if(!empty($info) && $info->approve == 1 ){ echo "selected"; }?>>อนุมัติ</option>
                            <option value="2" <?php if(!empty($info) && $info->approve == 2 ){ echo "selected"; }?>>ไม่อนุมัติ</option>
                           
                        </select>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" > <h5 class="block">ข้อมูล SEO</h5></label>
                    <div class="col-sm-7">
                       
                    </div>
                </div>
               
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อเรื่อง (Title)</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="" class="form-control" name="metaTitle">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำอธิบาย (Description)</label>
                    <div class="col-sm-7">
                        <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo isset($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
                    </div>
                </div>   
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำหลัก (Keyword)</label>
                    <div class="col-sm-7">
                        <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo isset($info->metaKeyword) ? $info->metaKeyword : NULL ?></textarea>
                    </div>
                </div> 
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="courses">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->course_id) ? encode_id($info->course_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="contentVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><?php echo isset($lessons->title)? $lessons->title : NULL?></h3>
                <button type="button" class="close" id="pause-button" data-id="contentVideo">
                    ปิด X
                </button>

            </div>
            <div class="modal-body">
                <style>
                    #basic-addon2{
                        cursor: pointer;
                    }
                    .embed-container {
                        position: relative;
                        padding-bottom: 56.25%;
                        overflow: hidden;
                    }
                    .embed-container iframe {
                        position: absolute;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 100%;
                    }
                </style>
                <div class="embed-container">
                    <div data-vimeo-id="<?php echo isset($info->recommendVideo)? $info->recommendVideo :NULL?>"
                        id="handstick-c-contentVideo"
                        class="handstick-c"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=(isset($info->course_id)) ? $info->course_id : ''; ?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/'+file_id;

</script>
