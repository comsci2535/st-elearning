<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Incomes extends MX_Controller {

    private $_title = "จัดการรายได้";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับจัดการรายได้";
    private $_grpContent = "incomes";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        
        $this->load->library('ckeditor');
        $this->load->model("instructors/instructor_m");
        $this->load->model("incomes_m");
        $this->approve = array('รอโอนเงิน','โอนเงินเรียบร้อย');
    }
    
    public function index() {
        $this->load->module('template');
        
        $action[1][] = action_refresh(site_url("{$this->router->class}"));
       
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        $input['recycle'] = 0;
        $input['private'] = 1;
        
        $info = $this->instructor_m->get_rows($input);
        $infoCount = $this->instructor_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }

        $i = $input['start']+1;
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->user_id);
            $private="";
            if($rs->private==1){
                $private=" (ผู้สอน Private)";
            }
            
            $action = array();
            $action[1][] = table_view(site_url("{$this->router->class}/report_courses/{$id}"));
            
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['no'] = $i;
            $column[$key]['title'] = $rs->fullname.$private;
            $column[$key]['excerpt'] = $rs->phone;
            $column[$key]['total_courses'] = $this->instructor_m->count_courses_instructors_by_user_id($rs->user_id);
            $column[$key]['total'] = $this->incomes_m->count_income_instructors_by_user_id($rs->user_id, 0);
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
            $i++;
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function report_courses($id= '') {
        
        $this->load->module('template');
        // toobar
        
        $action[1][] = action_refresh(site_url("{$this->router->class}/report_courses/".$id));
        // $action[1][] = action_filter();
        
        $action[1][] = action_history(site_url("{$this->router->class}/report_courses_history/".$id));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        $data['user_id']        = $id;
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/report_courses";
        $data['pageScript']     = "scripts/backend/{$this->router->class}/report_courses.js";

        $this->template->layout($data);
    }

    public function report_courses_history($id= '') {
        
        $this->load->module('template');
        // toobar
        
        $action[1][] = action_refresh(site_url("{$this->router->class}/report_courses/".$id));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        $data['user_id']        = $id;
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/history_report_courses";
        $data['pageScript']     = "scripts/backend/{$this->router->class}/history_report_courses.js";

        $this->template->layout($data);
    }
    
    public function ajax_data_report_courses() {
        $input = $this->input->post();
        $input['user_id'] = decode_id($input['user_id']);
        $input['status'] = 0;
        $info = $this->incomes_m->get_income_courses_instructors_by_user_id($input);
        $infoCount = $this->incomes_m->count_income_courses_by_instructors($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->income_instructors_id);
            
            switch ($rs->status) {
                case 0 : $style = 'btn-warning'; break; //รอการอนุมัติ
                case 1 : $style = 'btn-success'; break; //อนุมัติเรียบร้อย
                default: $style = 'btn-default'; break;
            }

            $statusMethod = site_url("{$this->router->class}/action/approve");
            $statusPicker = form_dropdown('statusPicker', $this->approve, $rs->status, "class='form-control custom-select-sm  statusPicker' data-method='{$statusMethod}' data-id='{$rs->income_instructors_id}' data-style='btn-flat custom-select-sm {$style}'");


            $status = $rs->status ? '<span class="badge badge-success">โอนแล้ว</span>' : '<span class="badge badge-danger">รอการโอน</span>';
            
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $no;
            $column[$key]['course_id']  = $rs->course_id;
            $column[$key]['cycle']      = 'รอบที่ '.$rs->cycle_no;
            
            $column[$key]['title']      = $rs->title;
            $column[$key]['active']     = $status;
            $column[$key]['stuQty']     = number_format($rs->discount, 2);
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['cycle_date'] = $rs->cycle_date;
            $column[$key]['statusPicker'] = $statusPicker;
           
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function ajax_data_report_history() {
        $input = $this->input->post();
        $input['user_id'] = decode_id($input['user_id']);
        $input['status'] = 1;
        $info = $this->incomes_m->get_income_courses_instructors_by_user_id($input);
        $infoCount = $this->incomes_m->count_income_courses_by_instructors($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->income_instructors_id);
            
            switch ($rs->status) {
                case 0 : $style = 'btn-warning'; break; //รอการอนุมัติ
                case 1 : $style = 'btn-success'; break; //อนุมัติเรียบร้อย
                default: $style = 'btn-default'; break;
            }

            $statusMethod = site_url("{$this->router->class}/action/approve");
            $statusPicker = form_dropdown('statusPicker', $this->approve, $rs->status, "class='form-control custom-select-sm  statusPicker' data-method='{$statusMethod}' data-id='{$rs->income_instructors_id}' data-style='btn-flat custom-select-sm {$style}'");


            $status = $rs->status ? '<span class="badge badge-success">โอนแล้ว</span>' : '<span class="badge badge-danger">รอการโอน</span>';
            
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $no;
            $column[$key]['course_id']  = $rs->course_id;
            $column[$key]['cycle']      = 'รอบที่ '.$rs->cycle_no;
            
            $column[$key]['title']      = $rs->title;
            $column[$key]['active']     = $status;
            $column[$key]['stuQty']     = number_format($rs->discount, 2);
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['cycle_date'] = $rs->cycle_date;
            $column[$key]['statusPicker'] = $statusPicker;
           
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            $result = false;

            if ( $type == "approve" ) {
                $value['status'] = $input['approve'];
                $result = $this->incomes_m->update_income_in($input['id'], $value);
            }
           
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
}
