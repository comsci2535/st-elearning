
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายงานประจำเดือน
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <div class="col-sm-6">
                        <?php echo form_dropdown('month', $ddMonth, date('m'), "class='form-control daily-month'") ?>
                    </div>                    
                    <div class="col-sm-6">
                        <?php echo form_dropdown('year', $ddYear, current(array_keys($ddYear)), "class='form-control daily-year'") ?>
                    </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <div id="daily-device" style="min-width: 310px; height: 600px; margin: 10px auto"></div>
            <div id="dialy-device-data"></div>
            <div id="daily-page" style="min-width: 310px; height: 600px; margin: 10px auto"></div>
            <div id="dialy-page-data"></div>

        </div>
    </div>
</div>


<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายงานประจำปี
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                     <div class="col-sm-12">
                        <?php echo form_dropdown('year', $ddYear, current(array_keys($ddYear)), "class='form-control monthly-year'") ?>
                    </div>
            </div>
        </div>
        <div class="m-portlet__body">

             <div id="monthly-device" style="min-width: 310px; height: 600px; margin: 10px auto"></div>
            <div id="monthly-device-data"></div>
            <div id="monthly-page" style="min-width: 310px; height: 600px; margin: 10px auto"></div>
            <div id="monthly-page-data"></div>

        </div>
    </div>
</div>