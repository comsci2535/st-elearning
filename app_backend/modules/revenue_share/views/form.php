<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">หัวข้อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" required>
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>
                    <div class="col-sm-7">
                        <textarea name="detail" rows="3" class="form-control" id="detail" required><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                    </div>
                </div>  
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="Instructor_price">ส่วนแบ่งฝั่งผู้สอน</label>
                    <div class="col-sm-4">
                        <input value="<?php echo isset($info->Instructor_price) ? $info->Instructor_price : NULL ?>" type="text" class="form-control m-input amount" name="Instructor_price" id="Instructor_price" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="owner_price">ส่วนแบ่งฝั่งเจ้าของ</label>
                    <div class="col-sm-4">
                        <input value="<?php echo isset($info->owner_price) ? $info->owner_price : NULL ?>" type="text" class="form-control m-input amount" name="owner_price" id="owner_price" required>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="revenue_share">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->revenue_share_id) ? encode_id($info->revenue_share_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>
</div>





