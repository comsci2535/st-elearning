<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>
        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal  frm-main', 'method' => 'post')) ?>
        <div class="m-portlet__body">
            
           
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="set_time">วันที่ตัดรายได้</label>
                <div class="col-sm-4">
                    <select id="set_time" name="set_time" class="form-control select2">
                        <option value="">เลือกวันที่</option>
                        <?php
                        $set_time = isset($info['set_time']) ? $info['set_time'] : 0;
                       
                        for($i = 1; $i <= 31; $i++):
                            if($i == $set_time):
                                $selected = 'selected';
                            else:
                                $selected = '';
                            endif;
                        ?>
                            <option value="<?=$i?>" <?=$selected?>><?=$i?></option>
                        <?php
                        endfor;
                        ?>
                    </select>
                    <p style="color:red">จะตัดรอบรายได้ตามวันที่เลือกของทุกๆ เดือน</p>
                </div>
            </div>                  
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">

            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button type="submit" class="btn btn-primary pullleft">บันทึก</button>
                <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                    </div>
                </div>
                
            </div>
        </div>
        
        <?php echo form_close() ?>

    </div>  
</div>
