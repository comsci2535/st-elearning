<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                 <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">รหัสคูปอง <span class="text-danger"> *</span></label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->couponCode) ? $info->couponCode : $couponCode ?>" type="text" id="input-couponCode" class="form-control" name="couponCode"  >
                    </div>
                </div>   
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">ชื่อคูปอง <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">Slug <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug" required>
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                </div>

                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">รายละเอียดย่อ</label>
                    <div class="col-sm-7">
                        <textarea name="excerpt" rows="3" class="form-control summernote" id="excerpt" ><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">ประเภทคูปอง <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <label class="icheck-inline"><input id="displayType1"  type="radio" name="couponType" value="0" class="icheck" <?php if(isset($info->couponType) && $info->couponType=="0"){ echo "checked"; }?> checked /> คอร์สเรียนทั้งหมด</label>
                        <label class="icheck-inline"><input id="displayType2" type="radio" name="couponType" value="1" class="icheck" <?php if(isset($info->couponType) && $info->couponType=="1"){ echo "checked"; }?>/> กำหนดคอร์สเรียน</label>  
                    </div>
                </div>
                <div id="courses_select" >
                 <div class="form-group m-form__group row"  >
                    <label class="col-sm-2 col-form-label" for="title">เลือกคอร์สเรียน <span class="text-danger"> *</span></label>
                    <div class="col-sm-8" >
                       
                        <select id="course_id" name="course_id[]"  class="form-control m-input  m-select2" multiple >
                            <option value="">เลือก</option>
                            <?php
                            if(isset($courses)): foreach($courses as $item): ?>
                                    <option value="<?=$item->course_id;?>" <?php if(!empty($coupons_courses) && in_array($item->course_id, $coupons_courses)){ echo "selected"; }?> ><?=$item->title;?></option>
                            <?php endforeach; endif; ?>
                            
                        </select>
                    </div>
                  </div>
                </div>

                <div class="form-group m-form__group row" id="courses_select2" >
                    <label class="col-sm-2 col-form-label" for="title">จำนวนการใช้รหัสส่วนลดต่อคน <span class="text-danger"> *</span></label>
                    <div class="col-sm-3">
                       <select class="form-control select2" name="isCourse" id="isCourse">
                           <option value="0" <?php if(!empty($info) && $info->isCourse==0){ echo "selected"; }?>>ไม่จำกัด</option>
                           <option value="1" <?php if(!empty($info) && $info->isCourse==1){ echo "selected"; }?>>จำกัด</option>
                       </select>
                       
                    </div>
                    
                    <div class="col-sm-2" style="display: none;" id="isCourseNum-d">
                      
                       <input placeholder="ระบุ" value="<?php echo isset($info->isCourseNum) ? $info->isCourseNum : NULL ?>" type="number" id="input-title" class="form-control" name="isCourseNum" > 
                    </div>
                    <div class="col-sm-1" >
                      
                        ครั้ง
                    </div>
                </div>

               

                 <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">จำนวนคูปองที่ใช้ได้ <span class="text-danger"> *</span></label>
                    <div class="col-sm-3">
                        <select class="form-control select2" name="isMember" id="isMember">
                           <option value="0" <?php if(!empty($info) && $info->isMember==0){ echo "selected"; }?>>ไม่จำกัด</option>
                           <option value="1" <?php if(!empty($info) && $info->isMember==1){ echo "selected"; }?>>จำกัด</option>
                       </select>
                       
                    </div>
                   
                    <div class="col-sm-2"  style="display: none;" id="isMemberNum-d">
                      
                       <input placeholder="ระบุ"  value="<?php echo isset($info->isMemberNum) ? $info->isMemberNum : NULL ?>" type="number" id="input-title" class="form-control" name="isMemberNum" > 
                    </div>
                      <div class="col-sm-1" >
                        คูปอง
                      </div>
                   
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">ประเภทส่วนลด <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <label class="icheck-inline"><input id="display1"  type="radio" name="type" value="1" class="icheck" <?php if(isset($info->type) && $info->type=="1"){ echo "checked"; }?> checked /> บาท</label>
                        <label class="icheck-inline"><input id="display2" type="radio" name="type" value="2" class="icheck" <?php if(isset($info->type) && $info->type=="2"){ echo "checked"; }?>/> ส่วนลด %</label>
                       
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">จำนวนส่วนลด <span class="text-danger"> *</span></label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->discount) ? number_format($info->discount) : NULL ?>" type="text" id="input-discount" class="amount-coupon form-control" name="discount" required>
                        <div id="alert_e" style="color: red;"></div>
                    </div>
                </div> 

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">ช่วงเวลาคูปอง <span class="text-danger"> *</span></label>
                    <div class="col-sm-3">
                            <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>" required />
                            <input type="hidden" name="startDate" value="<?php echo isset($info->startDate) ? $info->startDate : NULL ?>" />
                            <input type="hidden" name="endDate" value="<?php echo isset($info->endDate) ? $info->endDate : NULL ?>" />
                    </div>
                </div>

            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="coupons">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->coupon_id) ? encode_id($info->coupon_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=$info->coupon_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->coupon_id;?>';

</script>




