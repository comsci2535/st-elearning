<style type="text/css">
    .hidden{
        display: none;
    }
</style>
<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       จัดเรียงลำดับ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                 <?php echo $boxAction; ?>
            </div>
        </div>

       
         <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-sm-2 text-right hidden">
                        <button title="ขยายทั้งหมด" id="expand-all" type="button" class="btn btn-xs btn-flat bg-purple"><i class="fa fa-plus"></i></button>
                        <button title="ยุบทั้งหมด" id="collapse-all" type="button" class="btn btn-xs btn-flat bg-purple"><i class="fa fa-minus"></i></button>
                    </div>
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="dd" id="nestable"></div>
                    </div>
                </div>  

                <div class="form-group m-form__group row">
                    <textarea class="form-control hidden" name="order" id="nestable-output"></textarea>  
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div id="overlay-box" class="overlay hidden">
                <i class="fa fa-refresh fa-spin"></i>
            </div> 
            <input type="hidden" name="course_id" id="input-course_id" value="<?php echo $course_id ?>">
            <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
            <input type="hidden" name="categoryType" id="input-catetory-type" value="<?php echo isset($categoryType) ? $categoryType : NULL ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>


<script>
    var category = <?php echo json_encode($treeData) ?>;
    var options = {'json': category}
    var categoryType = "";
</script>
