<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">สถานะ</label>
                    <div class="col-sm-7 icheck-inline">
                        <label><input <?php echo $info['active']==1 ? "checked" : NULL ?> type="radio" name="active" class="icheck" value="1" > เปิด</label>
                        <label><input <?php echo $info['active']==0 ? "checked" : NULL ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">เนื้อหาหลัก</label>
                    <div class="col-sm-7">
                        <?php echo $parentModule ?>
                    </div>
                </div>                  
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title"><font color=red>*</font> ชื่อเนื้อหา</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info['title']) ? $info['title'] : NULL ?>" type="text" class="form-control m-input " name="title" id="-input-title" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
              <!--   <div class="form-group m-form__group row" style="display: none;">
                    <label class="col-sm-2 col-form-label" for="slug">Slug</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info['slug']) ? $info['slug'] : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug" required>
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                </div> -->
                <div id="c-content" style="display: block;">
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label" for="title">รายละเอียดย่อ</label>
                        <div class="col-sm-7">
                            <textarea name="excerpt" rows="3" class="form-control" id="excerpt" ><?php echo isset($info['excerpt']) ? $info['excerpt'] : NULL ?></textarea>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>
                        <div class="col-sm-7">
                            <textarea name="detail" rows="3" class="form-control summernote" id="detail" ><?php echo isset($info['detail']) ? $info['detail'] : NULL ?></textarea>
                        </div>
                    </div>  
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label"><font color=red>*</font> ประเภทเนื้อหา</label>
                        <div class="col-sm-4">
                            <?php $type = array(""=>'เลือก',0=>'ตัวอย่างดูฟรี', 1=>'ลงทะเบียน') ?>
                            <?php echo form_dropdown('type', $type,  isset($info['type']) ? $info['type'] : NULL ,  'class="form-control select2" required') ?>
                        </div>
                    </div> 
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label">ลิงก์วิดีโอ</label>
                        <div class="col-sm-7">
                            <input value="<?php echo isset($info['videoLink']) ? $info['videoLink'] : NULL ?>" type="text" id="input-videoLink" class="form-control" name="videoLink" required>
                        </div>
                    </div> 
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label">ความยาววิดีโอ</label>
                        <div class="col-sm-2">
                            <input value="<?php echo isset($info['videoLength']) ? $info['videoLength'] : NULL ?>" type="text" id="input-videoLength" class="form-control" name="videoLength" required>
                        </div>
                         <span class="col-sm-2 text-left">
                            นาที
                        </span>
                    </div> 
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label">ไฟล์เอกสาร</label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input class="icheck" name="fileTypeUpload" id="rd_upload_type_system" value="1" type="radio" <?php  if(!empty($info['fileTypeUpload']) && $info['fileTypeUpload'] == 1){ echo "checked"; } ?> <?php if(empty($info['fileTypeUpload'])){ echo "checked"; } ?>  /> อัพโหลดผ่านหน้าเว็บ 
                                 <input class="icheck" name="fileTypeUpload" id="rd_upload_type_record" value="2" type="radio" <?php  if(!empty($info['fileTypeUpload']) && $info['fileTypeUpload'] == 2){ echo "checked"; } ?> /> ลิงก์ไฟล์       
                            </div>
                        </div>
                    </div> 
                    <div class="form-group m-form__group row" id="f-file" >
                        <label class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-7">
                            <input id="file" name="file" type="file" data-preview-file-type="text">
                            <input type="hidden" name="outfile" value="<?=(isset($info['fileUrl'])) ? $info['fileUrl'] : ''; ?>">
                       </div>
                   </div>
                   <div class="form-group m-form__group row" id="f-url" style="display: none">
                        <label class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-7">
                             <input value="<?php echo isset($info['fileUrl']) ? $info['fileUrl'] : NULL ?>" type="text" id="input-fileUrl" class="form-control" name="fileUrl" >
                        </div>
                    </div>
               </div>

            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="course_id" id="input-course_id" value="<?php echo $course_id ?>">
            <input type="hidden" class="form-control" name="db" id="db" value="courses_lesson">
            <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">

            <input type="hidden" name="id" id="input-id" value="<?php echo isset($info['course_lesson_id']) ? encode_id($info['course_lesson_id']) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>
</div>
<script type="text/javascript">
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info['fileUrl'])) ? $this->config->item('root_url').$info['fileUrl'] : ''; ?>';
    var file_id         = '<?=(isset($info['course_lesson_id'])) ? $info['course_lesson_id'] : '0'; ?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/'+file_id;

</script>
