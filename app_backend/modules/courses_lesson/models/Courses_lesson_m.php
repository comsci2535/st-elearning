<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Courses_lesson_m Extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_rows($param) {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $this->db->order_by('order', 'asc')
                ->order_by('parent_id', 'asc');        
        
        $query = $this->db
                        ->select('a.*')
                        ->from('courses_lesson a')
                        ->get();
        return $query;
    }
    
    public function get_count($param){
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('courses_lesson a')
                        ->get();
        return $query->num_rows();
    }
    
    private function _condition($param){


        if ( isset($param['course_id']) ) 
            $this->db->where('a.course_id', $param['course_id']);
        
        if ( isset($param['course_lesson_id']) ) 
            $this->db->where('a.course_lesson_id', $param['course_lesson_id']);
        
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        
        if ( $this->router->method == 'order' ) {
            $this->db->where('active', 1);
        }
    }
    
    public function get_by_id($id)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('courses_lesson a')
                        ->where('a.course_lesson_id', $id)
                        ->get()
                        ->row_array();
        return $query; 
    }
    
    public function row_by_class($class) {
        
        $query = $this->db
                        ->select('*')
                        ->from('courses_lesson a')
                        ->where('class', $class)
                        ->get();
        return $query->row();
    }    
    
    public function row_by_class_method($class, $method) {
        
        $query = $this->db
                        ->select('*')
                        ->from('courses_lesson a')
                        ->where('class', $class)
                        ->or_like('class', $class)
                        ->get();
        return $query->row();
    }
    
    public function get_courses_lesson($course_id)
    {
        $this->db
                ->order_by('parent_id', 'asc')
                ->order_by('order', 'asc')
                ->order_by('course_lesson_id', 'asc'); 
        $query = $this->db
                        ->from('courses_lesson a')
                        ->select('a.course_lesson_id, a.title, a.parent_id, a.order')
                        ->where('a.recycle', 0)
                        ->where('a.course_id', $course_id)
                        ->get()
                        ->result_array();
        return $query;
    }
    
    public function get_courses_lesson_order()
    {
        $query = $this->db
                        ->from('courses_lesson a')
                        ->select('a.course_lesson_id, a.title, a.parent_id, a.order')
                        ->where('a.status', 1)
                        ->where('a.recycle', 0)
                        ->order_by('a.order')
                        ->order_by('a.parent_id')
                        ->get()
                        ->result_array();
        return $query;
    }
    
    public function insert($value)
    {
        $this->db->insert('courses_lesson', $value);
        return $this->db->insert_id();
    }
    
    public function delete($param)
    {
        $rs = $this->db
                    ->where_in('course_lesson_id', $param)
                    ->delete('courses_lesson');
        return $rs;
    }
    
    public function update($value, $id)
    {
        $rs = $this->db
                    ->where('course_lesson_id', $id)
                    ->update('courses_lesson', $value);
        return $rs;        
    }  
    
    public function update_order($value)
    {
        $rs = $this->db
                    ->update_batch('courses_lesson', $value, 'course_lesson_id');
        return $rs;   
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('course_lesson_id', $id)
                        ->update('courses_lesson', $value);
        return $query;
    }

    public function get_courses_lesson_by_course_id_all($id) 
    {
        $this->db->where('course_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $query = $this->db->get('courses_lesson');
        return $query;
    }

    public function get_courses_lesson_by_course_id($course_id) 
    {
        $this->db->select('a.*');
        $this->db->select('b.slug as course_slug');
        $this->db->where('a.course_id', $course_id);
        $this->db->where('a.parent_id', 0);
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->join('courses b','b.course_id=a.course_id','left');
        $query = $this->db->get('courses_lesson a');
                          
        return $query;
    }

    public function get_courses_lesson_by_parent_id($parent_id = 0) 
    {
        $this->db->where('parent_id', $parent_id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $query = $this->db->get('courses_lesson');
        return $query;
    }
    
    public function get_courses_lesson_member_by_id($course_lesson_id, $user_id='') 
    {
        if(!empty($user_id)):
            $this->db->where('user_id', $user_id);
        endif;
        $this->db->where('course_lesson_id', $course_lesson_id);
        $query = $this->db->get('courses_lesson_member');
        return $query;
    }
}

