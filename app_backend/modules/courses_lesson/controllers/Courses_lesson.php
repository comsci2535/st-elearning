<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Courses_lesson extends MX_Controller {

    private $_title = 'จัดการเนื้อหาคอร์สเรียน';
    private $_pageExcerpt = 'การจัดการข้อมูลเกี่ยวกับเนื้อหาคอร์สเรียน';
    private $_grpContent = 'courses_lesson';
    private $_permission;
    private $_treeData;
    private $_orderData = array();

    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr', 'error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->_treeData = new stdClass();
        $this->load->model('courses_lesson_m');
        $this->load->model("courses/courses_m");
        $this->load->library('uploadfile_library');
       
    }

    public function index($course_id="") {
        $this->load->module('template');

        // toobar
        $action[1][] = action_refresh(base_url("{$this->router->class}/index/{$course_id}"));
        //$action[1][] = action_filter();
        $action[2][] = action_order(base_url("{$this->router->class}/order/{$course_id}"));
        $action[2][] = action_add(base_url("{$this->router->class}/create/{$course_id}"));
        $action[3][] = action_trash_multi("{$this->router->class}/destroy");
        $action[3][] = action_trash_view(base_url("{$this->router->class}/trash/{$course_id}"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);


        
        $input['course_id'] = $course_id;
        $courses = $this->courses_m->get_rows($input)->row();
        $data['course_id'] = $course_id;

        // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($courses->title, base_url("courses"));
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}"));
        

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";

        $this->template->layout($data);
    }

    public function data_index() 
    {
        $input = $this->input->post();
        $input['recycle'] = 0;
        //arr($input);exit();
        $input['grpContent'] = $this->_grpContent;
        $info = $this->courses_lesson_m->get_rows($input);
        $infoCount = $this->courses_lesson_m->get_count($input);
        $tree = $this->_build_tree($info);
        $treeData = $this->_print_tree($tree);
        $column = array();
        $key = 0;
        foreach ($treeData as $rsTree) {
            $rs = $rsTree->info;
            if ( isset($input['search']['value']) && $input['search']['value'] != "" ) {
                if ( mb_stripos($rsTree->title, $input['search']['value']) !== false ) {
                    $found = true;
                } else {
                    $found = false;
                }
            } else {
                $found = true;
            }

            //--------------------------
            
            $manage = array();
            if($rs->parent_id != 0){
                $manage[] = action_custom(site_url("exercise/index/{$rs->course_lesson_id}"),'btn-info','add','','fa-swatchbook','','แบบฝึกหัด');
                $manage[] = action_custom(site_url("homeworks/index/{$rs->course_lesson_id}"),'btn-success','add','','fa-book','','การบ้าน');
            }
                //--------------------------


            if ( $found ) {
                $id = encode_id($rs->course_lesson_id);
                $action = array();
                $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$input['course_id']}/{$id}"));
                $active = $rs->active ? "checked" : null;
                $column[$key]['DT_RowId'] = $id;
                $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
                $column[$key]['title'] = $rsTree->title;
                $column[$key]['excerpt'] = $rs->excerpt;
                $column[$key]['active'] = toggle_active($active, "{$this->router->class}/action/active");
                $column[$key]['manage'] = implode($manage," "); 
                $column[$key]['created_at'] = datetime_table($rs->created_at);
                $column[$key]['updated_at'] = datetime_table($rs->updated_at);
                $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
                $key++;
            }
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }  

    public function trash($course_id) {
        $this->load->module('template');

        // toobar
        $action[1][] = action_list_view(site_url("{$this->router->class}/index/{$course_id}"));
        $action[2][] = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}/index/{$course_id}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash/{$course_id}"));

        $data['course_id'] = $course_id;
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/trash";

        $this->template->layout($data);
    }

    public function data_trash() 
    {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $input['grpContent'] = $this->_grpContent;
        $info = $this->courses_lesson_m->get_rows($input);
        $infoCount = $this->courses_lesson_m->get_count($input);
        $tree = $this->_build_tree($info);
        $treeData = $this->_print_tree($tree);
        $column = array();
        $key = 0;
        foreach ($treeData as $rsTree) {
            $rs = $rsTree->info;
            if ( isset($input['search']['value']) && $input['search']['value'] != "" ) {
                if ( mb_stripos($rsTree->title, $input['search']['value']) !== false ) {
                    $found = true;
                } else {
                    $found = false;
                }
            } else {
                $found = true;
            }
            if ( $found ) {
                $id = encode_id($rs->course_lesson_id);
                $action = array();
                $action[1][] = table_restore("{$this->router->class}/action/restore");
                $active = $rs->active ? "checked" : null;
                $column[$key]['DT_RowId'] = $id;
                $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
                $column[$key]['title'] = $rsTree->title;
                $column[$key]['excerpt'] = $rs->excerpt;
                $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
                $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
                $key++;
            }
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }  

    private function _parent_dropdwon($deep,$course_id) {
        $info = $this->courses_lesson_m->get_courses_lesson($course_id);
        foreach ($info as &$rs ){
            $rs['id'] = $rs['course_lesson_id'];
        }
        $tree = Modules::run('utils/build_tree_2', $info);
        $print_tree = Modules::run('utils/print_tree', $tree, $deep);
        return $print_tree;
    }

    public function create($course_id) {

        $this->load->module('template');
        
        $info['active'] = 1;
        $data['info'] = $info;

        $dropDown = form_dropdown('parent_id', $this->_parent_dropdwon(1,$course_id), '', 'class="form-control select2" required');
        $data['parentModule'] = $dropDown;

        
        
        $data['frmAction'] = site_url("{$this->router->class}/storage");

        
        $input['course_id'] = $course_id;
        $course = $this->courses_m->get_rows($input)->row();
        $data['course_id'] = $course_id;

        // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("courses"));
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}/index/{$course_id}"));
        $data['breadcrumb'][] = array("เพิ่มใหม่", base_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";

        $this->template->layout($data);
    }

    public function storage() {
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $id = $this->courses_lesson_m->insert($value);
        $value['course_lesson_id'] = $id;
        if ( $id ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }

        redirect(base_url("{$this->router->class}/index/{$input['course_id']}"));
    }

    public function edit($course_id,$id = 0) {
        $this->load->module('template');
        
        

        $id = decode_id($id);
        $info = $this->courses_lesson_m->get_by_id($id);

        $dropDown = form_dropdown('parent_id', $this->_parent_dropdwon(1,$course_id), $info['parent_id'], 'class="form-control select2" required');
        $data['parentModule'] = $dropDown;
        $data['info'] = $info;

        //arr($data['info']);exit();
        $data['frmAction'] = base_url("{$this->router->class}/update");

        $input['course_id'] = $course_id;
        $course = $this->courses_m->get_rows($input)->row();
        $data['course_id'] = $course_id;

        
       // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("courses"));
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}/index/{$course_id}"));
        $data['breadcrumb'][] = array("แก้ไข", base_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";

        $this->template->layout($data);
    }

    public function update() {

        $input = $this->input->post();
       
        $value = $this->_build_data($input);
        $input['id'] = decode_id($input['id']);
        $result = $this->courses_lesson_m->update($value, $input['id']);
        if ( $result ) {
           
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(base_url("{$this->router->class}/index/{$input['course_id']}"));
    }

    public function order($course_id) {
        $this->load->module('template');
        $input['course_id'] = $course_id;
        $input['recycle'] = 0;
        $input['order'][0]['column'] = 1;
        $input['order'][0]['dir'] = 'asc';
        $info = $this->courses_lesson_m->get_rows($input);
       //arrx($info->result());
        foreach ($info->result() as $rs) {
            $thisref = &$refs[$rs->course_lesson_id];
            $thisref['id'] = $rs->course_lesson_id;
            $thisref['content'] = $rs->title;
            if ($rs->parent_id != 0) {
                $refs[$rs->parent_id]['children'][] = &$thisref;
            } else {
                $treeData[] = &$thisref;
            }
        }
       //arrx($treeData);
        $data['treeData'] = $treeData;
        $data['frmAction'] = site_url("{$this->router->class}/update_order");
        
        
        // toobar
        $action[1][] = action_refresh(base_url("{$this->router->class}/order/{$course_id}"));
        $action[1][] = action_list_view(base_url("{$this->router->class}/index/{$course_id}"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb

        
        $course = $this->courses_m->get_rows($input)->row();
        $data['course_id'] = $course_id;

        // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("courses"));
        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}/index/{$course_id}"));
        $data['breadcrumb'][] = array('จัดลำดับ', base_url("{$this->router->class}/order/{$course_id}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/order";
        //$data['pageScript'] = "scripts/backend/{$this->router->class}/order.js";

        $this->template->layout($data);
    }
    
    public function update_order(){
        $input = $this->input->post();
        $order = json_decode($input['order']);
        $value = $this->_build_data_order($order);
        $result = $this->db->update_batch('courses_lesson', $value, 'course_lesson_id');
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/order/{$input['course_id']}"));
    }
    
    private function _build_data_order($info, $parent_id=0){
        $order = 0;
        foreach ($info as $rs) {
            $order++;
            $this->_orderData[$rs->id]['course_lesson_id'] = $rs->id; 
            $this->_orderData[$rs->id]['order'] = $order;
            $this->_orderData[$rs->id]['parent_id'] = $parent_id;
            if ( isset($rs->children) )
                $this->_build_data_order ($rs->children, $rs->id);
        }
        return $this->_orderData;
    }

    private function _build_tree($info, $parent_id=0) {
        $tree = new stdClass();
        foreach ($info->result() as $d) {
            if ($d->parent_id == $parent_id) {
                $children = $this->_build_tree($info, $d->course_lesson_id);
                if ( !empty($children) ) {
                    $d->children = $children;
                }
                $tree->{$d->course_lesson_id} = $d;
            }
        }
        return $tree;
    }
    
    private function _print_tree($tree, $level = 5, $r = 1, $p = null, $d = NULL) {
        foreach ( $tree as $t ) {
            $dash = ($t->parent_id == 0) ? '' : $d . ' &#8594; ';
            $this->_treeData->{$t->course_lesson_id} = new stdClass();
            $this->_treeData->{$t->course_lesson_id}->title = $dash . $t->title;
            $this->_treeData->{$t->course_lesson_id}->info = $t;
            if ( isset($t->children) ) {
                if ( $r < $level ) {
                    $this->_print_tree($t->children, $level, $r + 1, $t->parent_id, $dash . $t->title);
                }
            }
        }
        return $this->_treeData;
    }

    private function _build_data($input) {

        $value                      = array();
        $value['course_id']         = (int)$input['course_id'];
        $value['active']            = (int)$input['active'];
        $value['parent_id']         = (int)$input['parent_id'];
        $value['type']              = (int)$input['type'];
        $value['title']             = $input['title'];
        $value['slug']              = $input['slug'];
        $value['videoLink']         = $input['videoLink'];
        $value['videoLength']       = $input['videoLength'];
        $value['detail']            = html_escape($input['detail']);
        $value['excerpt']           = html_escape($input['excerpt']);
        $value['fileTypeUpload']    = isset($input['fileTypeUpload']) ? $input['fileTypeUpload'] : null;
        $value['fileUrl']           = isset($input['fileUrl']) ? $input['fileUrl'] : null;
        if($input['fileTypeUpload']=='1'){
            $path        = 'courses_lesson';
            $upload      = $this->uploadfile_library->do_upload('file',TRUE,$path);
            $file        = '';
            if(isset($upload['index'])){
                $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
                $outfile = $input['outfile'];
                if(isset($outfile)){
                    $this->load->helper("file");
                    unlink($outfile);
                }
                $value['fileUrl'] = $file;
            }
        }
       
        if ($input['mode'] == 'create') {
            $value['created_at'] = db_datetime_now();
            $value['updated_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
        }

        // arr($value);
        // exit();

        return $value;
    }

    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->courses_lesson_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->courses_lesson_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }

    public function delete() {
        Modules::run('utility/permission', 2);
        $input = $this->input->post();
        $param = $input['id'];
        $id = $this->courses_lesson_m->delete($param);
        if ($id) {
            Modules::run('utility/notify', 'warning', 'ลบรายการเรียบร้อย', 'โปรดปรับปรุงสิทธิผู้ดูแลระบบ');
        } else {
            Modules::run('utility/notify', 'error', 'ลบรายการไม่สำเร็จ');
        }
        redirect(base_url("{$this->router->class}"));
    }
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs) $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->courses_lesson_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycle_at'] = $dateTime;
                $value['recycle_by'] = $this->session->users['user_id'];
                $result = $this->courses_lesson_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->courses_lesson_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->courses_lesson_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids){

		$arrayName = array('fileUrl' => $ids);
		echo json_encode($arrayName);
	}
    

}
