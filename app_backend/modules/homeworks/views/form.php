<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="titles">หัวข้อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="titles" required>
                        
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>
                    <div class="col-sm-7">
                        <textarea name="detail" rows="3" class="form-control" id="detail"><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                    </div>
                </div>  
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="courses_homeworks">
             <input type="hidden" class="form-control" name="course_lesson_id" id="course_lesson_id" value="<?php echo !empty($info->course_lesson_id) ? $info->course_lesson_id : $course_lesson_id ?>">
             
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->courses_homework_id) ? encode_id($info->courses_homework_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>
</div>





