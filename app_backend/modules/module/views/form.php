<style type="text/css">
    .hidden {
    display: none!important;
}
</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">Separate</label>
                    <div class="col-sm-7 icheck-inline">
                        <label><input <?php echo $info['type']==2 ? "checked" : NULL ?> type="radio" name="type" class="icheck" value="2" > ใช่</label>
                        <label><input <?php echo $info['type']==1 ? "checked" : NULL ?> type="radio" name="type" class="icheck" value="1"> ไม่ใช่</label>
                    </div>
                </div>         
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">สถานะ</label>
                    <div class="col-sm-7 icheck-inline">
                        <label><input <?php echo $info['active']==1 ? "checked" : NULL ?> type="radio" name="active" class="icheck" value="1" > เปิด</label>
                        <label><input <?php echo $info['active']==0 ? "checked" : NULL ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
                    </div>
                </div>     
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">เมนูด้านข้าง</label>
                    <div class="col-sm-7 icheck-inline">
                        <label><input <?php echo $info['isSidebar']==1 ? "checked" : NULL ?> type="radio" name="isSidebar" class="icheck" value="1" > ใช่</label>
                        <label><input <?php echo $info['isSidebar']==0 ? "checked" : NULL ?> type="radio" name="isSidebar" class="icheck" value="0"> ไม่ใช่</label>
                    </div>
                </div>  
                <?php if ($this->router->method == 'create') : ?>
                    <div class="form-group m-form__group row">
                        <label class="col-sm-2 col-form-label">สร้างไฟล์โมดูล</label>
                        <div class="col-sm-7 icheck-inline">
                            <label><input <?php echo $info['createModule']==1 ? "checked" : NULL ?> type="radio" name="createModule" class="icheck" value="1"> สร้าง</label>
                            <label><input <?php echo $info['createModule']==0 ? "checked" : NULL ?> type="radio" name="createModule" class="icheck" value="0" > ไม่สร้าง</label>
                        </div>
                    </div>         
                <?php endif; ?>

                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">หมวดหมู่หลัก</label>
                    <div class="col-sm-7">
                        <?php echo $parentModule ?>
                    </div>
                </div> 

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">ชื่อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info['title']) ? $info['title'] : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>
                    </div>
                </div>

                <div class="form-group m-form__group row" style="display: none;">
                    <label class="col-sm-2 col-form-label">Directory</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info['directory']) ? $info['directory'] : NULL ?>" type="text" id="input-clss" class="form-control" name="directory">
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">Class</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info['class']) ? $info['class'] : NULL ?>" type="text" id="input-clss" class="form-control" name="class">
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="icon">ไอคอน</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info['icon']) ? $info['icon'] : "fa fa-angle-right" ?>" type="text" id="input-icon" class="form-control" name="icon">
                    </div>
                </div>   

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="descript">อธิบาย</label>
                    <div class="col-sm-7">
                        <textarea name="descript" rows="3" class="form-control"><?php echo isset($info['descript']) ? $info['descript'] : "การจัดการข้อมูลเกี่ยวกับ ___" ?></textarea>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
            <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
            <input type="hidden" name="id" id="input-id" value="<?php echo isset($info['moduleId']) ? encode_id($info['moduleId']) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>




