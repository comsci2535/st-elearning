<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Certificates extends MX_Controller {

    private $_title = "ข้อมูลเกียรติบัตร";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับข้อมูลเกียรติบัตร";
    private $_grpContent = "certificates";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->library('uploadfile_library');
        $this->load->model("certificates_m");
    } 

    public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}
		echo $picture;
    }

    public function deletefile($ids){

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
    }
    
    public function manage($course_id=""){
        $this->load->module('template');

        $info = $this->certificates_m->get_certificates_by_coursesid($course_id)->row_array();

        $data['info'] = $info;
        $data['courses_id'] = $course_id;

        if(!empty($info)){
            $data['frmAction'] = site_url("{$this->router->class}/update");
        }else{
             $data['frmAction'] = site_url("{$this->router->class}/storage");
        }

        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $data['breadcrumb'][] = array("คอร์สเรียน", site_url("courses/index"));
        $data['breadcrumb'][] = array("เกียรติบัตร", site_url("{$this->router->class}/{$this->router->method}"));
        
        $this->template->layout($data);
    }

    public function storage(){
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->certificates_m->insert($value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }

    public function update(){
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->certificates_m->update($id,$value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/manage/{$input['courses_id']}"));
    }

    private function _build_data($input) {

        $value['courses_id'] = $input['courses_id'];
        $value['detail']     = html_escape($this->input->post('certificate', FALSE));
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = $this->session->users['user_id'];
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = $this->session->users['user_id'];
        $path                = 'certificates';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
        $file = '';
        if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
            $outfile = $input['outfile'];
            if(isset($outfile)){
                 $this->load->helper("file");
                 unlink($outfile);
            }
            $value['file'] = $file;
        }
      
        return $value;
    }
    
}
