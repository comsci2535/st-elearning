<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        แบบฟอร์ม 
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal  frm-main', 'method' => 'post')) ?>

        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="file">พื้นหลัง</label>
                <div class="col-sm-7">
                    <input id="file" name="file" type="file" data-preview-file-type="text">
                     <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : ''; ?>">
                </div>
            </div> 
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="aboutUs"></label>
                <div class="col-sm-7">
                    <h4>ตัวแปรการสร้างเกียรติบัตร</h4>
                    <div class="alert alert-success">
                        <h4>ชื่อคอร์สเรียน : <strong> certificate.getCourseName()</strong></h4>
                        <h4>ชื่อนักเรียน : <strong> certificate.getStudentName()</strong></h4>
                        <h4>วันที่ปริ้น TH : <strong> certificate.getDateTH()</strong></h4>
                        <h4>วันที่ปริ้น EN : <strong> certificate.getDateEN()</strong></h4>
                    </div>
                </div>
            </div>    
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="aboutUs"></label>
                <div class="col-sm-7">
                    <textarea name="certificate" rows="3" class="form-control summernote" id="aboutUs"><?php echo !empty($info['detail']) ? $info['detail'] : '' ?></textarea>
                </div>
            </div>                  
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">

            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <input type="hidden" name="courses_id" value="<?=$courses_id?>">
                        <input type="hidden" name="id" id="input-id" value="<?php echo isset($info['certificate_id']) ? encode_id($info['certificate_id']) : 0 ?>">
                        <button type="submit" class="btn btn-primary pullleft">บันทึก</button>
                <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                    </div>
                </div>
                
            </div>
        </div>
        
        <?php echo form_close() ?>

    </div>  
</div>
<script>
    //set par fileinput;
    var required_icon   = false; 
    var file_image      = '<?=(isset($info['file'])) ? $this->config->item('root_url').$info['file'] : '-'; ?>';
    var file_id         = '<?=$info['certificate_id'];?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info['certificate_id'];?>';

</script>