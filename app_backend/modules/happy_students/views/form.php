<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-12 col-md-2 col-form-label" for="title"></label>
                    <div class="col-sm-12 col-md-8">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php if(!empty($info->recommend) &&  $info->recommend=='1'){ echo "checked";}?> type="checkbox" name="recommend" valus="1"> รายการแนะนำ
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-12 col-md-2 col-form-label" for="title">ผู้รีวิว <span class="text-danger"> *</span></label>
                    <div class="col-sm-12 col-md-8">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                     <label class="col-sm-12 col-md-2 col-form-label" for="slug">Slug <span class="text-danger"> *</span></label>
                    <div class="col-sm-12 col-md-8">
                        <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug" required>
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                     <label class="col-sm-12 col-md-2 col-form-label" for="file">ไฟล์</label>
                    <div class="col-sm-12 col-md-8">
                        <input id="file" name="file" type="file" data-preview-file-type="text">
                        <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : ''; ?>">
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                     <label class="col-sm-12 col-md-2 col-form-label" for="title">รายละเอียด</label>
                    <div class="col-sm-12 col-md-8">
                        <textarea name="excerpt" rows="3" class="form-control summernote" id="excerpt"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="happy_students">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->happy_student_id) ? encode_id($info->happy_student_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : 'img'; ?>';
    var file_id         = '<?=$info->happy_student_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->happy_student_id;?>';

</script>




