<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                    
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำถาม :</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->exam_topic_name) ? $info->exam_topic_name : NULL ?>" type="text" id="input-exam_topic_name" class="form-control" name="exam_topic_name" required>
                    </div>
                </div>
                <!-- <div class="form-group m-form__group row">
                    <label class="col-sm-2 control-label">รูปภาพประกอบ :</label>
                    <div class="col-sm-7">
                        
                    </div>
                </div> --> 
                <div class="col-sm-12"  id="exam_type_id2"> 
                    <div class="form-group m-form__group row">
                        <label class="col-sm-3 control-label icheck-inline"> 
                        <input class="icheck" name="answer_true[]" id="answer_true1" value="0" type="radio"  <?php if(!empty($item_answer[0]) && $item_answer[0]->point=='1' ){ ?>  checked='checked'  <?php } ?>/> </label>
                        <div class="col-sm-6">
                          <input type="hidden" class="form-control" id="exam_answer_id" name="exam_answer_id[]" value="<?php if(!empty($item_answer[0])){ echo $item_answer[0]->exam_answer_id ; } ?>" >
                            <input type="text" class="form-control" id="exam_answer_name1" name="exam_answer_name[]" value="<?php if(!empty($item_answer[0])){ echo $item_answer[0]->exam_answer_name ;  }?>" >
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-sm-3 control-label icheck-inline">
                        <input class="icheck" name="answer_true[]" id="answer_true2" value="1" type="radio"  <?php if(!empty($item_answer[1]) && $item_answer[1]->point=='1' ){ ?>  checked='checked'  <?php } ?>  /></label>
                        <div class="col-sm-6">
                        <input type="hidden" class="form-control" id="exam_answer_id" name="exam_answer_id[]" value="<?php if(!empty($item_answer[1])){ echo $item_answer[1]->exam_answer_id ; } ?>" >
                            <input type="text" class="form-control" id="exam_answer_name2" name="exam_answer_name[]" value="<?php if(!empty($item_answer[1])){ echo $item_answer[1]->exam_answer_name ;  }?>" >
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-sm-3 control-label icheck-inline">
                        <input class="icheck" name="answer_true[]" id="answer_true3" value="2" type="radio"   <?php if(!empty($item_answer[2]) && $item_answer[2]->point=='1' ){ ?>  checked='checked'  <?php } ?>   /></label>
                        <div class="col-sm-6">
                        <input type="hidden" class="form-control" id="exam_answer_id" name="exam_answer_id[]" value="<?php if(!empty($item_answer[2])){ echo $item_answer[2]->exam_answer_id ; } ?>" >
                            <input type="text" class="form-control" id="exam_answer_name3" name="exam_answer_name[]" value="<?php if(!empty($item_answer[2])){ echo $item_answer[2]->exam_answer_name ;  }?>" >
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-sm-3 control-label icheck-inline">
                        <input class="icheck" name="answer_true[]" id="answer_true4" value="3" type="radio"  <?php if(!empty($item_answer[3]) && $item_answer[3]->point=='1' ){ ?>  checked='checked'  <?php } ?>    /> </label>
                        <div class="col-sm-6">
                        <input type="hidden" class="form-control" id="exam_answer_id" name="exam_answer_id[]" value="<?php if(!empty($item_answer[3])){ echo $item_answer[3]->exam_answer_id ; } ?>" >
                            <input type="text" class="form-control" id="exam_answer_name4" name="exam_answer_name[]" value="<?php if(!empty($item_answer[3])){ echo $item_answer[3]->exam_answer_name ;  }?>" >
                        </div>
                    </div>
                     <div class="form-group m-form__group row">
                        <label class="col-sm-3 control-label icheck-inline">
                        <input class="icheck" name="answer_true[]" id="answer_true5" value="4" type="radio"   <?php if(!empty($item_answer[4]) && $item_answer[4]->point=='1' ){ ?>  checked='checked'  <?php } ?>   /></label>
                        <div class="col-sm-6">
                        <input type="hidden" class="form-control" id="exam_answer_id" name="exam_answer_id[]" value="<?php if(!empty($item_answer[4])){ echo $item_answer[4]->exam_answer_id ; } ?>" >
                            <input type="text" class="form-control" id="exam_answer_name5" name="exam_answer_name[]" value="<?php if(!empty($item_answer[4])){ echo $item_answer[4]->exam_answer_name ;  }?>" >
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
            <input type="hidden" name="exam_id" id="input-examId" value="<?php echo $exam_id ?>">
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
            <!--  <input type="hidden" class="form-control" name="db" id="db" value="articles"> -->
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->exam_topic_id) ? encode_id($info->exam_topic_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>

<script>
    //set par fileinput;
    //var required_icon   = true; 
   // var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    //var file_id         = '<?=$info->article_id;?>';
    //var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->article_id;?>';

</script>




