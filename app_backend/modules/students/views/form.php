<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>

        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title"></label>
                <div class="col-sm-7">
                    <label class="m-checkbox m-checkbox--brand">
                        <input <?php if(!empty($info->recommend) &&  $info->recommend=='1'){ echo "checked";}?> type="checkbox" name="recommend" valus="1"> รายการแนะนำ
                        <span></span>
                        <input type="hidden" name="approve" value="<?php echo isset($info->approve) ? $info->approve : 0 ?>">
                    </label>
                </div>
            </div>
            <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="file">โปรไฟล์ <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input id="file" name="file" type="file" data-preview-file-type="text">
                         <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : '' ?>">
                    </div>
                </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >ชื่อ <span class="text-danger"> *</span></label>
                <div class="col-sm-7">
                    <input value="" type="text" id="input-fname" class="form-control" name="fname" required>
                </div>
            </div>  

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >นามสกุล <span class="text-danger"> *</span></label>
                <div class="col-sm-7">
                    <input value="" type="text" id="input-lname" class="form-control" name="lname" required>
                </div>
            </div> 
           
            
           <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >อีเมล <span class="text-danger"> *</span></label>
                <div class="col-sm-7">
                    <input value="" type="text" id="input-username" class="form-control" name="username" required>
                </div>
            </div>  
           <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >เบอร์โทร </label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info->phone) ? $info->phone : NULL ?>" type="text" id="input-phone" class="form-control" name="phone" >
                </div>
            </div>  
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label">รหัสผ่าน <span class="text-danger"> *</span></label>
                <div class="col-sm-7">
                    <input value="" type="password" id="input-password" class="form-control" name="password" required>
                </div>
            </div>
            
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label">ยืนยันรหัสผ่าน <span class="text-danger"> *</span></label>
                <div class="col-sm-7">
                    <input value="" type="password" id="input-repassword" class="form-control" name="rePassword" required>
                </div>
            </div>  

            

        </div>

        <div class="m-portlet__foot m-portlet__foot--fit">

            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button type="submit" class="btn btn-success">บันทึก</button>
                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                    </div>
                </div>

            </div>
        </div>
        <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
        <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->user_id) ? encode_id($info->user_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>



</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?php if(isset($info->file)){ echo $this->config->item('root_url').$info->file; }else{ echo $this->config->item('root_url').'/images/user.png'; } ?>';
    var file_id         = '<?=$info->user_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->user_id;?>';
    
</script>
