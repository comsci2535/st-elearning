<?php 
class Notification_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function noti_all(){

		$query = $this->db
					->where('a.type', 0)
					->or_where('a.user_id', $this->session->users['UID'])
					->get('notification a');
		return $query;         
	}

	public function noti_check($id=null){

		$query = $this->db
					->where('a.noti_id', $id)
					->get('notification_read a');
		return $query;         
	}

	public function read_noti($id) {
		$data = array(
			'user_id' => $this->session->users['UID'],
			'noti_id' => $id,
            'date' => date('Y-m-d H:i:s'),
		);
        $this->db->insert('notification_read', $data);
        return $this->db->insert_id();
	}
	
	public function noti_save($data) {
        $this->db->insert('notification', $data);
        return $this->db->insert_id();
    }

}