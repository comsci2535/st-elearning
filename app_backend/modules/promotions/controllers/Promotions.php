<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Promotions extends MX_Controller {

    private $_title = "จัดการข้อมูลโปรโมชั่น";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับโปรโมชั่น";
    private $_grpContent = "promotions";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
     
        $this->load->model("promotions_m");
        $this->load->model("courses/courses_m");

    }
    
    public function index($course_id) {
        $this->load->module('template');
        
        
        $action[1][] = action_refresh(site_url("{$this->router->class}/index/{$course_id}"));
        //$action[1][] = action_filter();
        $action[1][] = action_add(site_url("{$this->router->class}/create/{$course_id}"));
       
        $action[3][] = action_trash_multi("{$this->router->class}/destroy");
        $action[3][] = action_trash_view(site_url("{$this->router->class}/trash/{$course_id}"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        

        $input['course_id'] = $course_id;
        $course = $this->courses_m->get_rows($input)->row();
        
        $data['course_id'] = $course_id;

        // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("courses"));
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}/index/{$course_id}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['recycle'] = 0;
        $info = $this->promotions_m->get_rows($input);
        $infoCount = $this->promotions_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $dateRang = "ไม่กำหนดช่วงเวลา";
            if($rs->startDate!="0000-00-00" && $rs->endDate!="0000-00-00" ){
                $dateRang = date('d-m-Y', strtotime($rs->startDate)).' ถึง '.date('d-m-Y', strtotime($rs->endDate));
            }
            

            $id = encode_id($rs->promotion_id);
            $action = array();
            $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$input['course_id']}/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $dateRang;
            $column[$key]['active'] = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create($course_id) {
        $this->load->module('template');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/storage");
        
        // breadcrumb
        $input['course_id'] = $course_id;
        $course = $this->courses_m->get_rows($input)->row();
        $data['course_id'] = $course_id;
        $data['courseTitle'] = $course->title;
        $data['price'] = number_format($course->price);

        // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("courses"));
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}/index/{$course_id}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("{$this->router->class}/create/{$course_id}"));

        $input_u['exclude'] = $course_id;
        $input_u['active'] = 1;
        $input_u['recycle'] = 0;
        $courseExclude = $this->courses_m->get_rows($input_u)->result();
        $data['courseExclude']=array();
        foreach ($courseExclude as $key => $value) {
            $data['courseExclude'][$value->course_id]=$value->title;
        }
        
        
        //arr($data['list']);exit();
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage() {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->promotions_m->insert($value);


        if ( $result ) {
            // $value_p = $this->_build_promotion($result,$input['courseExclude']);
            // $this->promotions_m->update_promotion($result,$value_p);
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['course_id']}"));
    }
    
    public function edit($course_id,$id="") {
        $this->load->module('template');
        
        $id = decode_id($id);
        $input['promotion_id'] = $id;
        $info = $this->promotions_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;

        // $data['promotion'] = array();
        // $promotion=$this->promotions_m->get_promotions($id);
        // if($promotion){
        //     foreach ($promotion as $key => $value) {
        //          $data['promotion'][]=$value['course_id'];
        //     }
           
        // }

        $data['dateRang'] ="";
        if($info->startDate!="0000-00-00" && $info->endDate!="0000-00-00"){
            $data['dateRang'] = date('d-m-Y', strtotime($info->startDate)).' ถึง '.date('d-m-Y', strtotime($info->endDate));
        }
        
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/update");


        $input['course_id'] = $course_id;
        $course = $this->courses_m->get_rows($input)->row();
        $data['course_id'] = $course_id;
        $data['courseTitle'] = $course->title;
        $data['price'] = number_format($course->price);

        $input_u['exclude'] = $course_id;
        $input_u['active'] = 1;
        $input_u['recycle'] = 0;
        $courseExclude = $this->courses_m->get_rows($input_u)->result();
        $data['courseExclude']=array();
        foreach ($courseExclude as $key => $value) {
            $data['courseExclude'][$value->course_id]=$value->title;
        }
        

        // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("courses"));
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}/index/{$course_id}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("{$this->router->class}/edit/{$course_id}/{$id}"));
        

        

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
         //arr($input);exit();
        $value = $this->_build_data($input);
        $result = $this->promotions_m->update($id, $value);
        if ( $result ) {
            // $value_p = $this->_build_promotion($id,$input['courseExclude']);
            // $this->promotions_m->update_promotion($id,$value_p);
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['course_id']}"));
    }
    
    private function _build_data($input) {
        
        $value['title'] = $input['title'];
        $value['slug']  = $input['slug'];
        $value['excerpt'] = $input['excerpt'];
        $value['detail'] = $input['detail'];
        $value['type'] = $input['type'];
        $value['course_id'] = $input['course_id'];
        $value['discount'] = $input['discount'];
        $value['startDate'] = $input['startDate'];
        $value['endDate'] = $input['endDate'];
        if($input['type']!='1'){
            $value['discount'] = "";
        }else{
            $value['discount'] = str_replace(",","",$input['discount']);  
        }

        if($input['metaTitle']!=""){
            $value['metaTitle'] = $input['metaTitle'];
        }else{
             $value['metaTitle'] = str_replace(","," ",$input['title']);
        }
        if($input['metaDescription']!=""){
            $value['metaDescription'] = $input['metaDescription'];
        }else{
            $value['metaDescription'] = $input['excerpt'];
        }
        if($input['metaKeyword']!=""){
            $value['metaKeyword'] = $input['metaKeyword'];
        }else{
             $value['metaKeyword'] = str_replace(","," ",$input['title']);
        }

        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }
  
    private function _build_promotion($id, $course_id){
       //arr($course_id);exit();
        foreach ($course_id as $key=>$rs) {        
            $value[] = [
                
                'promotion_id' => $id,
                'course_id' => $rs,
            ];
        }
        return $value;
    }
    
   
    
    
    
    public function trash($course_id) {
        $this->load->module('template');
        
        // toobar
        $action[1][] = action_list_view(site_url("{$this->router->class}/index/{$course_id}"));
        $action[2][] = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
         $input['course_id'] = $course_id;
        $course = $this->courses_m->get_rows($input)->row();
        $data['course_id'] = $course_id;
        $data['courseTitle'] = $course->title;
        $data['price'] = number_format($course->price);
        // breadcrumb
         $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("courses"));
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}/index/{$course_id}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash/{$course_id}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->promotions_m->get_rows($input);
        $infoCount = $this->promotions_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $dateRang = "ไม่กำหนดช่วงเวลา";
            if($rs->startDate!="0000-00-00" && $rs->endDate!="0000-00-00" ){
                $dateRang = date('d-m-Y', strtotime($rs->startDate)).' ถึง '.date('d-m-Y', strtotime($rs->endDate));
            }

            $id = encode_id($rs->promotion_id);
            $action = array();
            $action[1][] = table_restore("{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $dateRang;//$rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->promotions_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->promotions_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            
            $result = false;
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->promotions_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['user_id'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->promotions_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycle_at'] = $dateTime;
                $value['recycle_by'] = $this->session->users['user_id'];
                $result = $this->promotions_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->promotions_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->promotions_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  
    
}
