<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Promotions_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('promotions a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('promotions a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['promotion_id']) ) 
            $this->db->where('a.promotion_id', $param['promotion_id']);

         if ( isset($param['course_id']) ) 
            $this->db->where('a.course_id', $param['course_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }
    
    public function insert($value) {
        $this->db->insert('promotions', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('promotion_id', $id)
                        ->update('promotions', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('promotion_id', $id)
                        ->update('promotions', $value);
        return $query;
    }  



    public function get_content_rows($param) 
    {
        $this->_condition_content($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*,b.name,c.username')
                        ->from('content a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->join('user c', 'a.createBy = c.userId', 'left')
                        ->get();
        return $query;
    }

    public function get_content_count($param) 
    {
        $this->_condition_content($param);
        $query = $this->db
                        ->select('a.*,b.name')
                        ->from('content a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->get();
        return $query->num_rows();
    }

    private function _condition_content($param) 
    {
        //$this->db->where('a.grpContent', $param['grpContent']);
        //$this->db->where_in('a.grpContent',array('new_house','house','rental','land'));
        $this->db->where('a.recycle',0);
        $this->db->where('a.active',1);
        $this->db->where('a.statusBuy',0);


        if (isset($param['grpContent'])) {
            $this->db
                    ->where('a.grpContent', $param['grpContent']);
        }

        $this->db->order_by('a.title', 'DESC');
        //$this->db->order_by('createDate', 'DESC');
        //$this->db->order_by('updateDate', 'DESC');


    }  

     public function update_promotions($id, $value)
    {
       // arr($value);exit();
        $query = $this->db
                        ->where('promotion_id', $id)
                        ->delete('promotions_content');  
        if($value){
              $query = $this->db
                        ->insert_batch('promotions_content', $value);
        
               return $this->db->affected_rows();
        }
      
    }

    public function get_promotions($promotion_id)
    {
        $query = $this->db
                        ->select('*')
                        ->from('promotions_content')
                        ->where('promotion_id', $promotion_id)
                        ->get();
        return $query->result_array();
    }

}
