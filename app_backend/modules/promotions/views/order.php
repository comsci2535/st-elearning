<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">รายการ</h3>
        <div class="box-tools pull-right">
            <?php echo $boxAction; ?>
        </div>
    </div>
    <?php $this->load->view('filter') ?>
    <div class="box-body">
        <?php echo form_open($frmAction, array('class' => 'form-horizontal', 'method' => 'post')) ?>
        <h4 class="block">ข้อมูลทั่วไป</h4>     
        <div class="form-group">

        </div>
        
        <div class="form-group">
            <div class="col-sm-2 text-right">
                <button title="ขยายทั้งหมด" id="expand-all" type="button" class="btn btn-xs btn-flat bg-purple"><i class="fa fa-plus"></i></button>
                <button title="ยุบทั้งหมด" id="collapse-all" type="button" class="btn btn-xs btn-flat bg-purple"><i class="fa fa-minus"></i></button>
            </div>
            <div class="col-sm-7">
                <div class="dd" id="nestable"></div>
            </div>
        </div>  
        
        <div class="form-group">
            <textarea class="form-control hidden" name="order" id="nestable-output"></textarea>  
        </div>
        
    </div>  
    <div class="box-footer">
        <div class="col-sm-7 col-sm-offset-2">
            <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <div id="overlay-box" class="overlay hidden">
        <i class="fa fa-refresh fa-spin"></i>
    </div> 
    
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="departmentType" id="input-catetory-type" value="<?php echo isset($departmentType) ? $departmentType : NULL ?>">
    <?php echo form_close() ?>
</div>
<script>
    var category = <?php echo json_encode($treeData) ?>;
    var options = {'json': category}
    var departmentType = "<?php echo $departmentType; ?>";
</script>
