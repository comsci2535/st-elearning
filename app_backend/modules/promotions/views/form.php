<style type="text/css">
    .hidden {
    display: none!important;
}
</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
       <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                    
             

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คอร์สเรียน</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($courseTitle) ? $courseTitle : NULL ?>" type="text" id="input-title" class="form-control m-input " name="title" disabled>
                        <input class="form-control" name="course_id" id="course_id" type="hidden" value="<?php echo $course_id;?>">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">ราคาปกติ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($price) ? $price : NULL ?>" type="text" id="input-title" class="form-control m-input " name="price" disabled>
                      
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">ชื่อโปรโมชั่น</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="title" class="form-control m-input " name="title" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">Slug</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug"  required>
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                </div>
<!-- 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="excerpt">คำโปรย</label>
                    <div class="col-sm-7">
                        
                    </div>
                </div>  --> 

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">ประเภท</label>
                    <div class="col-sm-7">
                        <label class="icheck-inline"><input id="display1"  type="radio" name="type" value="1" class="icheck" <?php if(isset($info->type) && $info->type=="1"){ echo "checked"; }?> <?php if(!isset($info->type)){ echo "checked"; }?> /> ปกติ</label>
                        <label class="icheck-inline"><input id="display2" type="radio" name="type" value="2" class="icheck" <?php if(isset($info->type) && $info->type=="2"){ echo "checked"; }?>/> กำหนดราคาเอง</label>
                       
                    </div>
                </div> 
                <div class="form-group m-form__group row" id="dc">
                    <label class="col-sm-2 col-form-label">ราคาโปรโมชั่น</label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->discount) ? number_format($info->discount) : NULL ?>" type="text" id="input-discount" class="amount form-control m-input " name="discount" >
                        <div id="alert_e" style="color: red;"></div>
                    </div>
                </div> 

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">ช่วงเวลาโปรโมชั่น</label>
                    <div class="col-sm-3">
                            <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>" />
                            <input type="hidden" name="startDate" value="<?php echo isset($info->startDate) ? $info->startDate : NULL ?>" />
                            <input type="hidden" name="endDate" value="<?php echo isset($info->endDate) ? $info->endDate : NULL ?>" />
                    </div>
                </div>     

                <div class="form-group m-form__group row hidden">
                    <label class="col-sm-2 col-form-label" for="descript">รายละเอียด</label>
                    <div class="col-sm-7">
                       
                    </div>
                </div>  

                

                <!-- <div class="form-group m-form__group row">
                    <div class="col-sm-7 col-md-offset-2">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab2_0" data-toggle="tab" aria-expanded="true"><i class="fa fa-list-ul"></i> คอร์สเรียนที่แถม</a></li>
                                
                            </ul>
                            <div class="tab-content" style="">
                                <div class="tab-pane active" id="tab2_0">
                                    <div class="row" id="cover-image" style="margin-bottom: 15px; padding:0px 15px">
                                        <div class="col-sm-12">
                                            <input value="" type="text" id="filter-category" class="form-control" placeholder="กรองคอร์ส">
                                            <ul class="filter-category" >
                                            
                                                        <?php foreach ($courseExclude as $key_ => $rs_) { ?>
                                                            <li><input type="checkbox" class="icheck" name="courseExclude[]" value="<?php echo $key_;?>" <?php if (!empty($promotion) && in_array($key_, $promotion) ) { ?> checked <?php } ?>/>  <?php echo $rs_;?></li>
                                                        <?php } ?>
                                                        


                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div> -->

               
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label"> <h4>ข้อมูล SEO</h4></label>
                    <div class="col-sm-7 icheck-inline">
                      
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อเรื่อง (Title)</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="" class="form-control m-input " name="metaTitle">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำอธิบาย (Description)</label>
                    <div class="col-sm-7">
                        <textarea class="form-control" rows="5"  class="form-control m-input " name="metaDescription"><?php echo isset($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
                    </div>
                </div>   
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำหลัก (Keyword)</label>
                    <div class="col-sm-7">
                        <textarea class="form-control" rows="3"  class="form-control m-input " name="metaKeyword"><?php echo isset($info->metaKeyword) ? $info->metaKeyword : NULL ?></textarea>
                    </div>
                </div>          
                
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
            <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
            <input type="hidden" class="form-control" name="db" id="db" value="promotions">
            <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->promotion_id) ? encode_id($info->promotion_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>



