


<input class="form-control" name="course_id" id="course_id" type="hidden" value="<?php echo $course_id;?>">

<div class="col-md-12 ">
    <div class="m-portlet m-portlet--mobile box">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <?php echo $boxAction; ?>
            </div>
        </div>
        
        <div class="m-portlet__body">

            <form  role="form">
                <table id="data-list" class="table table-hover dataTable" width="100%">
                    <thead>
                        <tr>
                            <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                            <th>รายการ</th>
                            <th>ช่วงเวลาโปรโมชั่น</th>
                            <th>ขยะ</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </form>
            <div id="overlay-box" class="overlay">
                <i class="fa fa-circle-o-notch fa-spin"></i>
            </div>   
        </div>
    </div>
</div>
