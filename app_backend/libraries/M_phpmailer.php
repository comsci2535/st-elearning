<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once APPPATH.'/third_party/phpmailer/PHPMailerAutoload.php';

class M_phpmailer {

    public function __construct()
    {
        $this->mail = new PHPMailer;
    }
}
