"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true,
            excerpt: {
                required:true
            },
            url: {
                required:true
            }
        }
    });
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
