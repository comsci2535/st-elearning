"use strict";

$(document).ready(function () {
    $('.form-horizontal').validate();
    
    $('input.check-all').on('ifToggled', function(){
        var row = $(this).closest('tr')
        if ( $(this).prop('checked') ) {
            $(row).find('input:not(.check-all)').iCheck('check')
        } else {
            $(row).find('input:not(.check-all)').iCheck('uncheck')
        }
    })
    
    $('input.modify').on('ifChecked', function(){
        var row = $(this).closest('tr')
        if ( $(this).prop('checked') ) {
            $(row).find('input.access').iCheck('check')
        }
    })
    
    $('input.access').on('ifUnchecked', function(){
        var row = $(this).closest('tr')
        $(row).find('input.modify').iCheck('uncheck')
        $(row).find('input:not(.check-all)').iCheck('uncheck')
    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
