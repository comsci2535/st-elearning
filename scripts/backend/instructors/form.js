"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            username: {
                remote: {
                    url: "users/check_username",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            },
            rePassword: {equalTo: "#input-password"},
            m_select2_11:true
        },
        messages: {
            username: {remote: 'พบ Email ซ้ำในระบบ'},
            rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
        }
    });

    $('.frm-edit').validate({
        rules: {
            username: {
                remote: {
                    url: "users/check_username",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            }
        },
        messages: {
            username: {remote: 'พบ Email ซ้ำในระบบ'}
        }
    });

    $('.frm-change-password').validate({
        rules: {
            rePassword: {equalTo: "#input-password"}
        },
        messages: {
            rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
        }
    });

    if(method == 'create'){
        $('#file').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: true,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Picture",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop picture here …",
        });
    }

    if(method == 'edit'){
        $('#file').fileinput({
            initialPreview: [file_image],
            initialPreviewAsData: false,
            initialPreviewConfig: [
            { downloadUrl: file_image  ,extra: {id: file_id, csrfToken: get_cookie('csrfCookie')} },
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 0,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize:1024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Select Image",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop icon here …",
        });
    }

    $('#m_select2_11').select2({
        placeholder: "Add a tag",
        tags: true
    });
  
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
// $(document).ready(function($) {

//     $("#skill").select2({
//         tags : arrJS
//         ,tokenSeparators  : [",", " "]
//     })
//     .select2("val", [arrEdit])
//     .on('change', function(event){
//         var tagsIdVal = $('input[name="skill"]').val(event.val.toString());
//      });
  
// });