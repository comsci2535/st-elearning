"use strict";
$(document).ready(function () {
    
    dataList.DataTable({
        //language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie')},
        },
        order: [[1, "asc"]],
        pageLength: 10,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "title", className: "", orderable: true},
            {data: "excerpt", className: "", orderable: true},
            {data: "created_at", width: "100px", className: "", orderable: true},
            {data: "updated_at", width: "100px", className: "", orderable: true},
            {data: "active", width: "50px", className: "text-center", orderable: false},
            {data: "action", width: "30px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
             $('.statusPicker').selectpicker({width: '100%' });
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    });

    $(document).on('change','.statusPicker', function(){
        if(jQuery.type($(this).data('method')) != "undefined"){
            var style
            $('#overlay-box').removeClass('hidden');

            switch ($(this).val())
            { 
                case "0" : style = 'btn-warning custom-select-sm'; break; 
                case "1" : style = 'btn-success custom-select-sm'; break;
                case "2" : style = 'btn-danger custom-select-sm'; break; 
                default : style = 'btn-default custom-select-sm'; 
            }        
            $(this).selectpicker('setStyle', 'btn-danger btn-warning btn-success btn-info btn-default', 'remove')
            $(this).selectpicker('setStyle', style)
            $(this).selectpicker('setStyle', 'btn-flat', 'add')

            arrayId.push($(this).data('id'))
            var url = $(this).data('method');
            var approve = $(this).val();
            bootbox.dialog({
                message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
                className : "my_width",
                buttons: {
                    "success" :{
                        "label" : "<i class='fa fa-check'></i> ตกลง",
                        "className" : "btn-sm btn-success",
                        "callback": function() {
                            $("#modal_formRemove").modal({
                                "backdrop"  : "static",
                                "keyboard"  : true,
                                "show"      : true
                            });

                            $.post(url, {id: arrayId, approve: approve, csrfToken: csrfToken})
                                .done(function (data) {
                                    $('#overlay-box').addClass('hidden');
                                    if (data.success === true) {
                                        toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                                    } else if (data.success === false) {
                                        toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                                        errorToggle = true
                                        bootstrapToggle.bootstrapToggle('toggle')
                                        errorToggle = false
                                    }

                                    arrayId = []
                                })
                                .fail(function () {
                                    $('#overlay-box').addClass('hidden');
                                    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
                                    errorToggle = true
                                    bootstrapToggle.bootstrapToggle('toggle')
                                    errorToggle = false
                                    arrayId = []
                                })

                        },
                    "cancel" :{
                        "label" : "<i class='fa fa-times'></i> ยกเลิก",
                        "className" : "btn-sm btn-white",
                        }
                    }
                }
            });

        }
    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
