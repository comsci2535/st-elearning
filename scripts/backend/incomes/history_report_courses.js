"use strict";
$(document).ready(function () {
    var user_id = $('#text-user-id').val();

    load_data_table();

    function load_data_table(){
        var table = $('#data-list').DataTable();
            table.destroy();
            table = $('#data-list').DataTable({
            serverSide: true,
            // responsive: true,
            ajax: {
                url: controller+"/ajax_data_report_history",
                type: 'POST',
                data: {
                     csrfToken : get_cookie('csrfCookie')
                    ,user_id   : user_id
                },
            },
            order: [[1, "DESC"]],
            pageLength: 10,
            columns: [
                {data: "no", width: "5%", className: "text-center", orderable: false},
                {data: "cycle", width: "10%", className: "text-center", orderable: true},
                {data: "title" , width: "30%", className: "", orderable: true},
                {data: "stuQty" , width: "10%", className: "text-center", orderable: false},
                {data: "cycle_date" , width: "15%", className: "text-center", orderable: false},
                {data: "active", width: "15%", className: "text-center", orderable: false},
            ]
        }).on('draw', function () {
            $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
            $('.tb-check-single').iCheck(iCheckboxOpt);
        }).on('processing', function(e, settings, processing) {
            if ( processing ) {
                $('#overlay-box').removeClass('hidden');
            } else {
                $('#overlay-box').addClass('hidden');
            }
        })
    }

    $(document).on('change','.statusPicker', function(){
        if(jQuery.type($(this).data('method')) != "undefined"){
            var style
            $('#overlay-box').removeClass('hidden');

            switch ($(this).val())
            { 
                case "0" : style = 'btn-warning custom-select-sm'; break; 
                case "1" : style = 'btn-success custom-select-sm'; break;
                case "2" : style = 'btn-danger custom-select-sm'; break; 
                default : style = 'btn-default custom-select-sm'; 
            }        
            $(this).selectpicker('setStyle', 'btn-danger btn-warning btn-success btn-info btn-default', 'remove')
            $(this).selectpicker('setStyle', style)
            $(this).selectpicker('setStyle', 'btn-flat', 'add')

            arrayId.push($(this).data('id'))
            var url = $(this).data('method');
            var approve = $(this).val();
            bootbox.dialog({
                message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
                className : "my_width",
                buttons: {
                    "success" :{
                        "label" : "<i class='fa fa-check'></i> ตกลง",
                        "className" : "btn-sm btn-success",
                        "callback": function() {
                            $("#modal_formRemove").modal({
                                "backdrop"  : "static",
                                "keyboard"  : true,
                                "show"      : true
                            });

                            $.post(url, {id: arrayId, approve: approve, type : 'approve', csrfToken: csrfToken})
                                .done(function (data) {
                                    $('#overlay-box').addClass('hidden');
                                    if (data.success === true) {
                                        toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                                        load_data_table();
                                    } else if (data.success === false) {
                                        toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                                        errorToggle = true
                                        bootstrapToggle.bootstrapToggle('toggle')
                                        errorToggle = false
                                    }

                                    arrayId = []
                                })
                                .fail(function () {
                                    $('#overlay-box').addClass('hidden');
                                    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
                                    errorToggle = true
                                    bootstrapToggle.bootstrapToggle('toggle')
                                    errorToggle = false
                                    arrayId = []
                                })

                        },
                    "cancel" :{
                        "label" : "<i class='fa fa-times'></i> ยกเลิก",
                        "className" : "btn-sm btn-white",
                        }
                    }
                }
            });

        }
    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
