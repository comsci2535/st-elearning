"use strict";

$(document).ready(function () {

    $('.amount-coupon').keyup( function() {
        var type = $('input[name=type]:checked').val();
         if(!type){        
            $('#alert_e').html('กรุณาเลือกประเภทส่วนลด');
            $('#input-discount').val('');
            return false;
        }else{
            $('#alert_e').html('');
            $(this ).val( formatAmount( $( this ).val() ) );
        }
          
    });
    
    $('.frm-create').validate({
        rules: {
            couponCode: {
                remote: {
                    url: "coupons/check_coupon_code",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            },
            title: true,
            slug:true,
            managearticle_id: {
                required:true
            }
        },
        messages: {
            couponCode: {remote: 'พบ รหัสคูปอง ซ้ำในระบบ'},
            
        }
    });

    // form validate editor
    $('form').each(function () {
        if ($(this).data('validator'))
            $(this).data('validator').settings.ignore = ".note-editor *";
    });

    var isCourse = document.getElementById("isCourse");
    var isCourseid = isCourse.options[isCourse.selectedIndex].value;
    if(isCourseid=='1'){
        $('#isCourseNum-d').show(100);    
    }else{
        $('#isCourseNum-d').hide(100);   
    } 

    var isMember = document.getElementById("isMember");
    var isMemberid = isMember.options[isMember.selectedIndex].value;
    if(isMemberid=='1'){
        $('#isMemberNum-d').show(100);    
    }else{
        $('#isMemberNum-d').hide(100);   
    } 
    

    $('#isCourse').change(function(obj) {
        var e = document.getElementById("isCourse");
        var id = e.options[e.selectedIndex].value;
        if(id=='1'){
            $('#isCourseNum-d').show(100);    
        }else{
            $('#isCourseNum-d').hide(100);   
        }        
    });
    $('#isMember').change(function(obj) {
        var e = document.getElementById("isMember");
        var id = e.options[e.selectedIndex].value;
        if(id=='1'){
            $('#isMemberNum-d').show(100);    
        }else{
            $('#isMemberNum-d').hide(100);   
        } 
              
    });
    
    $('input[name="dateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $('input[name="startDate"]').val(picker.startDate.format('YYYY-MM-DD'))
        $('input[name="endDate"]').val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $('input[name="startDate"]').val('')
        $('input[name="endDate"]').val('')        
    })  
     //$('#courses_select').hide(100);

     $('input[name="couponType"]').on('ifChecked', function(event){
       //  alert($(this).val())
        if($(this).val()=='0'){
           
            $('#courses_select').hide(100);
             $('#courses_select2').show(100);
        }else{
             $('#courses_select2').hide(100);
            $('#courses_select').show(100);
        }
    });
    couponType();
})

function couponType(){

     var couponType = $('input[name=couponType]:checked').val();
    // //alert($('input[name="couponType"]').val());
    if(couponType==0){
        $('#courses_select').hide(100);
        $('#courses_select2').show(100);
    }else{
         $('#courses_select2').hide(100);
         $('#courses_select').show(100);
    }

  
    
}

const regex = /[A-Za-z1-9]/;
function validate(e) {
  const chars = e.target.value.split('');
  const char = chars.pop();
  if (!regex.test(char)) {
    e.target.value = chars.join('');
    //console.log(`${char} is not a valid character.`);
  }
}
document.querySelector('#input-couponCode').addEventListener('input', validate);

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
