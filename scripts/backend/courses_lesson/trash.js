"use strict";
$(document).ready(function () {

     var course_id = $('#course_id').val();
    
    dataList.DataTable({
       // language: {url: siteUrl + "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_trash",
            type: 'POST',
            data: {'csrfToken': get_cookie('csrfCookie'),course_id:course_id},
        },
        order: [[1, "asc"]],
        pageLength: 10,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "title", className: "", orderable: true},
            {data: "excerpt", className: "", orderable: true},
            {data: "recycle_at", className: "", orderable: true},
            {data: "action", width: "100px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
