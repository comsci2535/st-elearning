

"use strict";


$(document).ready(function () {
   $('.form-horizontal').validate();

     var parent_id = $('select[name=parent_id]').val();
    if(parent_id!=0){
        $('#c-content').show(100);    
    }else{
        $('#c-content').hide(100);   
    } 

    $('select[name=parent_id]').change(function(obj) {
        //alert(1);
        var id = $('select[name=parent_id]').val();
       // var id = e.options[e.selectedIndex].value;
         if(id!=0){
            $('#c-content').show(100);    
           // var file_required=true;
        }else{
            $('#c-content').hide(100); 
           // var file_required=false;  
        }      
    });

    $('.select2').change(function () {
        $('.form-horizontal').validate().element('.select2')
    })

    if($('input[name=fileTypeUploadType]:checked').val()==1)
    {
        $('#f-file').show(100);
        $('#f-url').hide(100);
       
    } else if($('input[name=fileTypeUploadType]:checked').val()==2){
        $('#f-file').hide(100);
        $('#f-url').show(100);
       
    }

    /* fileinput */
    if(method == 'create'){
        $('#file').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: false,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            allowedFileExtensions: [
                "jpg",
                "png", 
                "gif",
                "jpeg",
                "pdf",
                "bmp",
                "doc",
                "docx",
                "txt",
                "xls",
                "xlsx",
                "csv",
                "pptx",
                "zip",
                "7z",
            ],
            browseLabel: "Picture",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop picture here …",
        });
    }
    
    if(method == 'edit'){
        
        $('#file').fileinput({
            initialPreview: [file_image],
            initialPreviewConfig: [
                { 
                    filename: file_image,
                    downloadUrl: file_image,
                    extra: {
                        id: file_id, csrfToken: get_cookie('csrfCookie')
                    },
                   
                },
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
            previewFileIconSettings: { // configure your icon file extensions
                'doc': '<i class="fas fa-file-word fa-2x text-primary"></i>',
                'xls': '<i class="fas fa-file-excel fa-2x text-success"></i>',
                'ppt': '<i class="fas fa-file-powerpoint fa-2x text-danger"></i>',
                'pdf': '<i class="fas fa-file-pdf fa-2x text-danger"></i>',
                'zip': '<i class="fas fa-file-archive fa-2x text-muted"></i>',
                'htm': '<i class="fas fa-file-code fa-2x text-info"></i>',
                'txt': '<i class="fas fa-file-alt fa-2x text-info"></i>',
                'mov': '<i class="fas fa-file-video fa-2x text-warning"></i>',
                'mp3': '<i class="fas fa-file-audio fa-2x text-warning"></i>',
                // note for these file types below no extension determination logic 
                // has been configured (the keys itself will be used as extensions)
                'jpg': '<i class="fas fa-file-image fa-2x text-danger"></i>', 
                'gif': '<i class="fas fa-file-image fa-2x text-muted"></i>', 
                'png': '<i class="fas fa-file-image fa-2x text-primary"></i>'
            },
            previewFileExtSettings: { // configure the logic for determining icon file extensions
                'doc': function(ext) {
                    return ext.match(/(doc|docx)$/i);
                },
                'xls': function(ext) {
                    return ext.match(/(xls|xlsx)$/i);
                },
                'ppt': function(ext) {
                    return ext.match(/(ppt|pptx)$/i);
                },
                'zip': function(ext) {
                    return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                },
                'htm': function(ext) {
                    return ext.match(/(htm|html)$/i);
                },
                'txt': function(ext) {
                    return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
                },
                'mov': function(ext) {
                    return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
                },
                'mp3': function(ext) {
                    return ext.match(/(mp3|wav)$/i);
                }
            },
            allowedFileExtensions: [
                "jpg",
                "png", 
                "gif",
                "jpeg",
                "pdf",
                "bmp",
                "doc",
                "docx",
                "txt",
                "xls",
                "xlsx",
                "csv",
                "pptx",
                "zip",
                "7z",
            ],
            browseLabel: "Select Image",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop icon here …",
        });
    }

})

$("[name='fileTypeUpload']").on('ifChecked', function () { 
    displayUploadType($(this).val());
});

function displayUploadType(value){
    if(value==1){
        $('#f-file').show(100);
        $('#f-url').hide(100);
    }else if(value==2){
        $('#f-file').hide(100);
        $('#f-url').show(100);
    } 
}

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
