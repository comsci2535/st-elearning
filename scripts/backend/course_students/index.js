"use strict";
$(document).ready(function () {
    var course_id = $('#course_id').val();
    dataList.DataTable({
        //language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {
                 csrfToken  : get_cookie('csrfCookie')
                ,course_id  : course_id
            },
        },
        order: [[1, "DESC"]],
        pageLength: 10,
        columns: [
            {data: "no", width: "5%", className: "text-center", orderable: false},
            {data: "updated_at", width: "20%", className: "", orderable: true},
            {data: "fullname", width: "30%", className: "", orderable: true},
            {data: "created_at", width: "20%", className: "", orderable: true},
            {data: "status", width: "30%", className: "text-center", orderable: false},
            {data: "action", width: "30%", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
