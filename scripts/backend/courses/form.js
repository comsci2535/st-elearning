"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true,
            'course_categorie_sub_id[]':'required',
        }
    });
    
    // create Daterange 
    $('input[name="dateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $(this).nextAll().eq(0).val(picker.startDate.format('YYYY-MM-DD'))
        $(this).nextAll().eq(1).val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $(this).nextAll().eq(0).val('')
        $(this).nextAll().eq(1).val('')        
    }) 
    
    /* -- fileinput */
    if(method == 'create'){
        $('#file').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: true,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Picture",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop picture here …",
        });
    }
    
    if(method == 'edit'){
        $('#file').fileinput({
            initialPreview: [file_image],
            initialPreviewAsData: false,
            initialPreviewConfig: [
            { downloadUrl: file_image  ,extra: {id: file_id, csrfToken: get_cookie('csrfCookie')} },
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 0,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize:1024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Select Image",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop icon here …",
        });
    }

    $('#instructor_id').select2({
        placeholder: "Select a state",
    });

    $('#pause-button').click(function(){
        pausePlayer($(this).attr('data-id'));
    });

    function pausePlayer(id){
        var handstickPlayerC = new Vimeo.Player('handstick-c-'+id);
        handstickPlayerC.pause();
        $('#'+id).modal('hide')
    }

    //เมื่อติกถูก แสดงวีดีโอตัวอย่าง ก็จะบังค้บให้ใส่(ลิงก์วิดีโอแนะนำคอร์ส)
    $('#checkview').click(function(){
        if($(this).is(':checked')){
            $('#recommendVideo').prop('required',true);
            $('#star-red').removeClass('d-none');
        }else{
            $('#recommendVideo').prop('required',false);
            $('#star-red').addClass('d-none');
        }
    });

    

    $('input[name="publish"]').on('ifChecked', function(event){
       //  alert($(this).val())
        if($(this).val()=='private'){
            $('#dateRang1').show(100);
            
        }else{
           $('#dateRang1').hide(100);
        }
    });
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
