<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/** load the CI class for Modular Extensions **/
require dirname(__FILE__).'/Base.php';

/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library replaces the CodeIgniter Controller class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Controller.php
 *
 * @copyright	Copyright (c) 2015 Wiredesignz
 * @version 	5.5
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
class MX_Controller 
{
	public $autoload = array();
	
	public function __construct() 
	{
		$class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
		log_message('debug', $class." MX_Controller Initialized");
		Modules::$registry[strtolower($class)] = $this;	
		
		/* copy a loader instance and initialize */
		$this->load = clone load_class('Loader');
		$this->load->initialize($this);	
		$this->check_login_page();	
		
		/* autoload module items */
		$this->load->_autoloader($this->autoload);

        
        //arr($code['codeId']);exit();
        
       
        //arr($this->session->code_data['codeId']);exit();
        $this->_set_config();
	}

     private function _set_config()
    {
       
        $query=$this->db
                ->select('a.*')
                ->from('config a')
                ->where('a.type', 'general')
                ->get()->result_array();
       
        foreach ($query as $rs) $this->config->set_item($rs['variable'], $rs['value']);
        
        return true;
        
    }
	
	public function __get($class) 
	{
		return CI::$APP->$class;
    }
    
    public function check_login_page() {
        $this->login_page->check_login();
    }

	public function pagin($uri, $total, $segment, $per_page = 30) {

        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
        //$config['num_links'] = $num_links;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
		$config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item "><a href="javascript:void(0);" class="page-link active">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_link'] = '>>';
        $config['prev_link'] = '<<';
        $config['next_tag_open'] = '<li class="pg-next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pg-prev">';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }
}