<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Exercise_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('exercise a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('exercise a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {
        
        $this->db->where('a.recycle',0);
        $this->db->where('a.active',1);


        // $this->db->where("( (CONVERT(datetime,a.startDate) IS NULL AND CONVERT(datetime,a.endDate) IS NULL) OR (CONVERT(datetime,a.startDate) <= convert(datetime,'".date('Y-m-d')."') AND CONVERT(datetime,a.endDate) >= convert(datetime,'".date('Y-m-d')."')) )");


       
        if (isset($param['exercise_id'])){ 
           if ($param['exercise_id']!=""){ 
                $this->db->where('a.exercise_id', $param['exercise_id']);
            }
        }
       
        
    
        $this->db->where('a.course_lesson_id', $param['course_lesson_id']);
             
        

        $this->db->order_by('a.active', 'DESC');
        $this->db->order_by('a.created_at', 'DESC');
                  

    }

    
    
    public function get_course_lesson_by_id($id)
    {
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title as course_lesson_name , b.slug as course_lesson_slug , b.price as price')
                        ->from('exercise a')   
                        ->join('courses_lesson b', "a.course_lesson_id = b.course_lesson_id", 'left')           
                        ->where('a.exercise_id', $id)
                        ->get()
                        ->row_array();
                        
                        foreach ($query as $key => $value) {
                            $query['amount']=$this->db
                                              ->select('*')
                                              ->from('quiz_exercise')
                                              ->where('exercise_id', $id)
                                              ->where('user_id', $this->session->users['UID'])
                                              ->get()
                                              ->num_rows();
                        }
        return $query;
    }
    
    public function get_quiz_by_id($id)
    {
        $query = $this->db
                        ->from('quiz_exercise a')
                        ->where('a.quizId', $id)
                        ->where('a.user_id', $this->session->users['UID'])
                        ->get()
                        ->row_array();
        return $query;
    }
    
    public function get_quiz_detail_by_id($id)
    {
        $query = $this->db
                        ->from('quiz_exercise_detail a')
                        ->where('a.quizId', $id)
                        ->get()
                        ->result_array();
        return $query;
    }    
    
     
    public function get_question($param)
    {
       

       $query = $this->db
                        ->select('a.exercise_topic_id, a.exercise_topic_name, a.exercise_type_id, a.exercisePath, a.exerciseFile,a.active')
                        ->select('b.exercise_answer_id, b.exercise_answer_name, b.point')
                        ->from('exercise_topic a')
                        ->join('exercise_answer b', 'a.exercise_topic_id = b.exercise_topic_id', 'left')
                        ->where('a.active', 1)
                        ->where('a.exercise_id', $param['exercise_id'])
                        ->order_by('a.exercise_topic_id', 'ASC')
                        ->get()
                        ->result_array();
     
        return $query;
        
       
    }
    
    
    public function objective_score($answer)
    {
        $query = $this->db
                        ->select("SUM(point) point")
                        ->from('exercise_answer')
                        ->where_in('exercise_answer_id', $answer)
                        ->get()
                        ->row_array();
        return $query['point'];
    }
    
    public function all_child_category($table="", $categoryId="", $parentField="", $parentId=0 )
    {
        $sql = "SELECT GROUP_CONCAT(lv SEPARATOR ',') AS allChild FROM (
            SELECT @pv:=(SELECT GROUP_CONCAT({$categoryId} SEPARATOR ',') FROM {$table} WHERE
            FIND_IN_SET({$parentField}, @pv)) AS lv FROM {$table}
            JOIN (SELECT @pv:=?)tmp WHERE {$categoryId} IN (@pv)) a;";
        $query = $this->db
                        ->query($sql, array($parentId))
                        ->row_array();
        if ($query['allChild'] == "") {
           $category = $parentId;
        } else {
           $category = $query['allChild'].','.$parentId; 
        }
        $category = explode(',', $category);
        return $category;
    }    
    
    public function all_parent_category($table="", $categoryId="", $parentField="", $currentId=0 )
    {
        $sql = "SELECT GROUP_CONCAT(T2.categoryId SEPARATOR ',') AS allParent
                FROM (
                    SELECT
                        @r AS _id,
                        (SELECT @r := {$parentField} FROM {$table} WHERE {$categoryId} = _id) AS parent_id,
                        @l := @l + 1 AS lvl
                    FROM
                        (SELECT @r := ?, @l := 0) vars,
                        {$table} h
                    WHERE @r <> 0) T1
                JOIN {$table} T2
                ON T1._id = T2.{$categoryId}
                ORDER BY T1.lvl DESC";
        $query = $this->db
                        ->query($sql, array($currentId))
                        ->row_array();
        $category = explode(',', $query['allParent']);
        return $category;
    }
}
