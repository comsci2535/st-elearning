<div class="container">
    <div class="background-image">
        <div class="row">
            <div class="offset-sm-2 col-sm-8">
                <div class="my-5">
                    <div class="title Bold">
                        <h1 class="">ติดต่อเรา</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet__body">
                    <div class="m-pricing-table-3 m-pricing-table-3--fixed">
                        <div class="m-pricing-table-3__items">
                            <div class="row m-row--no-padding">
                                <div class="col-lg-8">
                                   <!--  <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"> -->
                                        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'id'=>'frm-save' , 'method' => 'post')) ?>
                                        <div class="m-portlet__body">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6">
                                                    <label class="Bold">ชื่อ :</label>
                                                    <input name="fname"  id="fname" type="text" class="form-control m-input" placeholder="">
                                                    <div class="alert_fname"></div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="Bold">นามสกุล :</label>
                                                    <input name="lname" id="lname" type="text" class="form-control m-input" placeholder="">
                                                    <div class="alert_lname"></div>
                                                </div>
                                                
                                            </div>

                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6">
                                                    <label class="Bold">email :</label>
                                                    <input name="email" id="email" type="email" class="form-control m-input" placeholder="">
                                                     <div class="alert_email"></div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="Bold">เบอร์โทร :</label>
                                                    <input name="phone" id="phone" type="text" class="form-control m-input" placeholder="">

                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <label class="Bold">หัวข้อ :</label>
                                                    <input type="text" name="title" id="title" class="form-control m-input" placeholder="">
                                                     <div class="alert_title"></div>
                                                </div>
                                               
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <label class="Bold">ข้อความ :</label>
                                                    <textarea class="form-control m-input m-input--air"
                                                        id="detail" name="detail" rows="3"></textarea>
                                                        <div class="alert_detail"></div>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <input type="hidden" name="mode" value="create" />
                                                    <div id="html_element" style="margin-bottom: 10px"></div>
                                                    <input type="hidden" name="robot"  class="form-control">
                                                    <div id="form-success-div" class="text-success"></div> 
                                                    <div id="form-error-div" class="text-danger"></div>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <button type="submit" class="btn btn-info"><span id="form-img-div"></span> ส่งข้อความ</button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close() ?>
                                </div>
                                <div class="m-pricing-table-3__item m-pricing-table-3__item--focus m--bg-info col-lg-4 text-cnter">
                                    <h2 class="text-center mt-4 text-white"><b>ผ่านช่องทาง</b></h2>
                                    <div class="cus-space-icon-top">
                                        <?php if(!empty($this->config->item('email'))){ ?>
                                        <a href="mailto:<?=$this->config->item('email'); ?>">
                                            <i class="fa fa-envelope icon-size" data-toggle="m-tooltip" data-placement="right" title="<?=$this->config->item('email'); ?>"></i>
                                        </a>
                                       <?php } ?>
                                       <?php if(!empty($this->config->item('facebook'))){ ?>
                                        <a target="_blanl" href="https://m.me/<?=$this->config->item('facebook'); ?>">
                                            <i class="fab fa-facebook-messenger icon-size" data-toggle="m-tooltip" data-placement="right" title="<?=$this->config->item('facebook'); ?>"></i>
                                        </a>
                                        <?php } ?>
                                         <?php if(!empty($this->config->item('line'))){ ?>
                                       <a href="javascript:void(0)" onclick="window.open('http://line.me/ti/p/~<?=$this->config->item('line'); ?>', '_blank');">
                       
                                            <i class="fab fa-line icon-size" data-toggle="m-tooltip" data-placement="right" title="<?=$this->config->item('line'); ?>"></i>
                                        </a>
                                        <?php } ?>
                                         <?php if(!empty($this->config->item('phone'))){ ?>
                                        <a href="tel:<?=$this->config->item('phone'); ?>">
                                            <i class="fab fa-whatsapp icon-size" data-toggle="m-tooltip" data-placement="right" title="<?=$this->config->item('phone'); ?> ( <?=$this->config->item('time'); ?> )"></i>
                                        </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>