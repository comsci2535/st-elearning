<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->_set_contactus();
		$this->_set_email();
	}

     private function _set_contactus()
    {
       
        $query=$this->db
                ->select('a.*')
                ->from('config a')
                ->where('a.type', 'contactus')
                ->get()->result_array();
       
        foreach ($query as $rs) $this->config->set_item($rs['variable'], $rs['value']);
        
        return true;
        
	}
	
     private function _set_email()
    {
       
        $query=$this->db
                ->select('a.*')
                ->from('config a')
                ->where('a.type', 'mail')
                ->get()->result_array();
       
        foreach ($query as $rs) $this->config->set_item($rs['variable'], $rs['value']);
        
        return true;
        
    }

    private function seo(){
		$title          = 'ST Developer';
		$robots         = 'ST Developer';
		$description    = 'ST Developer';
		$keywords       = 'ST Developer';
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url()."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
		return $meta;
    }
    
    public function index(){
        $data = array(
            'menu'          => 'contactus',
            'seo'           => $this->seo(),
            'header'        => 'header',
            'content'       => 'contactus',
            'footer'        => 'footer',
            'background'    => 'bg',
            'function'      =>  array('custom','contactus'),
		);
	  $data['frmAction'] = site_url("contactus/save");
      $this->load->view('template/body', $data);
        
    }

    public function save()
    {

        $input = $this->input->post();

	        $value = $this->_build_data($input);
	        $result = $this->db->insert('contact_us', $value);
	        if ( $result ) {
		
				$this->load->library('send_email');
				$param_send['subject']		= $input['title'];
				$param_send['detail']		= $input['detail'];
				$param_send['email_send']	= $input['email'];
				$param_send['email_to']		= $this->config->item('SMTPusername');
				$param_send['send_date']	= db_datetime_now();
				$this->send_email->send($param_send);
	        	
	            $resp_msg = array('info_txt'=>"success",'msg'=>'ส่งข้อความสำเร็จ','msg2'=>'กรุณารอสักครู่...');
	                echo json_encode($resp_msg);
	                return false;
	        } else {
	            $resp_msg = array('info_txt'=>"error",'msg'=>'ส่งข้อความไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
	            echo json_encode($resp_msg);
	            return false;
	        }
    }
    private function _build_data($input) {
       
        
        $value['fname'] = $input['fname'];
        $value['lname'] = $input['lname'];
        $value['phone'] = $input['phone'];
        $value['email'] = $input['email'];
        $value['title'] = $input['title'];
        $value['detail'] = $input['detail'];
        $value['created_at'] = db_datetime_now();
        $value['updated_at'] = db_datetime_now();
     
      
        return $value;
    }

}
