<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Social extends MX_Controller {
    
    public $ogUrl;
    public $ogType;
    public $ogTitle;
    public $ogDescription;
    public $ogImage;
    public $ogSiteName;
    
    public function __construct() 
    {
        parent::__construct();
        //$this->load->model('social_m');
    }
    
    
    
    public function set_share($info=array()) 
    {
        if ( isset($info['ogUrl']) ) {
            $this->ogUrl = $info['ogUrl'];
        } else {
            $this->ogUrl = base_url();
        }
        
        if ( isset($info['ogType']) ) { 
            $this->ogType = $info['ogType'];
        } else {
            $this->ogType = 'website';
        }
        
        if ( isset($info['ogTitle']) )  { 
            $this->ogTitle = $info['ogTitle'];
        } else {
           $this->ogTitle = config_item('metaTitle'); 
        }
        
        if ( isset($info['ogDescription']) ) { 
            $this->ogDescription = $info['ogDescription'];
        } else {
            $this->ogDescription = config_item('metaDescription');
        }
        
        if ( isset($info['ogImage']) ) { 
            $this->ogImage = $info['ogImage'];
        } else {
            $this->ogImage = config_item('ogImage');
        }
        
        if ( isset($info['ogSitename']) ) { 
            $this->ogSitename = $info['ogSitename'];
        } else {
            $this->ogSitename = config_item('sitename');
        }
        
        if ( isset($info['twImage']) ) { 
            $this->twImage = $info['twImage'];
        } else {
            $this->twImage = config_item('twImage');
        }        
    }
    
    public function share()
    {
        $this->load->view('share');
    }
    public function share_button()
    {
        $this->load->view('share_button');
    }
}
