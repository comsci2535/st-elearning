<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Track extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->config('front');
    }
	
    public function keyword($param)
    {
        if ($param['keyword']) {
            if ( $param['categoryId'] && $param['publisherId']) {
                $type = "หมวดหมู่และหนังสือพิมพ์";
            } elseif ( $param['categoryId'] && !$param['publisherId'] ) {
                $type = "หนังสือพิมพ์";
            } elseif ( !$param['categoryId'] && $param['publisherId'] ) {
                $type = "หมวดหมู่";
            } else {
                $type = "";
            }
            $value['memberId'] = $this->session->member['memberId'];
            $value['ip'] = $this->input->ip_address();
            $value['searchDate'] = db_datetime_now();
            $value['keyword'] = $param['keyword'];
            $value['type'] = $type;
            $this->db->insert('keyword_tracking', $value);
        } else {
            return FALSE;
        }
        return TRUE;
    }
    
    public function read($info = array())
    {
        if ( $info ) {
            $value['userId'] = $this->session->user['id'];
            $value['ip'] = $this->input->ip_address();
            $value['readDate'] = db_datetime_now();
            $value['newsId'] = $info['id'];
            $value['code'] = $info['code'];
            $this->db->insert('read_tracking', $value);
            $sql = ("update clip_news set viewCount = (viewCount+1) where id={$info['id']}");
            $this->db->query($sql);
        } else {
            return FALSE;
        }
        return TRUE;
    }
    
    public function front($contentId=NULL, $method=NULL, $controller=NULL)
    {
        if ( !config_item('keepTrack') ) return FALSE;
        $value['memberId'] = $this->session->users['UID'];
        $value['ip'] = $this->input->ip_address();
        $value['createDate'] = db_datetime_now();
        $value['controller'] = $controller ? $controller : $this->router->class;
        $value['method'] = $method ? $method : $this->router->method;
        $value['contentId'] = $contentId;
        $value['uri'] = urldecode($this->input->server('REQUEST_URI'));
        $value['sessionId'] = $this->session->session_id;
        $value['type'] = 'front';
        $value['isMobile'] = $this->agent->is_mobile();
        $value['referrer'] = $this->agent->referrer();
        $value['platform'] = $this->agent->platform();
        if ( !empty($value) ) {
            $rs = $this->db->insert('track', $value);
        } else {
            $rs = FALSE;
        }
        return $rs;
    }
    
    public function update_read()
    {
        set_time_limit(0);
        $read = $this->db->get('view_count')->result_array();
        foreach ($read as $rs)
        {
            echo ($rs['doc_id'])."<br>";
            $sql = ("update clip_news set viewCount = {$rs['num']} where code={$rs['doc_id']}");
            $result = $this->db->query($sql);
        }
    }
    
    public function agent()
    {
        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        if ( $this->agent->is_mobile() ) {
            echo "- ".$this->agent->is_mobile()."<br/>";
        }
        if ( $this->agent->is_browser() ) {
            echo "--- ".$this->agent->is_browser()."<br/>";
        }        
           echo $agent."<br/>";
        if ($this->agent->is_referral())
        {
           echo $this->agent->referrer();
        }
           echo $this->agent->referrer()."<br/>";
           echo $this->agent->platform(); // Platform info (Windows, Linux, Mac, etc.)        
    } 
    
}
