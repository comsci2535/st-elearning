<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Articles_categories_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        if ( isset($param['article_categorie_id']) ) 
            $this->db->where('a.article_categorie_id', $param['article_categorie_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);
        $this->db->order_by('a.created_at', 'DESC');
        
        $query = $this->db
                        ->select('a.*')
                        ->from('articles_categories a')
                        ->get();
        return $query;
    }  

    // new function query

    public function count_articles_categories_all() 
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('articles_categories');
    }

    public function get_articles_categories_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('articles_categories');
        return $query;
    }

    public function get_articles_categories_option_all($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('title', $param['search']);
            $this->db->or_like('excerpt', $param['search']);
            $this->db->or_like('detail', $param['search']);
        endif;

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('articles_categories');
        
        return $query;
    }

    public function get_articles_categories_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('articles_categories');
        
        return $query;
    }

    public function get_articles_categories_by_id($id)
    {
        $this->db->where('article_categorie_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('articles_categories');
        
        return $query;
    }

    public function get_article_join_categorie_by_id($id)
    {
        $this->db->where('a.article_categorie_id', $id);
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->order_by('a.created_at', 'DESC');
        $this->db->select('a.*, b.*');
        $this->db->join('articles_categories b', 'a.article_categorie_id = b.article_categorie_id');
        $query = $this->db->get('articles a');
        
        return $query;
    }

    public function get_article_join_categorie_by_slug($slug)
    {
        $this->db->where('b.slug', $slug);
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->order_by('a.created_at', 'DESC');
        $this->db->select('a.*, b.*');
        $this->db->join('articles_categories b', 'a.article_categorie_id = b.article_categorie_id');
        $query = $this->db->get('articles a');
        
        return $query;
    }

}
