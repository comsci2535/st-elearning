<div class="container GETUP_BLOG">
        <div class="row">
            <div class="col-sm-12 mt-5 mb-5">
                <div class="title">
                    <h1>GETUP BLOG</h1>
                </div>
                <div class="text-center">
                    <p class="h3">
                        บทความออนไลน์ อ่านฟรี 24 ชั่วโมง ทุกหมวดหมู่ที่คุณสนใจ ถูกรวมไว้แล้ว
                    </p>
                </div>
            </div>

            <?php 
                if(!empty($info)):
                    foreach($info as $key => $item):
            ?>
                <div class="col-sm-4 mb-5 wow fadeIn" data-wow-delay="0.<?=$key;?>s">
                    <a href="<?php echo base_url('articles/category/'.$item->slug)?>">
                        <div class="boxx">
                            <img src="<?php echo !empty($item->file)? base_url().$item->file : ''?>" class="ImgFluid" alt="<?=$item->title?>" onerror="this.src='<?php echo base_url("images/Getup_Teacher.jpg"); ?>'">
                            <div class="text">
                                <span>หมวดหมู่</span>
                                <p><?=$item->title?></p>
                            </div>
                        </div>
                    </a>
                </div>
            <?php 
                    endforeach;
                endif;
            ?>
        </div>
        <div class="text-center mb-5">
            <a href="<?=base_url()?>articles"> <button type="button" class="btn btn-custom">บทความทั้งหมด</button> </a>
        </div>
    </div>