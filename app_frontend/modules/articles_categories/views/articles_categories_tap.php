<form action="" method="post">

    <div class="form-group">
        <label class="head" for="sel1">หมวดหมู่ </label>
        <?php 
            if(!empty($info)):
                foreach($info as $list):
        ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkbox-fiter-articles" value="<?php echo !empty($list->article_categorie_id) ? $list->article_categorie_id : '';?>">
                            <span class="cr"><i class="cr-icon fas fa-check"></i></span>
                            <span class="name"><?php echo !empty($list->title) ? $list->title : '';?></span>
                        </label>
                    </div>
        <?php  
                endforeach;
            endif; 
        ?>
    </div>

</form>