<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles_categories extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('articles_categories_m');
    }

    public function index(){

    }
    
    public function articles_categories_home()
    {
        $input['recycle'] = 0;
        $input['active']  = 1;
        $input['length']  = 6;
        $input['start']   = 0;
        // get articles_categories
		$info  = $this->articles_categories_m->get_rows($input)->result();
		$data['info'] = $info;
        $this->load->view('articles_categories_home', $data); 
    }
    
    public function articles_menu()
	{
		// get articles_categories
		$info  = $this->articles_categories_m->get_articles_categories_all()->result();
		$data['info'] = $info;
        $this->load->view('articles_categories_tap', $data); 
	}

}
