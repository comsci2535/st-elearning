<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('courses_m');
		$this->load->model('seos/seos_m');
		$this->load->model('courses_instructors/courses_instructors_m');
		
	}

    private function seo(){

		$obj_seo = $this->seos_m->get_seos_by_display_page('courses')->row();
		$title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
		$robots         = !empty($obj_seo->title)? $obj_seo->title : 'คอร์สเรียนออลไลน์';
		$description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : 'คอร์สเรียนออลไลน์';
		$keywords       = !empty($obj_seo->detail)? $obj_seo->detail : 'คอร์สเรียนออลไลน์';
		$img       		= !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url('courses')."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".$img."'/>";
		return $meta;
	}

    private function seo_detail($code){

        $obj_seo=$this->courses_m->get_courses_by_slug($code)->row();
        $title          = !empty($obj_seo->metaTitle)? config_item('siteTitle').' | '.$obj_seo->metaTitle : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
        $robots         = !empty($obj_seo->metaTitle)? $obj_seo->metaTitle : 'คอร์สเรียนออลไลน์';
        $description    = !empty($obj_seo->metaDescription)? $obj_seo->metaDescription : 'คอร์สเรียนออลไลน์';
        $keywords       = !empty($obj_seo->metaKeyword)? $obj_seo->metaKeyword : 'คอร์สเรียนออลไลน์';
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('courses/'.$code)."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

	public function index(){
        Modules::run('track/front','');
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'courses',
            'footer'  => 'footer',
            'function'=>  array('custom','courses'),
		);

		$data['total'] = $this->courses_m->count_courses_all();

        $this->load->view('template/body', $data);
	}

    public function detail($code="")
    {
      
        $info = $this->courses_m->get_courses_by_slug($code)->row();
        if (!empty($info->course_id)) {
            $infoUser = $this->courses_instructors_m->get_courses_instructors_join_user_by_course_id($info->course_id)->row();
        }

        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, config_item('siteTitle'));
        
		$data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo_detail($code),
            'header'  => 'header',
            'content' => 'detail',
            'footer'  => 'footer',
            'function'=> array('custom','courses'),
            'courses' => $info,
            'infoUser' => $infoUser
		);

        $code_['codeId'] =""; 
        if(!empty($this->input->get('code'))){
             $code_['codeId'] = $this->input->get('code');
             $this->session->set_userdata('code_data', $code_);
        }else{
             $code_['codeId'] = "";
             $this->session->set_userdata('code_data', $code_);
        }

        Modules::run('track/front',$info->course_id);

        $param['ogUrl'] = 'courses/'.$code;
        Modules::run('social/set_share', $param);
        
        $this->load->view('template/body', $data);
	}

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    }  

	public function courses_home(){
		$param['length'] = 6;
		$param['start']	 = 0;
		$info  = $this->courses_m->get_courses_option_all($param)->result();
		
		if($info):
			foreach($info as $item):
				$item->list_users = $this->courses_instructors_m->get_courses_instructors_join_user_by_course_id($item->course_id)->row();
			endforeach;
		endif;

		$data['info'] = $info;
        $this->load->view('courses_home', $data);
    }
    
    public function courses_recommend($course_id,$course_categorie_id){
		$param['length'] = 4;
		$param['start']	 = 0;
        $param['exclude']=$course_id;
        $param['course_categorie_id']=$course_categorie_id;
		$info  = $this->courses_m->get_courses_option_all($param)->result();
		if($info):
			foreach($info as $item):
				$item->list_users = $this->courses_instructors_m->get_courses_instructors_join_user_by_course_id($item->course_id)->row();
			endforeach;
		endif;

		$data['info'] = $info;
        $this->load->view('courses_recommend', $data);
	}

	public function ajax_load_courses()
	{
		$mgs 	= 'แจ้งเตือน';
		$status = 0;
        $html   = '';
        
        $input = $this->input->post();
		$input['length'] = 6;
        $input['start']  = $input['length']*$input['page'];
        
        $fiter_arr = array();
        if(!empty($input['fiter_arr'])):
            
            foreach($input['fiter_arr'] as $fiter):
                array_push($fiter_arr, $fiter);
            endforeach;

            $input['course_id'] = array('-');
            $courses_categories = array();
           if(count($fiter_arr) > 1){
                $courses_categories=$this->db
                                    ->where_in('course_categorie_sub_id', $fiter_arr)
                                    ->select('course_id')
                                    ->group_by('course_id')
                                    ->having("count(course_id) > 1", null, false)
                                    ->get('courses_categories_map')
                                    ->result();
           }else{
                 $courses_categories=$this->db
                                    ->where_in('course_categorie_sub_id', $fiter_arr)
                                    ->select('course_id')
                                    ->group_by('course_id')
                                    ->get('courses_categories_map')
                                    ->result();
           }
            
            
            foreach ($courses_categories as $rs) {
                array_push($input['course_id'], $rs->course_id);
            }
        endif;
        
        
		$info = $this->courses_m->get_courses_option_all($input)->result();
        $info_count = $this->courses_m->get_courses_count_option_all($input);
        $total='<p>'.$info_count.' หลักสูตร</p>';
        
        $pageAll=$info_count;
        $pageAll= 0 ? 1 : ceil($pageAll/$input['length']);
        
        if($pageAll==($input['page']+1)){
            $pageAll=0;
        }
		if($info):
			
			foreach($info as $item):
				$infoUser = $this->courses_instructors_m->get_courses_instructors_join_user_by_course_id($item->course_id)->row();
                
                $promotion = Modules::run('promotion/get_promotion', $item->course_id);
                $Today = date("Y-m-d H:i:s");
                $NewDate = date ("Y-m-d H:i:s", strtotime("+7 day", strtotime($item->created_at)));
                $myCourse   = Modules::run('profile/get_mycourses_array', $item->course_id);

            $html.='<div class="Item">';
                        if(!empty($myCourse) && $myCourse['status']==1):
                            $html.='<a href="'.site_url("courses-lesson/{$item->slug}").'">';
                        else:
                            $html.='<a href="'.site_url("courses/{$item->slug}").'">';
                        endif;

                        $html.='<div class="boxx">
                            <div class="ribbin">';
                            $html.='<input type="hidden" id="course_id-'.$item->course_id.'" value="'.$item->course_id.'">   <div  id="showRemain-mobile-'.$item->course_id.'" style="display:none" ></div>';
                            if($Today < $NewDate){
                                $html.='<div class="box new">
                                            <span class="a1">New</span>
                                        </div>';
                            }

                            if(!empty($promotion)){
                                $html.='<div class="box pro protext-'.$item->course_id.'">
                                            <span class="a1">Promotion</span>
                                        </div>';
                            }
                            $img = "'".base_url('images/cose.jpg')."'";
                            $html.='</div>
                            <div class="img">
                                
                                <img src="'.base_url($item->file).'" class="ImgFluid imgBig" alt="'.$item->title.'" onerror="this.src= '.$img.' ">
                                <div class="hovv">
                                    <div class="text Bold">
                                            <span><i class="fas fa-link" style="font-size: 18px;"></i> รายละเอียดคอร์สเรียน</span>
                                    </div>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="name Bold">
                                    <p>'.$item->title.'</p>
                                </div>
                                <div class="teacher">
                                    <span class="Bold">โดยคุณครู</span>
                                    <span>'.$infoUser->fullname.' ('.$infoUser->fname.')</span>
                                </div>
                                    <div class="price">';

                                    // if ($infoUser->user_id===$this->session->users['UID']) {
                                    //     $html.='<div class="free">
                                    //                 <span class="Bold">คอร์สของฉัน</span>
                                    //             </div>';
                                    // }else{

                                    if($item->price > 0 && !empty($promotion)){
                                        if(!empty($myCourse) && $myCourse['status']==0):

                                            $html.='<div class="confirm">
                                                    <span class="Bold ">รอการอนุมัติ</span>
                                                    <div id="showRemain-'.$item->course_id.'" style="display: none;"></div>
                                                </div>';

                                        elseif(!empty($myCourse) && $myCourse['status']==1):

                                            $html.='<div class="free">
                                                    <span class="Bold">คลิกเพื่อเข้าเรียน</span>
                                                    <div id="showRemain-'.$item->course_id.'" style="display: none;"></div>
                                                </div>';

                                         elseif(!empty($myCourse) && $myCourse['status']==3):

                                            $html.='<div class="free">
                                                    <span class="Bold">รายละเอียดคอร์ส</span>
                                                    <div id="showRemain-'.$item->course_id.'" style="display: none;"></div>
                                                </div>';


                                        else:

                                         $html.='<div class="promotion protext-'.$item->course_id.'">
                                                    <div class="left Bold">
                                                        <div id="showRemain-'.$item->course_id.'"></div>
                                                        <p class="ket">'.number_format($item->price).'</p>
                                                    </div>
                                                    <div class="right">
                                                        <span class="Bold">'.number_format($promotion['discount']).' ฿</span>
                                                    </div>
                                                </div>
                                                <div class="normal protext2-'.$item->course_id.'" style="display: none;">
                                                    <span class="Bold">'.number_format($item->price).' ฿</span>
                                                </div>';
                                        endif;
                                        
                                    }else if($item->price==0){
                                        $html.='<div class="free">
                                                    <span class="Bold">FREE</span>
                                                </div>';
                                    }else{

                                        if(!empty($myCourse) && $myCourse['status']==0):

                                            $html.='<div class="confirm">
                                                    <span class="Bold">รอการอนุมัติ</span>
                                                </div>';

                                        elseif(!empty($myCourse) && $myCourse['status']==1):

                                            $html.='<div class="free">
                                                    <span class="Bold">คลิกเพื่อเข้าเรียน</span>
                                                </div>';
                                        elseif(!empty($myCourse) && $myCourse['status']==3):

                                            $html.='<div class="free">
                                                    <span class="Bold">รายละเอียดคอร์ส</span>
                                                </div>';
                                        else:
                                            $html.='<div class="normal">
                                                        <span class="Bold">'.number_format($item->price).' ฿</span>
                                                    </div>';
                                        endif;
                                    }
                                //}
                            $html.='</div>
                            </div>
                        </div>
                        </a>
                    </div>';
			endforeach;

			$status = 1;
		endif;
		
		$data = array(
			'data' 		=> $html
			,'mgs' 		=> $mgs
            ,'status' 	=> $status
            ,'test'     => $fiter_arr
            ,'pageAll' => $pageAll
            ,'total' => $total
		);

		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    
    public function learning(){
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'learning',
            'footer'  => 'footer',
            'function'=>  array('custom','courses'),
		);
        $this->load->view('template/body', $data);
	}
}
