<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Courses_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param){
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        if ( isset($param['course_id']) ) 
            $this->db->where('a.course_id', $param['course_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);
        
        if ( isset($param['slug']) )
            $this->db->where('a.slug', $param['slug']);
        if ( isset($param['course_categorie_id']) ) 
            $this->db->where('a.course_categorie_id', $param['course_categorie_id']);

        $this->db->where('a.approve', 1);
        
        $this->db->order_by('a.created_at', 'DESC');    
        
        $query = $this->db
                        ->select('a.*')
                        ->from('courses a')
                        ->get();
        return $query;
    }

    public function count_rows($param) 
    {
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
    
        if ( isset($param['course_id']) ) 
            $this->db->where('a.course_id', $param['course_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);
        
        if ( isset($param['slug']) )
            $this->db->where('a.slug', $param['slug']);
            
        if ( isset($param['course_categorie_id']) ) 
            $this->db->where('a.course_categorie_id', $param['course_categorie_id']);

        $this->db->where('a.approve', 1);
    
        return $this->db->count_all_results('courses a');
    }

    // new function query

    public function count_courses_all() 
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('approve', 1);
        return $this->db->count_all_results('courses');
    }

    public function count_courses_categorie_by_id($id) 
    {
        $this->db->where('course_categorie_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('approve', 1);
        return $this->db->count_all_results('courses');
    }

    public function get_courses_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('approve', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('courses');
        return $query;
    }

    public function get_courses_by_course_id($course_id) 
    {
        $this->db->where('course_id', $course_id);
        $query = $this->db->get('courses');
        return $query;
    }

    public function get_courses_option_all($param)
    {
       
        if (!empty($param['length'])):
           
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('title', $param['search']);
            $this->db->or_like('excerpt', $param['search']);
            $this->db->or_like('detail', $param['search']);
        endif;

        if (!empty($param['exclude'])):   
            $this->db->where('course_id !=',$param['exclude']);
        endif;

        // if (!empty($param['course_categorie_id'])):   
        //     $this->db->where('course_categorie_id',$param['course_categorie_id']);
        // endif;

        // if(!empty($param['fiter_arr'])):
        //     $cate_sub=$this->db->where_in('course_categorie_id', $param['fiter_arr']);
        //     $this->db->where_in('course_categorie_id', $param['fiter_arr']);
        // endif;

        if(!empty($param['course_id'])):
            $this->db->where_in('course_id', $param['course_id']);
        endif;

        $order_by = !empty($param['sort'])? $param['sort'] : 'DESC'; 

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('approve', 1);
        $this->db->where('publish', 'public');
        $this->db->order_by('recommend', $order_by);
        $this->db->order_by('created_at', $order_by);
        $this->db->select('*');
        $query = $this->db->get('courses');
        
        return $query;
    }

    public function get_courses_count_option_all($param)
    {
        
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('title', $param['search']);
            $this->db->or_like('excerpt', $param['search']);
            $this->db->or_like('detail', $param['search']);
        endif;

        // if (!empty($param['course_categorie_id'])):   
        //     $this->db->where('course_categorie_id',$param['course_categorie_id']);
        // endif;

        // if(!empty($param['fiter_arr'])):
        //     $cate_sub=$this->db->where_in('course_categorie_id', $param['fiter_arr']);
        //     $this->db->where_in('course_categorie_id', $param['fiter_arr']);
        // endif;

        if(!empty($param['course_id'])):
            $this->db->where_in('course_id', $param['course_id']);
        endif;

        $order_by = !empty($param['sort'])? $param['sort'] : 'DESC'; 

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('approve', 1);
        $this->db->where('publish', 'public');
        $this->db->order_by('recommend', $order_by);
        $this->db->order_by('created_at', $order_by);
        $this->db->select('*');
        $query = $this->db->get('courses');
        
        return $query->num_rows();
    }

    public function get_courses_by_slug($slug)
    {
        $this->db->where("(publish = 'public' OR  ( publish = 'private' AND (startDate ='0000-00-00' AND endDate ='0000-00-00') OR  (startDate <='".date('Y-m-d')."' AND endDate >='".date('Y-m-d')."')) )");
        $this->db->where('slug', $slug);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('approve', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('courses');
        
        return $query;
    }

    public function get_courses_categorie_by_id($id)
    {
        $this->db->where('course_categorie_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('approve', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('courses');
        
        return $query;
    }

    public function get_courses_join_categorie_by_id($id)
    {
        $this->db->where('a.course_categorie_id', $id);
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->where('a.approve', 1);
        $this->db->order_by('a.recommend', 'DESC');
        $this->db->order_by('a.created_at', 'DESC');
        $this->db->select('a.*, b.*');
        $this->db->join('courses_categories b', 'a.course_categorie_id = b.course_categorie_id');
        $query = $this->db->get('courses a');
        
        return $query;
    }

    public function get_courses_join_categorie_by_slug($slug)
    {
        $this->db->where('b.slug', $slug);
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->where('a.approve', 1);
        $this->db->order_by('a.recommend', 'DESC');
        $this->db->order_by('a.created_at', 'DESC');
        $this->db->select('a.*, b.*');
        $this->db->join('courses_categories b', 'a.course_categorie_id = b.course_categorie_id');
        $query = $this->db->get('courses a');
        
        return $query;
    }

    public function insert($value) {
        $this->db->insert('courses', $value);
        return $this->db->insert_id();
    }

    public function insert_courses_instructors($course_id,$value) {
        $this->db
                ->where('course_id', $course_id)
                ->delete('courses_instructors');

        $query = $this->db->insert_batch('courses_instructors', $value);
        return $query;
    }

    public function insert_courses_categories_map($course_id,$value) {
        $this->db
                ->where('course_id', $course_id)
                ->delete('courses_categories_map');

        $query = $this->db->insert_batch('courses_categories_map', $value);
        return $query;
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('course_id', $id)
                        ->update('courses', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('course_id', $id)
                        ->update('courses', $value);
        return $query;
    }   

    public function get_courses_by_id($course_id)
    {
        $this->db->where('course_id', $course_id);
        $query = $this->db->get('courses');
        return $query;
    }
}
