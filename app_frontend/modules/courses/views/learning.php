<section class="courses_learning">

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="text-center Bold mt-5 mb-2">
                    <h1>ตะลุยโจทย์ คณิตศาตร์ ม.ปลาย 2562</h1>
                </div>
            </div>
            <div class="col-md-8 detail_left">
                <div class="video">
                    <img src="<?=base_url('images/detail.jpg');?>" class="ImgFluid pro" alt="getupschool">
                </div>
                <div class="courses-detail">
                    <h2 class="Bold">1. การทดสอบ</h2>
                    <div class="overlate">
                        <p>
                            Note about Shortcuts for Windows users
                            Section 1, Lecture 2
                            This video has been recorded using a Mac.
                            On Mac, most of the shortcuts are a combination of the CMD key with other keys on the
                            keyboard.
                            If you are using Windows, you just need to use the CTRL key instead of the CMD key.
                            This is the only difference, everything else is the same.
                        </p>
                    </div>
                    <a href="javascript:void(0)" id="overlate">
                        <i class="fas fa-plus"></i> ดูเพิ่มเติม
                    </a>
                </div>
            </div>



            <div class="col-md-4 detail_rigth">
                <div class="courses_lerning">
                    <div class="names">
                        <span>เนื้อหาคอร์สเรียน</span> <a href="http://"><button type="button"
                                class="btn">ทำแบบทดสอบคอร์ส</button></a>
                    </div>
                    <div class="boox">
                        <?php for ($i=0; $i < 2; $i++) { ?>
                        <div class="head Bold">
                            <span>บทที่ 1 : เรื่อง ทดสอบบทที่ 1</span>
                        </div>
                        <?php for ($ii=0; $ii < 5; $ii++) { ?>
                        <div class="listt">
                            <div class="name Bold">
                                <i class="fas fa-play"></i>
                                <span>1. การทดสอบ</span>
                            </div>
                            <div class="rigth">
                                <span class="m Bold">5.00 นาที</span>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>