<section class="courses">
    <div id="slide_banner" class="owl-carousel owl-theme">
        <div class="item">
            <img src="<?=base_url('images/slide_courses.jpg');?>" class="ImgFluid" alt="getupschool">
        </div>
    </div>

    <div class="container">
        <div class="title Bold">
            <h1>คอร์สเรียนทั้งหมด</h1>
            <div class="text">
                <p><?php echo !empty($total)? $total : 0;?> หลักสูตร</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="menu_left Bold">
                    <form action="" method="post">
                        <div class="input-group">
                            <input type="text" class="form-control" id="demo" name="email"
                                placeholder="ค้นหาคอร์สเรียน..." autocomplete="off">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                            </div>
                        </div>
                    </form>
                    <form action="" method="post">
                        <div class="form-group">
                            <label class="head" for="sel1">เรียงลำดับโดย</label>
                            <select class="form-control" id="sel1">
                                <option>ล่าสุด</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </form>
                    <?php
                    echo Modules::run('courses_categories/categories_menu');
                    ?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="CoursesItem">
                    <?php for ($i=0; $i < 4; $i++) { ?>
                    <div class="Item">
                        <div class="boxx">
                            <div class="ribbin">
                                <div class="box new">
                                    <span>New</span>
                                </div>
                                <div class="box pro">
                                    <span>Promotion</span>
                                </div>
                            </div>
                            <div class="img">
                                <img src="<?=base_url('images/cose.jpg')?>" class="ImgFluid imgBig" alt="คอสเรียน">
                                <div class="hovv">
                                    <div class="text Bold">
                                        <a href="<?=site_url('courses/detail');?>">
                                            <span>รายละเอียดคอร์สเรียน</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="name Bold">
                                    <p>การจัดการเรียนรู้ที่สร้างแรงบันดาลใจ ปฏิบัติการที่ช่วยให้คุณครู
                                        การจัดการเรียนรู้ที่สร้างแรงบันดาลใจ ปฏิบัติการที่ช่วยให้คุณครู</p>
                                </div>
                                <div class="teacher">
                                    <span class="Bold">โดยคุณครู</span>
                                    <span>Adam Bradshaw (อ.อดัม)</span>
                                </div>
                                <a href="<?=site_url('courses/detail');?>">
                                    <div class="price">
                                        <div class="promotion">
                                            <div class="left Bold">
                                                <p>29 วัน 00:00:00</p>
                                                <p>3,000</p>
                                            </div>
                                            <div class="right">
                                                <span class="Bold">2,500 ฿</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="Item">
                        <div class="boxx">
                            <div class="ribbin">
                                <div class="box new">
                                    <span>New</span>
                                </div>
                                <div class="box pro">
                                    <span>Promotion</span>
                                </div>
                            </div>
                            <div class="img">
                                <img src="<?=base_url('images/cose.jpg')?>" class="ImgFluid imgBig" alt="คอสเรียน">
                                <div class="hovv">
                                    <div class="text Bold">
                                        <a href="<?=site_url('courses/detail');?>">
                                            <span>รายละเอียดคอร์สเรียน</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="name Bold">
                                    <p>การจัดการเรียนรู้ที่สร้างแรงบันดาลใจ ปฏิบัติการที่ช่วยให้คุณครู
                                        การจัดการเรียนรู้ที่สร้างแรงบันดาลใจ ปฏิบัติการที่ช่วยให้คุณครู</p>
                                </div>
                                <div class="teacher">
                                    <span class="Bold">โดยคุณครู</span>
                                    <span>Adam Bradshaw (อ.อดัม)</span>
                                </div>
                                <a href="<?=site_url('courses/detail');?>">
                                    <div class="price">
                                        <div class="normal">
                                            <span class="Bold">2,500 ฿</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="Item">
                        <div class="boxx">
                            <div class="ribbin">
                                <div class="box new">
                                    <span>New</span>
                                </div>
                                <div class="box pro">
                                    <span>Promotion</span>
                                </div>
                            </div>
                            <div class="img">
                                <img src="<?=base_url('images/cose.jpg')?>" class="ImgFluid imgBig" alt="คอสเรียน">
                                <div class="hovv">
                                    <div class="text Bold">
                                        <a href="<?=site_url('courses/detail');?>">
                                            <span>รายละเอียดคอร์สเรียน</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="name Bold">
                                    <p>การจัดการเรียนรู้ที่สร้างแรงบันดาลใจ ปฏิบัติการที่ช่วยให้คุณครู
                                        การจัดการเรียนรู้ที่สร้างแรงบันดาลใจ ปฏิบัติการที่ช่วยให้คุณครู</p>
                                </div>
                                <div class="teacher">
                                    <span class="Bold">โดยคุณครู</span>
                                    <span>Adam Bradshaw (อ.อดัม)</span>
                                </div>
                                <a href="<?=site_url('courses/detail');?>">
                                    <div class="price">
                                        <div class="free">
                                            <span class="Bold">FREE</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>
</section>