<?php
$promotion = Modules::run('promotion/get_promotion', $courses->course_id);
$Today = date("Y-m-d H:i:s");
$NewDate = date("Y-m-d H:i:s", strtotime("+7 day", strtotime($item->created_at)));
$myCourse = Modules::run('profile/get_mycourses_array', $courses->course_id);
?>
<style>
    .video-js .vjs-big-play-button {
        left: 0;
        right: 0;
        margin-left: auto;
        margin-right: auto;
        top: 0;
        margin-top: auto;
        margin-bottom: auto;
        bottom: 0;
    }
    .embed-container{
        padding-bottom: unset;
    }
</style>
<div class="CoursesDetail">
    <div class="img min-sm">
        <img src="<?= !empty($courses->file) ? base_url() . $courses->file : ''; ?>"
             onerror="this.src= '<?php echo base_url('images/cose.jpg') ?>'" class="ImgFluid imgBig"
             alt="<?php echo !empty($courses->title) ? $courses->title : '' ?>">
    </div>
    <input type="hidden" id="course_id-<?= $courses->course_id ?>" value="<?= $courses->course_id ?>">
    <div class="price">

        <div class="num">

            <?php
            if (empty($myCourse) || (!empty($myCourse) && $myCourse['status'] == 2)) {

                if (!empty($promotion)):
                    ?>
                    <p class="a Bold"><?= number_format($promotion['discount']) ?> ฿</p>
                    <p class="b Bold">
                        <del><?= number_format($courses->price) ?></del>
                        ฿
                    </p>
                <?php
                else:
                    if ($courses->price != 0):
                        ?>
                        <p class="a Bold"><?= number_format($courses->price) ?> ฿</p>
                    <?php
                    endif;
                endif;

            }
            ?>
        </div>
        <div class="bt">

            <div class="aa">
                <?php
                if (!empty($this->session->users['UID'])) {
                    if ($courses->price == 0):
                        ?>
                        <a href="<?= site_url('courses-lesson/' . $courses->slug); ?>"
                           class="btn-custom Bold">เข้าสู่บทเรียน</a>
                    <?php
                    else:

                        if (!empty($myCourse) && $myCourse['status'] == 0):
                            ?>
                            <a href="javascript:void(0);" class="btn-custom Bold btn-promo">รอการอนุมัติ</a>
                        <?php
                        elseif (!empty($myCourse) && $myCourse['status'] == 1):
                            ?>
                            <a href="<?= site_url('courses-lesson/' . $courses->slug); ?>"
                               class="btn-custom Bold">คลิกเพื่อเข้าเรียน</a>
                        <?php
                        elseif (!empty($myCourse) && $myCourse['status'] == 3):
                            ?>
                            <a href="<?= site_url('courses-lesson/' . $courses->slug); ?>"
                               class="btn-custom Bold">คลิกเพื่อเข้าเรียน</a>
                        <?php
                        else:
                            ?>
                            <a href="javascript:void(0);" id="btn-a-courses-2"
                               data-id="<?php echo !empty($courses->course_id) ? $courses->course_id : '' ?>"
                               class="btn-custom Bold <?php if (!empty($promotion)) {
                                   echo 'btn-promo';
                               } ?>">ชำระเงินเพื่อเรียน</a>
                        <?php
                        endif;
                    endif;
                } else {
                    if ($courses->price == 0) {
                        ?>
                        <a href="javascript:void(0);" id="loginface" class="btn-custom Bold">คลิกเพื่อเข้าเรียน</a>
                    <?php } else { ?>
                        <a href="javascript:void(0);" id="loginface"
                           class="btn-custom Bold <?php if (!empty($promotion)) {
                               echo 'btn-promo';
                           } ?>">ชำระเงินเพื่อเรียน</a>
                    <?php }
                } ?>
            </div>
            <?php
            if (!empty($promotion)):
                ?>
                <div class="bb protext-<?= $courses->course_id ?>" <?php if (!empty($myCourse) && $myCourse['status'] != 2) { ?> style="display: none;"  <?php } ?>>
                    <div id="showRemain-<?= $courses->course_id ?>"
                         class="showRemain-<?= $courses->course_id ?> btn-custom Bold"></div>
                </div>
            <?php
            endif;
            ?>


        </div>
    </div>
    <?php if (!empty($courses->receipts)) { ?>
        <div class="text">
            <h2 class="Bold">สิ่งที่ได้รับ</h2>
            <div class="p">
                <?php echo html_entity_decode($courses->receipts); ?>
            </div>

        </div>
    <?php } ?>
    <div id="scoll">
        <ul class="list-group">
            <a href="#scoll1">
                <li class="list-group-item active">ภาพรวม</li>
            </a>
            <a href="#scoll2">
                <li class="list-group-item">เนื้อหาคอร์สเรียน</li>
            </a>
            <a href="#scoll3">
                <li class="list-group-item">อาจารย์ผู้สอน</li>
            </a>
            <a href="#scoll4">
                <li class="list-group-item">ความคิดเห็นผู้เรียน</li>
            </a>
            <a href="#scoll5">
                <li class="list-group-item">รีวิวคอร์สเรียน</li>
            </a>
        </ul>
        <div class="shrare">
            <a href="#scoll6">
                <span><i class="fas fa-share"></i> แบ่งปัน</span>
            </a>
        </div>
    </div>
</div>
