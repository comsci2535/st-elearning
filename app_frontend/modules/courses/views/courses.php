<section class="courses">
  
     <?php echo  Modules::run('banners/courses');?>

    <div class="container">
        <div class="title Bold">
            <h1>คอร์สเรียนทั้งหมด</h1>
            <div class="text" id="total">
                <p><?php echo !empty($total)? $total : 0;?> หลักสูตร</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="menu_left Bold">
                    <form action="" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" id="text-search" name="search" placeholder="ค้นหาบทความ..." autocomplete="off">
                                <i class="fas fa-search"></i>
                        </div>
                    </form>
                    <form action="" method="post">
                        <div class="form-group">
                            <label class="head" for="sel1">เรียงลำดับโดย</label>
                            <select class="form-control" id="text-sort" name="sort">
                                <option value="desc">ล่าสุด</option>
                                <option value="asc">ก่อนหน้า</option>
                            </select>
                        </div>
                    </form>
                    <?php
                    echo Modules::run('courses_categories/categories_menu');
                    ?>
                </div>
            </div>
            <div class="col-md-9">
                <div id="CoursesItem" class="CoursesItem"></div>
                <input type="hidden" name="page" id="page" value="0">
                <div class="text-center mb-5" style="display: none" id="views-more-1">

                    <a href="javascript:void(0)" class="btn-custom btn-click-load-page" data-page="0"> <span id="form-img-div"></span> แสดงคอร์สเรียนเพิ่มเติม</a>
                </div>
            </div>
        </div>

    </div>
</section>