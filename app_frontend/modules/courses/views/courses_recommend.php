<?php
if($info): ?>
<div class="Bold my-4">
    <h2>คอร์สเรียนอื่นๆที่น่าสนใจ</h2>
</div>
<div class="CoursesItem mb-4">
<?php
    foreach($info as $item):
        $infoUser = $this->courses_instructors_m->get_courses_instructors_join_user_by_course_id($item->course_id)->row();
        $promotion = Modules::run('promotion/get_promotion', $item->course_id);
        $Today = date("Y-m-d H:i:s");
        $NewDate = date ("Y-m-d H:i:s", strtotime("+7 day", strtotime($item->created_at)));
?>
    <div class="Item colum4">
        <div class="boxx">
            <div class="ribbin">
            <input type="hidden" id="course_id-<?=$item->course_id?>" value="<?=$item->course_id?>">
            <?php
                if($Today < $NewDate){
                    ?>
                    <div class="box new">
                                <span class="a1">New</span>
                            </div>
                            <?php
                            }

                        if(!empty($promotion)){
                            ?>
                            <div class="box pro protext-<?=$item->course_id?>">
                                <span class="a1">Promotion</span>
                            </div>
                        <?php
                        }
                        ?>
                        </div>
                        <div class="img">
                            
                            <img src="<?=base_url($item->file)?>" class="ImgFluid imgBig" alt="<?=$item->title?>" onerror="this.src= '<?=base_url("images/cose.jpg");?>' ">
                            <div class="hovv">
                                <div class="text Bold">
                                    <a href="<?=site_url("courses/".$item->slug)?>">
                                        <span><i class="fas fa-link" style="font-size: 18px;"></i> รายละเอียดคอร์สเรียน</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="detail">
                            <div class="name Bold">
                                <p><?php echo $item->title?></p>
                            </div>
                            <div class="teacher">
                                <span class="Bold">โดยคุณครู</span>
                                <span><?php echo $infoUser->fullname?> (<?=$infoUser->fname?>)</span>
                            </div>
                            <a href="<?=site_url('courses/detail/'.$item->slug)?>">
                                <div class="price">
                                <?php
                                if($infoUser->user_id==$this->session->users['UID']){
                                ?>
                                <div class="free">
                                    <span class="Bold">คอร์สของฉัน</span>
                                </div>
                                <?php
                                }else{
                                if($item->price > 0 && !empty($promotion)){
                                ?>
                                    <div class="promotion protext-<?=$item->course_id?>">
                                        <div class="left Bold">
                                            <div id="showRemain-<?=$item->course_id?>" ></div>
                                           <!--  <div id="showRemainbycourses-<?=$item->course_id?>" style="display:none"></div>
                                            <div id="showRemainrecommend-<?=$item->course_id?>" style="display:none"></div> -->
                                            <p><?=number_format($item->price)?></p>
                                        </div>
                                        <div class="right">
                                            <span class="Bold"><?=number_format($promotion['discount'])?> ฿</span>
                                        </div>
                                    </div>
                                    <div class="normal protext2-<?=$item->course_id?>" style="display: none;">
                                        <span class="Bold"><?=number_format($item->price)?> ฿</span>
                                    </div>
                                <?php   
                                }else if($item->price==0){
                                ?>
                                    <div class="free">
                                        <span class="Bold">FREE</span>
                                    </div>
                                <?php
                                }else{
                                ?>
                                    <div class="normal">
                                        <span class="Bold"><?=number_format($item->price)?> ฿</span>
                                    </div>
                                <?php
                                }
                                }
                                ?>
                        </div>
                            </a>
                        </div>
                    </div>
                </div>
            <?php
        endforeach;
   ?>
</div>
<?php  endif;
    ?>