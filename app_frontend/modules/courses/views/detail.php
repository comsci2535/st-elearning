<?php
$promotion = Modules::run('promotion/get_promotion', $courses->course_id);
$Today = date("Y-m-d H:i:s");
$NewDate = date("Y-m-d H:i:s", strtotime("+7 day", strtotime($item->created_at)));
$myCourse = Modules::run('profile/get_mycourses_array', $courses->course_id);
?>
<input type="hidden" name="course_id" id="course_id" value="<?= $courses->course_id ?>">
<section class="courses_detail" id="scoll1">

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="text-center Bold mt-5 mb-2">
                    <h1><?php echo !empty($courses->title) ? $courses->title : '' ?></h1>
                </div>
            </div>
            <div class="col-md-4 detail_rigth max-xs">
                <?php $this->load->view('detail_right_mobile'); ?>
            </div>
            <div class="col-md-8 detail_left">
                <div class="video">
                    <?php
                    if ($courses->video_preview > 0):
                        ?>
                        <div class="embed-container">
                            <!--<div id="dd"></div>-->
                            <!--  <div data-vimeo-id="<?php //echo $courses->recommendVideo; ?>"  id="player-<?php //echo $courses->course_id; ?>"></div> -->
<!--                            <input type="hidden" name="course_id2" id="course_id2"-->
<!--                                   value="--><?php //echo $courses->course_id; ?><!--">-->
<!--                            <input type="hidden" name="recommendVideo" id="recommendVideo"-->
<!--                                   value="--><?php //echo $courses->recommendVideo; ?><!--">-->
                            <!--                        <iframe src="https://player.vimeo.com/video/-->
                            <?php //echo $courses->recommendVideo;
                            ?><!--?autoplay=1" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>-->

                            <video
                                    id="my-video"
                                    class="video-js vjs-theme-fantasy"
                                    controls
                                    preload="auto"
                                    width="640"
                                    height="264"
                                    poster="<?=!(empty($courses->file)) ? base_url($courses->file) : ''?>"
                                    data-setup="{}"
                            >
                                <source src="<?php echo $courses->recommendVideo; ?>" type="video/mp4" />
                                <p class="vjs-no-js">
                                    <a href="https://videojs.com/html5-video-support/" target="_blank"
                                    >supports HTML5 video</a>
                                </p>
                            </video>

                        </div>
                    <?php
                    else:
                        ?>
                        <input type="hidden" name="course_id2" id="course_id2" value="">
                        <img src="<?php echo !(empty($courses->file)) ? base_url($courses->file) : ''; ?>"
                             class="ImgFluid pro" alt="getupschool"
                             onerror="this.src='<?php echo base_url("images/cose.jpg"); ?>'">
                    <?php
                    endif;
                    ?>
                    <?php echo Modules::run('social/share_button'); ?>

                </div>
                <?php
                if (!empty($courses->excerpt) || !empty($courses->detail)) {
                    ?>
                    <div class="courses-detail mt-5">
                        <h2 class="Bold">รายละเอียด</h2>

                        <div id="overlate-detail" class="">
                            <p>
                                <?php echo !empty($courses->excerpt) ? $courses->excerpt : '' ?>
                            </p>
                            <p>
                                <?php echo !empty($courses->detail) ? html_entity_decode($courses->detail) : '' ?>
                            </p>
                        </div>
                        <a href="javascript:void(0)" id="overlate" class="overlate-hide" data-line="overlate-detail"
                           data-hide="0" style="display:none;">
                            <i class="fas fa-plus"></i> ดูเพิ่มเติม
                        </a>
                    </div>
                <?php } ?>

                <?php echo Modules::run('courses_lesson/courses_lesson', $courses->course_id); ?>
                <!-- load ผู้สอน -->
                <?php echo Modules::run('instructors/instructor_courses', $courses->course_id); ?>

                <!-- load reviews stars courses -->
                <?php echo Modules::run('reviews/reviews_stars_courses', $courses->course_id); ?>
            </div>
            <div class="col-md-4 detail_rigth min-sm">
                <?php $this->load->view('detail_right'); ?>
            </div>
        </div>

        <!-- load recommend courses -->
        <?php echo Modules::run('courses/courses_recommend', $courses->course_id, $courses->course_categorie_id); ?>

    </div>
</section>