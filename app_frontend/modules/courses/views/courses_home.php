<div class="container">
    <div class="title Bold">
        <h1>Special courses</h1>
    </div>
</div>

<div class="container">
        <div class="CoursesItem">
            <?php 
                if(!empty($info)):
                    foreach($info as $item):

                        $promotion = Modules::run('promotion/get_promotion', $item->course_id);
                        $Today = date("Y-m-d H:i:s");
                        $NewDate = date ("Y-m-d H:i:s", strtotime("+7 day", strtotime($item->created_at)));
                         $myCourse   = Modules::run('profile/get_mycourses_array', $item->course_id);
            ?>
            <div class="Item">
            <?php  if(!empty($myCourse) && $myCourse['status']==1): ?>
                <a href="<?=site_url('courses-lesson/'.$item->slug);?>">
            <?php  else: ?>
                <a href="<?=site_url('courses/'.$item->slug);?>">
            <?php endif; ?>
                <div class="boxx">
                    <div class="ribbin">
                    <input type="hidden" id="course_id-<?=$item->course_id?>" value="<?=$item->course_id?>">
                     <div  id="showRemain-mobile-<?=$item->course_id?>" class=" hidden" ></div>
                        <?php
                        if($Today < $NewDate){
                        ?>
                        <div class="box new">
                            <span>New</span>
                        </div>
                        <?php
                        }

                        if(!empty($promotion)){
                        ?>
                        <div class="box pro protext-<?=$item->course_id?>">
                            <span>Promotion</span>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="img">
                    <img src="<?=base_url($item->file)?>" class="ImgFluid imgBig" alt="<?=$item->title? $item->title : ''?>" onerror="this.src='<?=base_url("images/cose.jpg");?>'">
                        <div class="hovv">
                            <div class="text Bold">
                                    <span><i class="fas fa-link" style="font-size: 18px;"></i> รายละเอียดคอร์สเรียน</span>
                            </div>
                        </div>
                    </div>
                    <div class="detail">
                        <div class="name Bold">
                            <p><?=$item->title? $item->title : ''?></p>
                        </div>
                        <div class="teacher">
                            <span class="Bold">โดยคุณครู</span>
                            <span><?=$item->list_users->fullname? $item->list_users->fullname : ''?> (<?=$item->list_users->fname? $item->list_users->fname : ''?>)</span>
                        </div>
                            <div class="price">
                            
                                <?php
                                //if ($item->list_users->user_id===$this->session->users['UID']) {
                                ?>
                                <!-- <div class="free">
                                    <span class="Bold">คอร์สของฉัน</span>
                                </div> -->
                                <?php
                                //}else{
                                 if($item->price > 0 && !empty($promotion)){
                                ?>
                                <?php  if(!empty($myCourse) && $myCourse['status']==0): ?>
                                    <div class="confirm">
                                        <span class="Bold">รอการอนุมัติ</span>
                                        <div id="showRemain-<?=$item->course_id?>" style="display: none;"></div>
                                    </div>
                                <?php  elseif(!empty($myCourse) && $myCourse['status']==1): ?>
                                    <div class="free">
                                        <span class="Bold">คลิกเพื่อเข้าเรียน</span>
                                        <div id="showRemain-<?=$item->course_id?>" style="display: none;"></div>
                                    </div>
                                <?php  elseif(!empty($myCourse) && $myCourse['status']==3): ?>
                                    <div class="free">
                                        <span class="Bold">รายละเอียดคอร์ส</span>
                                        <div id="showRemain-<?=$item->course_id?>" style="display: none;"></div>
                                    </div>
                                <?php else: ?>
                                    <div class="promotion protext-<?=$item->course_id?>">
                                        <div class="left Bold">
                                            <div id="showRemain-<?=$item->course_id?>"></div>
                                            <p class="ket"><?=number_format($item->price)?></p>
                                        </div>
                                        <div class="right">
                                            <span class="Bold"><?=number_format($promotion['discount'])?> ฿</span>
                                        </div>
                                    </div>
                                    <div class="normal protext2-'.$item->course_id.'" style="display: none;">
                                        <span class="Bold"><?=number_format($item->price)?> ฿</span>
                                    </div>
                                <?php endif; ?>
                                <?php
                                }else if($item->price==0){
                                ?>     
                                <div class="free">
                                    <span class="Bold">FREE</span>
                                </div>
                                <?php
                                }else{
                                ?>
                                <?php  if(!empty($myCourse) && $myCourse['status']==0): ?>
                                    <div class="confirm">
                                        <span class="Bold">รอการอนุมัติ</span>
                                    </div>
                                <?php  elseif(!empty($myCourse) && $myCourse['status']==1): ?>
                                    <div class="free">
                                        <span class="Bold">คลิกเพื่อเข้าเรียน</span>
                                    </div>
                                <?php elseif(!empty($myCourse) && $myCourse['status']==3): ?>
                                    <div class="free">
                                        <span class="Bold">รายละเอียดคอร์ส</span>
                                    </div>
                                <?php else: ?>
                                    <div class="normal">
                                        <span class="Bold"><?=number_format($item->price)?> ฿</span>
                                    </div>
                                 <?php endif; ?>
                                <?php
                                }
                                ?>
                                <?php
                                //}
                                ?>
                            </div>
                    </div>
                </div>
            </a>
            </div>
            <?php 
                endforeach; 
                endif; 
            ?>
        </div>
        <div class="text-center mb-5">
            <a href="<?=site_url('courses');?>" class="btn-custom">คอร์สเรียนทั้งหมด</a>
        </div>
    </div>



