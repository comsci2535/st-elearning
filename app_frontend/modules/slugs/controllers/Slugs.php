<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slugs extends MX_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('slug_library');
	}

	public function index() {
		$slug = $this->slug_library->getSlug(
			$name,
			$id
		);

		echo $slug;
	}

	public function check_name() {

		$slug = $this->slug_library->getName(
			$this->input->post('name'),
			$this->input->post('id'),
			$this->input->post('db')
		);

		echo json_encode($slug);

	}

	public function check_slugs() {

		$slug = $this->slug_library->getSlug(
			$this->input->post('name'),
			$this->input->post('id'),
			$this->input->post('db')
		);

		echo json_encode($slug);

	}

}
