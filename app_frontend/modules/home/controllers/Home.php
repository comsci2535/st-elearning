<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('seos/seos_m');
	}

  

	 private function seo(){

        $obj_seo = $this->seos_m->get_seos_by_display_page('home')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | คอร์สเรียนออนไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : config_item('siteTitle');
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : config_item('metaDescription');
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

	public function index(){
		
		Modules::run('track/front','');
        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'home',
            'footer'  => 'footer',
            'function'=>  array('custom','home'),
		);

        $this->load->view('template/body', $data);
    }
    
    public function check_instructer() 
	{
        $instructors = $this->db->get_where('instructors', array('user_id' => $this->session->users['UID']));
		return $instructors;
    }
}
