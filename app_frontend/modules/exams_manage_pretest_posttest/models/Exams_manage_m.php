<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Exams_manage_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_exams_by_all($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";

            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
        
        $this->db->where('a.recycle', 0);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        $this->db->where('a.pretest_posttest', 1);

        $query = $this->db
                        ->select('a.*')
                        ->from('exam a')
                        ->get();
        return $query;
    }

    public function count_exams_by_all($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
        
        $this->db->where('a.recycle', 0);
        $this->db->where('a.pretest_posttest', 1);

        return $this->db->count_all_results('exam a');
    }

    public function get_exams_by_exam_id($exam_id)
    {
        $this->db->where('a.recycle', 0);
        $this->db->where('a.exam_id', $exam_id);
        $query = $this->db
                        ->select('a.*')
                        ->from('exam a')
                        ->get();
        return $query;
    }

    public function insert($value) 
    {
        $this->db->insert('exam', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('exam_id', $id)
                        ->update('exam', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('exam_id', $id)
                        ->update('exam', $value);
        return $query;
    }    
}
