<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exams_manage_pretest_posttest extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('courses/courses_m');
		$this->load->model('exams_manage_m');
		$this->load->model('courses_instructors/courses_instructors_m');
		$this->load->library('uploadfile_library');
    }

    private function seo(){
        $title          = 'ST Developer';
        $robots         = 'ST Developer';
        $description    = 'ST Developer';
        $keywords       = 'ST Developer';
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
        return $meta;
    }

    public function index($id="")
    {
        $data = array(
            'menu'    => 'exams',
            'seo'     => "",
            'header'  => 'header',
            'content' => 'index',
            'footer'  => 'footer',
            'function'=>  array('custom','exams_manage_pretest_posttest'),
        );

        $data['course_id'] = $id;

        $this->load->view('template/body', $data);
    }
    
    public function ajax_data_exam() 
    {
        $input = $this->input->post();
        $info = $this->exams_manage_m->get_exams_by_all($input);
        $infoCount = $this->exams_manage_m->count_exams_by_all($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->exam_id);
            
            $manage = '<a href ="'.site_url('exams-pretest-posttest-list/index/'.$rs->exam_id).'" class="btn btn-primary btn-sm "><i class="fa fa-list"></i> รายการแบบทดสอบ</a>';
           
            $active = $rs->active ? '<span class="badge badge-success">เปิด</span>' : '<span class="badge badge-danger">ปิด</span>';
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $no;
            $column[$key]['course_id']  = $rs->course_id;
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['list']       = $manage;
            $column[$key]['active']     = $active;
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['updated_at'] = $rs->updated_at;
            $column[$key]['action']     = '<div class="btn-group">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="'.base_url('exams_manage_pretest_posttest/edit/'.$rs->course_id.'/'.$id).'"><i class="fa fa-edit"></i> แก้ไข</a>
                                                    <a class="dropdown-item btn-click-delete" href="javascript:void(0)" data-id="'.$id.'"><i class="fa fa-trash"></i> ลบ</a>
                                                </div>
                                            </div>';
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function create($id)
    {
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','exams_manage_pretest_posttest/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/storage");

        //loade view
        
        $data['course_id']     = $id;

        $course = $this->courses_m->get_courses_by_course_id($id)->row();
        $data['breadcrumb'] = $course->title;

        $this->load->view('template/body', $data);
    }

    public function edit($course_id, $id = 0)
    {
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','exams_manage_pretest_posttest/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/update");

        //loade view
        
        $data['course_id']     = $course_id;

        $course = $this->courses_m->get_courses_by_course_id($course_id)->row();
        $data['breadcrumb'] = $course->title;

        $info = $this->exams_manage_m->get_exams_by_exam_id($id);
        if ( $info->num_rows() == 0) {
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        if($info->startDate!="0000-00-00" && $info->endDate!="0000-00-00" ){
            $data['dateRang'] = date('d-m-Y', strtotime($info->startDate)).' ถึง '.date('d-m-Y', strtotime($info->endDate));
        }

        $this->load->view('template/body', $data);
    }

    public function storage()
    {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->exams_manage_m->insert($value);
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['course_id']}"));
    }

    public function update()
    {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->exams_manage_m->update($id, $value);
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['course_id']}"));
    }

    private function _build_data($input)
    {
        
        $value['title']             = $input['title'];
        $value['slug']             = $input['slug'];
        $value['excerpt']           = $input['excerpt'];
        $value['course_id']         = $input['course_id'];
        $value['examTime']          = $input['examTime'];
        $value['examNo']            = $input['examNo'];
        $value['answerRandom']      = $input['answerRandom'];
        $value['examNumber']        = $input['examNumber'];
        $value['pass']              = $input['pass'];
        $value['startDate']         = $input['startDate'];
        $value['endDate']           = $input['endDate'];
        $value['active']            = $input['active'];
        $value['pretest_posttest']  = 1;
        
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['updated_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['UID'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['UID'];
        }
        return $value;
    }

    public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = base_url().'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function action(){
        
        $toastr['type']     = 'error';
        $toastr['lineOne']  = config_item('appName');
        $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success']    = false;
        $data['toastr']     = $toastr;

        $input               = $this->input->post();
        $dateTime            = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['UID'];
        $result = false;
        
        if ( $input['type'] == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['UID'];
            $result = $this->exams_manage_m->update_in($input['id'], $value);
        }
        
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    
    $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));        
    } 

}
