<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses_lesson extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('courses_lesson_m');
        $this->load->model('courses/courses_m');
        $this->load->model('exams/exams_m');
        $this->load->model('exercise/exercise_m');
        $this->load->model('courses_categories/courses_categories_m');
        $this->load->library('uploadfile_library');
        $this->load->model('seos/seos_m');
        $this->load->model('homeworks/homeworks_m');
        
    }

    private function seo()
    {

        $obj_seo = $this->seos_m->get_seos_by_display_page('courses')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : 'คอร์สเรียนออลไลน์';
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : 'คอร์สเรียนออลไลน์';
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : 'คอร์สเรียนออลไลน์';
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('articles')."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

    private function seo_detail($code)
    {

        $obj_seo=$this->courses_m->get_courses_by_slug($code)->row();
        $title          = !empty($obj_seo->metaTitle)? config_item('siteTitle').' | '.$obj_seo->metaTitle : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
        $robots         = !empty($obj_seo->metaTitle)? $obj_seo->metaTitle : 'คอร์สเรียนออลไลน์';
        $description    = !empty($obj_seo->metaDescription)? $obj_seo->metaDescription : 'คอร์สเรียนออลไลน์';
        $keywords       = !empty($obj_seo->metaKeyword)? $obj_seo->metaKeyword : 'คอร์สเรียนออลไลน์';
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('courses/'.$code)."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }
    
    public function index()
    {
        $input['active']='1';
        $input['recycle']='0';
        // get courses_lesson
		$info  = $this->courses_lesson_m->get_rows($input);
		$data['courses_lesson'] = $info->result();
        
        $this->load->view('courses_lesson', $data); 
    }

    public function courses_lesson($code = '')
    {
        // get courses_lesson
        $info  = $this->courses_lesson_m->get_courses_lesson_by_course_id($code)->result();
        if($info):
            foreach($info as $item):
                $item->info_lesson  = $this->courses_lesson_m->get_courses_lesson_by_parent_id($item->course_lesson_id)->result();
            endforeach;
        endif;

        $myCourse   = Modules::run('profile/get_mycourses_array', $code);

        $data['info'] = $info;
        $data['myCourse'] = $myCourse;

        $this->load->view('courses_lesson', $data); 
    }

    public function detail($course_id = '',$course_lesson_id = null)
    {
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo_detail($course_id),
            'header'  => 'header',
            'content' => 'detail',
            'footer'  => 'footer',
            'function'=>  array('custom','courses_lesson'),
        );

        $input_courses['slug']  = $course_id;
        $data['course_slug']  = $course_id;
        $info_courses  = $this->courses_m->get_rows($input_courses)->row();
        $data['info_courses'] = $info_courses;

        $info_categories  = $this->courses_categories_m->get_rows($info_courses->course_categorie_id)->row();
        $data['categories'] = $info_categories;

        ////เช็คเปอร์เซนการเรียน
        $lesson_nums = $this->exams_m->get_lesson_persen($course_id);
        $lesson_nums = $lesson_nums*100;
        $lesson_persen_nums = $this->exams_m->get_lesson_member_persen($course_id);
        $lesson_persen=($lesson_persen_nums['persen_all']*100)/($lesson_nums); 
        $data['lesson_persen']=$lesson_persen;

        
        $input_exam['course_id']=$info_courses->course_id;
        $input_exam['pretest_posttest']=0;
        $data['exams_final'] = $this->exams_m->get_rows($input_exam)->row_array();
        $data['quiz_final'] = $this->exams_m->get_quiz_final($data['exams_final']['exam_id']);

        $data['quiz_pretest']=1;
        $data['quiz_posttest']=1;
        $input_exam['pretest_posttest'] = 1;
        $data['exams_pretest_posttest'] = $this->exams_m->get_rows($input_exam)->row_array();
        if($info_courses->pretest_posttest==1 && !empty($data['exams_pretest_posttest'])){
            $data['quiz_pretest'] = $this->exams_m->get_quiz_pretest_posttest($data['exams_pretest_posttest']['exam_id'],'pretest');
            $data['quiz_posttest'] = $this->exams_m->get_quiz_pretest_posttest($data['exams_pretest_posttest']['exam_id'],'posttest');
        }

        $input_l['course_lesson_id'] = $course_lesson_id;
        $input_l['course_id'] = $info_courses->course_id;
        $input_l['recycle'] = 0;
        $input_l['active'] = 1;
        $data['course_content'] = $this->courses_lesson_m->get_list_byid($input_l)->row_array();
        $userId=$this->session->users['UID'];
        if($course_lesson_id = null){
             $this->plus_view_video($data['course_content']['course_lesson_id'],$userId);
        }else{
             $this->plus_view_video($course_lesson_id,$userId);
        }

        // ตรวจว่าได้อนุมัติเข้าเรียนหรือคอร์สเรียนฟรี
        $myCourse=Modules::run('profile/get_mycourses', $info_courses->course_id); 
        if($myCourse == 0  &&  $info_courses->price != 0 ){ 
            redirect_back(); exit();
        }
        //loade view
        $input['active'] = 1;
        $input['parent_id'] = 0;
        $input['recycle'] = '0';
        $input['course_id'] = $info_courses->course_id;
        // get courses_lesson
        $info  = $this->courses_lesson_m->get_rows($input)->result();
        $arr_homeworks = array();
        if($info):
            foreach($info as $item):
                $input_lesson['active'] = 1;
                $input_lesson['parent_id'] = $item->course_lesson_id;
                $input_lesson['recycle'] = 0;
                $item->info_lesson  = $this->courses_lesson_m->get_rows($input_lesson)->result();
                foreach ($item->info_lesson as $key => $value) {
                    $value->active = $this->db
                                ->select('*')
                                ->from('courses_lesson_member')
                                ->where('course_lesson_id', $value->course_lesson_id)
                                ->where('user_id',  $this->session->users['UID'] )
                                ->get()->row_array();

                    $info_homeworks = $this->homeworks_m->get_homeworks_by_user_homework_id($value->course_lesson_id, $this->session->users['UID'])->result();
                    array_push($arr_homeworks, array(
                        'title' => $value->title,
                        'homeworks' => $info_homeworks
                    ));
                    unset($info_homeworks);
                }
            endforeach;
        endif;
        $data['info'] = $info;
        $input_exercise['course_lesson_id']=$data['course_content']['course_lesson_id'];
        $data['exercise'] = $this->exercise_m->get_rows($input_exercise)->row_array();
        
        $lessonFlie = $this->courses_lesson_m->get_courses_lesson_by_course_id_all($info_courses->course_id)->result();
       
        $file_arr = array();
        if(!empty($lessonFlie)):
            foreach($lessonFlie as $item):
                if(!empty($item->fileUrl)):
                    $exe = explode('.',$item->fileUrl);
                    array_push($file_arr, array(
                            'title'    => $item->title
                            ,'fileUrl'  => $item->fileUrl
                            ,'exe'      => $exe[1]
                        )
                    );
                endif;
            endforeach;
            unset($lessonFlie);
        endif;

        $data['lessonFlie'] = $file_arr;

        $data['info_homeworks'] = $arr_homeworks;

        $this->load->view('template/body', $data);
    }

    public function lesson_persen()
    {
        $input = $this->input->post(null, true);

        $lesson_nums = $this->exams_m->get_lesson_persen($input['course_slug']);
        $lesson_nums = $lesson_nums*100;
        $lesson_persen_nums = $this->exams_m->get_lesson_member_persen($input['course_slug']);
        
        $lesson_persen=($lesson_persen_nums['persen_all']*100)/($lesson_nums); 

        $input_courses['slug']  = $input['course_slug'];
        $info_courses  = $this->courses_m->get_rows($input_courses)->row();
        
        $input_exam['course_id']=$info_courses->course_id;
        $input_exam['pretest_posttest'] = 1;
        $data['exams_pretest_posttest'] = $this->exams_m->get_rows($input_exam)->row_array();
        $data['quiz_posttest'] = $this->exams_m->get_quiz_pretest_posttest($data['exams_pretest_posttest']['exam_id'],'posttest');
        $data['lesson_persen']=$lesson_persen;
        $data['pretest_posttest']=$info_courses->pretest_posttest;
        echo json_encode($data);
    }

    public function get_course_content()
    {
        $input = $this->input->post(null, true);
        $data['course_content_l'] = $this->courses_lesson_m->get_list_byid2($input['course_lesson_id'])->row_array();
        $input_f['contentId'] = $data['course_content_l']['course_lesson_id'];

        
        $data['course_content_l']['title_'] = '<div class="title-content"><h3>'.$data['course_content_l']['title'].'</h3></div>';
        $data['course_content_l']['descript'] = '<div class="detail-content">'.html_entity_decode($data['course_content_l']['detail']).'</div>';

        $input_exercise['course_lesson_id']=$input['course_lesson_id'];
        $exercise = $this->exercise_m->get_rows($input_exercise)->row_array();

        $data['course_content_l']['exercise'] = '';
        if(!empty($exercise)){
            $data['course_content_l']['exercise'] = '<div class="title-file Bold"><h3 >แบบฝึกหัดท้ายบท</h3>'.$data['course_content_l']['title'].'</div><a id="various3" class="stop_vdo-'.$input['course_lesson_id'].'" href="'.site_url("exercise/exer-exercise/".$exercise['exercise_id']).'"><button type="button"
                                class="btn">ทำแบบฝึกหัด</button></a>';
        }

        $user_id=$this->session->users['UID'];
        $this->plus_view_video($input['course_lesson_id'],$user_id);
        $data['course_content_l']['status'] = $this->get_view_video($input['course_lesson_id'],$user_id);



        echo json_encode($data);
    }

    public function get_view_video($listId,$user_id)
    {
        $info = $this->db
                        ->select('*')
                        ->from('courses_lesson_member a')
                        ->where('a.course_lesson_id', $listId)
                        ->where('a.user_id', $user_id)
                        ->get()->row();
        return $info;
    }

    public function plus_view_video($listId,$user_id)
    {
        $info = $this->db
                        ->select('*')
                        ->from('courses_lesson_member a')
                        ->where('a.course_lesson_id', $listId)
                        ->where('a.user_id', $user_id)
                        ->get()->row();
        if(empty($info)){
            $value['course_lesson_id']=$listId;
            $value['user_id']=$user_id;
            $value['updateDate']=db_datetime_now();
            $value['persen']=0;
            $value['seconds']=0;
           $this->db->insert('courses_lesson_member', $value);
        }
    }
    
    public function update_view_video()
    {   
        $input = $this->input->post(null, true);
        $user_id=$this->session->users['UID'];
        $course_lesson_id=$input['course_lesson_id'];
        foreach ($course_lesson_id as $key => $rs) {
            $info = $this->db
                        ->select('seconds')
                        ->from('courses_lesson_member a')
                        ->where('a.course_lesson_id', $rs)
                        ->where('a.user_id', $user_id)
                        ->get()->row();

            $value['updateDate']=db_datetime_now();
            $value['status']=1;
            $value['persen']=$input['persen'][$key];
            $value['seconds']=$input['seconds'][$key];

            //if($input['seconds'][$key] > $info->seconds){
                $this->db
                  ->where('course_lesson_id', $rs)
                  ->where('user_id', $user_id)
                  ->update('courses_lesson_member', $value);
           // }
            
        }
        

        $data['status'] = 1;

        echo json_encode($data);
    }

    public function plus_view_video2()
    {   
        $input = $this->input->post(null, true);
        
        $user_id=$this->session->users['UID'];
        $course_lesson_id=$input['course_lesson_id'];
           
        $value['updateDate']=db_datetime_now();
        $value['status']=1;
        $this->db
          ->where('course_lesson_id', $course_lesson_id)
          ->where('user_id', $user_id)
          ->update('courses_lesson_member', $value);

        $data['status'] = 1;

        echo json_encode($data);
    }

    public function lesson($id = '')
    {
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'index',
            'footer'  => 'footer',
            'function'=>  array('custom','courses_lesson/index'),
        );

        $data['course_id'] = $id;

        $this->load->view('template/body', $data);
    }
    
    public function ajax_data_lesson() 
    {
        $input = $this->input->post();
        $info = $this->courses_lesson_m->get_courses_lesson_by_courses_id($input);
        $infoCount = $this->courses_lesson_m->count_courses_lesson_by_courses_id($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->course_lesson_id);
            $manage = '';
            if($rs->parent_id != 0):
                $manage.= '<a href ="'.site_url('exercise_manage/index/'.$rs->course_lesson_id).'" class="btn btn-primary btn-sm "><i class="fa fa-swatchbook"></i> แบบฝึกหัด</a> ';
                $manage.= '<a href ="'.site_url('homeworks/index/'.$rs->course_lesson_id).'" class="btn btn-success btn-sm "><i class="fa fa-book"></i> การบ้าน</a>';
                
            endif;
            $active = $rs->active ? '<span class="badge badge-success">เปิด</span>' : '<span class="badge badge-danger">ปิด</span>';
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $no;
            $column[$key]['course_id']  = $rs->course_id;
            $column[$key]['title']      = $rs->title;
            $column[$key]['manage']     = $manage;
            $column[$key]['active']     = $active;
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['updated_at'] = $rs->updated_at;
            $column[$key]['action']     = '<div class="btn-group">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="'.base_url('courses_lesson/edit/'.$rs->course_id.'/'.$id).'"><i class="fa fa-edit"></i> แก้ไข</a>
                                                    <a class="dropdown-item btn-click-delete" href="javascript:void(0)" data-id="'.$id.'"><i class="fa fa-trash"></i> ลบ</a>
                                                </div>
                                            </div>';
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function create($course_id)
    {
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','courses_lesson/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/storage");

        //loade view

        $data['type'] = array(0=>'ตัวอย่างดูฟรี', 1=>'ลงทะเบียน');
        
        $data['course_id']     = $course_id;

        $course = $this->courses_m->get_courses_by_course_id($course_id)->row();
        $data['breadcrumb'] = $course->title;

        $infoLesson = $this->courses_lesson_m->get_courses_lesson_by_parent_id_course_id($course_id, 0)->result();
        $data['infoLesson'] = $infoLesson;

        $this->load->view('template/body', $data);
    }

    public function edit($course_id, $id = 0)
    {
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','courses_lesson/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/update");

        //loade view

        $data['type'] = array(0=>'ตัวอย่างดูฟรี', 1=>'ลงทะเบียน');
        
        $data['course_id']     = $course_id;

        $course = $this->courses_m->get_courses_by_course_id($course_id)->row();
        $data['breadcrumb'] = $course->title;

        $info = $this->courses_lesson_m->get_courses_lesson_by_id($id);
        if ( $info->num_rows() == 0) {
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;

        $infoLesson = $this->courses_lesson_m->get_courses_lesson_by_parent_id_course_id($course_id, 0)->result();
        $data['infoLesson'] = $infoLesson;

        $this->load->view('template/body', $data);
    }

    public function update() 
    {

        $input = $this->input->post();
       
        $value = $this->_build_data($input);
        $input['id'] = decode_id($input['id']);
        $result = $this->courses_lesson_m->update($value, $input['id']);
        if ( $result ) {
           
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(base_url("{$this->router->class}/lesson/{$input['course_id']}"));
    }

    public function storage() 
    {
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $id = $this->courses_lesson_m->insert($value);
        $value['course_lesson_id'] = $id;
        if ( $id ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }

        redirect(base_url("{$this->router->class}/lesson/{$input['course_id']}"));
    }

    private function _build_data($input) 
    {

        $value                      = array();
        $value['course_id']         = (int)$input['course_id'];
        $value['active']            = (int)$input['active'];
        $value['parent_id']         = (int)$input['parent_id'];
        $value['type']              = (int)$input['type'];
        $value['title']             = $input['title'];
        $value['slug']              = $input['slug'];
        $value['videoLink']         = $input['videoLink'];
        $value['videoLength']       = $input['videoLength'];
        $value['detail']            = html_escape($input['detail']);
        $value['excerpt']           = html_escape($input['excerpt']);
        $value['fileTypeUpload']    = isset($input['fileTypeUpload']) ? $input['fileTypeUpload'] : null;
        $value['fileUrl']           = isset($input['fileUrl']) ? $input['fileUrl'] : null;
        if($input['fileTypeUpload']=='1'){
            $path        = 'courses_lesson';
            $upload      = $this->uploadfile_library->do_upload('file',TRUE,$path);
            $file        = '';
            if(isset($upload['index'])){
                $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
                $outfile = $input['outfile'];
                if(isset($outfile)){
                    $this->load->helper("file");
                    unlink($outfile);
                }
                $value['fileUrl'] = $file;
            }
        }
       
        if ($input['mode'] == 'create') {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['UID'];
            $value['updated_at'] = db_datetime_now();
        } else {
            $value['updated_by'] = $this->session->users['UID'];
            $value['updated_at'] = db_datetime_now();
        }

        return $value;
    }

    public function sumernote_img_upload()
    {
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids)
    {

		$arrayName = array('fileUrl' => $ids);
		echo json_encode($arrayName);
    }
    
    public function action()
    {
        
        $toastr['type']     = 'error';
        $toastr['lineOne']  = config_item('appName');
        $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success']    = false;
        $data['toastr']     = $toastr;

        $input               = $this->input->post();
        $dateTime            = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['UID'];
        $result = false;
        
        if ( $input['type'] == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['UID'];
            $result = $this->courses_lesson_m->update_in($input['id'], $value);
        }
        
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 
}
