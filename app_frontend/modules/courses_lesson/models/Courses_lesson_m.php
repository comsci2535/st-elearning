<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Courses_lesson_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param){
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        if ( isset($param['course_id']) ) 
            $this->db->where('a.course_id', $param['course_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);
        
        if ( isset($param['slug']) )
            $this->db->where('a.slug', $param['slug']);
        
        if ( isset($param['parent_id']) )
        $this->db->where('a.parent_id', $param['parent_id']);
        
        $this->db->order_by('order', 'asc')
                ->order_by('parent_id', 'asc');       
        
        $query = $this->db
                        ->select('a.*')
                        ->from('courses_lesson a')
                        ->get();
        return $query;
    }

    public function count_rows($param) 
    {

        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);
    
        return $this->db->count_all_results('courses_lesson a');
    }

    public function get_list_byid($param)
    {   

        $this->_condition_byid($param);
    
        $query = $this->db
                        ->select('a.*')
                        ->from('courses_lesson a')
                        ->get();
        return $query;
    }

    private function _condition_byid($param)
    {   
        
        if ( isset($param['active']) ) {
            $this->db->where('a.active', $param['active']);
        }   

        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']); 

        $this->db->where('a.parent_id !=', 0);   

        if ( isset($param['course_lesson_id']) && $param['course_lesson_id']!=0 )
            $this->db->where('a.course_lesson_id', $param['course_lesson_id']); 

        if ( isset($param['slug']) && $param['slug']!="" )
            $this->db->where('a.slug', $param['slug']);       

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

        $this->db->order_by('a.order', 'asc');


    }
    
    public function get_list_byid2($course_lesson_id)
    {   

        $this->db->where('course_lesson_id', $course_lesson_id); 
    
        $query = $this->db
                        ->select('*')
                        ->from('courses_lesson')
                        ->get();
        return $query;
    }

    // new function query

    public function count_courses_lesson_all($parent_id = 0) 
    {
        $this->db->where('parent_id', $parent_id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('courses_lesson');
    }

    public function count_courses_lesson_by_course_id($id, $parent_id = 0) 
    {
        $this->db->where('course_id', $id);
        $this->db->where('parent_id', $parent_id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('courses_lesson');
    }

    public function get_courses_lesson_by_parent_id_course_id($id, $parent_id = 0) 
    {
        $this->db->where('course_id', $id);
        $this->db->where('parent_id', $parent_id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);

        $query = $this->db->get('courses_lesson');
        return $query;
    }

    public function get_courses_lesson_by_course_id_all($id) 
    {
        $this->db->where('course_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $query = $this->db->get('courses_lesson');
        return $query;
    }

    public function get_courses_lesson_by_parent_id($parent_id = 0) 
    {
        $this->db->where('parent_id', $parent_id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('order', 'asc');
        $query = $this->db->get('courses_lesson');
        return $query;
    }

    public function get_courses_lesson_by_id($course_lesson_id) 
    {
        $this->db->where('course_lesson_id', $course_lesson_id);
        $this->db->where('recycle', 0);
        // $this->db->where('active', 1);
        $query = $this->db->get('courses_lesson');
        return $query;
    }

    public function get_courses_lesson_by_course_id($course_id) 
    {
        $this->db->select('a.*');
        $this->db->select('b.slug as course_slug');
        $this->db->where('a.course_id', $course_id);
        $this->db->where('a.parent_id', 0);
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->join('courses b','b.course_id=a.course_id','left');
        $this->db->order_by('a.order', 'asc');
        $query = $this->db->get('courses_lesson a');
                          
        return $query;
    }

    public function get_courses_lesson_member_by_id($course_lesson_id, $user_id='') 
    {
        if(!empty($user_id)):
            $this->db->where('user_id', $user_id);
        endif;
        $this->db->where('course_lesson_id', $course_lesson_id);
        $query = $this->db->get('courses_lesson_member');
        return $query;
    }

    public function get_courses_lesson_by_slug($slug, $parent_id)
    {
        $this->db->where('slug', $slug);
        $this->db->where('parent_id', $parent_id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('courses_lesson');
        
        return $query;
    }

    public function get_courses_lesson_by_courses_id($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.title";

            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
        
        $this->db->where('a.recycle', 0);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        $query = $this->db
                        ->select('a.*')
                        ->from('courses_lesson a')
                        ->get();
        return $query;
    }

    public function count_courses_lesson_by_courses_id($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
        
        $this->db->where('a.recycle', 0);

        return $this->db->count_all_results('courses_lesson a');
    }

    public function insert($value)
    {
        $this->db->insert('courses_lesson', $value);
        return $this->db->insert_id();
    }
    
    public function delete($param)
    {
        $rs = $this->db
                    ->where_in('course_lesson_id', $param)
                    ->delete('courses_lesson');
        return $rs;
    }
    
    public function update($value, $id)
    {
        $rs = $this->db
                    ->where('course_lesson_id', $id)
                    ->update('courses_lesson', $value);
        return $rs;        
    }  
    
    public function update_order($value)
    {
        $rs = $this->db
                    ->update_batch('courses_lesson', $value, 'course_lesson_id');
        return $rs;   
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('course_lesson_id', $id)
                        ->update('courses_lesson', $value);
        return $query;
    }
     
}
