<?php  if(!empty($info)): ?>
<div class="Bold my-4" id="scoll2">
    <h2>เนื้อหาคอร์สเรียน</h2>
</div>
<div class="courses_lerning">
    <?php foreach($info as $item): ?>
    <div class="boox">
        <div class="head Bold">
            <span><?php echo !empty($item->title)? $item->title : '' ?></span>
        </div>
       <?php
        if(!empty($item->info_lesson)):
            foreach($item->info_lesson as $lessons):

            if(!empty($myCourse) && $myCourse['status']==1){ 
        ?>
         <a href="<?=site_url("courses-lesson/{$item->course_slug}/{$lessons->course_lesson_id}");?>" >
        <div class="listt">
            <div class="name Bold">
                <i class="fas fa-play"></i>
                <span><?php echo !empty($lessons->title)? $lessons->title:'';?></span>
            </div>
            <div class="rigth">
                <?php 
                    if($lessons->type==0):
                ?>
                    <span class="e">ตัวอย่างดูฟรี</span>
                    <span class="a2">
                       
                            <i class="fa fa-play-circle"></i>
                       
                    </span>
                <?php 
                    endif; 
                ?>
                <?php
                if(!empty($lessons->fileUrl)):
                ?>
                    <span class="i"><i class="fas fa-file-alt"></i></span>
                <?php
                endif;
                ?>
                <span class="m Bold"><?php echo !empty($lessons->videoLength)? $lessons->videoLength:'';?> นาที</span>
            </div>
        </div>
         </a>
        <?php }else{ ?>

        <div class="listt">
            <div class="name Bold">
                <i class="fas fa-play"></i>
                <span><?php echo !empty($lessons->title)? $lessons->title:'';?></span>
            </div>
            <div class="rigth">
                <?php 
                    if($lessons->type==0):
                ?>
                    <span class="e">ตัวอย่างดูฟรี</span>
                    <span class="a2">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#contentVideo-<?php echo $lessons->course_lesson_id; ?>">
                            <i class="fa fa-play-circle"></i>
                        </a>
                    </span>
                <?php 
                    endif; 
                ?>
                 <?php
                if(!empty($lessons->fileUrl)):
                ?>
                    <span class="i"><i class="fas fa-file-alt"></i></span>
                <?php
                endif;
                ?>
                <span class="m Bold"><?php echo !empty($lessons->videoLength)? $lessons->videoLength:'';?> นาที</span>
            </div>
        </div>

        <?php 
            if($lessons->type==0) :
        ?>
            <!-- Modal -->
            <div class="modal fade" id="contentVideo-<?php echo $lessons->course_lesson_id ?>" tabindex="-1"
                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="exampleModalLabel"><?php echo $lessons->title ?></h3>
                            <button type="button" class="close" id="pause-button"
                                onclick="pausePlayer(<?php echo $lessons->course_lesson_id ?>)">
                                ปิด X
                            </button>

                        </div>
                        <div class="modal-body">
                            <div class="embed-container">
                                <div data-vimeo-id="<?php echo $lessons->videoLink; ?>"
                                    id="handstick-c-<?php echo $lessons->course_lesson_id ?>"
                                    class="handstick-c"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php 
            endif;
        ?>

        <?php

        }
            endforeach;
        endif;
        ?>
    </div>
    <?php
        endforeach;
    
    ?>
</div>
<?php endif; ?>