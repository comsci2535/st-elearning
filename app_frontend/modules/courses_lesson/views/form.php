<section class="Sec_Courses">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 pt-4">
                <h4 class="pt-4">
                    <a href="<?=site_url('courses_lesson/lesson/'.$info->course_id);?>">ชื่อเนื้อหาทั้งหมด</a> > <span><?php echo !empty($breadcrumb)? $breadcrumb : '';?></span>
                </h4>
                <hr>
            </div>
            
            <div class="col-sm-12 mb-5">
                <div class="col-sm-12">
                    <form class="form-horizontal frm-create" method="post" action="<?=$frmAction?>" autocomplete="off" enctype="multipart/form-data">
                        
                        <?php
                        if($this->router->method == 'edit'):
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">สถานะ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <label><input <?php if(isset($info->active) && $info->active== 1){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="1" > เปิด</label>
                                <label><input <?php if(isset($info->active) && $info->active== 0){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
                            </div>
                        </div>
                        <?php
                        endif;
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="parent_id">เนื้อหาหลัก <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <select  id="parent_id" name="parent_id" class="form-control select2" required>
                                    <option value="">เลือกรายการ</option>
                                    <option value="0" <?php echo ($info->parent_id == 0) ? 'selected' :'';?>>ไม่กำหนด</option>
                                    <?php
                                        if(!empty($infoLesson)):
                                            foreach($infoLesson as $key => $lesson):
                                                $selected = '';
                                                if($info->parent_id == $lesson->course_lesson_id):
                                                    $selected = 'selected';
                                                endif;
                                    ?>
                                            <option value="<?=$lesson->course_lesson_id?>" <?= $selected?>><?=$lesson->title?></option>
                                    <?php
                                            endforeach;
                                        endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ชื่อเนื้อหา <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="-input-title" required>
                                    <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                            </div>
                        </div>
                        <div id="c-content" style="display: block;">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">รายละเอียดย่อ <span class="text-danger"> *</span></label>
                                <div class="col-sm-10"> 
                                    <textarea name="excerpt" rows="3" class="form-control" id="excerpt" ><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="detail">รายละเอียด <span class="text-danger"> *</span></label>
                                <div class="col-sm-10"> 
                                    <textarea name="detail" rows="3" class="form-control summernote" id="detail" ><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="type">ประเภทเนื้อหา <span class="text-danger"> *</span></label>
                                <div class="col-sm-10"> 
                                    <select  id="type" name="type"  class="form-control select2" required>
                                        <option value="">เลือก</option>
                                        <?php
                                            if(!empty($type)):
                                                foreach($type as $key => $item):
                                                    $selected = '';
                                                    if($info->type == $key):
                                                        $selected = 'selected';
                                                    endif;
                                        ?>
                                                <option value="<?=$key?>" <?=$selected?>><?=$item?></option>
                                        <?php
                                                endforeach;
                                            endif;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">ไฟล์เอกสาร <span class="text-danger"> *</span></label>
                                <div class="col-sm-10"> 
                                    <input class="icheck" name="fileTypeUpload" id="rd_upload_type_system" value="1" type="radio" <?php  if(!empty($info->fileTypeUpload) && $info->fileTypeUpload == 1){ echo "checked"; } ?> <?php if(empty($info->fileTypeUpload)){ echo "checked"; } ?>  /> อัพโหลดผ่านหน้าเว็บ 
                                    <input class="icheck" name="fileTypeUpload" id="rd_upload_type_record" value="2" type="radio" <?php  if(!empty($info->fileTypeUpload) && $info->fileTypeUpload == 2){ echo "checked"; } ?> /> ลิงก์ไฟล์       
                                </div>
                            </div>
                            <div class="form-group" id="f-file">
                                <label class="control-label col-sm-2" for="pwd">ไฟล์เอกสาร <span class="text-danger"> *</span></label>
                                <div class="col-sm-10"> 
                                    <input id="file" name="file" type="file" data-preview-file-type="text">
                                    <input type="hidden" name="outfile" value="<?=(isset($info->fileUrl)) ? $info->fileUrl : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group" id="f-url" style="display: none">
                                <label class="control-label col-sm-2" for="pwd">ไฟล์เอกสาร <span class="text-danger"> *</span></label>
                                <div class="col-sm-10"> 
                                    <input value="<?php echo isset($info->fileUrl) ? $info->fileUrl : NULL ?>" type="text" id="input-fileUrl" class="form-control" name="fileUrl" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="course_id" id="input-course_id" value="<?php echo $course_id ?>">
                                <input type="hidden" class="form-control" name="db" id="db" value="courses_lesson">
                                <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                                <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->course_lesson_id) ? encode_id($info->course_lesson_id) : 0 ?>">
                                <button type="submit" class="btn btn-success">บันทึก</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->fileUrl)) ? base_url().$info->fileUrl : ''; ?>';
    var file_id         = '<?=(isset($info->course_lesson_id)) ? $info->course_lesson_id : ''; ?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/'+file_id;

</script>