<section class="courses_learning">

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="text-center Bold mt-5 mb-2">
                    <h1><?php echo !empty($info_courses->title) ? $info_courses->title : '' ?></h1>
                </div>
            </div>
            <div class="col-md-8 detail_left">
                <div class="video">
                    <div class="embed-container">
                        <!--<div class="dd">
                        </div>-->
                        <div style="height: 460px">
                            <iframe id="youtube"
                                    width="560" height="315"
                                    src="https://www.youtube.com/embed/4bPabz6hDkU"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                    </div>

                    <input type="hidden" name="course_lesson_id" id="course_lesson_id"
                           value="<?php echo $course_content['course_lesson_id']; ?>">
                </div>
                <div class="courses-detail">
                    <h2 class="Bold title-content"><?php echo $course_content['title']; ?></h2>
                    <div id="text-row-course-lesson" class="">
                        <div class="detail-content"><?php echo html_entity_decode($course_content['detail']) ?></div>
                    </div>
                    <!-- <a href="javascript:void(0)" id="overlate" class="overlate-hide" data-hide="0" data-line="text-row-course-lesson">
                        <i class="fas fa-plus"></i> ดูเพิ่มเติม
                    </a> -->
                </div>

            </div>

            <div class="col-md-4 detail_rigth">
                <div class="courses_lerning">
                    <div class="names">
                        <span style="font-size: 24px;">เนื้อหาคอร์สเรียน</span>
                        <span id="exams-final-p">
                             <a id="final-exams-p" href="<?php echo site_url() ?>" style="display: none;"></a>
                         </span>
                        <span id="exams-final" class="float-right">
                        <?php if (!empty($exams_final)) { ?>
                            <?php if ($lesson_persen >= 85 && empty($quiz_final)) { ?>
                                <a id="final-exams"
                                   href="<?php echo site_url('exams/index/' . $course_slug . '/' . $exams_final['exam_id']) ?>"><button
                                            type="button" class="btn-2  btn-success">ทำแบบทดสอบคอร์ส</button></a>
                            <?php } elseif ($lesson_persen >= 85 && !empty($quiz_final)) { ?>
                                <a id="final-exams"
                                   href="<?php echo site_url('exams/index/' . $course_slug . '/' . $exams_final['exam_id']) ?>"><button
                                            type="button" class="btn-2  btn-red">ทำแบบทดสอบคอร์ส</button></a>
                            <?php } else { ?>
                                <!-- <button type="button" class="btn">ทำแบบทดสอบคอร์ส</button> -->
                                <a id="final-exams"
                                   href="<?php echo site_url('exams/index/' . $course_slug . '/' . $exams_final['exam_id']) ?>"
                                   style="display: none;"><button type="button" class="btn-2  btn-success">ทำแบบทดสอบคอร์ส</button></a>
                                <button id="final-exams-b" type="button" class="btn" data-toggle="tooltip"
                                        data-placement="top" title="กรุณาเรียนให้ครบทุกบทเรียน เพื่อรับเกียรติบัตร">ทำแบบทดสอบคอร์ส</button>

                            <?php } ?>
                        <?php } ?>
                        </span>
                    </div>
                    <div class="boox">
                        <?php foreach ($info as $key => $rs) { ?>
                            <div class="head Bold">
                                <span><?php echo $rs->title ?></span>
                            </div>
                            <?php foreach ($rs->info_lesson as $key_ => $rs_) { ?>
                                <div class="listt" id="listt-<?php echo $rs_->course_lesson_id ?>">
                                    <a href="javascript:void(0)" id="vimeo-<?php echo $rs_->course_lesson_id ?>"
                                       class="vimeo-b">
                                        <div class="name Bold">

                                            <div id="specificChart-<?php echo $rs_->course_lesson_id ?>"
                                                 class="donut-size">
                                                <div class="pie-wrapper">
                                    <span class="label">
                                        <span class="num">0</span><span class="smaller">%</span>
                                    </span>
                                                    <div class="pie">
                                                        <div class="left-side half-circle"></div>
                                                        <div class="right-side half-circle"></div>
                                                    </div>
                                                    <div class="shadow-c"></div>
                                                </div>
                                            </div>
                                            <input type="hidden"
                                                   name="course_lesson_id[<?php echo $rs_->course_lesson_id ?>]"
                                                   class="input_course_lesson_id"
                                                   value="<?php echo $rs_->course_lesson_id ?>">
                                            <input type="hidden" name="persen[<?php echo $rs_->course_lesson_id ?>]"
                                                   id="persen-<?php echo $rs_->course_lesson_id ?>" class="input_persen"
                                                   value="<?php echo $rs_->active['persen'] ?>">
                                            <input type="hidden" name="seconds[<?php echo $rs_->course_lesson_id ?>]"
                                                   id="seconds-<?php echo $rs_->course_lesson_id ?>"
                                                   class="input_seconds" value="<?php echo $rs_->active['seconds'] ?>">
                                            <span class="course-lesson-title"><?php echo $rs_->title ?></span>
                                        </div>
                                        <div class="rigth">
                                            <span class="m Bold"><?php echo $rs_->videoLength ?> นาที</span>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>

                    <div class="names" style="padding-top: 15px">
                        <div>
                            <h3 class="title-file Bold">ดาวน์โหลดเอกสาร</h3>
                            <div class="boox">
                                <?php
                                foreach ($lessonFlie as $key => $rs) {
                                    ?>
                                    <p><a href="<?= base_url() . $rs['fileUrl']; ?>" target="_blank"><i
                                                    class="fas fa-file-alt"
                                                    style="color: blue;"></i> <?= $rs['title'] . '.' . $rs['exe']; ?>
                                        </a></p>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="names" style="padding-top: 15px">
                        <div class="courses-exercise">
                         <span id="exams-final">
                            <div class="title-file Bold"><h3>แบบฝึกหัดท้ายบท</h3></div>
                        <?php if (!empty($exercise)) { ?>  <a class="various3"
                                                              href="<?= site_url("exercise/index2/" . $course_content['slug'] . '/' . $exercise['exercise_id']) ?>"><button
                                    type="button"
                                    class="btn ">ทำแบบฝึกหัด</button></a> <?php } ?>
                        </span>
                        </div>
                    </div>

                    <div class="names" style="padding-top: 15px">
                        <div>
                            <h3 class="title-file Bold">การบ้าน</h3>
                            <div class="boox">
                                <?php

                                foreach ($info_homeworks as $key => $item):
                                    ?>
                                    <h4><?= $item['title']; ?></h4>
                                    <ul>
                                        <?php
                                        foreach ($item['homeworks'] as $list):
                                            if ($list->qty > 0):
                                                ?>
                                                <li><p><?= $list->title; ?> <br><a href="javascript:void(0)"
                                                                                   style="color: red;"><i
                                                                    class="fa fa-check" aria-hidden="true"></i> ส่งแล้ว
                                                    </p></a></li>
                                            <?php
                                            else:
                                                ?>
                                                <li><p><?= $list->title; ?> <br><a href="javascript:void(0)"
                                                                                   class="btn-click-homeworks"
                                                                                   data-id="<?= $list->courses_homework_id ?>"
                                                                                   data-name="<?= $list->title; ?>"><i
                                                                    class="fa fa-paper-plane" aria-hidden="true"></i>
                                                            ส่งการบ้าน</p></a></li>
                                            <?php
                                            endif;
                                        endforeach;
                                        ?>
                                    </ul>
                                    <hr>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- The Modal -->
<div class="modal fade" id="modal-homeworks">
    <div class="modal-dialog">
        <?php echo form_open_multipart(site_url('homeworks/send'), array('class' => '', 'id' => 'frm-homework', 'method' => 'post')) ?>
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">ส่งการบ้าน</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <h4 for="">คำถาม : </h4>
                    <p id="text-data-name"></p>
                    <input type="hidden" id="courses_homework_id" name="courses_homework_id">
                </div>
                <div class="form-group">
                    <label for="">คำตอบ <span class="text-danger"> *</span></label>
                    <textarea name="detail" id="detail" class="form-control" cols="20" rows="5"
                              placeholder="กรุณาระบุคำตอบ" required></textarea>

                </div>
                <div class="form-group">
                    <label for="">ไฟล์เอกสาร</label>
                    <input type="file" class="form-control" id="file" name="file">
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="hidden" name="mode" value="create">
                <input type="hidden" name="slug" value="<?= $course_slug ?>">
                <button type="submit" class="btn btn-success"><i class="fa fa-paper-plane" aria-hidden="true"></i>
                    ส่งการบ้าน
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิดหน้าต่าง</button>
            </div>

        </div>
        <?php echo form_close() ?>
    </div>
</div>