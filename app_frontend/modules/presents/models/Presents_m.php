<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Presents_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param){
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        if ( isset($param['present_id']) ) 
            $this->db->where('a.present_id', $param['present_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);
            
        $query = $this->db
                        ->select('a.*')
                        ->from('presents a')
                        ->get();
        return $query;
    }
}
