<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Certificates_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_certificates_all($length = 0, $start = 0)
    {
        
        $this->db->limit($length, $start);
        $this->db->where('active', 1);
        $this->db->where('recycle', 0);
        $this->db->select('*');
        $query = $this->db->get('certificates');
        return $query;
    }

    public function get_certificates_by_id($certificate_id)
    {
        $this->db->where('certificate_id', $certificate_id);
        $this->db->where('active', 1);
        $this->db->where('recycle', 0);
        $this->db->select('*');
        $query = $this->db->get('certificates');
        return $query;
    }

    public function get_certificates_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        $this->db->where('active', 1);
        $this->db->where('recycle', 0);
        $this->db->select('*');
        $query = $this->db->get('certificates');
        return $query;
    }

    public function get_certificates_by_option_all($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('title', $param['search']);
            $this->db->or_like('excerpt', $param['search']);
            $this->db->or_like('detail', $param['search']);
        endif;

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('certificates');
        
        return $query;
    }

    public function get_certificates_by_coursesid($courses_id=null)
    {
        if (isset($courses_id))
            $this->db->where('courses_id', $courses_id);
        $query = $this->db
                        ->from('certificates')
                        ->get();
        return $query;
    }

    public function count_certificates_by_courses_id($courses_id)
    {
        $this->db->where('courses_id', $courses_id);
        $this->db->from('certificates');
        return $this->db->count_all_results();
    }
}
