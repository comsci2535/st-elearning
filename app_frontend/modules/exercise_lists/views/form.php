<section class="Sec_Courses">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 pt-4">
                <h4 class="pt-4">
                    <a href="<?=site_url('exercise_lists/index/'.$exercise_id);?>"> แบบทดสอบทั้งหมด</a>
                </h4>
                <hr>
            </div>
            
            <div class="col-sm-12 mb-5">
                <div class="col-sm-12">
                    <form class="form-horizontal frm-create" method="post" action="<?=$frmAction?>" autocomplete="off" enctype="multipart/form-data">
                        
                        
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">คำถาม <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo isset($info->exercise_topic_name) ? $info->exercise_topic_name : NULL ?>" type="text" id="input-exercise_topic_name" class="form-control" name="exercise_topic_name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">
                                <input class="icheck" name="answer_true[]" id="answer_true1" value="0" type="radio"  <?php if(!empty($item_answer[0]) && $item_answer[0]->point=='1' ){ ?>  checked='checked'  <?php } ?>/> </label>
                            </label>
                            <div class="col-sm-6">
                                <input type="hidden" class="form-control" id="exercise_answer_id" name="exercise_answer_id[]" value="<?php if(!empty($item_answer[0])){ echo $item_answer[0]->exercise_answer_id ; } ?>" >
                                <input type="text" class="form-control" id="exercise_answer_name1" name="exercise_answer_name[]" value="<?php if(!empty($item_answer[0])){ echo $item_answer[0]->exercise_answer_name ;  }?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label icheck-inline">
                                <input class="icheck" name="answer_true[]" id="answer_true2" value="1" type="radio"  <?php if(!empty($item_answer[1]) && $item_answer[1]->point=='1' ){ ?>  checked='checked'  <?php } ?>  />
                            </label>
                            <div class="col-sm-6">
                                <input type="hidden" class="form-control" id="exercise_answer_id" name="exercise_answer_id[]" value="<?php if(!empty($item_answer[1])){ echo $item_answer[1]->exercise_answer_id ; } ?>" >
                                <input type="text" class="form-control" id="exercise_answer_name2" name="exercise_answer_name[]" value="<?php if(!empty($item_answer[1])){ echo $item_answer[1]->exercise_answer_name ;  }?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label icheck-inline">
                                <input class="icheck" name="answer_true[]" id="answer_true3" value="2" type="radio"   <?php if(!empty($item_answer[2]) && $item_answer[2]->point=='1' ){ ?>  checked='checked'  <?php } ?>   />
                            </label>
                            <div class="col-sm-6">
                            <input type="hidden" class="form-control" id="exercise_answer_id" name="exercise_answer_id[]" value="<?php if(!empty($item_answer[2])){ echo $item_answer[2]->exercise_answer_id ; } ?>" >
                                <input type="text" class="form-control" id="exercise_answer_name3" name="exercise_answer_name[]" value="<?php if(!empty($item_answer[2])){ echo $item_answer[2]->exercise_answer_name ;  }?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label icheck-inline">
                                <input class="icheck" name="answer_true[]" id="answer_true4" value="3" type="radio"  <?php if(!empty($item_answer[3]) && $item_answer[3]->point=='1' ){ ?>  checked='checked'  <?php } ?>    />
                             </label>
                            <div class="col-sm-6">
                            <input type="hidden" class="form-control" id="exercise_answer_id" name="exercise_answer_id[]" value="<?php if(!empty($item_answer[3])){ echo $item_answer[3]->exercise_answer_id ; } ?>" >
                                <input type="text" class="form-control" id="exercise_answer_name4" name="exercise_answer_name[]" value="<?php if(!empty($item_answer[3])){ echo $item_answer[3]->exercise_answer_name ;  }?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label icheck-inline">
                                <input class="icheck" name="answer_true[]" id="answer_true5" value="4" type="radio"   <?php if(!empty($item_answer[4]) && $item_answer[4]->point=='1' ){ ?>  checked='checked'  <?php } ?>   />
                            </label>
                            <div class="col-sm-6">
                            <input type="hidden" class="form-control" id="exercise_answer_id" name="exercise_answer_id[]" value="<?php if(!empty($item_answer[4])){ echo $item_answer[4]->exercise_answer_id ; } ?>" >
                                <input type="text" class="form-control" id="exercise_answer_name5" name="exercise_answer_name[]" value="<?php if(!empty($item_answer[4])){ echo $item_answer[4]->exercise_answer_name ;  }?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">สถานะ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <label><input <?php if(isset($info->active) && $info->active== 1){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="1" checked> เปิด</label>
                                <label><input <?php if(isset($info->active) && $info->active== 0){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
                            </div>
                        </div>
                       
                        <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="exercise_id" id="input-course_id" value="<?php echo $exercise_id ?>">
                                <input type="hidden" class="form-control" name="db" id="db" value="exercise_topic">
                                <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                                <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->exercise_topic_id) ? encode_id($info->exercise_topic_id) : 0 ?>">
                                <button type="submit" class="btn btn-success">บันทึก</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>
