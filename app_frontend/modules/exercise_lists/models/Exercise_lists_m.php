<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Exercise_lists_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_exercise_list_by_all($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.exercise_topic_name', $param['search']['value'])
                    ->or_like('a.subject', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.exercise_topic_name";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.subject";
            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        if ( isset($param['exercise_topic_id']) )
            $this->db->where('a.exercise_topic_id', $param['exercise_topic_id']);
        
        if ( isset($param['exercise_id']) ) 
            $this->db->where('a.exercise_id', $param['exercise_id']);
        
        $this->db->where('a.recycle', 0);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        $query = $this->db
                        ->select('a.*')
                        ->from('exercise_topic a')
                        ->get();
        return $query;
    }

    public function get_exercise_list_by_exercise_topic_id($exercise_topic_id)
    {
        
        $this->db->where('a.exercise_topic_id', $exercise_topic_id);
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('exercise_topic a')
                        ->get();
        return $query;
    }

    public function count_exercise_list_by_all($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.exercise_topic_name', $param['search']['value'])
                    ->or_like('a.subject', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['exercise_topic_id']) )
            $this->db->where('a.exercise_topic_id', $param['exercise_topic_id']);
        
        if ( isset($param['exercise_id']) ) 
            $this->db->where('a.exercise_id', $param['exercise_id']);

        $this->db->where('a.recycle', 0);

        return $this->db->count_all_results('exercise_topic a');
    }
    
    public function insert($value) {
        $this->db->insert('exercise_topic', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db->where('exercise_topic_id', $id)->update('exercise_topic', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db->where_in('exercise_topic_id', $id)->update('exercise_topic', $value);
        return $query;
    }  

    public function get_exercise_answerID($id=0)
    {
        $sql = "select * from exercise_answer where exercise_topic_id =".$id."";
        $query = $this->db->query($sql);
        return $query->result();
    }  

    public function get_exercise_type()
    {
        return $this->db->get('exam_type')->result();
    }

    public function update_answer($id, $value)
    {
        $this->db->where('exercise_topic_id', $id)->delete('exercise_answer');
        if(!empty($value)){
            $query = $this->db->insert_batch('exercise_answer', $value);
            return $query;
        }
    } 
}
