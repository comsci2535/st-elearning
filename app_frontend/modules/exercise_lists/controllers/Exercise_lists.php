<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exercise_lists extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('courses/courses_m');
        $this->load->model('exercise_lists_m');
        $this->load->model('exercise/exercise_m');
        
		$this->load->library('uploadfile_library');
    }

    private function seo(){
        $title          = 'ST Developer';
        $robots         = 'ST Developer';
        $description    = 'ST Developer';
        $keywords       = 'ST Developer';
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
        return $meta;
    }
    
    
    public function index($id="")
    {
        $data = array(
            'menu'    => 'exams',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'index',
            'footer'  => 'footer',
            'function'=>  array('custom','exercise_lists'),
        );

        $data['exercise_id'] = $id;

        $this->load->view('template/body', $data);
    }
    
    public function ajax_data_exercise() 
    {
        $input = $this->input->post();
        $info = $this->exercise_lists_m->get_exercise_list_by_all($input);
        $infoCount = $this->exercise_lists_m->count_exercise_list_by_all($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->exercise_topic_id);

            $active = $rs->active ? '<span class="badge badge-success">เปิด</span>' : '<span class="badge badge-danger">ปิด</span>';
            
            $answer_data="";
            $exam_answer = $this->exercise_lists_m->get_exercise_answerID($rs->exercise_topic_id);
            if($exam_answer){
                foreach ($exam_answer as $key2 => $answer_) {
                    if($answer_->point=="1"){
                       $answer_data.='<label class="control-label text-success" for="inputSuccess"><i class="fa fa-check"></i> '.$answer_->exercise_answer_name.'</label></br>';
                    }else{
                        $answer_data.='<label class="control-label" for="inputError"><i class="fa fa-times"></i> '.$answer_->exercise_answer_name.'</label></br>';
                    }
                }
            }else{
                $answer_data.='-';
            }

            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $no;
            $column[$key]['title']      = $rs->exercise_topic_name;
            $column[$key]['excerpt']    = $answer_data;
            $column[$key]['active']     = $active;
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['updated_at'] = $rs->updated_at;
            $column[$key]['action']     = '<div class="btn-group">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="'.base_url('exercise_lists/edit/'.$input['exercise_id'].'/'.$id).'"><i class="fa fa-edit"></i> แก้ไข</a>
                                                    <a class="dropdown-item btn-click-delete" href="javascript:void(0)" data-id="'.$id.'"><i class="fa fa-trash"></i> ลบ</a>
                                                </div>
                                            </div>';
        }

        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function create($id)
    {
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','exams_lists/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/storage");

        //loade view
        
        $data['exercise_id']     = $id;

        $this->load->view('template/body', $data);
    }

    public function edit($exercise_id, $id = 0)
    {
        $id = decode_id($id);
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','exams_lists/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/update");

        //loade view
        
        $data['exercise_id']     = $exercise_id;

        $info = $this->exercise_lists_m->get_exercise_list_by_exercise_topic_id($id);
        if ( $info->num_rows() == 0) {
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;

        $data['item_answer']=$this->exercise_lists_m->get_exercise_answerID($info->exercise_topic_id);
        $input_['exercise_id']  = $exercise_id;
        $info_              = $this->exercise_m->get_rows($input_);
        $info_              = $info_->row();
        $data['info_']      = $info_;


        $this->load->view('template/body', $data);
    }

    public function storage()
    {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->exercise_lists_m->insert($value);
        if ( $result ) {
            $value_a = $this->_build_data_answer($result,$input);
            $this->exercise_lists_m->update_answer($result, $value_a);
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['exercise_id']}"));
    }

    public function update()
    {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->exercise_lists_m->update($id, $value);
        if ( $result ) {
            $value_a = $this->_build_data_answer($id,$input);
            $this->exercise_lists_m->update_answer($id, $value_a);
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['exercise_id']}"));
    }

    private function _build_data($input)
    {
        
        $value['exercise_topic_name'] = $input['exercise_topic_name'];
        $value['exercise_type_id'] = 2;
        $value['exercise_id'] = $input['exercise_id'];
        $value['publish'] = 1;
        $value['active'] = $input['active'];
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['updated_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['UID'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['UID'];
        }
        return $value;
    }

    private function _build_data_answer($id,$input) 
    {
        
        $exercise_answer_name   = $this->input->post('exercise_answer_name');
        $exercise_answer_id     = $this->input->post('exercise_answer_id');
        $answer_true        = $this->input->post('answer_true');

        $value = array();

        if ( isset($input['exercise_answer_name']) ) {
            foreach ( $input['exercise_answer_name'] as $key => $rs ) {
                if($answer_true[0]==$key){  $point="1"; }else{  $point="0";  }
                if($rs!=""){
                    $value[] = array(
                        'exercise_topic_id'     => $id,
                        'exercise_answer_name'  => $rs,
                        'point'             => $point,
                        'order_number'      => ($key+1),
                        'updated_at'        => db_datetime_now(),
                        'updated_by'        => $this->session->users['user_id']
                    );
                }
                
            }
        }
        
        return $value;
    }

    public function sumernote_img_upload()
    {
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = base_url().'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function action()
    {
        
        $toastr['type']     = 'error';
        $toastr['lineOne']  = config_item('appName');
        $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success']    = false;
        $data['toastr']     = $toastr;

        $input               = $this->input->post();
        $dateTime            = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['UID'];
        $result = false;
        
        if ( $input['type'] == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['UID'];
            $result = $this->exercise_lists_m->update_in($input['id'], $value);
        }
        
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));        
    } 

}
