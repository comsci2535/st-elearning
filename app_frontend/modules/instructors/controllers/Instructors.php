<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instructors extends MX_Controller {

	function __construct() {
		parent::__construct();

        $this->load->model('login/users_model');
        $this->load->library('login/users_library');
        $this->load->model('instructors_m');
        $this->load->model('courses_students/courses_students_m');
        $this->load->model('courses/courses_m');
        $this->load->model('courses_categories/courses_categories_m');
        $this->load->library('uploadfile_library');
        $this->load->library('send_email');

	}

    private function seo(){
		$title          = 'ST Developer';
		$robots         = 'ST Developer';
		$description    = 'ST Developer';
		$keywords       = 'ST Developer';
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url()."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
		return $meta;
	}

	public function index(){
        $data = array(
            'menu'    => 'instructor',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'instructor',
            'footer'  => 'footer',
            'function'=>  array('custom','courses'),
		);
        $this->load->view('template/body', $data);
	}

	public function register(){
        $data = array(
            'menu'    => 'instructor',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'register',
            'footer'  => 'footer',
            'background'  => 'bg',
            'function'=>  array('custom','courses','instructors'),
		);
		
        $find_users = $this->instructors_m->find_users_by_user($this->session->users['UID']);
        
        $salt = $this->users_library->salt();
        $password = $this->users_library->hash_password($find_users->password, $salt);

        $data['Users'] = $find_users;
        $data['password'] = $password;

        $this->load->view('template/body', $data);
	}

	public function register_update(){
        $input = $this->input->post(null, true);
        $id = $input['id'];
        
        $userData['fname'] = !empty($input['fname'])?$input['fname']:'';
        $userData['lname'] = !empty($input['lname'])?$input['lname']:'';
        $userData['fullname'] = $userData['fname'].' '.$userData['lname'];
        $userData['phone'] = !empty($input['phone'])?$input['phone']:'';
		$userData['type'] = 'instructor';
		
        $userData2['districts'] = !empty($input['districts_name'])?$input['districts_name']:'';
        $userData2['amphures'] = !empty($input['amphures_name'])?$input['amphures_name']:'';
        $userData2['provinces'] = !empty($input['provinces_name'])?$input['provinces_name']:'';
        $userData2['zip_code'] = !empty($input['zip_code'])?$input['zip_code']:'';
        $userData2['facebook'] = !empty($input['facebook'])?$input['facebook']:'';
        $userData2['lineID'] = !empty($input['lineID'])?$input['lineID']:'';
        $userData2['experience'] = !empty($input['experience'])?$input['experience']:'';

        $update_s = $this->instructors_m->instructors_update_user($id, $userData);
        $update_s = $this->instructors_m->instructors_update($id, $userData2);
        
        // if ($update_s) {
        //     redirect(base_url('instructors/register'),'refresh');
		// }
        redirect(base_url('instructors/courses'),'refresh');		
	}

	public function courses(){

        $instructors = $this->db->get_where('instructors', array('user_id' => $this->session->users['UID']))->row();
        if ($instructors->approve==1) {
            $view = 'courses';
        } else {
            $view = 'approve';
        }
        
        // $param_send['subject']='สมัครสอนกับเรา';
        // $param_send['detail']='ขอบคุณที่เป็นส่วนหนึ่งกับเรา <br>';
        // $param_send['email_send']='';
        // $param_send['email_to']=$this->session->users['Username'];
        // $param_send['file']='';
        // $this->send_email->send($param_send);
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' =>  $view,
            'footer'  => 'footer',
            'function'=>  array('custom','instructors'),
		);
        //loade view


        $this->load->view('template/body', $data);
    }

    public function report_courses(){

        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'report_courses',
            'footer'  => 'footer',
            'function'=>  array('custom','instructors/report_courses'),
		);
        //loade view


        $this->load->view('template/body', $data);
    }

    public function create(){
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','instructors/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/storage");

        //loade view
        $data['breadcrumb']     = 'เพิ่มข้อมูล';
       
        $data['revenue_share']  = $this->instructors_m->get_revenue_share_all()->result();
        
        $infotype               = $this->courses_categories_m->get_courses_categories_parent_all();
        //arr($infotype);exit();
        $data['infotype']       = $infotype;
        $data['instructors']    = $this->instructors_m->get_instructors_by_user()->result();

        $this->load->view('template/body', $data);
    }

    public function storage() {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
       
        $result = $this->courses_m->insert($value);
        if ( $result ) {
            $value_ = $this->_build_data_instructors($result,$input);
            $this->courses_m->insert_courses_instructors($result,$value_);

            $value_map = $this->_build_course_categorie_map($result,$input);
            $this->courses_m->insert_courses_categories_map($result,$value_map);
            
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        
        redirect(site_url("{$this->router->class}/courses"));
    }

    public function edit_courses($id = ''){
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','instructors/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/update");

        //loade view
        
        $input['course_id']     = $id;

        $info = $this->courses_m->get_courses_by_course_id($id);
        if ( $info->num_rows() == 0) {
            redirect_back();
        }
        $info = $info->row();
        $data['breadcrumb']     = $info->title;
        $input['course_id']     = $info->course_id;
        $data['info']           = $info;
        if($info->startDate!="0000-00-00" && $info->endDate!="0000-00-00" ){
            $data['dateRang']   = date('d-m-Y', strtotime($info->startDate)).' ถึง '.date('d-m-Y', strtotime($info->endDate));
        }

        $infotype               = $this->courses_categories_m->get_courses_categories_parent_all();
        $data['infotype']       = $infotype;
        $data['instructors']    = $this->instructors_m->get_instructors_by_user()->result();
        $data['revenue_share']  = $this->instructors_m->get_revenue_share_all()->result();
        
        $courses_instructors=$this->db
                    ->select('a.*')
                    ->from('courses_instructors a')
                    ->where('course_id',$input['course_id'])
                    ->get()->result();

        $data['courses_instructors'] = array();
        if(!empty($courses_instructors)){
            foreach ($courses_instructors as $key => $value) {
                $data['courses_instructors'][]=$value->user_id;
            }
        }

        $courses_categories_map=$this->db
                ->select('a.*')
                ->from('courses_categories_map a')
                ->where('course_id',$id)
                ->get()->result();
        $data['courses_categories_map'] = array();
        if(!empty($courses_categories_map)){
            foreach ($courses_categories_map as $key => $value) {
                $data['courses_categories_map'][] = $value->course_categorie_sub_id;
            }
        }

        $this->load->view('template/body', $data);
    }

    public function update() {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        
        $result = $this->courses_m->update($id, $value);
        if ( $result ) {
            $value_ = $this->_build_data_instructors($id,$input);
            $this->courses_m->insert_courses_instructors($id,$value_);

            $value_map = $this->_build_course_categorie_map($id,$input);
            $this->courses_m->insert_courses_categories_map($id,$value_map);

            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/courses"));
    }

    private function _build_data($input) {
        
        $value['title']               = $input['title'];
        $value['slug']                = $input['slug'];
        $value['excerpt']             = $input['excerpt'];
        $value['detail']              = $input['detail'];
        $value['receipts']            = $input['receipts'];
        $value['recommendVideo']      = $input['recommendVideo'];
        $value['price']               = str_replace(",","",$input['price']);
        $value['publish']             = $input['publish']; 
        $value['startDate']           = $input['startDate'];        
        $value['endDate']             = $input['endDate'];
        //$value['course_categorie_id'] = $input['course_categorie_id'];
        $value['recommend']           = ($input['recommend'] == 'on') ? 1 : 0;
        $value['video_preview']       = ($input['video_preview'] == 'on') ? 1 : 0;
        $value['pretest_posttest']       = ($input['pretest_posttest'] == 'on') ? 1 : 0;
        $value['revenue_share_id']    = $input['revenue_share_id'];
        
        if($input['metaTitle']!=""){
            $value['metaTitle'] = $input['metaTitle'];
        }else{
             $value['metaTitle'] = $input['title'];
        }
        if($input['metaDescription']!=""){
            $value['metaDescription'] = $input['metaDescription'];
        }else{
            $value['metaDescription'] = $input['excerpt'];
        }
        if($input['metaKeyword']!=""){
            $value['metaKeyword'] = $input['metaKeyword'];
        }else{
             $value['metaKeyword'] = $input['title'];
        }

        $path   = 'courses';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
        $file = '';
		if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
            $outfile = $input['outfile'];
			if(isset($outfile)){
				$this->load->helper("file");
				unlink($outfile);
            }
            $value['file'] = $file;
        }

        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['updated_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['UID'];
        } else {
            $value['active']     = $input['active'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['UID'];
        }
        return $value;
    }

    private function _build_data_instructors($course_id,$input) {
        
         if ( isset($input['instructor_id']) ) {
            foreach ( $input['instructor_id'] as $key1 => $rs ) {
                $value[] = array(
                    'course_id' => $course_id,
                    'user_id' => $rs
                );
            }
        }
        return $value;
    }

    private function _build_course_categorie_map($course_id,$input) {

        if ( isset($input['course_categorie_id']) ) {
           foreach ( $input['course_categorie_id'] as $key => $rs ) {
               if(!empty($input['course_categorie_sub_id'][$key])):
                    $value[] = array(
                        'course_id'                  => $course_id,
                        'course_categorie_id'        => $rs,
                        'course_categorie_sub_id'    => $input['course_categorie_sub_id'][$key]
                    );
                endif;
           }
       }
       return $value;
    }

    public function ajax_data_courses() {
        $input = $this->input->post();
        $input['user_id'] = $this->session->users['UID'];
        $input['recycle'] = 0;
        $info = $this->instructors_m->get_courses_instructors_by_user_id($input);
        $infoCount = $this->instructors_m->count_courses_by_instructors($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->course_id);

            $stuQty = $this->courses_students_m->count_courses_studentsroom_by_course_id($rs->course_id);

            $pretest_posttest="";
            if($rs->pretest_posttest==1){
                $pretest_posttest='<a class="dropdown-item" href="'.base_url('exams-pretest-posttest/index/'.$id).'"><i class="fa fa-copy"></i> แบบทดสอบก่อนและหลังเรียน</a>';
            }

            $manage='';
            if($rs->approve==1){
                $manage='<a class="dropdown-item" href="'.base_url('courses_students/lists/'.$id).'"><i class="fa fa-address-book"></i> ดูข้อมูลนักเรียน</a>
                        <a class="dropdown-item" href="'.base_url('courses_lesson/lesson/'.$id).'"><i class="fa fa-book"></i> เนื้อหาคอร์สเรียน</a>
                        <a class="dropdown-item" href="'.base_url('exams_manage/index/'.$id).'"><i class="fa fa-copy"></i> แบบทดสอบ Final</a>'.$pretest_posttest.'
                        <a class="dropdown-item" href="'.base_url('promotion/index/'.$id).'"><i class="fa fa-map-signs"></i> โปรโมชั่น</a>';
            }

            $active = $rs->active ? '<span class="badge badge-success">เปิด</span>' : '<span class="badge badge-danger">ปิด</span>';
            $img    = '<img src="'.base_url($rs->file).'" class="img-rounded" style="width: 100px;">';
            
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $no;
            $column[$key]['course_id']  = $rs->course_id;
            $column[$key]['title']      = $rs->title;
            $column[$key]['img']        = $img;
            $column[$key]['active']     = $active;
            $column[$key]['stuQty']     = $stuQty;
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['updated_at'] = $rs->updated_at;
            $column[$key]['action']     = '<div class="btn-group">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
                                                <div class="dropdown-menu">
                                                    '.$manage.'
                                                    <a class="dropdown-item" href="'.base_url('instructors/courses/edit/'.$id).'"><i class="fa fa-edit"></i> แก้ไข</a>
                                                    <a class="dropdown-item btn-click-delete" href="javascript:void(0)" data-id="'.$id.'"><i class="fa fa-trash"></i> ลบ</a>
                                                </div>
                                            </div>';
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function ajax_data_report_courses() {
        $input = $this->input->post();
        $input['user_id'] = $this->session->users['UID'];
        $info = $this->instructors_m->get_income_courses_instructors_by_user_id($input);
        $infoCount = $this->instructors_m->count_income_courses_by_instructors($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->income_instructors_id);
            
            $status = $rs->status ? '<span class="badge badge-success">โอนแล้ว</span>' : '<span class="badge badge-danger">รอการโอน</span>';
            
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $no;
            $column[$key]['course_id']  = $rs->course_id;
            $column[$key]['cycle']      = 'รอบที่ '.$rs->cycle_no;
            
            $column[$key]['title']      = $rs->title;
            $column[$key]['active']     = $status;
            $column[$key]['stuQty']     = number_format($rs->discount, 2);
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['cycle_date'] = $rs->cycle_date;
           
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function students(){
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'courses',
            'footer'  => 'footer',
            'function'=>  array('custom','courses'),
		);
        //loade view
        
        $input['user_id'] = $this->session->users['UID'];

        $total		= $this->instructors_m->count_courses_by_instructors($input);
       
        $segment	= 5;
		$per_page 	= 6;
		
		$uri = 'instructors/courses/';
		

        $data["pagination"] = $this->pagin($uri, $total, $segment, $per_page);
        
        $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;

		$input['length']  = $per_page;
        $input['start']	  = $page;


        $info           = $this->instructors_m->courses_instructors($input)->result();
        $data['info']   = $info;
        $data['count']  = $total;

        $this->load->view('template/body', $data);
	}

	public function courses_students(){
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'courses_students',
            'footer'  => 'footer',
            'function'=>  array('custom','courses'),
		);
		//loade view
        $data['result'] = $this->instructors_m->courses_students($this->uri->segment(3));

        $this->load->view('template/body', $data);
	}

	public function instructor_courses($code =''){
		// get instructors
        $info  = $this->instructors_m->get_courses_instructors_join_users_by_course_id($code)->result();
        if($info):
            foreach($info as $item):
                $skill_arr = array_map('intval', explode(',', $item->skill));
                $item->skills = $this->instructors_m->get_instructors_skill_by_skill($skill_arr)->result();
            endforeach;
        endif;
        
		$data['info'] = $info;
		$this->load->view('instructor_courses', $data); 
	}

	public function instructor_tab($code =''){

		$input['course_id'] = $code;
		$input['recycle'] 	= 0;
		$input['active']  	= 1;
		// get instructors
		$info  = $this->instructors_m->get_courses_instructors($input);
		$data['info'] = $info->result();
		$this->load->view('instructor_tab', $data); 
	}

    public function get_instructor_by_courses($course_id =''){

        $input['course_id'] = $course_id;
        $input['recycle']   = 0;
        $input['active']    = 1;
        // get instructors
        $info  = $this->instructors_m->get_courses_instructors($input);
        return  $info->result();
        
    }

    public function instructor_home(){
        $input['recycle']    = 0;
        $input['active']     = 1;
        $input['approve']    = 1;
        $input['recommend']  = 1;
        $input['length']     = 6;
		$input['start']	     = 0;
		// get courses
        $info  = $this->instructors_m->get_users_instructors($input)->result();
        if($info):
            foreach($info as $item):
                if(!empty($item->skill)){
                    $input_skill['skill_id']	=  explode(',', $item->skill);
                    $item->skills = $this->instructors_m->get_instructors_skill($input_skill)->result();

                }
                
            endforeach;
        endif;
        
        $data['info'] = $info;

        $this->load->view('instructor_home', $data); 
    }

    public function demo(){
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'courses_demo',
            'footer'  => 'footer',
            'function'=>  array('custom','courses'),
		);
        //loade view
        
        $this->load->view('template/body', $data);
    }
    
    public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = base_url().'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids){

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
    }
    
    public function action(){
        
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;

            $input               = $this->input->post();
            $dateTime            = db_datetime_now();
            $value['updated_at'] = $dateTime;
            $value['updated_by'] = $this->session->users['UID'];
            $result = false;
            
            if ( $input['type'] == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycle_at'] = $dateTime;
                $value['recycle_by'] = $this->session->users['UID'];
                $result = $this->courses_m->update_in($input['id'], $value);
            }
            
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 
}
