<div class="container">
    <div class="row">
        <div class="col-sm-12 mt-5 wow bounceInDown" data-wow-delay="0.2s">
            <div class="title">
                <h1>Great Teacher </h1>
            </div>
            <div class="text-center">
                <p>
                    “ มิ้นต์เรียนจนอาจารย์มิ้นต์ให้ไปสอนเพื่อนในคลาสเรียน เคยเรียนมากสุดวันละ 17 ครั้ง <br>
                    ทุกวันนี้คือเป็นมากกว่าการเรียนภาษาอังกฤษไปแล้ว ถ้าเลิกเรียนแล้วจะรู้สึกขาดหายไป
                    หยุดเรียนไม่ได้<br>
                    มิ้นต์เรียนเกือบครบทุกโค้ชในโกลบิช
                    ทุกวันนี้ก็ยังเรียนอยู่เหมือนเป็นส่วนหนึ่งของชีวิตประจำวันไปแล้ว ปกติไม่เคยตื่นเช้า<br>
                    ก็ตื่นเช้ามาเรียน ทุกวันมีความรู้ใหม่ๆ ถ้าหยุดเรียนรู้สึกความรู้ลดลง ”
                </p>
            </div>
        </div>
        <div class="col-sm-12 mt-5 mb-5 wow bounceInUp" data-wow-delay="0.2s">
            <div id="Great_Teacher" class="owl-carousel owl-theme">
                <?php 
                if(!empty($info)):
                    foreach($info as $item):
                ?>
                <div class="item">
                    <div class="">
                        <div class="detail">
                            <p>
                                <?php echo !empty($item->fullname)? $item->fullname : '' ;?><br>
                                <?php
                                    if(!empty($item->skills)):
                                        foreach($item->skills as $skill):
                                            echo !empty($skill->skill_name)? $skill->skill_name.'&nbsp' : '';
                                        endforeach;
                                    endif;
                                ?>
                            </p>
                        </div>
                        <div class="img mt-4">
                            <img src="<?php echo !empty($item->file)? base_url().$item->file : '';?>" class="ImgFluid" alt="<?php echo  !empty($item->fullname)? $item->fullname : '' ;?>" onerror="this.src='<?php echo base_url("images/user.png"); ?>'">
                        </div>
                    </div>
                </div>
                <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>