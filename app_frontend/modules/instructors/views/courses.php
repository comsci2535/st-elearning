<section class="Sec_Courses">

    <div class="container">
        <div class="row">

            <div class="col-sm-12 text-right mt-4 mb-4">
                <a href="<?=site_url('instructors/report_courses');?>"><button type="button" class="btn btn-success"><i
                            class="fas fa-clipboard"></i> รายได้</button></a>
                <a href="<?=site_url('instructors/create');?>"><button type="button" class="btn btn-success"><i
                            class="fas fa-plus"></i> เพิ่มคอร์ส</button></a>
                
            </div>
            
            <div class="col-sm-12 mb-5">
                <table id="data-list" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
                <thead>
                    <tr>
                        <th>ลำดับ</th>
                        <th>รูปภาพ</th>
                        <th>ชื่อคอร์ส</th>
                        <th>จำนวนผู้เรียน</th>
                        <th>วันที่ล่าสุด</th>
                        <th>สถานะ</th>
                        <th>จัดการ</th>
                    </tr>
                </thead>
                </table>
            </div>
        </div>
    </div>

</section>