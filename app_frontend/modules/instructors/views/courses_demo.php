<section class="Sec_instructors_courses">
    <!-- <div class="container-fluid">
        <div class="row">
            <div id="slide_banner" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="<?=base_url('images/slide_courses.JPG');?>" class="ImgFluid" alt="">
                </div>
            </div>
        </div>
    </div> -->

    <div class="container">
        <div class="row">

            <div class="col-sm-12 mt-5 mb-5">
                <div class="title">
                    <h1>My Courses</h1>
                    <div class="text">
                        <p>127 หลักสูตร</p>
                    </div>
                </div>
            </div>

            <!-- <div class="col-sm-12 text-right mb-4">
                <a href="<?=site_url('instructor/create');?>"><button type="button" class="btn btn-success"><i
                            class="fas fa-plus"></i> เพิ่มคอร์ส</button></a>
                <hr>
            </div> -->
            <div class="col-sm-3 mb-5">
                <div class="course-profile-instructors">
                    <div class="users">
                        <img src="<?=base_url('images/user.png');?>" class="img" alt="">
                        <div class="name">
                            <h2>Mark Andre</h2>
                            <p>mark.example@info.com</p>
                        </div>
                        <div class="social">
                            <a href=""><i class="fab fa-facebook-f"></i></a>
                            <a href=""><i class="fab fa-google-plus-g"></i></a>
                            <a href=""><i class="fab fa-linkedin-in"></i></a>
                            <a href=""><i class="fab fa-twitter"></i></a>
                        </div>
                    </div>
                    <div class="course-info-list scroll-page scroller">
                        <ul class="navbar">
                            <li><a class="nav-link active" href="#Overview"><i class="far fa-file-alt"></i> Courses</a>
                            </li>
                            <li><a class="nav-link" href="#Curriculum"><i class="far fa-file-alt"></i> Courses</a>
                            </li>
                            <li><a class="nav-link" href="#Instructor"><i class="far fa-file-alt"></i> Courses</a>
                            </li>
                            <li><a class="nav-link" href="#reviews"><i class="far fa-file-alt"></i> Courses</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 mb-5">
                <div class="list-views-courses">
                    <div class="head">
                        <span>MY COURSES</span>
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ชื่อ นามสกุล</th>
                                <th>Email</th>
                                <th>เบอร์โทร</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for ($i=0; $i < 30; $i++) { ?>
                            <tr>
                                <td>22</td>
                                <td>22</td>
                                <td>22</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-12">
                <ul class="pagination justify-content-end">
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">
                            << </a> </li> <li class="page-item"><a class="page-link active"
                                    href="javascript:void(0);">1</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">4</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);"> >> </a></li>
                </ul>
            </div>
        </div>
    </div>

</section>