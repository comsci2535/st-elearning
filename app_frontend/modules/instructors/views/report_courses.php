<section class="Sec_Courses">

    <div class="container" style="margin-top: 60px;">
        <div class="row">
            <div class="col-sm-12 mb-5">
                <div class="row">
                    <table id="data-list" class="table table-striped- table-bordered table-hover table-checkable" width="100%">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รอบการโอน</th>
                            <th>ชื่อคอร์ส</th>
                            <th>จำนวนเงิน</th>
                            <th>วันที่ตัดรอบ</th>
                            <!-- <th>วันที่สร้าง</th> -->
                            <th>สถานะ</th>
                        </tr>
                    </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>