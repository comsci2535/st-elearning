<div class="Bold my-4" id="scoll3">
    <h2>อาจารย์ผู้สอน</h2>
</div>
<?php

if(!empty($info)):
    foreach($info as $key => $item):
    if(!empty($item->file)){
        $img=base_url($item->file);
    }else{
        $img=$item->oauth_picture;
    }
?>
<div class="courses-teaher">
    <div class="img">
        <img src="<?php echo $img;?>" class="ImgFluid" alt="คอสเรียน" onerror="this.src='<?php echo base_url("images/Great_Teacher.jpg"); ?>'">
    </div>
    <div class="text">
        <p class="name Bold"><?php echo !empty($item->fullname)? $item->fullname : '';?></p>
        <div class="pp">
            <div id="text-row-teaher-<?=$key?>" class="view-overlate">
                <p><?php echo !empty($item->experience)? $item->experience : '';?></p>
                <p><?php echo !empty($item->skill_long)? $item->skill_long : '';?></p>
            </div>
            <a href="javascript:void(0)" id="overlate" class="overlate-hide" data-line="text-row-teaher-<?=$key?>" data-hide="0" style="display:none;">
                <i class="fas fa-plus"></i> ดูเพิ่มเติม
            </a>
        </div>
    </div>
</div>
<?php
endforeach;
endif;
?>