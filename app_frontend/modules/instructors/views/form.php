<section class="Sec_Courses">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 pt-4">
                <h4 class="pt-4">
                    <a href="<?=site_url('instructors/courses');?>">คอร์สทั้งหมด</a> > <span><?php echo !empty($breadcrumb)? $breadcrumb : '';?></span>
                </h4>
                <hr>
            </div>
           
            <div class="col-sm-12 mb-5">
                <div class="col-sm-12">
                    <form class="form-horizontal frm-create" method="post" action="<?=$frmAction?>" autocomplete="off" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email"></label>
                            <div class="col-sm-4">
                                <input <?php if(!empty($info->recommend) &&  $info->recommend=='1'){ echo "checked";}?> type="checkbox" name="recommend" valus="1"> รายการแนะนำ
                                <span></span>
                            </div>
                        
                            <div class="col-sm-4">
                                <label>
                                    <input <?php if(!empty($info->pretest_posttest) &&  $info->pretest_posttest=='1'){ echo "checked";}?> type="checkbox" name="pretest_posttest" valus="1"> แบบทดสอบก่อนเรียนและหลังเรียน
                                    <span></span>
                                </label>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="revenue_share_id">ส่วนแบ่งรายได้ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <select id="revenue_share_id" name="revenue_share_id" class="form-control m-input  select2" required>
                                    <option value="">เลือกส่วนแบ่งรายได้</option>
                                <?php
                                if(!empty($revenue_share)):
                                    $selected = '';
                                    foreach($revenue_share as $item):
                                        if($info->revenue_share_id == $item->revenue_share_id):
                                            $selected = 'selected';
                                        else:
                                            $selected = '';
                                        endif;
                                    ?>
                                        <option value="<?=$item->revenue_share_id;?>" <?=$selected?>><?=$item->title.' ('.$item->Instructor_price.' ต่อ '.$item->owner_price.')';?></option>
                                    <?php
                                    endforeach;
                                endif;
                                ?>
                                
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">หัวข้อ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo !empty($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title"  required>
                                <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">slug <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo !empty($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug" required>
                                <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">รูปภาพหน้าปก </label>
                            <div class="col-sm-10"> 
                                <input id="file" name="file" type="file"  data-preview-file-type="text">
                                <input type="hidden" name="outfile" value="<?=(!empty($info->file)) ? $info->file : ''; ?>">
                            </div>
                        </div>

                        <?php
                        if(!empty($infotype)):
                            foreach($infotype as $key => $item):
                        ?>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="course_categorie_sub_id[]"><?=$item->title;?> <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input type="hidden" name="course_categorie_id[]" value="<?=$item->course_categorie_id?>">
                                <select id="<?=$item->course_categorie_id?>" name="course_categorie_sub_id[<?=$key?>]" class="form-control m-input select2" required>
                                    <option value="">เลือก<?=$item->title;?></option>
                                    <?php
                                    if(!empty($item->parent)):
                                        $selected2 = '';
                                        foreach($item->parent as $item2):

                                            if(!empty($courses_categories_map) && in_array($item2->course_categorie_id, $courses_categories_map)):
                                                $selected2 = 'selected';
                                            else:
                                                $selected2 = '';
                                            endif;
                                    ?>
                                    <option value="<?=$item2->course_categorie_id;?>" <?=$selected2?>> <?=$item2->title;?></option>
                                    <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <?php
                            endforeach;
                        endif;
                        ?>
                        
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">รายละเอียดย่อ</label>
                            <div class="col-sm-10"> 
                                <textarea name="excerpt" rows="3" class="form-control" id="excerpt" ><?php echo !empty($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">รายละเอียด</label>
                            <div class="col-sm-10"> 
                                <textarea name="detail" rows="3" class="form-control summernote" id="detail" ><?php echo !empty($info->detail) ? $info->detail : NULL ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">สิ่งที่ได้รับ</label>
                            <div class="col-sm-10"> 
                                <textarea name="receipts" rows="3" class="form-control summernote" id="receipts" ><?php echo !empty($info->receipts) ? $info->receipts : NULL ?></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="instructor_id[]" value="<?php echo $this->session->users['UID']; ?>">
                       <!--  <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ผู้สอน <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <select id="instructor_id" name="instructor_id[]"  class="form-control m-input  select2" required>
                                    <option value="">เลือก</option>
                                    <?php
                                    if(!empty($instructors)): foreach($instructors as $item): ?>
                                            <option value="<?=$item->user_id;?>" <?php if(!empty($courses_instructors) && in_array($item->user_id, $courses_instructors)){ echo "selected"; }?> ><?=$item->fullname;?></option>
                                    <?php endforeach; endif; ?>
                                </select>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd"></label>
                            <div class="col-sm-10"> 
                                <label class="m-checkbox m-checkbox--brand">
                                    <input <?php if(!empty($info->video_preview) &&  $info->video_preview=='1'){ echo "checked";}?> type="checkbox" name="video_preview" valus="1" id="checkview"> แสดงวีดีโอตัวอย่าง
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ลิงก์วิดีโอแนะนำคอร์ส <span id="star-red" class="text-danger d-none"> *</span></label>
                            <div class="col-sm-10"> 
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2" data-toggle="modal" data-target="#contentVideo"><i class="fa fa-video"></i></span>
                                    </div>
                                    <input value="<?php echo !empty($info->recommendVideo) ? $info->recommendVideo : NULL ?>" type="text" class="form-control m-input " name="recommendVideo" id="recommendVideo">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ราคา <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo !empty($info->price) ? number_format($info->price) : NULL ?>" type="text" class="form-control m-input amount" name="price" id="price" required >
                            </div>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label class="control-label col-sm-2" for="pwd">เผยแพร่ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <label><input <?php if(!empty($info->publish) && $info->publish=='public'){  echo  "checked"; } ?> <?php if(empty($info->publish)){ echo "checked"; } ?> type="radio" name="publish" class="icheck" value="public" > Public</label>
                                <label><input <?php if(!empty($info->publish) && $info->publish=='private'){  echo  "checked"; } ?> <?php if(empty($info->publish)){  echo  "checked"; }?> type="radio" name="publish" class="icheck" value="private"> Private</label>
                            </div>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label class="control-label col-sm-2" for="pwd">ช่วงวันที่แสดงผล</label>
                            <div class="col-sm-10"> 
                                <input type="text" name="dateRange" class="form-control" value="<?php echo !empty($dateRang) ? $dateRang : NULL ?>"/>
                                <input type="hidden" name="startDate"  value="<?php echo !empty($info->startDate) ? $info->startDate : NULL ?>"/>
                                <input type="hidden" name="endDate"  value="<?php echo !empty($info->endDate) ? $info->endDate : NULL ?>"/>
                            </div>
                        </div>
                         
                        <?php
                        if($this->router->method == 'edit_courses' && !empty($info->approve) && $info->approve == 1):
                        ?>
                        <div class="form-group" >
                            <label class="control-label col-sm-2" for="pwd">สถานะ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <label><input <?php if(isset($info->active) && $info->active== 1){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="1" > เปิด</label>
                                <label><input <?php if(isset($info->active) && $info->active== 0){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
                            </div>
                        </div>
                        <?php
                        endif;
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd"> <h5 class="block">ข้อมูล SEO</h5></label>
                            <div class="col-sm-10"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ชื่อเรื่อง (Title)</label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo !empty($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="" class="form-control" name="metaTitle">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">คำอธิบาย (Description)</label>
                            <div class="col-sm-10"> 
                                <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo !empty($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">คำหลัก (Keyword)</label>
                            <div class="col-sm-10"> 
                                <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo !empty($info->metaKeyword) ? $info->metaKeyword : NULL ?></textarea>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                                <input type="hidden" class="form-control" name="db" id="db" value="courses">
                                <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->course_id) ? encode_id($info->course_id) : 0 ?>">
                                <button type="submit" class="btn btn-success">บันทึก</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? base_url().$info->file : '0'; ?>';
    var file_id         = '<?=(isset($info->course_id)) ? $info->course_id : ''; ?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/'+file_id;

</script>