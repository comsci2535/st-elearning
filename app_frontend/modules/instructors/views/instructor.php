<section class="Sec_Courses">
    <!-- <div class="container-fluid">
        <div class="row">
            <div id="slide_banner" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="<?=base_url('images/slide_courses.JPG');?>" class="ImgFluid" alt="">
                </div>
            </div>
        </div>
    </div> -->

    <div class="container">
        <div class="row">

            <div class="col-sm-12 mt-5 mb-5">
                <div class="title">
                    <h1>My Courses</h1>
                    <div class="text">
                        <p>127 หลักสูตร</p>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 text-right mb-4">
                <a href="<?=site_url('instructor/create');?>"><button type="button" class="btn btn-success"><i
                            class="fas fa-plus"></i> เพิ่มคอร์ส</button></a>
                <hr>
            </div>
            <div class="col-sm-3 mb-5">
                <div class="MenuCourses">
                    <h2 class="text-center">เมนูใช้งาน</h2>
                    <hr>
                    <ul class="sidebar-menu">
                        <li><a href="<?=site_url('instructor/create');?>"><span><i class="fas fa-th-large"></i>
                                    คอร์ส</span></a></li>
                        <li><a href="#"><span><i class="fas fa-th-large"></i> บทเรียน</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9 mb-5">
                <div class="row">
                    <?php for ($i=1; $i < 10; $i++) { ?>
                    <div class="col-sm-12 wow fadeIn" data-wow-delay="0.1s">
                        <div class="CoursesItem_List cart mb-4">
                            <div class="img">
                                <img src="<?=base_url('images/cose.jpg');?>" class="ImgFluid imgBig" alt="คอสเรียน">
                            </div>
                            <div class="detail-text">
                                <div class="name">
                                    <p>หลักสูตรพัฒนาตนเพื่อการเรียน รู้ตลอดชีวิต ด้วยทักษะที่มีใน...</p>
                                </div>
                                <div class="detail">
                                    <p>การพัฒนาตัวจนเพื่อสร้างบุคลิกภาพที่ดีเป็นสิ่ง...</p>
                                </div>
                                <div class="user">
                                    <div class="left">
                                        <img src="<?=base_url('images/user.jpg');?>" class="ImgFluid" alt="คอสเรียน">
                                    </div>
                                    <div class="right">
                                        <h4>Adam Bradshaw (อ.อดัม)</h4>
                                        <div class="p">
                                            <p>เจ้าของสถาบันสอนภาษาอังกฤษชื่อดัง Hollywood Learning Center...</p>
                                        </div>
                                        <div class="star">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <span>(5.0)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="price">
                                    <div class="row">
                                        <div class="col-12 text-right">
                                            <span class="b1">2,500</span> <span class="b2">บาท</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php  } ?>
                </div>
            </div>
            <div class="col-sm-12">
                <ul class="pagination justify-content-end">
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">
                            << </a> </li> <li class="page-item"><a class="page-link active"
                                    href="javascript:void(0);">1</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);">4</a></li>
                    <li class="page-item"><a class="page-link" href="javascript:void(0);"> >> </a></li>
                </ul>
            </div>
        </div>
    </div>

</section>