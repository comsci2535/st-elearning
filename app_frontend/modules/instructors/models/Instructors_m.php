<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Instructors_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function courses_instructors($param)
    {

        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
        
        if ( isset($param['recycle']) )
            $this->db->where('b.recycle', $param['recycle']);
        
        if ( isset($param['active']) )
            $this->db->where('b.active', $param['active']);  
        
        $this->db->where('a.user_id', $param['user_id']);    
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        $query = $this->db
                        ->select('b.*')
                        ->from('courses_instructors a')
                        ->join('courses b', 'a.course_id = b.course_id', 'inner')
                        ->get();
        return $query;
    }

    public function count_courses_by_instructors($param)
    {
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['search']['value'])
                    ->or_like('b.excerpt', $param['search']['value'])
                    ->or_like('b.detail', $param['search']['value'])
                    ->group_end();
        }
        
        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
        
        if ( isset($param['recycle']) )
            $this->db->where('b.recycle', $param['recycle']);
        
        if ( isset($param['active']) )
            $this->db->where('b.active', $param['active']);  
        
        $this->db->where('a.user_id', $param['user_id']);    

        $this->db
        ->select('b.*')
        ->from('courses_instructors a')
        ->join('courses b', 'a.course_id = b.course_id', 'inner');
        return $this->db->count_all_results();
    }
    
    public function courses_students($id)
    {
        $query = $this->db
                        ->select('*')
                        ->from('courses_students a')
                        ->join('courses b', 'a.course_id = b.course_id', 'left')
                        ->join('users c', 'a.user_id = c.user_id', 'left')
                        ->where('b.slug', $id)
                        ->get();
        return $query->result();
	}

    public function instructors_update_user($id, $value)
    {
        $query = $this->db
                        ->where('user_id', $id)
                        ->update('users', $value);
        return $query;
	}
	
	public function instructors_update($id, $value)
    {
        $this->db->where('user_id', $id );
        $query_instructors = $this->db->get('instructors');
    
        if ( $query_instructors->num_rows() == 0 ) {
            $value['user_id'] = $id;
            $query = $this->db->insert('instructors', $value);

        } else {
            $query = $this->db
                            ->where('user_id', $id)
                            ->update('instructors', $value);
        }

        $set_session = $this->session->userdata('users');
        if($set_session == null || count($set_session) == 0){
            $set_session = array();
        } 
        $set_session['UserType'] = 'instructor';
        $this->session->set_userdata('users', $set_session);

        return $query;
    }
    
    public function find_users_by_user($username){
        $query = $this->db
                ->select('*')
                ->from('users a')
                ->join('instructors b', 'a.user_id = b.user_id', 'left')
                ->where('a.user_id',$username)
                ->get();
		return $query->row();         
	}
    
    public function get_rows($param){
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        if ( isset($param['course_id']) ) 
            $this->db->where('a.course_id', $param['course_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);
            
        $query = $this->db
                        ->select('a.*')
                        ->from('courses a')
                        ->get();
        return $query;
    }

    public function get_instructors_by_user(){
        $this->db->where('a.active', 1);
        $query = $this->db
                ->select('*')
                ->from('users a')
                ->join('instructors b', 'a.user_id = b.user_id', 'left')
                ->get();
		return $query;         
	} 
    
    public function get_courses_instructors_by_user_id($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['search']['value'])
                    ->or_like('b.excerpt', $param['search']['value'])
                    ->or_like('b.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 2) $columnOrder = "b.title";

            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
        
        if ( isset($param['recycle']) )
            $this->db->where('b.recycle', $param['recycle']);
        
        if ( isset($param['active']) )
            $this->db->where('b.active', $param['active']);  
        
        $this->db->where('a.user_id', $param['user_id']);    
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        $query = $this->db
                        ->select('b.*')
                        ->from('courses_instructors a')
                        ->join('courses b', 'a.course_id = b.course_id', 'inner')
                        ->get();
        return $query;
    }

    public function get_courses_instructors($param)
    {
        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
        
        if ( isset($param['recycle']) )
            $this->db->where('b.recycle', $param['recycle']);
        
        if ( isset($param['active']) )
            $this->db->where('b.active', $param['active']);  

         $this->db->where('c.private', 0);    
        
        $query = $this->db
                        ->select('*')
                        ->from('courses_instructors a')
                        ->join('users b', 'a.user_id = b.user_id', 'inner')
                        ->join('instructors c', 'a.user_id = c.user_id', 'inner')
                        ->get();
        return $query;
    }

    public function get_users_instructors($param)
    {
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        if ( isset($param['approve']) )
            $this->db->where('a.approve', $param['approve']);
        
        if ( isset($param['recommend']) )
            $this->db->where('a.recommend', $param['recommend']);
        
        if ( isset($param['recycle']) )
            $this->db->where('b.recycle', $param['recycle']);
        
        if ( isset($param['active']) )
            $this->db->where('b.active', $param['active']);   

        $this->db->order_by('b.created_at', 'DESC');

        $query = $this->db
                        ->select('a.instructor_id, a.user_id, a.experience, a.skill, b.fullname, b.file')
                        ->from('instructors a')
                        ->join('users b', 'a.user_id = b.user_id', 'inner')
                        ->get();
        return $query;
    }

    public function get_instructors_skill($param)
    {
        if ( isset($param['skill_id']) )
            $this->db->where_in('a.skill_id', $param['skill_id']);
        
            $query = $this->db->select('a.*')
                              ->from('instructors_skill a')
                              ->get();
        return $query;
    }

    //new function query

    public function get_instructors_by_user_id($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->select('*');
        $query = $this->db->get('instructors');
        return $query;
    }

    public function get_instructors_skill_by_skill($skill)
    {
        $this->db->where_in('skill_id', $skill);
        $this->db->select('*');
        $query = $this->db->get('instructors_skill');
        return $query;
    }

    public function get_users_instructors_join_users_by_user_id($user_id = '', $approve = 0, $recommend =0, $length = 10, $start = 0)
    {
        if(!empty($user_id)):
            $this->db->where('a.user_id', $user_id);
        endif;
        $this->db->limit($length, $start);
        $this->db->where('a.approve', $approve);
        $this->db->where('a.recommend', $recommend);
        $this->db->where('b.recycle', 0);
        $this->db->where('b.active', 1);   
        $this->db->order_by('b.created_at', 'DESC');
        $query = $this->db
                        ->select('a.instructor_id, a.user_id, a.experience, a.skill, b.fullname, b.file')
                        ->from('instructors a')
                        ->join('users b', 'a.user_id = b.user_id', 'inner')
                        ->get();
        return $query;
    }

    public function get_courses_instructors_join_users_by_course_id($course_id)
    {
        $this->db->where('a.course_id', $course_id);
        $this->db->where('b.recycle', 0);
        $this->db->where('b.active', 1); 
        $this->db->where('c.private', 0);    
        
        $query = $this->db
                        ->select('*')
                        ->from('courses_instructors a')
                        ->join('users b', 'a.user_id = b.user_id', 'inner')
                        ->join('instructors c', 'a.user_id = c.user_id', 'inner')
                        ->get();
        return $query;
    }

    public function get_revenue_share_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('revenue_share_id', 'asc');
        $this->db->select('*');
        $query = $this->db->get('revenue_share');
        
        return $query;
    }

    public function get_income_courses_instructors_by_user_id($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['search']['value'])
                    ->or_like('b.excerpt', $param['search']['value'])
                    ->or_like('b.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.cycle_no";
            if ($param['order'][0]['column'] == 2) $columnOrder = "b.title";

            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
        
        if ( isset($param['recycle']) )
            $this->db->where('b.recycle', $param['recycle']);
        
        if ( isset($param['active']) )
            $this->db->where('b.active', $param['active']);  
        
        $this->db->where('a.user_id', $param['user_id']);    
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
               
        $query = $this->db
                        ->select('b.title, a.*')
                        ->from('income_instructors a')
                        ->join('courses b', 'a.course_id = b.course_id', 'inner')
                        ->get();
        return $query;
    }

    public function count_income_courses_by_instructors($param)
    {
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['search']['value'])
                    ->or_like('b.excerpt', $param['search']['value'])
                    ->or_like('b.detail', $param['search']['value'])
                    ->group_end();
        }
        
        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
        
        if ( isset($param['recycle']) )
            $this->db->where('b.recycle', $param['recycle']);
        
        if ( isset($param['active']) )
            $this->db->where('b.active', $param['active']);  
        
        $this->db->where('a.user_id', $param['user_id']);    

        $this->db
        ->select('b.title, a.*')
        ->from('income_instructors a')
        ->join('courses b', 'a.course_id = b.course_id', 'inner');
        return $this->db->count_all_results();
    }
}
