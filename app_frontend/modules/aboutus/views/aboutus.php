<div class="container">
    <div class="background-image">
        <div class="row">
            <div class="offset-sm-2 col-sm-8">
                <div class="my-5">
                    <div class="title Bold">
                        <h1 class="mb-5">เกี่ยวกับเรา</h1>
                        <?php echo html_entity_decode($info); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>