<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Happy_students_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param){
        if ( isset($param['happy_student_id']) )
            $this->db->where('a.happy_student_id', $param['happy_student_id']);
             
        $query = $this->db
                        ->select('a.*')
                        ->from('reviews_stars a')
                        ->get();
        return $query;
    }

    public function get_happy_students_rows($param){
        if ( isset($param['recommend']) )
            $this->db->where('a.recommend', $param['recommend']);

        if ( isset($param['happy_student_id']) )
            $this->db->where('a.happy_student_id', $param['happy_student_id']);
        
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
       
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']); 
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
    
        $this->db->order_by('a.created_at', 'DESC');    

        $query = $this->db
                        ->select('a.*')
                        ->from('happy_students a')
                        ->get();
        return $query;
    }

    // new function query
    public function count_happy_students_all() 
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('happy_students');
    }

    public function get_happy_students_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('happy_students');
        return $query;
    }

    public function get_happy_students_by_id($id)
    {
        $this->db->where('happy_student_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('happy_students');
        
        return $query;
    }

    public function get_happy_students_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('happy_students');
        
        return $query;
    }

    public function get_happy_students_option_all($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;

        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('title', $param['search']);
        endif;

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('happy_students');
        
        return $query;
    }
}
