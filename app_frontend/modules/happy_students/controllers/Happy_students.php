<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Happy_students extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('happy_students_m');
    }

    public function index(){

    }

    public function happy_students()
    {
        $input['recommend'] ='1';
        $input['active']    ='1';
        $input['recycle']   ='0';
        $input['length']    = 10;
		$input['start']	    = 0;
        // get banners
		$info  = $this->happy_students_m->get_happy_students_rows($input);
		$data['info'] = $info->result();
        $this->load->view('happy_students', $data); 
    }

    public function happy_students_home()
    {
        $param['length']    = 10;
		$param['start']	    = 0;

        $data['info']  = $this->happy_students_m->get_happy_students_option_all($param)->result();
        
        $this->load->view('happy_students_home', $data); 
    }
}
