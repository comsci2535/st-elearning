<div class="Reviews">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title Bold">
                        <h1>รีวิวจากผู้เรียน</h1>
                    </div>
                </div>
            </div>
            <div id="Reviews" class="owl-carousel owl-theme">
                <?php 
                    if(!empty($info)):
                        foreach($info as $item):
                ?>
                <div class="item wow fadeIn" data-wow-delay="0.1s">
                    <div class="boxx">
                        <div class="detail">
                            <div class="user">
                                <div class="img">
                                    <img src="<?=base_url($item->file)?>" class="ImgFluid"
                                        alt="<?=$item->title? $item->title : ''?>" onerror="this.src='https://www.getupschool.com/images/user.png'">
                                </div>
                                <div class="name">
                                    <span><?=$item->title? $item->title : ''?></span>
                                </div>
                            </div>
                            <div class="text">
                                <p><?=$item->excerpt? $item->excerpt : ''?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                        endforeach; 
                    endif; 
                ?>
            </div>
        </div>
    </div>