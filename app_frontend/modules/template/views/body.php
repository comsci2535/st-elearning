<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= $seo; ?>
    <?php //echo Modules::run('social/share'); ?>
    <link rel="shortcut icon" href="<?= base_url(); ?>images/logo/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?= base_url('template/frontend/bootstrap/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/class/css/app.css'); ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="<?= base_url('template/frontend/toastr/toastr.css'); ?>" rel="stylesheet" type="text/css"/>


    <link rel="stylesheet" type="text/css"
          href="<?= base_url('template/frontend/fancybox3'); ?>/jquery.fancybox.min.css" media="screen"/>

    <link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet"/>
    <!-- City -->
    <link href="https://unpkg.com/video.js@7/dist/video-js.min.css" rel="stylesheet">
    <link href="https://unpkg.com/@videojs/themes@1/dist/fantasy/index.css" rel="stylesheet">
    <style>
        /*div#my-video {
            height: 360px;
        }*/

        .embed-container {
            padding-bottom: unset;
        }

        .vjs-theme-fantasy .vjs-play-progress, .vjs-theme-fantasy .vjs-play-progress:before {
            background-color: #fec90b;
        }
    </style>
    <?php
    if (!empty($function)) {
        $length1 = count($function);
        for ($x = 0; $x < $length1; $x++) {
            $this->load->view('function/' . $function[$x] . '/stylesheet');
        }
    }
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127405874-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-127405874-3');
    </script>


</head>
<body>
<?php
$backgrounds = NULL;
if (!empty($background) && $background == 'bg') {
    $backgrounds = 'background: url(' . base_url('images/bg/background.jpg') . ')';
    $backgrounds = 'background: url(' . base_url('images/bg/background.jpg') . ');background-position: center;';
}
?>
<div class="blog-container" style="<?= $backgrounds; ?>">
    <?php $this->load->view('template/' . $header); ?>
    <?php $this->load->view($content); ?>
    <?php $this->load->view('template/' . $footer); ?>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script type="text/javascript" src="<?= base_url('template/frontend/bootstrap/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/toastr/toastr.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('template/frontend/fancybox3'); ?>/jquery.fancybox.min.js"></script>

    <script src="https://vjs.zencdn.net/7.10.2/video.min.js"></script>

    <?php
    if (!empty($function)) {
        $length2 = count($function);
        for ($x = 0; $x < $length2; $x++) {
            $this->load->view('function/' . $function[$x] . '/script');
        }
    }
    ?>

    <script>
        $(document).ready(function () {
            <?php if ($this->session->toastr) : ?>
            setTimeout(function () {
                toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
            }, 500);
            <?php $this->session->unset_userdata('toastr'); ?>
            <?php endif; ?>
        });
    </script>

    <script type="text/javascript">
        var csrfToken = get_cookie('csrfCookie');
        var siteUrl = "<?php echo site_url(); ?>";
        var baseUrl = "<?php echo base_url() ?>";
        var controller = "<?php echo $this->router->class ?>";
        var method = "<?php echo $this->router->method ?>";

        function get_cookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0)
                    return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        $(document).ready(function () {

            $(".various5").fancybox({
                maxWidth: 820,
                maxHeight: 650,
                fitToView: false,
                width: '100%',
                height: '100%',
                autoSize: false,
                closeClick: false,
                helpers: {
                    overlay: {closeClick: false}
                },
                openEffect: 'none',
                closeEffect: 'none',
                afterClose: function () {
                    parent.location.reload(true);
                }
            });


            $("#various3").fancybox({
                'maxWidth': 820,
                'maxHeight': 650,
                'width': '90%',
                'height': '90%',
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'helpers': {
                    'overlay': {closeClick: false}
                },
            });

            $("#final-exams").fancybox({
                'maxWidth': 820,
                'maxHeight': 650,
                'width': '90%',
                'height': '90%',
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'helpers': {
                    'overlay': {closeClick: false}
                },
                //afterClose:function () { parent.location.reload(true);}
            });
            $("#final-exams-p").fancybox({
                'maxWidth': 820,
                'maxHeight': 650,
                'width': '90%',
                'height': '90%',
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'helpers': {
                    'overlay': {closeClick: false}
                },
                //afterClose:function () { parent.location.reload(true);}
            });


        });
    </script>
</div>
</body>
</html>