<header>
	<nav class="navbar navbar-expand-md">
		<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
				<i class="fas fa-align-left"></i>
			</button>
			<div class="navbar-collapse collapse" id="collapsingNavbar">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" href="<?=base_url()?>articles">บทความ</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?=base_url()?>activities">ข่าวสาร</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?=base_url()?>ourteacher">คุณครูของเรา</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?=base_url()?>aboutus">เกี่ยวกับเรา</a>
					</li>
					<?php
					$Qinstructer = null;
					if ($this->session->users['UID']) {
						$iid = '#';
						$checkinstructer = modules::run('home/check_instructer');
						if ($checkinstructer->num_rows()>0) {
							$href = site_url().'instructors/courses';
						} else {
							$href = site_url().'instructors/register';
						}
						$Qinstructer = $checkinstructer->row();
					}else{
						$href = '#';
						$iid = 'loginface';
					}
					// echo $Qinstructer->approve;
					?>
					<?php if ($Qinstructer!=1) { ?>
					<li class="nav-item">
						<a class="nav-link" id="<?=$iid;?>" href="<?=$href;?>">สอนกับเรา</a>
					</li>
					<?php } ?>
					<li class="nav-item">
						<a class="nav-link" href="<?=base_url()?>contactus">ติดต่อเรา</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="menu Bold">
		<div class="container">
			<div class="blog">
				<div class="boxx1">
					<a href="<?=site_url('home');?>">
						<img src="<?=base_url('images/logo/logo.png');?>" class="logo" alt="">
					</a>
					<div class="tv">
						<a href="<?=base_url()?>courses">
							<img src="<?=base_url('images/logo/tv.JPG');?>" class="img" alt="">
							<span>คอร์สเรียนออนไลน์</span>
						</a>
					</div>
				</div>
				<div class="boxx2">
					<?php if ($this->session->users['UID']) { ?>
					<div class="login">
						<div class="dropdown img">
							<button type="button" class="btn link dropdown-toggle Bold" data-toggle="dropdown">
							<?php if(!empty($this->session->users['Imgprofile'])) { ?>
								<img src="<?=base_url($this->session->users['Imgprofile'])?>" alt="" onerror="this.src='<?php echo base_url('images/user.png');?>'">
							<?php }else{ ?>
								<img src="<?=$this->session->users['ImgprofileFace']?>" alt="" onerror="this.src='<?php echo base_url('images/user.png');?>'">
							<?php } ?>
							</button>
							<div class="dropdown-menu">
									<!-- <a class="dropdown-item" href="<?=base_url('manage_article/courses')?>">บทความของฉัน</a> -->
									<a class="dropdown-item" href="<?=base_url('my-profile')?>">ข้อมูลส่วนตัว</a>
									<a class="dropdown-item" href="<?=base_url('logout')?>">ออกจากระบบ</a>
							</div>
						</div>
							<?php
							$numrows = array();
							$noti_all = modules::run('notification/noti_all');
							foreach ($noti_all->result() as $item) {
							$noti_check = modules::run('notification/noti_check', $item->id);
							if ($noti_check->noti_id != $item->id) {
									$numrows[] = $item->id;
								}
							}		
							?>
							<a href="javascript:void(0)" id="Noti" class="link"><i class="fas fa-bell"></i> <div class="noti"><?=count($numrows);?></div></a>
							<a href="<?=base_url('cart')?>" class="link"><i class="fas fa-shopping-cart"></i> <?php if(!empty($this->session->cart_data)){ echo '<div class="noti">'.count($this->session->cart_data).'</div>'; }?></a>
							<div class="dropdown">
								<button type="button" class="btn dropdown-toggle Bold" data-toggle="dropdown">คอร์สเรียนของฉัน <i class="fas fa-angle-down"></i></button>
								<div class="dropdown-menu">
								<?php
									if($this->session->users['UserType'] == "instructor"):
									?>
										<a class="dropdown-item" href="<?=base_url('instructors/courses')?>">จัดการคอร์ส</a>
									<?php
									endif;
									?>
										<a class="dropdown-item" href="<?=base_url('profile/my-course')?>">คอร์สของฉัน</a>
										<!-- <a class="dropdown-item" href="<?=base_url('manage_article/courses')?>">บทความของฉัน</a> -->
								</div>
							</div>
						</div>
						<div id="Noti_show">
							<?php 
							foreach ($noti_all->result() as $item) {
								$datatime = explode(',', date_languagefull($item->date, true,'th'));
								$noti_check = modules::run('notification/noti_check', $item->id);
							if ($noti_check->noti_id != $item->id) {
							?>
							<div class="line">
								<a href="<?=site_url('notification/read_noti/'.$item->id)?>">
									<div class="text"><p><?=$item->title;?></p></div>
									<div class="date"><p>วันที่ <?=$datatime[0];?> , เวลา <?=$datatime[1];?></p></div>
								</a>
							</div>
							<?php } ?>
							<?php } ?>
						</div>
					
					
					<?php } else { ?>
					<div class="login">
						<a href="<?=site_url('form-register')?>" class="btn-custom">ลงทะเบียน</a>
						<a href="#" id="loginface">เข้าสู่ระบบ</a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</header>