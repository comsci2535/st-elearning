<!-- fileinput -->
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/fileinput.min.js"></script>
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/locales/th.js"></script>

<script src="<?=base_url('template/frontend');?>/input-mask/jquery.inputmask.js"></script>
<script src="<?=base_url('template/frontend');?>/icheck/icheck.min.js"></script>
<script>
// $('#file').fileinput({
//     maxFileCount: 1,
//     validateInitialCount: true,
//     overwriteInitial: false,
//     showUpload: false,
//     showRemove: true,
//     required: true,
//     initialPreviewAsData: true,
//     maxFileSize:2024,
//     browseOnZoneClick: true,
//     allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
//     browseLabel: "อัปโหลดรูป",
//     browseClass: "btn btn-primary btn-block",
//     showCaption: false,
//     showRemove: true,
//     showUpload: false,
//     removeClass: 'btn btn-danger',
//     dropZoneTitle :"Drag & drop picture here …",
// });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        var please = 'กรุณากรอก';
        var Tmin = 'ต้องมีอย่างน้อย';
        var Tmax = 'ต้องมีมากสุด';
        var truee = 'กรอกให้ถูก';

        var iCheckboxOpt = {
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey'
        }

        $("#RegisterForm").validate({
            rules: {
                fname: {
                    required: true,
                    minlength: 2
                },
                lname: {
                    required: true,
                    minlength: 2
                },
                phone: {
                    required: false,
                    number: true,
                    minlength: 9,
                    maxlength: 10,
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "login/check_username",
                        type: "post",
                        data: {id: function () {
                                return $('#input-id').val();
                            }, mode: function () {
                                return $('#input-mode').val();
                            }}
                    }
                },
                // email: {
                //     remote: {
                //         url: "login/check_username",
                //         type: "post",
                //         data: {id: function () {
                //                 return $('#id').val();
                //             }, mode: function () {
                //                 return $('#input-mode').val();
                //             }}
                //     }
                // },
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
            },
            messages: {
                fname: {
                    required: ""+please+" ชื่อ",
                    minlength: ""+Tmin+" 2 คำ"
                },
                lname: {
                    required: ""+please+" นามสกุล",
                    minlength: ""+Tmin+" 2 คำ"
                },
                phone: {
                    number: ""+truee+" เบอร์โทร",
                    minlength: ""+Tmin+" 9 ตัว",
                    maxlength: ""+Tmax+" 10 ตัว",
                },
                email: {
                    required: ""+please+" email",
                    email:  ""+please+" email ให้ถูกต้อง",
                    remote: 'พบ email ซ้ำในระบบ',
                },
                password: {
                    required: ""+please+" รหัสผ่าน",
                    minlength: ""+Tmin+" 5 คำ"
                },
                confirm_password: {
                    required: ""+please+" ยืนยันรหัสผ่าน",
                    minlength: ""+Tmin+" 5 คำ",
                    equalTo: "รหัสผ่านไม่ตรงกัน"
                },
            },
            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
        });

        $(".format-phone").inputmask({
            "placeholder": "",
            "mask" : "999 999 9999",
        });

        $('#icheck-profile').click(function(){
            if($(this).is(':checked')){
                $('#file').prop('required',true);
                $('#profile-img').removeClass('d-none');
            }else{
                $('#file').prop('required',false);
                $('#profile-img').addClass('d-none');
            }
        });

        $('#is_Condition').click(function(){
            if($(this).is(':checked')){
                // alert(1);
                $(this).prop('required',false);
                $('#text_Condition').removeClass('text-danger');
                $('#text_Condition').addClass('text-success');
            }else{
                // alert(2);
                $(this).prop('required',true);
                $('#text_Condition').removeClass('text-success');
                $('#text_Condition').addClass('text-danger');
            }
        });
        // $('input.icheck').iCheck(iCheckboxOpt);

        $("#EditForm").validate({
            rules: {
                fname: {
                    required: true,
                    minlength: 2
                },
                lname: {
                    required: true,
                    minlength: 2
                },
                phone: {
                    required: false,
                    number: true,
                    minlength: 9,
                    maxlength: 10,
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
            },
            messages: {
                fname: {
                    required: ""+please+" ชื่อ",
                    minlength: ""+Tmin+" 2 คำ"
                },
                lname: {
                    required: ""+please+" นามสกุล",
                    minlength: ""+Tmin+" 2 คำ"
                },
                phone: {
                    number: ""+truee+" เบอร์โทร",
                    minlength: ""+Tmin+" 9 ตัว",
                    maxlength: ""+Tmax+" 10 ตัว",
                },
                confirm_password: {
                    required: ""+please+" ยืนยันรหัสผ่าน",
                    minlength: ""+Tmin+" 5 คำ",
                    equalTo: "รหัสผ่านไม่ตรงกัน"
                },
            },
            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
        });

        $('#update_password_check').validate({
            rules: {
                oldPassword: {
                    remote: {
                        url: '<?=site_url('login/update_password_check')?>',
                        type: "post",
                        data: {id: function () {
                                return $('#input-id2').val();
                            }, mode: function () {
                                return $('#input-mode').val();
                            }
                        }
                    }
                },
                rePassword: {equalTo: "#input-password"}
            },
            messages: {
                oldPassword: {remote: 'รหัสผ่านปัจจุบันไม่ถูกต้อง'},
                rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
            },
            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
        });
    });
    
</script>