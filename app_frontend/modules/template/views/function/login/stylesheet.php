<!-- fileinput -->
<link href="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

<style>
    #policy {
        width: 80%;
        height: 80%;
        overflow-y: scroll;
        scrollbar-width: thin;
        border-radius: 10px;
    }

    #policy::-webkit-scrollbar
    {
        border-radius: 10px;
        width: 12px;
        background-color: #F5F5F5;
    }

    #policy::-webkit-scrollbar-thumb
    {
        border-radius: 10px;
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #038FE1;
    }

</style>
