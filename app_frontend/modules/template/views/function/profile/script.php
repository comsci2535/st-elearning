<script src="<?=base_url('template/frontend');?>/datatables_respon/datatables.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/datatables_respon/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/datatables_respon/dataTables.responsive.min.js" type="text/javascript"></script>

<script>
$(document).ready(function() {

    var url = '<?=base_url()?>';
    
    load_data_table();

    function load_data_table(){
        var table = $('#my-course').DataTable();
            table.destroy();
            table = $('#my-course').DataTable({
            serverSide: true,
            ajax: {
                url: url+"profile/ajax_data_courses",
                type: 'POST',
                data: {

                },
            },
            order: [[2, "desc"]],
            pageLength: 10,
            columns: [
                {data: "file", width: "40%", className: "course", orderable: false},
                {data: "discount", width: "10%",  className: "text-right", orderable: true},
                {data: "updated_at", width: "15%",  className: "text-center", orderable: true},
                {data: "status", width: "15%", className: "text-center", orderable: false},
                {data: "action", width: "15%", className: "", orderable: false},
            ]
        }).on('draw', function () {
        
        }).on('processing', function(e, settings, processing) {
            
        })
    }

    // $('#my-course').DataTable();
    // alert();
});
</script>
