
<!-- include summernote css/js -->
<link href="<?=base_url('template/frontend');?>/summernote-bs4/summernote-bs4.css" rel="stylesheet">
<link href="<?=base_url('template/frontend');?>/icheck/skins/square/grey.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url('template/frontend');?>/summernote-bs4/summernote-bs4.js"></script>

<link href="<?=base_url('template/frontend');?>/icheck/skins/square/blue.css" rel="stylesheet" type="text/css"/>
<!-- <script src="<?=base_url('template/frontend');?>/input-mask/jquery.inputmask.js"></script> -->
<script src="<?=base_url('template/frontend');?>/icheck/icheck.min.js"></script>

<!-- daterangepicker -->
<script type="text/javascript" src="<?=base_url('template/frontend');?>/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?=base_url('template/frontend');?>/bootstrap-daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('template/frontend');?>/bootstrap-daterangepicker/daterangepicker.css" />

<!-- fileinput -->
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/fileinput.min.js"></script>
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/locales/th.js"></script>


<!-- select2 -->
<script src="<?=base_url('template/frontend');?>/select2/select2.full.js"></script>

<!-- slug -->
<script src="<?=base_url('template/frontend');?>/slug/slug.js"></script>

<script>

var method = '<?=$this->router->fetch_method();?>';
$(document).ready(function() {

    var iCheckboxOpt = {
        checkboxClass: 'icheckbox_square-grey',
        radioClass: 'iradio_square-grey'
    }

    $('input[type="checkbox"],input[type="radio"]').iCheck(iCheckboxOpt);

    $('.summernote').summernote({
        fontSizes: ['10', '11', '12', '14', '16','18', '20' ,'22' ,'24', '36', '48' , '64', '82', '100'],
        placeholder: 'Detail this',  
        height: 300,
        tabsize: 2,
        toolbar: [
        ['style'],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['link'],
        ['table'],
        ['media',['picture','video']],
        ['hr'],
        ['misc',['fullscreen','codeview','undo','redo','help']]
        ],
        callbacks: {
            onImageUpload: function(files) {
                sendFile(files[0]);
            }
        },
    });
    if(method == 'create'){
        $('#file').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: false,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "อัปโหลดรูป",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop picture here …",
        });
    }else{
        $('#file').fileinput({
            initialPreview: [file_image],
            initialPreviewAsData: false,
            initialPreviewConfig: [
            { downloadUrl: file_image  ,extra: {id: file_id} },
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 0,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize:1024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "อัปโหลดรูป",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop icon here …",
        });
    }

     // create Daterange 
     $('input[name="dateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $(this).nextAll().eq(0).val(picker.startDate.format('YYYY-MM-DD'))
        $(this).nextAll().eq(1).val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $(this).nextAll().eq(0).val('')
        $(this).nextAll().eq(1).val('')        
    }) 
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true,
        }
    });

    $('.select2').select2();

slugURL();

function slugURL() {

  var typingTimer;                
  var doneTypingInterval = 300;  
  var $slug = $('#slug');
  var $name = $('#title');
  var $id = $('#id');

  $slug.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTypingSlug, doneTypingInterval);
      $('#slug').val(slug($slug.val()));
      $('#slug').parent().addClass('focused');
  });

  $slug.on('keydown', function () {
      clearTimeout(typingTimer);
  });

  $name.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTypingName, doneTypingInterval);
      stypingTimer = setTimeout(doneTypingSlug, doneTypingInterval);
      $('#slug').val(slug($name.val()));
      $('#slug').parent().addClass('focused');
  });

  $name.on('keydown', function () {
      clearTimeout(typingTimer);
  });
} 

function doneTypingSlug () {
  $.ajax({
      url: '<?=base_url()."slugs/check_slugs"?>',
      type: 'POST',
      dataType: 'json',
      data: {name: $('#slug').val(), id: $('#id').val() , db: $('#db').val()},
  })
  .done(function(data){
      setTimeout(function(){

          if(data){
              $('#slug').parent().removeClass('error');  
              $('#slug-error-dup').hide()
              check_dup_slug=true;

          }else{
              $('#slug-error-dup').show()
              $('#slug').parent().addClass('error'); 
              check_dup_slug=false;
          }
      }, 300);    
  })
  .fail(function(e) {
      console.log(e);
  })
  .always(function(c) {
      console.log(c);
  });
}  

function doneTypingName ($name,$id = null) {
  $.ajax({
      url: '<?=base_url()."slugs/check_name"?>',
      type: 'POST',
      dataType: 'json',
      data: {name: $('#title').val(), id: $('#id').val() , db: $('#db').val()},
  })
  .done(function(data){
      setTimeout(function(){

          if(data){
              $('#title').parent().removeClass('error');  
              $('#name-error-dup').hide()
              check_dup_slug=true;

          }else{
              $('#name-error-dup').show()
              $('#title').parent().addClass('error'); 
              check_dup_slug=false;
          }
      }, 300);    
  })
  .fail(function(e) {
      console.log(e);
  })
  .always(function(c) {
      console.log(c);
  });
}

function sendFile(file, editor, welEditable) {

    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: 'POST',
        url: "<?=base_url()?>instructors/sumernote_img_upload",
        cache: false,
        contentType: false,
        processData: false,
        success: function(data){
            $('.summernote').summernote('editor.insertImage', data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus+" "+errorThrown);
        }
    });
    } 

});
</script>