<script src="<?=base_url('template/frontend');?>/datatables/datatables.bundle.js" type="text/javascript"></script>
<script>
    
    var url = '<?=base_url()?>';
    
    load_data_table();

    function load_data_table(){
        var table = $('#data-list').DataTable();
            table.destroy();
            table = $('#data-list').DataTable({
            serverSide: true,
            responsive: true,
            ajax: {
                url: url+"instructors/ajax_data_report_courses",
                type: 'POST',
                data: {

                },
            },
            order: [[2, "DESC"]],
            pageLength: 10,
            columns: [
                {data: "no", width: "5%", className: "text-center", orderable: false},
                {data: "cycle", width: "15%", className: "text-center", orderable: true},
                {data: "title" , width: "30%", className: "", orderable: true},
                {data: "stuQty" , width: "15%", className: "text-center", orderable: false},
                {data: "cycle_date" , width: "15%", className: "text-center", orderable: false},
                // {data: "created_at" , width: "20%", className: "text-center", orderable: false},
                {data: "active", width: "20%", className: "text-center", orderable: false},
            ]
        }).on('draw', function () {
        
        }).on('processing', function(e, settings, processing) {
            
        })
    }

    $(document).on('click', '.btn-click-delete', function(){
        var appName = "<?=config_item('appName');?>";
        var id = $(this).attr('data-id');
        swal({
            title: 'กรุณายืนยันการทำรายการ',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(function(result) {
            
            if (result.value) {
                $.post(url+'instructors/action', {
                    id: id,
                    type : 'trash'
                }).done(function (data) {
                    if (data.success === true) {
                        toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", appName)
                        load_data_table();
                    }
                })
                .fail(function () {
                    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                })
            }
        });
        
    });



</script>