<script src="<?php echo base_url() ?>template/frontend/countdown/jquery.countdown.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
       // alert(1);
    });
    var siteUrl = "<?php echo site_url(); ?>";
  
    var exam_id = <?php echo $info['exam_id']; ?>;
    var exam = <?php echo $exam; ?>;
    var examNum = <?php echo $examNum; ?>;
    var prev = 1;
    var current = 1;
    var answer = <?php echo $answer ?>;
    var timeUp = <?php echo $timeUp ?>;
    var examTime = <?php echo !empty($info['examTime'])?$info['examTime']: 0 ; ?>;
    var quizId = <?php echo $quizId; ?>;
   var isObjective = "";    
   var showSolve = "";    
</script>
<script>
   show_quiz(prev, current, '') 
         //show_quiz(prev, current,'') 

        $(document).on('change', '#optionsRadios1', function(){
            exam[current]['answer'] = $('input[name=answer]:checked').val();
        });

        $(document).on('keyup', '#subjective', function(){
            exam[current]['answer'] = $('#subjective').val();
        });
        
        $("#submit-quiz").click( function() {
            if ( timeUp ) {
                submit_quiz();
            } else {
                var r = confirm("ต้องการจะส่งแบบทดสอบใช่หรือไม่?");
                if ( r == true ) {
                    submit_quiz();
                }
            }
        })

        if(examTime!=0){

            var duration = <?php echo $info['examTime']*60*1000 ?>;
            var durationCD = new Date().getTime() + duration;
            $('#clock').countdown(durationCD, {})
                .on('update.countdown', function (event) {
                    var $this = $(this);
                    if ( event.elapsed ) {
                        timeUp = true;
                    } else {
                        $this.html(event.strftime('<span>%H:%M:%S</span>'));
                    }
            })
            .on('finish.countdown', function (event){
                $(this).html(event.strftime('<span>00:00:00</span>'));
                alert('หมดเวลาทำแบบทดสอบ');
                submit_quiz();
            });  

        } 

        
   $('.quiz-no').click(function(){ 
        prev = current;    
        current = parseInt($(this).attr('id'));
        show_quiz(prev, current, isObjective);
    });

    $('.next').click(function(){
        prev = current;
        current = current + 1;
        if ( current == (examNum + 1) ) current = examNum;
        show_quiz(prev, current, isObjective);
         if(current==examNum){
              $("#submit-quiz").show();
              $("#next").hide();
        }
        if ( $("button#"+current).hasClass('btn-success') ||  $("button#"+current).hasClass('btn-warning') ) {
            $("button#"+prev).removeClass('btn-success');
            $("button#"+prev).removeClass('btn-warning');
        }
        $("button#"+current).addClass('btn-primary');      
    });

    $('.prev').click(function(){
        if ( $("button#"+current).hasClass('btn-success') || $("button#"+current).hasClass('btn-warning') ) {
            $("button#"+prev).removeClass('btn-success');
            $("button#"+prev).removeClass('btn-warning');
        }
        $("button#"+current).addClass('btn-primary');   
         $("#submit-quiz").hide();
        $("#next").show();        
        prev = current;
        current = current - 1;
        if ( current == 0 ) current = 1;
        show_quiz(prev, current, isObjective);        
    });  
    function load_exam()
    {
        var url = siteUrl+"exam/load_exam?categoryId="+$("#category").val()+"&categorySubId="+$("#category-sub").val()+"&page="+$("#page").val();
        $.get(url)
                .done(function(data){
                    $("div#load-more").remove();
                    $(".load-exam").append(data.view);
                    $("#page").val(data.page);
                })
                .fail(function(){
                    alert('contection error');
                });
    }
    
    function submit_quiz()
    {
        $.post(siteUrl+"exams/submit_quiz",{exam:exam, quizId:quizId, exam_id:exam_id, isObjective:isObjective})
            .done(function(data){
                if ( data.showSlove ) {
                    window.location.href = siteUrl+"exams/exam_solve/"+exam_id+"/"+quizId
                } else {
                    alert('false');
                }
            })
            .fail(function(){ alert('connection error') });
    }
    
    function template_exam(no, q, c, a, s, isObjective)
    {

        console.log(isObjective);
        var text = "";
        text += '<p class=""><b>' + no + '/'+examNum+'</b><br><b>ข้อที่ ' + no + ' ' + q + '</b> </p>';
        if ( isObjective == 2 ) {
            $.each(c, function( index, data ) {
                var checked = null;
                if ( index == a ) checked = "checked";
                text += '<div class="radio col-md-offset-1"><input ' + checked + ' type="radio" id="optionsRadios1" value=' + index + ' name="answer"> ' + data + '</li></div>';
            });
        } else {
            text += '<textarea id="subjective" rows="5" class="form-control" name="answer"></textarea>';
       }

        return text;
    }
    
    function show_quiz(prev, current, isObjective)
    {
        $("button#"+prev).removeClass('btn-primary');

        if ( exam[prev]['answer'] !== null && exam[prev]['answer'] !== "" ) { 
            $("button#"+prev).addClass('btn-success');
        } else {
            if ( $("button#"+prev).hasClass('btn-success') ) {
                $("button#"+prev).removeClass('btn-success');
            } else {
                $("button#"+prev).removeClass('btn-primary');
                $("button#"+prev).addClass('btn-warning');
            }
        }
        $("#exam").html(template_exam(current, exam[current]['question'], exam[current]['choice'], exam[current]['answer'], exam[current]['solve'], exam[current]['exam_type_id']));
        if ( $("button#"+current).hasClass('btn-success') ||  $("button#"+current).hasClass('btn-warning') ) {
            $("button#"+current).removeClass('btn-success');
            $("button#"+current).removeClass('btn-warning');
        }
        $("button#"+current).addClass('btn-primary');      
        
        
        
        // if ( method == "lesson_slove" ) {
        //     $('#optionsRadios1*').attr('disabled','true');
        //     $.each(exam[current]['solve'], function( index, data ) {
        //         if ( data === "1" ) {
        //             $('#optionsRadios1[value="'+index+'"]').attr('checked','true');
        //         }
        //     });            
        // }
        
    }
</script>