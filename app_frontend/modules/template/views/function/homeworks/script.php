<script src="<?=base_url('template/frontend');?>/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?=base_url('template/metronic/') ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script>
    var url = '<?=base_url()?>';

    load_data_table();

    function load_data_table(){
        var course_lesson_id = $('#course_lesson_id').val();
    
        var table = $('#data-list').DataTable();
            table.destroy();
            table = $('#data-list').DataTable({
            serverSide: true,
            ajax: {
                url: url+"homeworks/ajax_data",
                type: 'POST',
                data: {
                    course_lesson_id : course_lesson_id
                },
            },
            order: [[1, "asc"]],
            pageLength: 10,
            columns: [
                {data: "no", width: "10px", className: "text-center", orderable: false},
                {data: "title", className: "", orderable: true},
                {data: "detail", className: "", orderable: false},
                {data: "created_at", width: "100px", className: "", orderable: true},
                {data: "updated_at", width: "100px", className: "", orderable: true},
                {data: "active", width: "50px", className: "text-center", orderable: false},
                {data: "action", width: "30px", className: "text-center", orderable: false},
            ]
    }).on('draw', function () {
        
    }).on('processing', function(e, settings, processing) {
        
    })

  }

  $(document).on('click', '.btn-click-delete', function(){
    var appName = "<?=config_item('appName');?>";
    var id = $(this).attr('data-id');
    swal({
        title: 'กรุณายืนยันการทำรายการ',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก'
    }).then(function(result) {
        
        if (result.value) {
            $.post(url+'homeworks/action', {
                id: id,
                type : 'trash'
            }).done(function (data) {
                if (data.success === true) {
                    toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", appName)
                    load_data_table();
                }
            })
            .fail(function () {
                toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
            })
        }
    });
    
});
</script>