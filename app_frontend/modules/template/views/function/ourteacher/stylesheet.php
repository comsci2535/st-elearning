<!--begin::Global Theme Styles -->
<link href="<?=base_url('template/frontend/metronic');?>/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

<!--RTL version:<link href="../assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
<link href="<?=base_url('template/frontend/metronic');?>/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

<style>
    .LiveChat {
        height: 0 !important;
    }

    .m-card-profile__name {
        font-size: 32px !important;
        font-weight: 600 !important;
    }

    .m-card-profile__email{
        padding: 0 !important;
        font-size: 16px !important;
    }
    .line-buttom{
        border-bottom: 1px solid;
        width: 150px;
    }

    .m-nav__link-text, .m-nav__link-icon, .m-nav-grid__text{
        font-size: 18px !important;
    }

    .m-tabs__link{
        font-size: 20px !important;
    }

    .m-demo__preview{
        border: 0 !important;
    }
</style>

<style>
    .CoursesItem .Item .detail {
        padding: 10px !important;
    }

    @-webkit-keyframes placeHolderShimmer {
        0% {
            background-position: -468px 0;
        }
        100% {
            background-position: 468px 0;
        }
    }
    @keyframes placeHolderShimmer {
        0% {
            background-position: -468px 0;
        }
        100% {
            background-position: 468px 0;
        }
    }

    .card {
        border-radius: 10px !important;
        /* border-bottom-left-radius: 20px;
        border-bottom-right-radius: 20px; */
        color: #464749;
        
    }

    .card .card-contact{
        border-radius: 10px !important;
        /* border-top-left-radius: 20px;
        border-top-right-radius: 20px; */
        background-color: #e0e1e3;
    }

    .content-placeholder {
        display: inline-block;
        -webkit-animation-duration: 3s;
        animation-duration: 3s;
        -webkit-animation-fill-mode: forwards;
        animation-fill-mode: forwards;
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
        -webkit-animation-name: placeHolderShimmer;
        animation-name: placeHolderShimmer;
        -webkit-animation-timing-function: linear;
        animation-timing-function: linear;
        background: #f6f7f8;
        background: -webkit-gradient(
            linear,
            left top,
            right top,
            color-stop(8%, #eeeeee),
            color-stop(18%, #dddddd),
            color-stop(33%, #eeeeee)
        );
        background: -webkit-linear-gradient(
            left,
            #eeeeee 8%,
            #dddddd 18%,
            #eeeeee 33%
        );
        background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
        -webkit-background-size: 800px 104px;
        background-size: 800px 104px;
        height: inherit;
        position: relative;
    }

    .post_data {
        padding: 24px;
        border: 1px solid #f9f9f9;
        border-radius: 5px;
        margin-bottom: 24px;
        box-shadow: 10px 10px 5px #eeeeee;
    }

</style>