<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<script>

$(document).on('input', '#text-search', function(){
    $('#page').val(0);
    get_load_ourteacher_html();
});

$(document).on('click', '.btn-click-load-page', function(){
    var start = $('#page').val();
    var page = parseInt(start)+1;
    $('#page').val(page);
    get_load_ourteacher(page);
});

get_load_ourteacher_html();

function get_load_ourteacher(page){
        
    var fiter_arr = [];
    // $(".checkbox-fiter-courses:checked").each(function(){
    //     fiter_arr.push($(this).val());
    // });
    var search = $('#text-search').val();
    // var sort   = $('#text-sort').val();

    $('#form-img-div').html('<img src="<?=base_url('images/loading1.gif')?>" alt="loading" title="loading" style="display:inline" width="20">');
    $.ajax({
        type: "POST",
        url: '<?=base_url('ourteacher/ajax_load_ourteacher')?>',
        dataType: 'json',
        async: false,
        data: {
                page      : page
            ,fiter_arr : fiter_arr
            ,search    : search
            // ,sort      : sort
        },
        success: function (results) {

            if(results.status > 0){
                $('#CoursesItem').append(results.data);
                $('#form-img-div').html('');

                    if(results.pageAll==0){
                    $('#views-more-1').hide(100);
                }else{
                    $('#views-more-1').show(100);
                }
            }
        }
    })
}

function get_load_ourteacher_html(){

    var page = parseInt($('.btn-click-load-page').attr('data-page'));
    $('.btn-click-load-page').attr('data-page', page);

    
    var search = $('#text-search').val();
    var page   = $('.btn-click-load-page').attr('data-page');

    $('#form-img-div').html('<img src="<?=base_url('images/loading1.gif')?>" alt="loading" title="loading" style="display:inline" width="20">');
    $.ajax({
        type: "POST",
        url: '<?=base_url('ourteacher/ajax_load_ourteacher')?>',
        dataType: 'json',
        async: false,
        data: {
            page      : page
            ,search    : search
            // ,sort      : sort
        },
        success: function (results) {
            $('#total').html(results.total);
            if(results.status > 0){
                $('#CoursesItem').html(results.data);
                $('#form-img-div').html('');

                if(results.pageAll==0){
                    $('#views-more-1').hide(100);
                }else{
                    $('#views-more-1').show(100);
                }

                get_load_promotion();
            }else{

                $('#CoursesItem').html('');
                $('#form-img-div').html('');
                $('#views-more-1').hide(100);
                
            }
        }
    })
}
</script>


<script>
var page = 1;
    $( document ).ready(function() {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href") // activated tab
        if(target === '#m_user_profile_tab_4')
            loadMoreData(page,url='');
            $(window).scroll(function() {
                scrollDistance = $(window).scrollTop() + $(window).height();
                footerDistance = $('footer').offset().top;
                
                if (scrollDistance >= footerDistance) {
                    var  num_row = $("#num_row").val();
                    var prepage= $("#prepage").val();
                    // var category = getUrlParameter('category');
                    var url = '?page=';
                    page++;
                    if(page <= Math.ceil(num_row/prepage) ){
                        if(typeof category != "undefined"){
                            url = '?page=' + page +'&category=' + category;
                        }else{
                            url = url + page;	
                        }
                        loadMoreData(page,url);
                    }   
                }
            });
        });
        
    });
    
    function loadMoreData(page,url){
        $.ajax(
            {
                url: '<?=base_url('ourteacher/ajax_load_courses')?>' + url,
                type: "get",
                data:{
                    user_id : $('#user_id').val()
                },
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data)
            {
                // console.log(data);
                if(data == " "){
                    $('.ajax-load').html("No more records found");
                    return;
                }
                setTimeout(function(){
                    $('.ajax-load').hide();
                    $("#post-data-courses").append(data);
                    get_load_promotion();
                }, 1000);
                
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                    alert('server not responding...');
            });
    }

</script>

<script>

	function get_load_promotion(){
	    $.post('<?=base_url('promotion/get_promotion_all')?>',{

        },function(data){
            if(data){       
                var res = JSON.parse(data);
                var course_id = 0 ;
                $.each(res,function(key,value){
                    var course_id = $('#course_id-'+value.course_id).val();
                    var timeA = new Date(value.start);
                    var timeB = new Date(value.end);
                    var timeDifference = timeB.getTime()-timeA.getTime(); 

                    if(timeDifference>=0 && course_id>0){
                        timeDifference=timeDifference/1000;
                        timeDifference=Math.floor(timeDifference);
                        var wan=Math.floor(timeDifference/86400);
                        var l_wan=timeDifference%86400;
                        var hour=Math.floor(l_wan/3600);
                        var l_hour=l_wan%3600;
                        var minute=Math.floor(l_hour/60);
                        var second=l_hour%60;
                        var showPart=document.getElementById('showRemain-'+value.course_id);
                        var deadline = new Date(value.end).getTime(); 
                        var x = setInterval(function() { 
                            var now = new Date().getTime(); 
                            var t = deadline - now; 
                            var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
                            var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
                            var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
                            var seconds = Math.floor((t % (1000 * 60)) / 1000); 
                            if(days==0){
                                showPart.innerHTML="<p>0 วัน "+hours+" : "+minutes+" : "+seconds+"</p>"; 
                            }else{
                                showPart.innerHTML="<p>"+days+" วัน "+hours+" : "+minutes+" : "+seconds+"</p>"; 
                            }

                            if (t < 0) { 
                                clearInterval(x); 
                                document.getElementById('showRemain-'+value.course_id).innerHTML = ""; 
                                $('.protext-'+value.course_id).hide(100);
                                $('.protext2-'+value.course_id).show(100);
                                
                            } 
                        }, 1000);
                    }

                });
            }
        });
    }

</script>