<script src="<?=base_url('template/frontend');?>/datatables/datatables.bundle.js" type="text/javascript"></script>
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
<link rel="stylesheet" href="<?=base_url('template/frontend');?>/jquery.Thailand.js/jquery.Thailand.min.css">
<script type="text/javascript" src="<?=base_url('template/frontend');?>/jquery.Thailand.js/jquery.Thailand.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var please = 'กรุณากรอก';
        var Tmin = 'ต้องมีอย่างน้อย';
        var Tmax = 'ต้องมีมากสุด';
        var truee = 'กรอกให้ถูก';

        $("#UpdateForm").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 9,
                    maxlength: 10,
                },
                email: {
                    required: true,
                    email: true,
                },
                districts_name: {
                    required: true,
                },
                amphures_name: {
                    required: true,
                },
                provinces_name: {
                    required: true,
                },
                zip_code: {
                    required: true,
                },
                checkbox_s: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: ""+please+" ชื่อ-นามสกุล",
                    minlength: ""+Tmin+" 2 คำ"
                },
                phone: {
                    required: ""+please+" เบอร์โทร",
                    number: ""+truee+" เบอร์โทร",
                    minlength: ""+Tmin+" 9 ตัว",
                    maxlength: ""+Tmax+" 10 ตัว",
                },
                email: {
                    required: ""+please+" email",
                    email:  ""+please+" email ให้ถูกต้อง"
                },
            },
            errorElement: "em",
        });

    });
</script>

<script>
    //zun
    $.Thailand({
        database: $('#datajson').val(),
        autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
        $district: $('#district'), // input ของตำบล
        $amphoe: $('#amphoe'), // input ของอำเภอ
        $province: $('#province'), // input ของจังหวัด
        $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์
        onComplete: function(data) {
        console.log('Autocomplete is ready!')
        },
        onDataFill: function(data){
            console.log(data);
        }
    });

    $.Thailand({
        database: $('#datajson').val(),
        autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
        $district: $('#district_vat'), // input ของตำบล
        $amphoe: $('#amphoe_vat'), // input ของอำเภอ
        $province: $('#province_vat'), // input ของจังหวัด
        $zipcode: $('#zipcode_vat'), // input ของรหัสไปรษณีย์
        onComplete: function(data) {
        console.log('Autocomplete is ready!')
    },
    onDataFill: function(data){
        console.log(data);
    }
    });

    var url = '<?=base_url()?>';

    // var table = $('#data-list').DataTable();
    //     table.destroy();
    //     table = $('#data-list').DataTable({
    //     serverSide: true,
    //     ajax: {
    //         url: url+"instructors/ajax_data_courses",
    //         type: 'POST',
    //         data: {

    //         },
    //     },
    //     order: [[2, "asc"]],
    //     pageLength: 10,
    //     columns: [
    //         {data: "no", width: "10px", className: "text-center", orderable: false},
    //         {data: "img", width: "20px", className: "text-center", orderable: false},
    //         {data: "title", className: "", orderable: true},
    //         {data: "stuQty", className: "text-center", orderable: false},
    //         {data: "updated_at", width: "50px", className: "text-center", orderable: false},
    //         {data: "active", width: "50px", className: "text-center", orderable: false},
    //         {data: "action", width: "30px", className: "text-center", orderable: false},
    //     ]
    // }).on('draw', function () {
      
    // }).on('processing', function(e, settings, processing) {
        
    // })


</script>