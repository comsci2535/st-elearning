<script>
	$('#slide_banner').owlCarousel({
        center: true,
		loop: false,
		autoplay:true,
        autoplayHoverPause:true,
		margin: 10,
		items: 1,
		nav: false,
		dots: false,
		navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>']
	})
</script>

<script>
	$('#Reviews').owlCarousel({
        // center: true,
        responsive : {
            // breakpoint from 0 up
            0 : {
                items: 1,

            },
            // breakpoint from 768 up
            768 : {
                items: 3,
            }
        },
		loop: false,
		autoplay:true,
        autoplayHoverPause:true,
		margin: 10,
		nav: false,
		dots: true,
		navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>']
	})

	get_load_promotion();
	
	function get_load_promotion(){
	    $.post('<?=base_url('promotion/get_promotion_all')?>',{

        },function(data){
            if(data){       
                var res = JSON.parse(data);
                var course_id = 0 ;
                $.each(res,function(key,value){
                    var course_id = $('#course_id-'+value.course_id).val();
                    var timeA = new Date(value.start);
                    var timeB = new Date(value.end);
                    var timeDifference = timeB.getTime()-timeA.getTime(); 

                    if(timeDifference>=0 && course_id>0){
                        timeDifference=timeDifference/1000;
                        timeDifference=Math.floor(timeDifference);
                        var wan=Math.floor(timeDifference/86400);
                        var l_wan=timeDifference%86400;
                        var hour=Math.floor(l_wan/3600);
                        var l_hour=l_wan%3600;
                        var minute=Math.floor(l_hour/60);
                        var second=l_hour%60;
                        var showPart=document.getElementById('showRemain-'+value.course_id);
                        var deadline = new Date(value.end).getTime(); 
                        var x = setInterval(function() { 
                            var now = new Date().getTime(); 
                            var t = deadline - now; 
                            var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
                            var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
                            var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
                            var seconds = Math.floor((t % (1000 * 60)) / 1000); 
                            if(days==0){
                                showPart.innerHTML="<p>0 วัน "+hours+" : "+minutes+" : "+seconds+"</p>"; 
                            }else{
                                showPart.innerHTML="<p>"+days+" วัน "+hours+" : "+minutes+" : "+seconds+"</p>"; 
                            }

                            if (t < 0) { 
                                clearInterval(x); 
                                document.getElementById('showRemain-'+value.course_id).innerHTML = ""; 
                                $('.protext-'+value.course_id).hide(100);
                                $('.protext2-'+value.course_id).show(100);
                                
                            } 
                        }, 1000);
                    }

                });
            }
        });
    }
</script>