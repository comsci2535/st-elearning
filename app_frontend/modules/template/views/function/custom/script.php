<!-- OwlCarousel2 -->
<script type="text/javascript" src="<?=base_url('template/frontend/OwlCarousel2-2.3.4/owl.carousel.min.js');?>">
</script>
<!-- wow -->
<script type="text/javascript" src="<?=base_url('template/frontend/wow/dist/wow.min.js');?>"></script>
<script>
    new WOW().init();
</script>
<!-- fancybox3 -->
<script type="text/javascript" src="<?=base_url('template/frontend/fancybox3/jquery.fancybox.min.js');?>"></script>


<script>
    //------- Header Scroll Class  js --------//
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.navbar').addClass('topp');
        } else {
            $('.navbar').removeClass('topp');
        }
    });
</script>

<script>
$('#Noti').click(function(){
    $("#Noti_show").toggle();
});
$(document).mouseup(function(e) {
    var container = $("#Noti_show");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
    }
});
</script>

<!-- validate -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<div class="modal fade" style="padding:0px;" aria-hidden="true" id="ModalLogin" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content Form_login">
            <!-- Modal Header -->
            <!-- <div class="modal-header">
                <h3 class="modal-title text-center ImgFluid">เข้าสู่ระบบหรือสมัครสมาชิก</h3>
                <button type="button" class="close" data-dismiss="modal">X</button>
            </div> -->
            <button type="button" class="close" data-dismiss="modal">X</button>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="blog-col">
                    <div class="blog">
                        <div class="box-left" id="ForgotLogin_show">
                            <h2 class="text-center Bold">เข้าสู่ระบบ</h2>
                            <form id="LoginForm" class="form-signin" action="<?=site_url('login/check_login_modal');?>"
                                method="post">
                                <div class="form-label-group">
                                    <label class="Bold" for="email1">อีเมล</label>
                                    <input type="email" id="email1" name="email" class="form-control"
                                        placeholder="email" autofocus>
                                    <div class="alert_email1"></div>
                                </div>

                                <div class="form-label-group">
                                    <label class="Bold" for="password1">รหัสผ่าน</label>
                                    <input type="password" id="password1" name="password" class="form-control"
                                        placeholder="Password">
                                    <div class="alert_password1"></div>
                                </div>
                                <div id="form-error-div_login"></div>

                                <div class="remember">
                                    <!-- <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Remember password</label> -->
                                    <div class="ww">
                                        <input type="checkbox" class="custom-control-input" name="remember" value="1"
                                            id="rememberme">
                                        <label class="custom-control-label" for="rememberme">จำรหัสผ่าน</label>
                                    </div>
                                    <div class="ww r">
                                        <a class="d" href="javascript:void(0)" id="ForgotPassword"><u>ลืมรหัสผ่าน</u></a>
                                        <button class="btn btn-primary Bold" type="submit" id="btn-login"><span
                                                id="form-img-div_login"></span>เข้าสู่ระบบ</button>
                                    </div>
                                </div>

                                <div class="hr form-label-group mt-5">
                                    <br>
                                    <hr>
                                    <div class="or">หรือ</div>
                                </div>

                                <!-- <h3 class="text-center"><u>ลืมรหัสผ่าน</u></h2> -->
                                <div class="login_face">
                                    <a href="<?php echo  Modules::run('login/url_login_face'); ?>">
                                        <img src="<?=base_url('images/logo/login.JPG');?>" alt="getupschool">
                                    </a>
                                </div>
                            </form>
                        </div>
                        <div class="box-left" id="ForgotPassword_show" style="display: none;">
                            <h2 class="text-center Bold">ลืมรหัสผ่าน</h2>
                            <form id="ForgotPassword" class="form-signin" action="<?=site_url('login/forgot_password');?>" method="post">
                                <div class="form-label-group">
                                    <label for="Forgot_email">อีเมล</label>
                                    <input type="email" id="Forgot_email" name="email" class="form-control"
                                        placeholder="email" autofocus>
                                    <div class="alert_Forgot_email"></div>
                                </div>

                                <div id="form-error-div_login"></div>

                                <div class="remember">
                                    <div class="ww r">
                                        <a href="javascript:void(0)" id="ForgotLogin"><u>กลับเข้าสู่ระบบ</u></a>
                                        <button class="btn btn-primary Bold" type="submit" id="btn-login"><span
                                                id="form-img-div_login"></span>ยืนยัน</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="blog"  style="background-image: url('<?=base_url('images/login.jpg');?>');">
                        <div class="box-rigth">
                            <div class="logos">
                                <img src="<?=base_url('images/logo/logo.png');?>" class="logo" alt="">
                            </div>
                            <div class="text Bold" style="font-size: 27px;color: #000000;">
                                <h2 class="text-center" style="font-size: 38px;">WHY I JOIN</h2>
                                <p>- หลักสูตรเรียนออนไลน์</p>
                                <p>- ชุมชนพัฒนาครู</p>
                                <p>- โลก CYBER ของนักเรียนยุคใหม่</p>
                                <p>- สมัครเรียน</p>
                                <p>- เปิดสอน ได้ที่นี่</p>
                            </div>
                            <div class="text-center">
                                <a href="<?=site_url('form-register');?>">
                                    <button type="button" class="btn btn-custom">สมัครสมาชิก</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
 $('a[id="ForgotPassword"]').click(function (e) {
    // alert(112);
    $('.modal-body .box-left').hide();
    $('#ForgotPassword_show').show();
});
</script>

<script>
 $('a[id="ForgotLogin"]').click(function (e) {
    // alert(112);
    $('.modal-body .box-left').hide();
    $('#ForgotLogin_show').show();
});
</script>

<script>
    $('a[id="loginface"]').click(function (e) {
        $("#ModalLogin").modal('show');
    });
    $(document).ready(function () {


        var base_url = "<?=base_url();?>"
        $('form#LoginForm').submit(function (event) {

            var username = $("#email1").val();
            var password = $("#password1").val();

            if (username == "") {

                $('#email1').focus();
                $(".alert_email1").html('<font color=red>* กรอกชื่อผู้ใช้งานหรืออีเมล</font>');
                return false;

            } else {
                $(".alert_email1").html('');
            }


            if (password == "") {
                $('#password1').focus();
                $(".alert_password1").html('<font color=red>* กรอกรหัสผ่าน</font>');
                return false;
            } else {
                $(".alert_password1").html('');
            }


            event.preventDefault();
            var formObj = $(this);
            var formURL = formObj.attr("action");
            var formData = new FormData(this);
            $('#form-img-div_login').html('<img src="' + base_url +
                'images/loading1.gif" alt="loading" title="loading" style="display:inline">');
            $.ajax({
                url: formURL,
                type: 'POST',
                data: formData,
                dataType: "json",
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    if (data.info_txt == "error") {
                        $('#form-img-div_login').html('');
                        $("#form-error-div_login").html("<font color=red>" + data.msg + "" +
                            data.msg2 + "</font>");
                        $("#form-error-div_login").slideDown(400);

                    }
                    if (data.info_txt == "success") {

                        setTimeout(function () {
                            $("#ModalLogin").modal('hide');
                            location.reload();
                        }, 1000);

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#form-img-div_login').html(' ');
                    $("#form-error-div_login").html(
                        "<font color=red>เข้าสู่ระบบไม่สำเร็จ,กรุณาลองใหม่อีกครั้ง!</font>"
                    );
                    $("#form-error-div_login").slideDown(400);
                }

            });


        });


        $('form#ForgotPassword').submit(function (event) {

            var username = $("#Forgot_email").val();
            if (username == "") {
                $('#Forgot_email').focus();
                $(".alert_Forgot_email").html('<font color=red>* กรอกอีเมล</font>');
                return false;
            } else {
                $(".alert_Forgot_email").html('');
            }

            event.preventDefault();
            var formObj = $(this);
            var formURL = formObj.attr("action");
            var formData = new FormData(this);
            $('#ForgotPassword #form-img-div_login').html('<img src="' + base_url +
                'images/loading1.gif" alt="loading" title="loading" style="display:inline">');
            $.ajax({
                url: formURL,
                type: 'POST',
                data: formData,
                dataType: "json",
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    if (data.info_txt == "error") {
                        $('#ForgotPassword #form-img-div_login').html('');
                        $("#ForgotPassword #form-error-div_login").html("<font color=red>" + data.msg + "" +
                            data.msg2 + "</font>");
                        $("#ForgotPassword #form-error-div_login").slideDown(400);

                    }
                    if (data.info_txt == "success") {

                        setTimeout(function () {
                            $("#ModalLogin").modal('hide');
                            location.reload();
                        }, 1000);

                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#ForgotPassword #form-img-div_login').html('');
                    $("#ForgotPassword #form-error-div_login").html(
                        "<font color=red>ลืมรหัสผ่านไม่สำเร็จไม่สำเร็จ</font>"
                    );
                    $("#ForgotPassword #form-error-div_login").slideDown(400);
                }

            });

        });


    });
</script>

<script>
    $(".dropdown-s #click").click(function (event) {
        event.preventDefault();
        if ($(this).hasClass("isDown")) {
            $(".dropdown-s .text").slideUp();
        } else {
            $(".dropdown-s .text").slideDown();
        }
        $(this).toggleClass("isDown");
        return false;
    });

    function windowPopup(url, width, height) {
        // Calculate the position of the popup so
        // it’s centered on the screen.
        var left = (screen.width / 2) - (width / 2),
            top = (screen.height / 2) - (height / 2);

        window.open(
            url,
            "",
            "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" +
            top + ",left=" + left
        );
    }

    //jQuery
    $(".js-social-share").on("click", function (e) {
        e.preventDefault();

        windowPopup($(this).attr("href"), 500, 300);
    });

    // Vanilla JavaScript
    var jsSocialShares = document.querySelectorAll(".js-social-share");
    if (jsSocialShares) {
        [].forEach.call(jsSocialShares, function (anchor) {
            anchor.addEventListener("click", function (e) {
                e.preventDefault();

                windowPopup(this.href, 500, 300);
            });
        });
    }

    $(document).ready(function () {
        $("span").tooltip();
        $("button").tooltip();
    });
</script>

<script>
$(document).on('click', '#OpenChat , #CloseCaht', function(e){
    $(".UserChat").animate({
      width: "toggle"
    });
});

$(document).on('click', '#OpenChat', function(e){
    $("body").css("position","fixed");
    $("body").css("left","0");
    $("body").css("right","0");
});

$(document).on('click', '#CloseCaht', function(e){
    $("body").css("position","unset");
});

$(document).on('click', '.UserChat .ListUser .box , .MassageChat #BackCaht , .MassageChat #Close', function(e){
    $(".MassageChat").animate({
      width: "toggle"
    });
});

$(document).on('click', '.ListUser .btn-student-course', function(){
   
    var user_id   = $(this).attr('data-user-id');
    var course_id = $(this).attr('data-course-id');
    $.post(baseUrl+'chats/api_instructors',{
            user_id: user_id,
            course_id: course_id,
            csrfToken:csrfToken
    }, function(result){
        if(result.status > 0){
            $('.MassageChat .profile #text-user-img-chat-by').attr('src', result.file);
            $('.MassageChat .profile #text-user-name-chat-by span').html(result.fullname)
        }
    }).fail(function(xhr, status, error) {
        console.log(error);
        // error handling
    });
});
$(document).on('click', '#OpenChat', function(){

    var user_id   = $(this).attr('data-user-id');
    var course_id = $(this).attr('data-course-id');
    $.post(baseUrl+'chats/api_student',{
            user_id: user_id,
            course_id: course_id,
            csrfToken:csrfToken
    }, function(result){
        if(result.status > 0){
            $('.HederUser .profile #text-user-img-chat').attr('src', result.file);
            $('.HederUser .profile #text-user-name-chat span').html(result.fullname)
            if(result.student.length > 0){
                var html ='';
                $(result.student).each(function(key, val){
                    // offline, online
                    html+='<div class="box '+val.onoff+' btn-student-course" data-user-id="'+val.user_id+'" data-course-id="'+val.course_id+'">';
                        html+='<img src="'+val.file+'" class="" alt="'+val.fullname+'" onerror="'+$('.ListUser').attr('onerror')+'">';
                        html+='<div class="content">';
                            html+='<div class="name">'+val.fullname+'</div>';
                            html+='<div class="noti">0</div>';
                            html+='<i class="fas fa-paper-plane"></i>';
                        html+='</div>';
                    html+='</div>';
                    
                });

                $('.UserChat .ListUser').html(html);
            }
        }
    }).fail(function(xhr, status, error) {
        console.log(error);
        // error handling
    });
})
</script>


<script>
$(document).ready(function () {
$("#scoll a").click(function(e) {
    // alert( $(this).attr('href') );
    // Prevent a page reload when a link is pressed
    $('html, body').animate({
      scrollTop: $( $(this).attr('href') ).offset().top - 180
    }, 1000)
});
});
</script>