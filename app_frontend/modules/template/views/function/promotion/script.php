<script src="<?=base_url('template/frontend');?>/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?=base_url('template/metronic/') ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script>

    var url = '<?=base_url()?>';
    
    load_data_table();

    function load_data_table(){

        var course_id = $('#course_id').val();
        
        var table = $('#data-list').DataTable();
            table.destroy();
            table = $('#data-list').DataTable({
            serverSide: true,
            ajax: {
                url: url+"promotion/ajax_data",
                type: 'POST',
                data: {
                    course_id : course_id
                },
            },
            order: [[2, "asc"]],
            pageLength: 10,
            columns: [
                {data: "no", width: "10px", className: "text-center", orderable: false},
                {data: "title", className: "", orderable: true},
                {data: "date", className: "text-center", orderable: false},
                {data: "discount", className: "text-center", orderable: false},
                {data: "created_at", width: "50px", className: "text-center", orderable: false},
                {data: "updated_at", width: "50px", className: "text-center", orderable: false},
                {data: "active", width: "50px", className: "text-center", orderable: false},
                {data: "action", width: "30px", className: "text-center", orderable: false},
            ]
        }).on('draw', function () {
        
        }).on('processing', function(e, settings, processing) {
            
        })
    }
    
  $(document).on('click', '.btn-click-delete', function(){
    var appName = "<?=config_item('appName');?>";
    var id = $(this).attr('data-id');
    swal({
        title: 'กรุณายืนยันการทำรายการ',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก'
    }).then(function(result) {
        
        if (result.value) {
            $.post(url+'promotion/action', {
                id: id,
                type : 'trash'
            }).done(function (data) {
                if (data.success === true) {
                    toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", appName)
                    load_data_table();
                }
            })
            .fail(function () {
                toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
            })
        }
    });
    
});


</script>