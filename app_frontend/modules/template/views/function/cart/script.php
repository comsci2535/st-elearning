<script src="<?=base_url('template/metronic/') ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
 <script src="<?=base_url('template/frontend/bootstrap-fileinput-master/') ?>js/fileinput.js" type="text/javascript"></script>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
$("#file-0").fileinput({
      maxFileCount: 1,
      validateInitialCount: true,
      overwriteInitial: true,
      showUpload: false,
      showRemove: false,
      required: true,
      initialPreviewAsData: true,
      browseOnZoneClick: true,
      allowedFileExtensions: ["jpg", "png", "gif" ,"svg" , "pdf"],
      browseLabel: "อัพโหลดสลิป",
      browseClass: "btn btn-primary btn-block",
      showCaption: false,
      //showRemove: true,
      showUpload: false,
      removeClass: 'btn btn-danger',
      dropZoneTitle :"Drag & drop picture here …"
});
var base_url = "<?=base_url()?>";

$(document).ready(function () {

  $('.amount').keyup( function() {
      //alert('ff');
      $(this ).val( formatAmount( $( this ).val() ) );
      $(".total").html(formatAmount( $( this ).val() ));

      var myStr = formatAmount($( this ).val());
      var newStr = myStr.replace(/,/g, '');
      $(".discount").val(newStr);
      $("#price_paypal").val(newStr);
  });

  getCoupon();
   $('.couponCode-a').keyup(function(event) {
     $("#coupon-error-div").html('');
     getCoupon();
   });

  $('form#frm-save').submit(function(event) {

      if($('#price-a').val()=="" || $('#price-a').val()==0 ){
        $('#price-a').focus();
        $(".alert_price-a").html('<font color=red>* กรอกจำนวนเงิน</font>'); 
        return false;
      }
      if($('#file-0').val()==""){
        $('#file-0').focus();
        $(".alert_file").html('<font color=red>* เลือกไฟล์</font>'); 
        return false;
      }


      $('#form-error-div').html(' ');
      $('#form-success-div').html(' ');

     // var conf = archiveFunction();
      //alert(conf);
     // if(confirm("ยืนยันการแจ้งชำระเงินอีกครั้ง !!")){


        event.preventDefault();             
        var formObj = $(this);
        var formURL = formObj.attr("action");
        var formData = new FormData(this);
        $('#form-img-div').html('<img src="'+base_url+'images/loading1.gif" alt="loading" title="loading" style="display:inline">');
        $.ajax({
          url: formURL,
          type: 'POST',
          data:formData,
          dataType:"json",
          mimeType:"multipart/form-data",
          contentType: false,
          cache: false,
          processData:false,
          success: function(data, textStatus, jqXHR){

           if(data.info_txt == "error")
           {
             $('#form-img-div').html(' ');
             $("#form-error-div").append("<p><strong><li class='text-red'></li>"+data.msg+"</strong>,"+data.msg2+"</p>");
             $("#form-error-div").slideDown(400);

           }
           if(data.info_txt == "success")
           {
                
                  setTimeout(function(){
                      $('#form-img-div').html(' ');
                      swal({
                          title: 'คุณทำรายการเรียบร้อย',
                          text: "ขอให้สนุกในการเรียนนะคะ",
                          type: 'success',
                          confirmButtonText: 'ตกลง'
                      }).then(function(result) {
                          
                          if (result.value) {
                              window.location=base_url+'profile/my-course';
                          }
                      });
                    },2000);

                }

              },
              error: function(jqXHR, textStatus, errorThrown){
               $('#form-img-div').html(' ');
               $("#form-error-div").append("<p><strong>บันทึกข้อมูลไม่สำเร็จ</strong>,กรุณาลองใหม่อีกครั้ง!</p>");
               $("#form-error-div").slideDown(400);
             }

           });
      // }else{
      //   return false;
      // }

    });

    function getCoupon(){
      var couponCode = $(".couponCode-a").val();
      var course_id = $(".course_id-a").val();
     // alert(course_id);
        if(couponCode){
          $("#coupon-error-div").html('');
          $("#omise-button2").html('');
          $.post(base_url+'coupons/check',{couponCode:couponCode,course_id:course_id},function(data){
            console.log(data);
            var res = JSON.parse(data);

            if(res.info_txt == "error")
            {
              $("#discount-0").html('');
              $("#discount-1").show(100);
              $("#sl1").hide(100);
              if($(".promotion").val()){
                 var price = $(".promotion").val();
              }else{
                 
                 var price = $("#price").val();
              }

              var total= formatAmount(price);
              $(".total").html(total);
              $(".coupon_id").val('');

              var newStr = total.replace(/,/g, '');
              $("#price_paypal").val(newStr);
              if($(".promotion").val()){
                $(".discount").val($(".promotion").val());
              }else{
                $(".discount").val($("#price").val());
              }
              
             
              $("#coupon-error-div").html("<font color=red><strong>"+res.msg+"</strong>,"+res.msg2+"</font>");
              //$("#coupon-error-div").slideDown(400);

              return false;  
            }

            if(res.info_txt == "success")
            {
              $("#sl1").show(100);
             
              if($(".promotion").val()){
                 var price = $(".promotion").val();
              }else{
                 
                 var price = $("#price").val();
              }

               //alert($("price").val());
             
              var coupon_id = res.coupon_id;
              var type = res.type_;
              var discount = res.discount;

              if(type==2){
                var rt=(price*discount)/100;
                var d = price-rt ;
                var total= formatAmount(d.toFixed(0));
                $("#sl").html(formatAmount(discount)+'%');
              }else{
                var rt=discount;
                var total= formatAmount(price-discount);
                $("#sl").html(formatAmount(discount)+' บาท');
              }
              // alert(rt);

              if(total==0){
                $("#discount-0").html(' <a href="javascript:clickFree();"  id="loginface" class="btn-custom Bold">คลิกเพื่อเข้าเรียน</a>');
                $("#discount-1").hide(100);
              }else{
                $("#discount-0").html('');
                $("#discount-1").show(100);
              }

              $(".total").html(total);
              

              $(".coupon_id").val(coupon_id);

              var newStr = total.replace(/,/g, '');
              $("#price_paypal").val(newStr);
              $(".discount").val(newStr);
              pay_omise(newStr);

              $("#coupon-error-div").html("<font color=green><strong>รหัสคูปองถูกต้อง</strong></font>");
              //$("#coupon-error-div").slideDown(400);

              return false;  

            }
          });
        }else{

          $("#discount-0").html('');
          $("#discount-1").show(100);
          $("#sl1").hide(100);

          if($(".promotion").val()){
             var price = $(".promotion").val();
          }else{
             
             var price = $("#price").val();
          }
          var total= formatAmount(price);
          $(".total").html(total);
          $(".coupon_id").val('');

          if($(".promotion").val()){
            $(".discount").val($(".promotion").val());
          }else{
            $(".discount").val($("#price").val());
          }

          var newStr = total.replace(/,/g, '');
          $("#price_paypal").val(newStr);

        }
     }

     function formatAmountNoDecimals( number ) {
        var rgx = /(\d+)(\d{3})/;
        while( rgx.test( number ) ) {
          var number = number.replace( rgx, '$1' + ',' + '$2' );
        }
        return number;
    }

    function formatAmount(str) {
      //    
      var number = str.toString();
      var number = number.replace( /[^0-9]/g, '' );
      //alert(number);
      var num = number.split(',');
      var num1 = num[0];
      var num2 = num.length > 1 ? ',' + num[1] : '';

      return formatAmountNoDecimals( num1 ) + num2;
    }
});
$(document).on('click','#btn-a-delete', function(){
	//alert(1)
    $.post(base_url+'cart/delete_cart', {course_id: $(this).attr('data-id')}, function(result){
       location.href = base_url+'cart'
    });
});

paypal.Button.render({
  // Set your environment
  env: 'production', // sandbox | production
  // Specify the style of the button
  style: {
    label: 'checkout',
      size:  'responsive',    // small | medium | large | responsive
      shape: 'pill',     // pill | rect
      color: 'gold',      // gold | blue | silver | black
      layout: 'horizontal',
      fundingicons: 'true',
    },
    funding: {
      allowed: [ paypal.FUNDING.CARD ],
      disallowed: [ paypal.FUNDING.CREDIT ]
    },
  // PayPal Client IDs - replace with your own
  // Create a PayPal app: https://developer.paypal.com/developer/applications/create
  client: {
      //sandbox: 'AU3XZt24lGXOOnlwYy5pNlwXF7kwS4DOfEZNDKT6Lb118Cy6TIT9in44aVSa1joWajec4_dFgo9L2BvF',
      //production: 'AQsixQN-9JIGcJiuspAd7XOBY7Cc4qd3Lg8w6UvO5Pq8hDDzhxfiDlppuNbGdKjq6omYF4SCrndy9COx'
      sandbox: 'AU3XZt24lGXOOnlwYy5pNlwXF7kwS4DOfEZNDKT6Lb118Cy6TIT9in44aVSa1joWajec4_dFgo9L2BvF',
      production: 'Af771Yh1NIo-x_0gLQgUMds-LPmhM81S5y194AV2RAPt7gLjulSlOc1Uku8seCgxUcf6wO9kBd--bMMg'
    },
    commit:true,

    payment: function(data, actions) {
      var price_paypal = $("#price_paypal").val();
      //console.log(price_paypal);
      return actions.payment.create({
        payment: {
          transactions: [
          {
            amount: { total: price_paypal , currency: 'THB' }
          }
          ]
        }
      });
    },
    onAuthorize: function(data, actions) {
    	//console.log(data.paymentID);
      var price = $("#price").val();
      var discount = $("#discount").val();
      var promotion = $("#promotion").val();
      var paymentID = data.paymentID;
      var coupon_id = $("#coupon_id").val();
      return actions.payment.execute().then(function() { 

        $.post(base_url+'payment/payment_paypal',
        {
        	price : price,
          discount:discount,
          promotion:promotion,
          paymentID:paymentID,
          coupon_id:coupon_id
        },function(data_result){
            
             var res = JSON.parse(data_result);
             if(res.info_txt == "error")
             {
               

             }
             if(res.info_txt == "success")
             {   
                 swal({
                    title: 'คุณทำรายการเรียบร้อย',
                    text: "ขอให้สนุกในการเรียนนะคะ",
                    type: 'success',
                    confirmButtonText: 'ตกลง'
                }).then(function(result) {
                    
                    if (result.value) {
                        window.location=base_url+'profile/my-course';
                    }
                });
            }
            

          });


      });
    }

}, '#paypal-button-container');

function clickFree()
{
      var price = $("#price").val();
      var discount = $("#discount").val();
      var promotion = $("#promotion").val();
      var coupon_id = $("#coupon_id").val();

      $.post(base_url+'payment/payment_free',
        {
          price : price,
          discount:discount,
          promotion:promotion,
          coupon_id:coupon_id
        },function(data_result){
            
             var res = JSON.parse(data_result);
             if(res.info_txt == "error")
             {
               

             }
             if(res.info_txt == "success")
             {   
                swal({
                    title: 'คุณทำรายการเรียบร้อย',
                    text: "ขอให้สนุกในการเรียนนะคะ",
                    type: 'success',
                    confirmButtonText: 'ตกลง'
                }).then(function(result) {
                    if (result.value) {
                        window.location=base_url+'profile/my-course';
                    }
                });
            }
            

        });
}

function check_file_img(obj)
{
  var filename = obj.value;
  var valid_extensions = /(\.jpeg|\.jpg|\.png|\.gif|\.pdf)$/i;
  if(valid_extensions.test(filename)){
    $(".alert_file").html(''); 
    return true;
  }else{
    //alert('รูปแบบไฟล์ไม่ถูกต้อง !');
    $(".alert_file").html('<font color=red>* รูปแบบไฟล์ไม่ถูกต้อง</font>'); 
    obj.value='';
  }
}


</script>
