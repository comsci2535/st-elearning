<link href="<?=base_url('template/metronic/') ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	#open_buy .t-mobile {
		    display: none;
		    width: 100%;
	}
	.btn-green {
	    background: #28a745;
	    color: #fff;
	    font-size: 22px;
	    padding: 1px 10px;
	    border-radius: 30px;
	}
	.btn-default {
	    background: #ccc;
	    color: #fff;
	    font-size: 22px;
	    padding: 1px 10px;
	    border-radius: 30px;
	}
	@media (min-width: 576px){
		.modal-dialog {
		    max-width: 60%;
		    top: calc(100vh - 95%);
		}
	}
	@media only screen and (max-width: 768px){
		#open_buy .t-mobile {
		    display: inline-block;
		}
		#open_buy .t-pc {
		    display: none;
		}
	}


</style>

 <link href="<?=base_url('template/frontend/bootstrap-fileinput-master/') ?>css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>