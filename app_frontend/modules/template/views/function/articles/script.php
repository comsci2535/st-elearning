<!-- sidebar-menu -->
<script>
$(function(){
    // bind change event to select
    $('#category').on('change', function () {
        var url = $(this).val(); // get selected value
        console.log(url);
        if (url) { // require a URL
            window.location = url+'#title'; // redirect
        }
        return false;
    });
});

$(document).on('click', '.btn-click-load-page', function(){
        var start = $('#page').val();
        var page = parseInt(start)+1;
        $('#page').val(page);
        get_load_article(page);
});

$(document).on('click', '.checkbox-fiter-articles', function(){
    
    $('#page').val(0);
    get_load_article_html();
});

$(document).on('input', '#text-search', function(){
        
    $('#page').val(0);
    get_load_article_html();
});

get_load_article_html();

function get_load_article_html(){

    var page = parseInt($('.btn-click-load-page').attr('data-page'));
    $('.btn-click-load-page').attr('data-page', page);

    
    var fiter_arr = [];
    $(".checkbox-fiter-articles:checked").each(function(){
        fiter_arr.push($(this).val());
    });
  
    var search = $('#text-search').val();
    var sort   = $('#text-sort').val();
    var page   = $('.btn-click-load-page').attr('data-page');

    $('#form-img-div').html('<img src="<?=base_url('images/loading1.gif')?>" alt="loading" title="loading" style="display:inline" width="20">');
    $.ajax({
        type: "POST",
        url: '<?=base_url('articles/ajax_load_articles')?>',
        dataType: 'json',
        async: false,
        data: {
            page      : page
            ,fiter_arr : fiter_arr
            ,search    : search
            // ,sort      : sort
        },
        success: function (results) {
            $('#total').html(results.total);
            if(results.status > 0){
                $('#CoursesItem').html(results.data);
                $('#form-img-div').html('');

                if(results.pageAll==0){
                    $('#views-more-1').hide(100);
                }else{
                    $('#views-more-1').show(100);
                }
            }else{

                $('#CoursesItem').html('');
                $('#form-img-div').html('');
                $('#views-more-1').hide(100);
                
            }
        }
    })
}

function get_load_article(page){
    var fiter_arr = [];
    $(".checkbox-fiter-articles:checked").each(function(){
        fiter_arr.push($(this).val());
    });
  
    var search = $('#text-search').val();
    var sort   = $('#text-sort').val();
   

    $('#form-img-div').html('<img src="<?=base_url('images/loading1.gif')?>" alt="loading" title="loading" style="display:inline" width="20">');
    $.ajax({
        type: "POST",
        url: '<?=base_url('articles/ajax_load_articles')?>',
        dataType: 'json',
        async: false,
        data: {
            page      : page
            ,fiter_arr : fiter_arr
            ,search    : search
            // ,sort      : sort
        },
        success: function (results) {
            $('#total').html(results.total);
            if(results.status > 0){
                $('#CoursesItem').append(results.data);
                $('#form-img-div').html('');

                if(results.pageAll==0){
                    $('#views-more-1').hide(100);
                }else{
                    $('#views-more-1').show(100);
                }
            }else{

                $('#CoursesItem').html('');
                $('#form-img-div').html('');
                $('#views-more-1').hide(100);
                
            }
        }
    })
}
</script>
