<script src="<?=base_url('template/frontend');?>/datatables_respon/datatables.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/datatables_respon/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/datatables_respon/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/metronic/') ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>

<script>
    

    var url = '<?=base_url()?>';
    
    load_data_table();

    function load_data_table(){
        var course_id  = $('#course_id').val();
        var table = $('#data-list').DataTable();
            table.destroy();
            table = $('#data-list').DataTable({
            serverSide: true,
            ajax: {
                url: url+"courses_students/ajax_data_courses",
                type: 'POST',
                data: {
                    course_id: course_id
                },
            },
            order: [[2, "asc"]],
            pageLength: 10,
            columns: [
                {data: "no", width: "", className: "text-center", orderable: false},
                {data: "img", width: "", className: "text-center", orderable: false},
                {data: "fullname", width: "", className: "", orderable: true},
                {data: "email", width: "", className: "text-center", orderable: false},
                {data: "phone", width: "", className: "text-center", orderable: false},
                {data: "created_at", width: "", className: "text-center", orderable: false},
                {data: "active", width: "", className: "text-center", orderable: false},
                {data: "action", width: "", className: "text-center", orderable: false},
            ]
        }).on('draw', function () {
        
        }).on('processing', function(e, settings, processing) {
            
        })
    }

    $(document).on('click', '.btn-click-view', function(){
        var user_id = $(this).attr('data-user-id');
        var course_id = $(this).attr('data-course-id');
        
        $.post(url+'courses_students/ajax_data_lesson', {
            user_id   : user_id,
            course_id : course_id
        }).done(function (result) {

            $('#text-courses-title').html(result.courses.title);
            var html = '';
            $.each(result.data, function(i, val) {
                html+= '<tr>';
                    html+= '<td>'+val.title+'</td>';
                    html+= '<td>';
                    html+= '</td>';
                html+= '</tr>';
                if(val.info_lesson.length > 0){
                    $.each(val.info_lesson, function(i, val_lesson) {
                        html+= '<tr>';
                        html+= '<td>&nbsp&nbsp&nbsp&nbsp&nbsp'+val_lesson.title+'</td>';
                        html+= '<td>';
                            if(val_lesson.lesson.length > 0){
                                html+= '<div class="progress">';
                                    html+= '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: '+parseInt(val_lesson.lesson[0].persen)+'%;">';
                                    html+= parseInt(val_lesson.lesson[0].persen)+'%';
                                    html+= '</div>';
                                html+= '</div>';
                            }
                        html+= '</td>';
                    html+= '</tr>';
                    });
                }
                
            });

            $('#table-text-lesson tbody').html(html);
            // table-text-lesson
            $('#modal-show-lesson').modal('show');
        })
        .fail(function () {
            
        })
    });

    $(document).on('click', '.btn-click-quiz', function(){
        var user_id = $(this).attr('data-user-id');
        var course_id = $(this).attr('data-course-id');
        
        $.post(url+'courses_students/ajax_data_quiz', {
            user_id   : user_id,
            course_id : course_id
        }).done(function (result) {
            
            if(result.data.pretest != undefined){
                $('#div-quiz-1').show();
                $('#msg-quiz-1').hide();
                $('#tab-quiz-1 .text-score-qty').html(result.data.pretest.qty);
                $('#tab-quiz-1 .text-courses').html(result.data.pretest.exam.title);
                $('#tab-quiz-1 .text-score').html(result.data.pretest.score);
                var html_pretest     = '';
                var html_pretest_chk = '';
                var html_pretestby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                $.each(result.data.pretest.exam_detail, function(key, val) {
                    html_pretest+='<tr>';
                        html_pretest+='<td>';
                            html_pretest+='<p>'+(key+1)+'.) '+val.exam_topic_name+'</p>';
                            html_pretest+='<ul>';
                            $.each(val.exam_answer, function(i, item) {
                                
                                html_pretest_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                if(item.point > 0){
                                    html_pretest_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                }
                                if(item.quiz_select.length > 0 && item.point > 0){
                                    html_pretestby_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                }else if(item.quiz_select.length > 0 && item.point == 0){
                                    html_pretestby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                }

                                html_pretest+='<li>'+item.exam_answer_name+' '+html_pretest_chk+'</li>';
                            });
                            html_pretest+='</ul>';
                        html_pretest+='</td>';
                        html_pretest+='<td>';
                            html_pretest+= html_pretestby_chk;
                        html_pretest+='</td>';
                    html_pretest+='</tr>';
                });

                $('#tab-quiz-1 #table-quiz-1 tbody').html(html_pretest);
            }else{
                $('#div-quiz-1').hide();
                $('#msg-quiz-1').show();
            }

            if(result.data.posttest != undefined){
                $('#div-quiz-2').show();
                $('#msg-quiz-2').hide();
                $('#tab-quiz-2 .text-score-qty').html(result.data.posttest.qty);
                $('#tab-quiz-2 .text-courses').html(result.data.posttest.exam.title);
                $('#tab-quiz-2 .text-score').html(result.data.posttest.score);
                var html_posttest       = '';
                var html_posttest_chk   = '';
                var html_posttestby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                $.each(result.data.posttest.exam_detail, function(key, val) {
                    html_posttest+='<tr>';
                        html_posttest+='<td>';
                            html_posttest+='<p>'+(key+1)+'.) '+val.exam_topic_name+'</p>';
                            html_posttest+='<ul>';
                            $.each(val.exam_answer, function(i, item) {

                                html_posttest_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                if(item.point > 0){
                                    html_posttest_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                }
                                if(item.quiz_select.length > 0 && item.point > 0){
                                    html_posttestby_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                }else if(item.quiz_select.length > 0 && item.point == 0){
                                    html_posttestby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                }

                                html_posttest+='<li>'+item.exam_answer_name+' '+html_posttest_chk+'</li>';
                            });
                            html_posttest+='</ul>';
                        html_posttest+='</td>';
                        html_posttest+='<td>';
                            html_posttest+= html_posttestby_chk;
                        html_posttest+='</td>';
                    html_posttest+='</tr>';
                });
                
                $('#tab-quiz-2 #table-quiz-2 tbody').html(html_posttest);
            }else{
                $('#div-quiz-2').hide();
                $('#msg-quiz-2').show();
            }

            if(result.data.final != undefined){
                $('#div-quiz-3').show();
                $('#msg-quiz-3').hide();
                $('#tab-quiz-3 .text-score-qty').html(result.data.final.qty);
                $('#tab-quiz-3 .text-courses').html(result.data.final.exam.title);
                $('#tab-quiz-3 .text-score').html(result.data.final.score);
                var html_final       = '';
                var html_final_chk   = '';
                var html_finalby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                $.each(result.data.final.exam_detail, function(key, val) {

                    html_final+='<tr>';
                        html_final+='<td>';
                            html_final+='<p>'+(key+1)+'.) '+val.exam_topic_name+'</p>';
                            html_final+='<ul>';
                                $.each(val.exam_answer, function(i, item) {

                                    html_final_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                    if(item.point > 0){
                                        html_final_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                    }
                                    if(item.quiz_select.length > 0 && item.point > 0){
                                        html_finalby_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                                    }else if(item.quiz_select.length > 0 && item.point == 0){
                                        html_finalby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                                    }

                                    html_final+='<li>'+item.exam_answer_name+' '+html_final_chk+'</li>';
                                });
                            html_final+='</ul>';
                        html_final+='</td>';
                        html_final+='<td>';
                            html_final+= html_finalby_chk;
                        html_final+='</td>';
                    html_final+='</tr>';
                   
                });
                
                $('#tab-quiz-3 #table-quiz-3 tbody').html(html_final);
            }else{
                $('#div-quiz-3').hide();
                $('#msg-quiz-3').show();
            }
            
            $('#modal-show-quiz').modal('show');
        })
        .fail(function () {
            
        })
    });

    $(document).on('click', '.btn-click-homeworks', function(){
        var user_id = $(this).attr('data-user-id');
        var course_id = $(this).attr('data-course-id');
        
        $.post(url+'courses_students/ajax_data_homeworks', {
            user_id   : user_id,
            course_id : course_id
        }).done(function (result) {
            var html = '';
            if(result.data.length){
                $.each(result.data, function(key, val) {
                    html += '<p><strong> บทเรียน</strong> : '+val.title+'</p>';
                    html += '<table id="" class="table table-hover" width="100%">';
                    html += '<thead>';
                        html += '<tr>';
                            html += '<th style="width:5%">ลำดับ</th>';
                            html += '<th class="text-left" style="width:30%">คำถาม</th>';
                            html += '<th class="text-left" style="width:30%">คำตอบ</th>';
                            html += '<th class="text-left" style="width:15%">ไฟล์</th>';
                        html += '</tr>';
                    html += '</thead>';
                    html += '<tbody>';
                    
                    if(val.courses_homeworks.length){
                        var i = 1;
                        $.each(val.courses_homeworks, function(key, list) {
                            var detail ='ไม่มีคำตอบ';
                            var url_link ='ไม่มีไฟล์';
                            if(list.info_homeworks.length){
                                detail = list.info_homeworks[0].detail;
                                if(list.info_homeworks[0].file != null){
                                    url_link = '<a href="'+url+list.info_homeworks[0].file+'" target="_blank" download=""><i class="fa fa-download" aria-hidden="true"></i> ดาวน์โหลดไฟล์</a>';
                                }else{
                                    url_link = url_link;
                                }
                            }
                        html += '<tr>';
                            html += '<td class="text-center">'+i+'</td>';
                            html += '<td>'+list.title+'</td>';
                            html += '<td>'+detail+'</td>';
                            html += '<td>'+url_link+'</td>';
                        html += ' </tr>';
                            i++;
                        });
                    }else{
                        html += '<tr>';
                            html += '<td class="text-center" colspan="4">ไม่มีการบ้าน</td>'; 
                        html += ' </tr>';
                    }
                    html += '</tbody>';
                    html += '</table>';
                });
            }
            $('#modal-show-homeworks #display-homeworks').html(html);

            $('#modal-show-homeworks').modal('show');
        })
        .fail(function () {
            
        })
    });
</script>