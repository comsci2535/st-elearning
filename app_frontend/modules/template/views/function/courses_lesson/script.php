<script src="<?= base_url('template/metronic/') ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template/frontend/bootstrap-fileinput-master/') ?>js/fileinput.js"
        type="text/javascript"></script>
<!-- <script src="<?php echo base_url("template/frontend/") ?>plyr-master/dist/plyr.js"></script> -->
<script src="<?= base_url('template/metronic/') ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.js"
        type="text/javascript"></script>

<script type="text/javascript">
    var base_url = "<?=base_url()?>";
    var quiz_pretest = "<?php echo isset($quiz_pretest) ? $quiz_pretest : ''?>";
    var quiz_posttest = "<?php echo isset($quiz_posttest) ? $quiz_posttest : ''?>";
    var course_id = "<?php echo isset($course_slug) ? $course_slug : ''?>";
    var exam_id = "<?php echo isset($exams_pretest_posttest['exam_id']) ? $exams_pretest_posttest['exam_id'] : ''?>";
    $(document).ready(function () {
        window.HELP_IMPROVE_VIDEOJS = false;
        //updateDonutChart('#specificChart-'+course_lesson_id, (respone.percent * 100).toFixed(1), true);
        var elem1 = document.getElementsByClassName("input_course_lesson_id");
        for (var i = 0; i < elem1.length; ++i) {

            var id_ = elem1[i].value;
            persen = $('#persen-' + id_).val();
            updateDonutChart('#specificChart-' + id_, persen, true);

        }

        lesson_persen(course_id);
        $('[data-toggle="tooltip"]').tooltip();


        $(".overlate-hide").click(function () {
            var hide = $(this).attr('data-hide');
            if (hide == 0) {
                $(this).html('<i class="fas fa-minus"></i> ย่อข้อความ');
                $("#" + $(this).attr('data-line')).css({"height": "100%", "max-height": "100%"});
                $(this).attr('data-hide', 1);
            } else {
                $(this).html('<i class="fas fa-plus"></i> ดูเพิ่มเติม');
                $("#" + $(this).attr('data-line')).css({"height": "", "max-height": ""});
                $(this).attr('data-hide', 0);
            }

        });


    });

    var course_lesson_id = $('#course_lesson_id').val();
    if (!quiz_pretest) {
        plusvideo(course_lesson_id, false);
        swal({
            title: 'คอร์สเรียนนี้มีแบบทดสอบก่อนเรียนและหลังเรียน',
            text: "รบกวนทำแบบทดสอบก่อนเข้าเรียนนะคะ ขอบคุณค่ะ",
            type: 'warning',
            confirmButtonText: 'ตกลง'
        }).then(function (result) {

            if (result.value) {
                var urlLink = base_url + 'exams/index-pretest/' + course_id + '/' + exam_id;
                $('#exams-final-p').find('a').attr('href', urlLink).click();
                //window.location = urlLink;
                //  window.location=base_url+'exams/index-pretest/'+course_id+'/'+exam_id;
            }
        });
    } else {
        plusvideo(course_lesson_id, true);
    }

    $('#frm-homework').validate({
        rules: {
            detail: true
        }
    });

    $("#file").fileinput({
        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: true,
        showUpload: false,
        showRemove: false,
        required: false,
        initialPreviewAsData: true,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg",
            "png",
            "gif",
            "jpeg",
            "pdf",
            "bmp",
            "doc",
            "docx",
            "txt",
            "xls",
            "xlsx",
            "csv",
            "pptx",
            "zip",
            "7z"],
        browseLabel: "อัพโหลดไฟล์",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        //showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle: "Drag & drop picture here …"
    });

    $(document).on('click', '.btn-click-homeworks', function () {
        $('#courses_homework_id').val($(this).attr('data-id'));
        $('#text-data-name').html($(this).attr('data-name'));
        $('#modal-homeworks').modal('show');
    })

    $('html').on('click', 'a.vimeo-b', function () {

        var vimeo_id = this.id.split('-')[1];
        var vimeoHeight = $(this).outerHeight();
        var vimeoWidth = $(this).outerWidth();
        $('#course_lesson_id').val(vimeo_id);

        plusvideo(vimeo_id, true);
        update_view_video();

        //set_course_lesson();

        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    });

    function plusvideo(course_lesson_id, play = true) {

        $.post(base_url + 'courses_lesson/get_course_content', {course_lesson_id: course_lesson_id}, function (data) {

            if (data) {
                var obj = JSON.parse(data);
                console.log(obj);
                //var $iframe = '<div id="plyr-vimeo-example-'+course_lesson_id+'"  data-vimeo-id="'+obj.course_content_l.videoLink+'"></div>';
                var img = '';
                if (obj.fileUrl != '') {
                    var img = base_url + obj.fileUrl;
                }

                var youtubelink = obj.course_content_l.videoLink;
                var iframe = 'https://www.youtube.com/embed/' + youtubelink
                console.log(iframe);
                $('.listt').removeClass('active');
                $('#listt-' + course_lesson_id).addClass('active');
                $('#youtube').attr('src',iframe);
                //$('.dd').html($iframe);
                $('.detail-content').html(obj.course_content_l.descript);
                $('.title-content').html(obj.course_content_l.title_);
                $('.courses-exercise').html(obj.course_content_l.exercise);
                $('.file-upload').html(obj.course_content_l.file);


                $("#various3").fancybox({
                    'maxWidth': 820,
                    'maxHeight': 650,
                    'width': '90%',
                    'height': '90%',
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'type': 'iframe',
                    'helpers': {
                        'overlay': {closeClick: false}
                    },
                });
                // console.log(obj.course_content_l.status.persen);
                if (obj.course_content_l.status.persen == '100') {
                    play = false;
                }
                var options = {
                    autoplay: play,
                };

                var options_videojs = {};
                /*
                var players = new Vimeo.Player("plyr-vimeo-example-"+course_lesson_id,options);

                if(play==true){
                   players.setCurrentTime(obj.course_content_l.status.seconds).then(function() {
                     return players.play();
                   });
                }


                players.on('timeupdate', function(respone) {
                   updateDonutChart('#specificChart-'+course_lesson_id, (respone.percent * 100).toFixed(1), true);
                   $('#persen-'+course_lesson_id).val((respone.percent * 100));
                   $('#seconds-'+course_lesson_id).val(respone.seconds);

                });

                players.on('pause', function(respone) {
                   update_view_video();

                });

                players.on('ended', function(event) {
                   update_view_video();
                     //   var course_slug = course_id;

                });
                */

            }


        });


    }

    function lesson_persen(course_slug) {

        $.post(base_url + 'courses_lesson/lesson_persen', {course_slug: course_slug}, function (data) {

            if (data) {

                var obj = JSON.parse(data);
                //console.log(obj.quiz_posttest)
                if (obj.exams_pretest_posttest && obj.pretest_posttest == 1 && obj.lesson_persen >= 85 && !obj.quiz_posttest && quiz_pretest) {

                    swal({
                        title: 'คุณเรียนคอร์สนี้เกิน 85% แล้ว',
                        text: "คุณสามารถทำแบบทดสอบหลังเรียน ขอบคุณค่ะ",
                        type: 'warning',
                        confirmButtonText: 'ตกลง'
                    }).then(function (result) {
                        $('#final-exams-b').hide();
                        $('#final-exams').show();
                        if (result.value) {
                            var urlLink = base_url + 'exams/index-posttest/' + course_slug + '/' + exam_id;
                            $('#exams-final-p').find('a').attr('href', urlLink).click();
                            //window.location=base_url+'exams/index-posttest/'+course_slug+'/'+exam_id;
                        }
                    });
                }
            }

        });


    }

    function update_view_video() {

        var elem1 = document.getElementsByClassName("input_course_lesson_id");
        var input_course_lesson_id = [];
        for (var i = 0; i < elem1.length; ++i) {
            if (typeof elem1[i].value !== "undefined") {
                input_course_lesson_id.push(elem1[i].value);
            }
        }

        var elem2 = document.getElementsByClassName("input_persen");
        var persen = [];
        for (var i = 0; i < elem2.length; ++i) {
            if (typeof elem2[i].value !== "undefined") {
                persen.push(elem2[i].value);
            }
        }

        var elem3 = document.getElementsByClassName("input_seconds");
        var seconds = [];
        for (var i = 0; i < elem3.length; ++i) {
            if (typeof elem3[i].value !== "undefined") {
                seconds.push(elem3[i].value);
            }
        }

        $.post(base_url + 'courses_lesson/update_view_video', {
            course_lesson_id: input_course_lesson_id,
            persen: persen,
            seconds: seconds
        }, function (data) {

            if (data) {

                var obj2 = JSON.parse(data);

            } else {

            }

        });

        lesson_persen(course_id);
    }

    function updateDonutChart(el, percent, donut) {
        percent = Math.round(percent);
        if (percent > 100) {
            percent = 100;
        } else if (percent < 0) {
            percent = 0;
        }
        var deg = Math.round(360 * (percent / 100));

        if (percent > 50) {
            $(el + ' .pie').css('clip', 'rect(auto, auto, auto, auto)');
            $(el + ' .right-side').css('transform', 'rotate(180deg)');
        } else {
            $(el + ' .pie').css('clip', 'rect(0, 1em, 1em, 0.5em)');
            $(el + ' .right-side').css('transform', 'rotate(0deg)');
        }
        if (donut) {
            $(el + ' .right-side').css('border-width', '0.1em');
            $(el + ' .left-side').css('border-width', '0.1em');
            $(el + ' .shadow-c').css('border-width', '0.1em');
        } else {
            $(el + ' .right-side').css('border-width', '0.5em');
            $(el + ' .left-side').css('border-width', '0.5em');
            $(el + ' .shadow-c').css('border-width', '0.5em');
        }
        $(el + ' .num').text(percent);
        $(el + ' .left-side').css('transform', 'rotate(' + deg + 'deg)');
    }

    // Pass in a number for the percent
    $('#percent').change(function () {
        var percent = $(this).val();
        var donut = $('#donut input').is(':checked');
        updateDonutChart('#specificChart', percent, donut);
    }).keyup(function () {
        var percent = $(this).val();
        var donut = $('#donut input').is(':checked');
        updateDonutChart('#specificChart', percent, donut);
    });
    ;

    $('#donut input').change(function () {
        var donut = $('#donut input').is(':checked');
        var percent = $("#percent").val();
        if (donut) {
            $('#donut span').html('Donut');
        } else {
            $('#donut span').html('Pie');
        }
        updateDonutChart('#specificChart', percent, donut);
    });

</script>