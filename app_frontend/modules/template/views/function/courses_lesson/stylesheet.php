<link href="<?=base_url('template/metronic/') ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='<?php echo base_url("template/frontend/") ?>plyr-master/dist/plyr.css' type='text/css' />
<style type="text/css">
.btn-2 {
    line-height: 1;
    font-size: 22px;
    color: #fff;
    cursor: pointer;
}
.btn-2 {
    display: inline-block;
    font-weight: 400;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
  }
</style>

<style type="text/css">
	#open_buy .t-mobile {
		    display: none;
		    width: 100%;
	}
	.btn-green {
	    background: #28a745;
	    color: #fff;
	    font-size: 22px;
	    padding: 1px 10px;
	    border-radius: 30px;
	}
	.btn-default {
	    background: #ccc;
	    color: #fff;
	    font-size: 22px;
	    padding: 1px 10px;
	    border-radius: 30px;
	}
	@media (min-width: 576px){
		.modal-dialog {
		    max-width: 60%;
		    top: calc(100vh - 95%);
		}
	}
	@media only screen and (max-width: 768px){
		#open_buy .t-mobile {
		    display: inline-block;
		}
		#open_buy .t-pc {
		    display: none;
		}
	}


</style>

 <link href="<?=base_url('template/frontend/bootstrap-fileinput-master/') ?>css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>