
<!-- include summernote css/js -->
<link href="<?=base_url('template/frontend');?>/summernote-bs4/summernote-bs4.css" rel="stylesheet">
<link href="<?=base_url('template/frontend');?>/icheck/skins/square/grey.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url('template/frontend');?>/summernote-bs4/summernote-bs4.js"></script>

<link href="<?=base_url('template/frontend');?>/icheck/skins/square/blue.css" rel="stylesheet" type="text/css"/>
<!-- <script src="<?=base_url('template/frontend');?>/input-mask/jquery.inputmask.js"></script> -->
<script src="<?=base_url('template/frontend');?>/icheck/icheck.min.js"></script>

<!-- daterangepicker -->
<script type="text/javascript" src="<?=base_url('template/frontend');?>/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?=base_url('template/frontend');?>/bootstrap-daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('template/frontend');?>/bootstrap-daterangepicker/daterangepicker.css" />

<!-- fileinput -->
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/fileinput.min.js"></script>
<script src="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/js/locales/th.js"></script>


<!-- select2 -->
<script src="<?=base_url('template/frontend');?>/select2/select2.full.js"></script>

<!-- slug -->
<script src="<?=base_url('template/frontend');?>/slug/slug.js"></script>

<script>

var method = '<?=$this->router->fetch_method();?>';
$(document).ready(function() {

    var iCheckboxOpt = {
        checkboxClass: 'icheckbox_square-grey',
        radioClass: 'iradio_square-grey'
    }

    $('input[type="checkbox"],input[type="radio"]').iCheck(iCheckboxOpt);

    $('.summernote').summernote({
        fontSizes: ['10', '11', '12', '14', '16','18', '20' ,'22' ,'24', '36', '48' , '64', '82', '100'],
        placeholder: 'Detail this',  
        height: 300,
        tabsize: 2,
        toolbar: [
        ['style'],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['link'],
        ['table'],
        ['media',['picture','video']],
        ['hr'],
        ['misc',['fullscreen','codeview','undo','redo','help']]
        ],
        callbacks: {
            onImageUpload: function(files) {
                sendFile(files[0]);
            }
        },
    });
    if(method == 'create'){
        $('#file').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: false,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            allowedFileExtensions: [
                "jpg",
                "png", 
                "gif",
                "jpeg",
                "pdf",
                "bmp",
                "doc",
                "docx",
                "txt",
                "xls",
                "xlsx",
                "csv",
                "pptx",
                "zip",
                "7z",
            ],
            browseLabel: "อัปโหลดไฟล์เอกสาร",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop picture here …",
        });
    }else{
        $('#file').fileinput({
            initialPreview: [file_image],
            initialPreviewConfig: [
                { 
                    filename: file_image,
                    downloadUrl: file_image,
                    extra: {
                        id: file_id, csrfToken: get_cookie('csrfCookie')
                    },
                   
                },
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
            previewFileIconSettings: { // configure your icon file extensions
                'doc': '<i class="fas fa-file-word fa-2x text-primary"></i>',
                'xls': '<i class="fas fa-file-excel fa-2x text-success"></i>',
                'ppt': '<i class="fas fa-file-powerpoint fa-2x text-danger"></i>',
                'pdf': '<i class="fas fa-file-pdf fa-2x text-danger"></i>',
                'zip': '<i class="fas fa-file-archive fa-2x text-muted"></i>',
                'htm': '<i class="fas fa-file-code fa-2x text-info"></i>',
                'txt': '<i class="fas fa-file-alt fa-2x text-info"></i>',
                'mov': '<i class="fas fa-file-video fa-2x text-warning"></i>',
                'mp3': '<i class="fas fa-file-audio fa-2x text-warning"></i>',
                // note for these file types below no extension determination logic 
                // has been configured (the keys itself will be used as extensions)
                'jpg': '<i class="fas fa-file-image fa-2x text-danger"></i>', 
                'gif': '<i class="fas fa-file-image fa-2x text-muted"></i>', 
                'png': '<i class="fas fa-file-image fa-2x text-primary"></i>'
            },
            previewFileExtSettings: { // configure the logic for determining icon file extensions
                'doc': function(ext) {
                    return ext.match(/(doc|docx)$/i);
                },
                'xls': function(ext) {
                    return ext.match(/(xls|xlsx)$/i);
                },
                'ppt': function(ext) {
                    return ext.match(/(ppt|pptx)$/i);
                },
                'zip': function(ext) {
                    return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
                },
                'htm': function(ext) {
                    return ext.match(/(htm|html)$/i);
                },
                'txt': function(ext) {
                    return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
                },
                'mov': function(ext) {
                    return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
                },
                'mp3': function(ext) {
                    return ext.match(/(mp3|wav)$/i);
                }
            },
            allowedFileExtensions: [
                "jpg",
                "png", 
                "gif",
                "jpeg",
                "pdf",
                "bmp",
                "doc",
                "docx",
                "txt",
                "xls",
                "xlsx",
                "csv",
                "pptx",
                "zip",
                "7z",
            ],
            browseLabel: "อัปโหลดไฟล์เอกสาร",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop icon here …",
        });
    }

    // create Daterange 
     $('input[name="dateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $(this).nextAll().eq(0).val(picker.startDate.format('YYYY-MM-DD'))
        $(this).nextAll().eq(1).val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $(this).nextAll().eq(0).val('')
        $(this).nextAll().eq(1).val('')        
    }) 
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true,
        }
    });

    $('.select2').select2();

    var parent_id = $('select[name=parent_id]').val();
    if(parent_id!=0){
        $('#c-content').show(100);    
    }else{
        $('#c-content').hide(100);   
    } 

    $('select[name=parent_id]').change(function(obj) {
        var id = $('select[name=parent_id]').val();
         if(id!=0){
            $('#c-content').show(100);    
        }else{
            $('#c-content').hide(100); 
        }      
    });

    $("[name='fileTypeUpload']").on('ifChecked', function () { 
        displayUploadType($(this).val());
    });

    
    $('.select2').change(function () {
        $('.frm-create').validate().element('.select2')
    })

    if($('input[name=fileTypeUploadType]:checked').val()==1)
    {
        $('#f-file').show(100);
        $('#f-url').hide(100);
       
    } else if($('input[name=fileTypeUploadType]:checked').val()==2){
        $('#f-file').hide(100);
        $('#f-url').show(100);
       
    }

    function displayUploadType(value){
        if(value==1){
            $('#f-file').show(100);
            $('#f-url').hide(100);
        }else if(value==2){
            $('#f-file').hide(100);
            $('#f-url').show(100);
        } 
    }


    function sendFile(file, editor, welEditable) {

        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: 'POST',
            url: "<?=base_url()?>instructors/sumernote_img_upload",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
                $('.summernote').summernote('editor.insertImage', data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus+" "+errorThrown);
            }
        });
    } 

});
</script>