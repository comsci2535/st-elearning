<script src="https://player.vimeo.com/api/player.js"></script>

<script>

    var url = '<?=base_url()?>';

    course_id2 = $('#course_id2').val();
    var options = {
        autoplay: true,
    };
    if (course_id2) {
        playPlayer(course_id2);
    }

    if ($('#overlate-detail').height() > 150) {
        $('#overlate-detail').addClass('overlate');
        $('a#overlate').show();
    } else {
        $('#overlate-detail').addClass('overlate');
        $('a#overlate').hide();
    }

    $('div.pp .view-overlate').each(function () {

        if ($(this).height() > 120) {
            $(this).addClass('p');
            $('[data-line="' + $(this).attr('id') + '"]').show();
        } else {
            $(this).addClass('p');
            $('[data-line="' + $(this).attr('id') + '"]').hide();
        }
    })

    videojs('my-video', {
        controls: true,
        autoplay: true,
        preload: 'auto',
        responsive: true,
        height:500
    });

    function playPlayer(course_id) {

        recommendVideo = $('#recommendVideo').val();
        var $iframe = '<div id="player-' + course_id + '"  data-vimeo-id="' + recommendVideo + '"></div>';

        $('#dd').html($iframe);
        var players = new Vimeo.Player("player-" + course_id, options);
        players.setCurrentTime(0).then(function () {
            return players.play();
        });
    }

    function pausePlayer(id) {

        var handstickPlayerC = new Vimeo.Player('handstick-c-' + id);
        handstickPlayerC.pause();

        $('#contentVideo-' + id).modal('hide')
    }
</script>

<script>
    $('#slide_banner').owlCarousel({
        center: true,
        loop: true,
        autoplay: true,
        // autoplayHoverPause:true,
        margin: 10,
        items: 1,
        nav: false,
        dots: false,
        responsive: {
            // breakpoint from 0 up
            0: {
                stagePadding: 0,
            },
            768: {
                stagePadding: 180,
            },
            992: {
                stagePadding: 250,
            },
            1200: {
                stagePadding: 350,
            },
            1366: {
                stagePadding: 400,
            },
        },
        navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>']
    })
</script>

<script>

    var base_url = "<?=base_url()?>";
    var course_id = $("#course_id").val();

    $(document).on('click', '#btn-a-courses', function () {
        $.post(base_url + 'cart/update_cart', {course_id: $(this).attr('data-id')}, function (result) {

        });
    });

    $(document).on('click', '#btn-a-courses-2', function () {
        $.post(base_url + 'cart/update_cart', {course_id: $(this).attr('data-id')}, function (result) {
            location.href = base_url + 'cart'
        });
    });

</script>

<script>

    $(document).on('click', '.btn-click-view', function () {
        var user_id = $(this).attr('data-user-id');
        var course_id = $(this).attr('data-course-id');

        $.post(url + 'courses_students/ajax_data_lesson', {
            user_id: user_id,
            course_id: course_id
        }).done(function (result) {

            $('#text-courses-title').html(result.courses.title);
            var html = '';
            $.each(result.data, function (i, val) {
                html += '<tr>';
                html += '<td>' + val.title + '</td>';
                html += '<td>';
                html += '</td>';
                html += '</tr>';
                if (val.info_lesson.length > 0) {
                    $.each(val.info_lesson, function (i, val_lesson) {
                        html += '<tr>';
                        html += '<td>&nbsp&nbsp&nbsp&nbsp&nbsp' + val_lesson.title + '</td>';
                        html += '<td>';
                        if (val_lesson.lesson.length > 0) {
                            html += '<div class="progress">';
                            html += '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: ' + parseInt(val_lesson.lesson[0].persen) + '%;">';
                            html += parseInt(val_lesson.lesson[0].persen) + '%';
                            html += '</div>';
                            html += '</div>';
                        }
                        html += '</td>';
                        html += '</tr>';
                    });
                }

            });

            $('#table-text-lesson tbody').html(html);
            // table-text-lesson
            $('#modal-show-lesson').modal('show');
        })
            .fail(function () {

            })
    });

    $(document).on('click', '.btn-click-quiz', function () {
        var user_id = $(this).attr('data-user-id');
        var course_id = $(this).attr('data-course-id');

        $.post(url + 'courses_students/ajax_data_quiz', {
            user_id: user_id,
            course_id: course_id
        }).done(function (result) {

            if (result.data.pretest != undefined) {
                $('#div-quiz-1').show();
                $('#msg-quiz-1').hide();
                $('#tab-quiz-1 .text-score-qty').html(result.data.pretest.qty);
                $('#tab-quiz-1 .text-courses').html(result.data.pretest.exam.title);
                $('#tab-quiz-1 .text-score').html(result.data.pretest.score);
                var html_pretest = '';
                var html_pretest_chk = '';
                var html_pretestby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                $.each(result.data.pretest.exam_detail, function (key, val) {
                    html_pretest += '<tr>';
                    html_pretest += '<td>';
                    html_pretest += '<p>' + (key + 1) + '.) ' + val.exam_topic_name + '</p>';
                    html_pretest += '<ul>';
                    $.each(val.exam_answer, function (i, item) {

                        html_pretest_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                        if (item.point > 0) {
                            html_pretest_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                        }
                        if (item.quiz_select.length > 0 && item.point > 0) {
                            html_pretestby_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                        } else if (item.quiz_select.length > 0 && item.point == 0) {
                            html_pretestby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                        }

                        html_pretest += '<li>' + item.exam_answer_name + ' ' + html_pretest_chk + '</li>';
                    });
                    html_pretest += '</ul>';
                    html_pretest += '</td>';
                    html_pretest += '<td>';
                    html_pretest += html_pretestby_chk;
                    html_pretest += '</td>';
                    html_pretest += '</tr>';
                });

                $('#tab-quiz-1 #table-quiz-1 tbody').html(html_pretest);
            } else {
                $('#div-quiz-1').hide();
                $('#msg-quiz-1').show();
            }

            if (result.data.posttest != undefined) {
                $('#div-quiz-2').show();
                $('#msg-quiz-2').hide();
                $('#tab-quiz-2 .text-score-qty').html(result.data.posttest.qty);
                $('#tab-quiz-2 .text-courses').html(result.data.posttest.exam.title);
                $('#tab-quiz-2 .text-score').html(result.data.posttest.score);
                var html_posttest = '';
                var html_posttest_chk = '';
                var html_posttestby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                $.each(result.data.posttest.exam_detail, function (key, val) {
                    html_posttest += '<tr>';
                    html_posttest += '<td>';
                    html_posttest += '<p>' + (key + 1) + '.) ' + val.exam_topic_name + '</p>';
                    html_posttest += '<ul>';
                    $.each(val.exam_answer, function (i, item) {

                        html_posttest_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                        if (item.point > 0) {
                            html_posttest_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                        }
                        if (item.quiz_select.length > 0 && item.point > 0) {
                            html_posttestby_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                        } else if (item.quiz_select.length > 0 && item.point == 0) {
                            html_posttestby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                        }

                        html_posttest += '<li>' + item.exam_answer_name + ' ' + html_posttest_chk + '</li>';
                    });
                    html_posttest += '</ul>';
                    html_posttest += '</td>';
                    html_posttest += '<td>';
                    html_posttest += html_posttestby_chk;
                    html_posttest += '</td>';
                    html_posttest += '</tr>';
                });

                $('#tab-quiz-2 #table-quiz-2 tbody').html(html_posttest);
            } else {
                $('#div-quiz-2').hide();
                $('#msg-quiz-2').show();
            }

            if (result.data.final != undefined) {
                $('#div-quiz-3').show();
                $('#msg-quiz-3').hide();
                $('#tab-quiz-3 .text-score-qty').html(result.data.final.qty);
                $('#tab-quiz-3 .text-courses').html(result.data.final.exam.title);
                $('#tab-quiz-3 .text-score').html(result.data.final.score);
                var html_final = '';
                var html_final_chk = '';
                var html_finalby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                $.each(result.data.final.exam_detail, function (key, val) {

                    html_final += '<tr>';
                    html_final += '<td>';
                    html_final += '<p>' + (key + 1) + '.) ' + val.exam_topic_name + '</p>';
                    html_final += '<ul>';
                    $.each(val.exam_answer, function (i, item) {

                        html_final_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                        if (item.point > 0) {
                            html_final_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                        }
                        if (item.quiz_select.length > 0 && item.point > 0) {
                            html_finalby_chk = '<i class="fa fa-check" aria-hidden="true"></i> ถูก';
                        } else if (item.quiz_select.length > 0 && item.point == 0) {
                            html_finalby_chk = '<i class="fa fa-times" aria-hidden="true"></i> ผิด';
                        }

                        html_final += '<li>' + item.exam_answer_name + ' ' + html_final_chk + '</li>';
                    });
                    html_final += '</ul>';
                    html_final += '</td>';
                    html_final += '<td>';
                    html_final += html_finalby_chk;
                    html_final += '</td>';
                    html_final += '</tr>';

                });

                $('#tab-quiz-3 #table-quiz-3 tbody').html(html_final);
            } else {
                $('#div-quiz-3').hide();
                $('#msg-quiz-3').show();
            }

            $('#modal-show-quiz').modal('show');
        })
            .fail(function () {

            })
    });

    $(document).on('click', '.btn-click-homeworks', function () {
        var user_id = $(this).attr('data-user-id');
        var course_id = $(this).attr('data-course-id');

        $.post(url + 'courses_students/ajax_data_homeworks', {
            user_id: user_id,
            course_id: course_id
        }).done(function (result) {
            var html = '';
            if (result.data.length) {
                $.each(result.data, function (key, val) {
                    html += '<p><strong> บทเรียน</strong> : ' + val.title + '</p>';
                    html += '<table id="" class="table table-hover" width="100%">';
                    html += '<thead>';
                    html += '<tr>';
                    html += '<th style="width:5%">ลำดับ</th>';
                    html += '<th class="text-left" style="width:30%">คำถาม</th>';
                    html += '<th class="text-left" style="width:30%">คำตอบ</th>';
                    html += '<th class="text-left" style="width:15%">ไฟล์</th>';
                    html += '</tr>';
                    html += '</thead>';
                    html += '<tbody>';

                    if (val.courses_homeworks.length) {
                        var i = 1;
                        $.each(val.courses_homeworks, function (key, list) {
                            var detail = 'ไม่มีคำตอบ';
                            var url_link = 'ไม่มีไฟล์';
                            if (list.info_homeworks.length) {
                                detail = list.info_homeworks[0].detail;
                                if (list.info_homeworks[0].file != null) {
                                    url_link = '<a href="' + url + list.info_homeworks[0].file + '" target="_blank" download=""><i class="fa fa-download" aria-hidden="true"></i> ดาวน์โหลดไฟล์</a>';
                                } else {
                                    url_link = url_link;
                                }
                            }
                            html += '<tr>';
                            html += '<td class="text-center">' + i + '</td>';
                            html += '<td>' + list.title + '</td>';
                            html += '<td>' + detail + '</td>';
                            html += '<td>' + url_link + '</td>';
                            html += ' </tr>';
                            i++;
                        });
                    } else {
                        html += '<tr>';
                        html += '<td class="text-center" colspan="4">ไม่มีการบ้าน</td>';
                        html += ' </tr>';
                    }
                    html += '</tbody>';
                    html += '</table>';
                });
            }
            $('#modal-show-homeworks #display-homeworks').html(html);

            $('#modal-show-homeworks').modal('show');
        })
            .fail(function () {

            })
    });

    $(".overlate-hide").click(function () {
        var hide = $(this).attr('data-hide');
        if (hide == 0) {
            $(this).html('<i class="fas fa-minus"></i> ย่อข้อความ');
            $("#" + $(this).attr('data-line')).css({"height": "100%", "max-height": "100%"});
            $(this).attr('data-hide', 1);
        } else {
            $(this).html('<i class="fas fa-plus"></i> ดูเพิ่มเติม');
            $("#" + $(this).attr('data-line')).css({"height": "", "max-height": ""});
            $(this).attr('data-hide', 0);
        }

    });

    $(document).on('click', '.btn-click-load-page', function () {
        var start = $('#page').val();
        var page = parseInt(start) + 1;
        $('#page').val(page);
        get_load_courses(page);
    });

    $('#review-form-submit-btn').click(function () {
        var score = $('input[name=rating]:checked').val();
        var comment = $('#review-form-comment').val();
        if (!score) {
            alert('กรุณาเลือกคะแนน');
            return false;
        }

        if (comment == "") {
            alert('กรุณากรอกความเห็น');
            return false;
        }

        $.post(base_url + 'reviews/reviews_stars_save', {
            course_id: course_id,
            score: score,
            comment: comment
        }, function (result) {
            if (result.status > 0) {
                getReviewsStar();
                $('#review-form-comment').val('');
                $('input[name="rating"]').prop('checked', false);
            }
        });
    });

    getReviewsStar();

    function getReviewsStar() {

        $.post(base_url + 'reviews/ajax_reviews_stars', {
            course_id: course_id
        }, function (result) {
            $("#progress-1").find('div').css({'width': result.percen1 + '%'});
            $("#progress-2").find('div').css({'width': result.percen2 + '%'});
            $("#progress-3").find('div').css({'width': result.percen3 + '%'});
            $("#progress-4").find('div').css({'width': result.percen4 + '%'});
            $("#progress-5").find('div').css({'width': result.percen5 + '%'});
            $("#percen-1").find('div').text(result.percen1 + '%');
            $("#percen-2").find('div').text(result.percen2 + '%');
            $("#percen-3").find('div').text(result.percen3 + '%');
            $("#percen-4").find('div').text(result.percen4 + '%');
            $("#percen-5").find('div').text(result.percen5 + '%');
            $("#p-value").text(result.v);
            var html = '';
            for (i = 1; i <= 5; i++) {
                if (i <= parseInt(result.v)) {
                    html += '<li class="active" style="padding: 2px;"><i class="fa fa-star"></i></li>';
                } else {
                    html += '<li class="" style="padding: 2px;"><i class="fa fa-star"></i></li>';
                }
            }
            $('#star_amount .cours-star').html(html);

        });
    }

    $(document).on('click', '.checkbox-fiter-courses', function () {

        $('#page').val(0);
        get_load_courses_html();
    });

    $(document).on('keypress', '#text-search', function () {

        $('#page').val(0);
        get_load_courses_html();
    });

    $(document).on('change', '#text-sort', function () {

        $('#page').val(0);
        get_load_courses_html();
    });

    get_load_courses_html();

    function get_load_courses(page) {

        var fiter_arr = [];
        $(".checkbox-fiter-courses:checked").each(function () {
            fiter_arr.push($(this).val());
        });
        var search = $('#text-search').val();
        var sort = $('#text-sort').val();

        $('#form-img-div').html('<img src="<?=base_url('images/loading1.gif')?>" alt="loading" title="loading" style="display:inline" width="20">');
        $.ajax({
            type: "POST",
            url: '<?=base_url('courses/ajax_load_courses')?>',
            dataType: 'json',
            async: false,
            data: {
                page: page
                , fiter_arr: fiter_arr
                , search: search
                , sort: sort
            },
            success: function (results) {

                if (results.status > 0) {
                    $('#CoursesItem').append(results.data);
                    $('#form-img-div').html('');

                    if (results.pageAll == 0) {
                        $('#views-more-1').hide(100);
                    } else {
                        $('#views-more-1').show(100);
                    }
                    get_load_promotion();
                }
            }
        })
    }

    function get_load_courses_html() {

        var page = parseInt($('.btn-click-load-page').attr('data-page'));
        $('.btn-click-load-page').attr('data-page', page);

        var fiter_arr = [];
        $(".checkbox-fiter-courses:checked").each(function () {
            fiter_arr.push($(this).val());
        });
        var search = $('#text-search').val();
        var sort = $('#text-sort').val();
        var page = $('.btn-click-load-page').attr('data-page');

        $('#form-img-div').html('<img src="<?=base_url('images/loading1.gif')?>" alt="loading" title="loading" style="display:inline" width="20">');
        $.ajax({
            type: "POST",
            url: '<?=base_url('courses/ajax_load_courses')?>',
            dataType: 'json',
            async: false,
            data: {
                page: page
                , fiter_arr: fiter_arr
                , search: search
                , sort: sort
            },
            success: function (results) {
                $('#total').html(results.total);
                if (results.status > 0) {
                    $('#CoursesItem').html(results.data);
                    $('#form-img-div').html('');

                    if (results.pageAll == 0) {
                        $('#views-more-1').hide(100);
                    } else {
                        $('#views-more-1').show(100);
                    }
                    get_load_promotion();
                } else {

                    $('#CoursesItem').html('');
                    $('#form-img-div').html('');
                    $('#views-more-1').hide(100);

                }
            }
        })
    }

    function get_load_promotion() {
        $.post('<?=base_url('promotion/get_promotion_all')?>', {}, function (data) {
            if (data) {
                // console.log(data)
                var res = JSON.parse(data);
                var course_id = 0;
                $.each(res, function (key, value) {
                    var course_id = $('#course_id-' + value.course_id).val();
                    var timeA = new Date(value.start);
                    var timeB = new Date(value.end);
                    var timeDifference = timeB.getTime() - timeA.getTime();

                    if (timeDifference >= 0 && course_id > 0) {
                        //alert(course_id);
                        timeDifference = timeDifference / 1000;
                        timeDifference = Math.floor(timeDifference);
                        var wan = Math.floor(timeDifference / 86400);
                        var l_wan = timeDifference % 86400;
                        var hour = Math.floor(l_wan / 3600);
                        var l_hour = l_wan % 3600;
                        var minute = Math.floor(l_hour / 60);
                        var second = l_hour % 60;
                        var showPart = document.getElementById('showRemain-' + value.course_id);
                        var showPartMobile = document.getElementById('showRemain-mobile-' + value.course_id);
                        var showPartbycourses = document.getElementById('showRemainbycourses-' + value.course_id);
                        var showRemainrecommend = document.getElementById('showRemainrecommend-' + value.course_id);
                        var showPartC = document.getElementsByClassName('showRemainC-' + value.course_id);
                        var deadline = new Date(value.end).getTime();

                        var x = setInterval(function () {
                            var now = new Date().getTime();
                            var t = deadline - now;
                            var days = Math.floor(t / (1000 * 60 * 60 * 24));
                            var hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                            var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
                            var seconds = Math.floor((t % (1000 * 60)) / 1000);
                            if (days == 0) {
                                showPart.innerHTML = "<p>0 วัน " + hours + " : " + minutes + " : " + seconds + "</p>";
                                showPartMobile.innerHTML = "<p>0 วัน " + hours + " : " + minutes + " : " + seconds + "</p>";
                                // showRemainrecommend.innerHTML="<p>0 วัน "+hours+" : "+minutes+" : "+seconds+"</p>"; 
                                // showPartbycourses.innerHTML="0 วัน "+hours+" : "+minutes+" : "+seconds;
                                //  showPartC.innerHTML="0 วัน "+hours+" : "+minutes+" : "+seconds;
                            } else {
                                showPart.innerHTML = "<p>" + days + " วัน " + hours + " : " + minutes + " : " + seconds + "</p>";
                                showPartMobile.innerHTML = "<p>" + days + " วัน " + hours + " : " + minutes + " : " + seconds + "</p>";

                                // showRemainrecommend.innerHTML="<p>"+days+" วัน "+hours+" : "+minutes+" : "+seconds+"</p>";
                                // showPartbycourses.innerHTML= days+" วัน "+hours+" : "+minutes+" : "+seconds; 
                                // showPartC.innerHTML= days+" วัน "+hours+" : "+minutes+" : "+seconds; 

                            }

                            if (t < 0) {
                                clearInterval(x);
                                document.getElementById('showRemain-' + value.course_id).innerHTML = "";
                                document.getElementById('showRemain-mobile-' + value.course_id).innerHTML = "";
                                $('.protext-' + value.course_id).hide(100);
                                $('.protext2-' + value.course_id).show(100);

                            }
                        }, 1000);
                    }

                });
            }
        });
    }
</script>