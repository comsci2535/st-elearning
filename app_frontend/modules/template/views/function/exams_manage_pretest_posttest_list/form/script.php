
<link href="<?=base_url('template/frontend');?>/icheck/skins/square/grey.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url('template/frontend');?>/icheck/icheck.min.js"></script>


<script>

var method = '<?=$this->router->fetch_method();?>';
$(document).ready(function() {

    var iCheckboxOpt = {
        checkboxClass: 'icheckbox_square-grey',
        radioClass: 'iradio_square-grey'
    }

    $('input[type="checkbox"],input[type="radio"]').iCheck(iCheckboxOpt);


    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true,
        }
    });
});

</script>