<!--begin::Global Theme Bundle -->
<script src="<?=base_url('template/metronic');?>/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<script src="<?=base_url('template/metronic/') ?>assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script>
var siteUrl = "<?php echo site_url()?>";
// alert(2);
$(document).ready(function(e) {

    $('form#frm-save').submit(function(event) {
      

        if($('#fname').val()==""){
            $('#fname').focus();
            $(".alert_fname").html('<font color=red>* กรอกชื่อ</font>'); 
            return false;
        }
        if($('#lname').val()==""){
            $('#lname').focus();
            $(".alert_lname").html('<font color=red>* กรอกนามสกุล</font>'); 
            return false;
        }
        if($('#email').val()==""){
            $('#email').focus();
            $(".alert_email").html('<font color=red>* กรอกอีเมล</font>'); 
            return false;
        }
         if($('#title').val()==""){
            $('#title').focus();
            $(".alert_title").html('<font color=red>* กรอกอีหัวข้อ</font>'); 
            return false;
        }
        if($('#detail').val()==""){
            $('#detail').focus();
            $(".alert_detail").html('<font color=red>* กรอกข้อความ</font>'); 
            return false;
        }

         $('#form-error-div').html(' ');
         $('#form-success-div').html(' ');


            event.preventDefault();             
            var formObj = $(this);
            var formURL = formObj.attr("action");
            var formData = new FormData(this);
           
           $('#form-img-div').html('<img src="<?=base_url('images/loading1.gif')?>" alt="loading" title="loading" style="display:inline" width="20">');
            $.ajax({
              url: formURL,
              type: 'POST',
              data:formData,
              dataType:"json",
              mimeType:"multipart/form-data",
              contentType: false,
              cache: false,
              processData:false,
              success: function(data, textStatus, jqXHR){
                console.log(data);
                 if(data.info_txt == "error")
                  {
                       $('#form-img-div').html(' ');
                       $("#form-error-div").append("<p><strong><li class='text-red'></li>"+data.msg+"</strong>,"+data.msg2+"</p>");
                       $("#form-error-div").slideDown(400);
                      
                  }
                  if(data.info_txt == "success")
                  {
                    $('#form-img-div').html(' ');
                    setTimeout(function(){
	                    swal({ 
	                      title: data.msg,
	                      text: "",
	                      type: "success" 
	                    }).then(function() {
	                        window.location.href = siteUrl;
	                    });
                    },2000);
                        
                  }
        
              },
              error: function(jqXHR, textStatus, errorThrown){
                 $('#form-img-div').html(' ');
                 $("#form-error-div").append("<p><strong><li class='text-red'></li>บันทึกข้อมูลไม่สำเร็จ</strong>,กรุณาลองใหม่อีกครั้ง!</p>");
                 $("#form-error-div").slideDown(400);
              }
        
            });
        // }else{
        //   return false;
        // }

    });

});

function archiveFunction() {
   
        swal({
          title: "Are you sure?",
          text: "But you will still be able to retrieve this file.",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, archive it!",
          cancelButtonText: "No, cancel please!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
             return true;  
          } else {
             return false;
          }
        });
}

function isEnglishchar(str){   
    var orgi_text="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890._-";   
    var str_length=str.length;   
    var isEnglish=true;   
    var Char_At="";   
    for(i=0;i<str_length;i++){   
        Char_At=str.charAt(i);   
        if(orgi_text.indexOf(Char_At)==-1){   
            isEnglish=false;   
            break;
        }      
    }   
    return isEnglish; 

    //alert(str);
}  
function onloadCallback () {
 
    grecaptcha.render('html_element', {
      'sitekey' : recaptcha_sitekey,
      'callback' : correctCaptcha,
      'expired-callback': expiredCallback
    });

};
function correctCaptcha(response) {
    $('input[name="robot"]').val(response);
    $('input[name="robot"]').next('span').addClass('hidden');
};
function expiredCallback(response) {
    grecaptcha.reset();
    $('input[name="robot"]').val("");
    $('input[name="robot"]').next('span').removeClass('hidden');
};
</script>