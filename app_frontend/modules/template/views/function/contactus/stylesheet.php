<!--begin::Global Theme Styles -->
<link href="<?=base_url('template/frontend/metronic');?>/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

<!--RTL version:<link href="../../../assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
<link href="<?=base_url('template/frontend/metronic');?>/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

<style>
    .LiveChat {
        height: 0 !important;
    }
    .m-pricing-table-3.m-pricing-table-3--fixed {
        width: 90% !important;
        margin: 0 auto;
    }

    .m-pricing-table-3__items{
        text-align: left !important;
    }

    .m-form__group{
        border-bottom: 0 !important;
        padding-top: 5px !important;
        padding-bottom: 5px !important;
    }
    
    .form-control{
        font-family: 'THSarabunNew' !important;
        font-size: 18px !important;
    }
    
    .m-form label {
        font-size: 20px !important;
    }

    .m-form button {
        width:100%;
        font-family: 'THSarabunNew' !important;
        font-size: 20px !important;
    }

    /* ----------------- */
    .icon-size{
        font-size: 80px;
        color:#ffffff;
    }
    .cus-space-icon-top{
        padding: 50px;
        margin: 50px;
        margin-top: 0;
        margin-bottom: 0;
        padding-bottom: 2px;
        text-align: center;
    }
    .cus-space-icon-buttom{
        padding: 50px;
        margin: 50px;
        margin-top: 0;
        padding-top: 2px;
        text-align: center;
    }
</style>
