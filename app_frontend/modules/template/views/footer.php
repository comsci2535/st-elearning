<footer>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 detail0 min-sm">
                     <div class="row mt-5">
                        <ul>
                            <li>
                               <a href="<?=site_url('page/ข้อกำหนดและเงื่อนไขในการใช้บริการ')?>">ข้อกำหนดและเงื่อนไขในการใช้บริการ</a> 
                            </li>
                            <li>
                                <a href="<?=site_url('page/นโยบายการคืนเงิน')?>">นโยบายการคืนเงิน</a>  
                            </li>
                            <li>
                                <a href="<?=site_url('page/นโยบายคุ้มครองข้อมูลส่วนบุคคล')?>">นโยบายคุ้มครองข้อมูลส่วนบุคคล</a> 
                            </li>
                            <li>
                                <a href="<?=site_url('page/วิธีการสมัคร')?>">วิธีการสมัคร</a>
                                
                            </li>
                        </ul>
                    </div>
                    <div class="social">
                        <?php for ($i=1; $i < 5; $i++) { 
                            $urlSocial = 'http://';
                            switch($i):
                                case 1:
                                    $urlSocial = 'https://www.facebook.com/Getupschools/';
                                break;
                                default:
                                    $urlSocial = 'http://';
                                break;
                            endswitch;
                        ?>
                        <div class="cool">
                            <a href="<?= $urlSocial;?>">
                                <img src="<?=base_url('images/logo/social'.$i.'.png');?>" class="ImgFluid"
                                    alt="คอสเรียน">
                            </a>
                        </div>
                        <?php }?>
                    </div>
                </div>
                <div class="col-sm-4 detail1">
                    <img src="<?=base_url('images/logo/logo.png');?>" class="ImgFluid" alt="คอสเรียน">
                    <div class="text mt-3">
                        <p>
                            โรงเรียนอนาคต ด้วยระบบเรียนรู้ออนไลน์<br>
                            ไม่ว่าท่านจะเป็นนักเรียน นักศึกษา ท่านจะสามารถเข้าถึง<br>
                            เนื้อหาความรู้ที่เราสรรหามา รวมไว้ที่นี่ได้เพียงไม่กี่นิ้วคลิก<br>
                            สะดวก ปลอดภัยด้วยระบบชำระเงินที่ได้มาตรฐานระดับโลก
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 detail2">
                    <div class="posit">
                        <p class="head">CONTACT INFO</p>
                        <div class="">
                            <a href="tel:0922988946">
                                <p>
                                    <div class="i"><i class="fas fa-phone"></i></div> +66 92-298-8946
                                </p>
                            </a>
                            <a href="https://goo.gl/maps/UuK9cGcJRQP2" target="_blank">
                                <p>
                                    <div class="i"><i class="fas fa-map-marker-alt"></i></div> 117 รังสินนครนายก 65
                                    ปทุมธานี 12130
                                </p>
                            </a>
                            <a href="mailto:your_mail@example.com">
                                <p>
                                    <div class="i"><i class="far fa-envelope"></i></div> support@getupschool.com
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 detail0 max-xs mt-3">
                    <div class="row mt-5">
                        <ul>
                            <li>
                               <a href="<?=site_url('page/ข้อกำหนดและเงื่อนไขในการใช้บริการ')?>">ข้อกำหนดและเงื่อนไขในการใช้บริการ</a> 
                            </li>
                            <li>
                                <a href="<?=site_url('page/นโยบายการคืนเงิน')?>">นโยบายการคืนเงิน</a>  
                            </li>
                            <li>
                                <a href="<?=site_url('page/นโยบายคุ้มครองข้อมูลส่วนบุคคล')?>">นโยบายคุ้มครองข้อมูลส่วนบุคคล</a> 
                            </li>
                            <li>
                                <a href="<?=site_url('page/วิธีการสมัคร')?>">วิธีการสมัคร</a>
                                
                            </li>
                        </ul>
                    </div>
                    <div class="social">
                        <?php for ($i=1; $i < 5; $i++) { ?>
                        <div class="cool">
                            <a href="http://">
                                <img src="<?=base_url('images/logo/social'.$i.'.png');?>" class="ImgFluid"
                                    alt="คอสเรียน">
                            </a>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="coppy">
        <p>สงวนลิขสิทธิ์ @ 2019 บริษัท นวัตกรรมสร้างสรรค์การศึกษา จำกัด</p>
    </div>
</footer>
<script src="//code.tidio.co/wdgw4pgo0bftdo0hzgnlniu2otkdp9wn.js" async></script>
<?php //echo Modules::run('chats/view');?>