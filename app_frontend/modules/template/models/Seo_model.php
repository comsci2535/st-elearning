<?php

class Seo_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_SeoAll() {

		$this->db->select('*');
		$this->db->from('seo');
		$this->db->where('type',0);
		$this->db->order_by('seo.name', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_SeoById($id) {
		$this->db->select('*');
		$this->db->from('seo');
		$this->db->where('id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->id;
			$row->name;
			$row->slug;
			$row->robots;
			$row->description;
			$row->keywords;
		}

		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('seo', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('name', $data->name);
		$this->db->set('slug', $data->slug);
		$this->db->set('robots', $data->robots);
		$this->db->set('description', $data->description);
		$this->db->set('keywords', $data->keywords);
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('id', $data->id);
		$update = $this->db->update('seo');

		return $update;
	}

	public function destroy($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('seo');

		return $delete;
	}
	
}