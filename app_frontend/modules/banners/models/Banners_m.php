<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Banners_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param){
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        if ( isset($param['banner_id']) ) 
            $this->db->where('a.banner_id', $param['banner_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
            
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);

        if ( isset($param['type']) )
            $this->db->where('a.type', $param['type']);
             
        $query = $this->db
                        ->select('a.*')
                        ->from('banners a')
                        ->get();
        return $query;
    }

    // new function query
    public function count_banners_all() 
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('banners');
    }

    public function get_banners_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('banners');
        return $query;
    }

    public function get_banners_by_id($id)
    {
        $this->db->where('banner_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('banners');
        
        return $query;
    }

    public function get_banners_by_type($type)
    {
        $this->db->where("((startDate ='0000-00-00' AND endDate ='0000-00-00') OR  (startDate <='".date('Y-m-d')."' AND endDate >='".date('Y-m-d')."'))");
        $this->db->where('type', $type);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('banners');
        
        return $query;
    }

    public function get_banners_option_all($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;

        if (!empty($param['type'])):
            $this->db->where('type', $param['type']);
        endif;

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('banners');
        
        return $query;
    }
}
