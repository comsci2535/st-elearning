<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('banners_m');
    }
    
    public function index()
    {
        $data['info'] = $this->banners_m->get_banners_by_type('home')->result();
        $this->load->view('banners',  $data); 
    }

    public function courses()
    {
        $data['info'] = $this->banners_m->get_banners_by_type('courses')->result();
        $this->load->view('banners',  $data); 
    }
}
