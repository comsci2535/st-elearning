<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function getByListsID($lists_id){
        
        $this->db->where_in('a.course_id', $lists_id);
            
        $query = $this->db
                        ->select('a.*')
                        ->from('courses a')
                        ->get();
        return $query;
    }

    public function insert_order($value) {
        $this->db->insert('orders', $value);
        return $this->db->insert_id();
    }

    public function insert_courses_students($value) {
        $query = $this->db->insert_batch('courses_students', $value);
        return $query;
    }
}
