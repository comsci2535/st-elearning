<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("payment_m");
        $this->load->library('send_email');
        $this->load->library('notification/notif_library');
	}

    private function seo(){
		$title          = 'ST Developer';
		$robots         = 'ST Developer';
		$description    = 'ST Developer';
		$keywords       = 'ST Developer';
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url()."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
		return $meta;
	}

    public function payment_file(){

        $input=$this->input->post();
        $cart_data = $this->session->userdata('cart_data');
        if($cart_data == null || count($cart_data) == 0){
            $cart_data = array();
        }

        if(count($cart_data) > 0){

            $lists_id = array();
            foreach ($cart_data as $key => $value) {
                $lists_id[] = $key;
            }

            $item= $this->payment_m->getByListsID($lists_id)->result();

            $price=0;
            $discount=0;
            $input['course_id']=array();
            $input['promotion_id']=array();
            $input['price_s']=array();
            $input['discount_s']=array();
            foreach ($item as $key => $value) {
                $value->promotion=Modules::run('promotion/get_promotion', $value->course_id); 
                if(!empty($value->promotion)){
                    $input['price_s'][$key]=$value->price;
                    $input['discount_s'][$key]=$value->promotion['discount'];
                    $input['promotion_id'][$key]=$value->promotion['promotion_id'];
                }else{
                    $input['price_s'][$key]=$value->price;
                    $input['discount_s'][$key]=$value->price;
                    $input['promotion_id'][$key]="";
                }
                $input['course_id'][$key]=$value->course_id;
            }

        } else {
            $item = array();
        }

        //print"<pre>";print_r($input);exit();
        //
        $value = $this->_build_data('file',$input);
        $result = $this->payment_m->insert_order($value);

        if ( $result ) {
            $data_noti = array(
                'title' => 'แจ้งเตือน - ยืนยันการชำระเงินของคุณ',
                'url' => site_url('profile/my-course'),
                'user_id' => $this->session->users['UID'],
                'type' => '1',
                'date' => date('Y-m-d H:i:s'),
            );
            $this->notif_library->noti_insert($data_noti);
    

            $value_ = $this->_build_data_course($result,'file',$input);
            $this->payment_m->insert_courses_students($value_);
            $this->session->unset_userdata('cart_data');

            $param_send['subject']='ยืนยันการชำระเงิน';
            $param_send['detail']='ยืนยันการชำระเงิน <br>';
            $param_send['email_send']='';
            $param_send['email_to']=$this->session->users['Username'];
            $param_send['file']=$value['premise'];
            $this->send_email->send($param_send);

            $resp_msg = array('info_txt'=>"success",'msg'=>'ลงทะเบียนเรียบร้อย','msg2'=>'กรุณารอสักครู่...','result'=>$result);
                echo json_encode($resp_msg);
                return false;
        } else {
            $resp_msg = array('info_txt'=>"error",'msg'=>'ลงทะเบียนไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                echo json_encode($resp_msg);
               return false;
        }
        
    }

	public function payment_paypal(){
        $input=$this->input->post();
		$cart_data = $this->session->userdata('cart_data');
        if($cart_data == null || count($cart_data) == 0){
            $cart_data = array();
        }

        if(count($cart_data) > 0){

            $lists_id = array();
            foreach ($cart_data as $key => $value) {
                $lists_id[] = $key;
            }

            $item= $this->payment_m->getByListsID($lists_id)->result();

            $price=0;
            $discount=0;
            $input['course_id']=array();
            $input['promotion_id']=array();
            $input['price_s']=array();
            $input['discount_s']=array();
            foreach ($item as $key => $value) {
                $value->promotion=Modules::run('promotion/get_promotion', $value->course_id); 
                if(!empty($value->promotion)){
                    $input['price_s'][$key]=$value->price;
                    $input['discount_s'][$key]=$value->promotion['discount'];
                    $input['promotion_id'][$key]=$value->promotion['promotion_id'];
                }else{
                    $input['price_s'][$key]=$value->price;
                    $input['discount_s'][$key]=$value->price;
                    $input['promotion_id'][$key]="";
                }
                $input['course_id'][$key]=$value->course_id;
            }

        } else {
            $item = array();
        }
        //
        $value = $this->_build_data('paypal',$input);
        $result = $this->payment_m->insert_order($value);

        if ( $result ) {
            $value_ = $this->_build_data_course($result,'paypal',$input);
            $this->payment_m->insert_courses_students($value_);
            $this->session->unset_userdata('cart_data');
            $resp_msg = array('info_txt'=>"success",'msg'=>'ลงทะเบียนเรียบร้อย','msg2'=>'กรุณารอสักครู่...','result'=>$result);
                echo json_encode($resp_msg);
                return false;
        } else {
            $resp_msg = array('info_txt'=>"error",'msg'=>'ลงทะเบียนไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                echo json_encode($resp_msg);
               return false;
        }
        
	}

    public function payment_free(){
        $input=$this->input->post();
        $cart_data = $this->session->userdata('cart_data');
        if($cart_data == null || count($cart_data) == 0){
            $cart_data = array();
        }

        if(count($cart_data) > 0){

            $lists_id = array();
            foreach ($cart_data as $key => $value) {
                $lists_id[] = $key;
            }

            $item= $this->payment_m->getByListsID($lists_id)->result();

            $price=0;
            $discount=0;
            $input['course_id']=array();
            $input['promotion_id']=array();
            $input['price_s']=array();
            $input['discount_s']=array();
            foreach ($item as $key => $value) {
                $value->promotion=Modules::run('promotion/get_promotion', $value->course_id); 
                if(!empty($value->promotion)){
                    $input['price_s'][$key]=$value->price;
                    $input['discount_s'][$key]=$value->promotion['discount'];
                    $input['promotion_id'][$key]=$value->promotion['promotion_id'];
                }else{
                    $input['price_s'][$key]=$value->price;
                    $input['discount_s'][$key]=$value->price;
                    $input['promotion_id'][$key]="";
                }
                $input['course_id'][$key]=$value->course_id;
            }

        } else {
            $item = array();
        }
        //
        $value = $this->_build_data('free',$input);
        $result = $this->payment_m->insert_order($value);

        if ( $result ) {
            $value_ = $this->_build_data_course($result,'free',$input);
            $this->payment_m->insert_courses_students($value_);
            $this->session->unset_userdata('cart_data');
            $resp_msg = array('info_txt'=>"success",'msg'=>'ลงทะเบียนเรียบร้อย','msg2'=>'กรุณารอสักครู่...','result'=>$result);
                echo json_encode($resp_msg);
                return false;
        } else {
            $resp_msg = array('info_txt'=>"error",'msg'=>'ลงทะเบียนไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                echo json_encode($resp_msg);
               return false;
        }
        
    }



	public function success_paypal($order_id){
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'detail',
            'footer'  => 'footer',
            'function'=>  array('custom','payment'),
		);
		//loade view
        $this->session->unset_userdata('cart_data');

        $this->load->view('template/body', $data);
	}

	private function _build_data($type,$input){
     
        if($type=="paypal"){
            $value['status'] = 1;
            $value['payment_type'] = 2;
            $value['premise'] = $input['paymentID'];
        }
        elseif($type=="free"){
            $value['status'] = 1;
            $value['payment_type'] = 3;
            $value['premise'] = 'ส่วนลด 100%';
        }else if($type=="file"){
            $value['payment_type'] = 1;
            $value['status'] = 0;
            $path = 'orders';
            $upload = $this->do_upload('fileUpload',TRUE,$path);
            //print_r($upload);exit();
            $file = '';
            if(isset($upload['index'])){
                $file = 'uploads/'.$path.'/'.$upload['index']['file_name'];
                $value['premise'] = $file;
            }
        }
        $value['order_code'] = "GS".time();;
        $value['price'] = $input['price'];
        $value['discount'] = $input['discount'];
        $value['promotion'] = $input['promotion'];
        $value['coupon_id'] = $input['coupon_id'];
        
        $value['created_at'] = date('Y-m-d H:i:s');
        $value['created_by'] = $this->session->users['UID'];
        $value['updated_at'] = date('Y-m-d H:i:s');
        $value['updated_by'] = $this->session->users['UID'];
     
      //arr($value);exit();
        return $value;
    }

    private function _build_data_course($order_id,$type,$input){
        if($type=="paypal"){
            $status=1;
        }else if($type=="free"){
            $status=1;
        }else if($type=="file"){
            $status=0;
        }
        if(!empty($input['course_id'])){
            foreach ($input['course_id'] as $key => $rs) {
                $value[] = array(
                    'order_id' => $order_id,
                    'course_id' => $rs,
                    'user_id' => $this->session->users['UID'],
                    'status' => $status,
                    'promotion_id' => $input['promotion_id'][$key],
                    'price' => $input['price_s'][$key],
                    'discount' => $input['discount_s'][$key]
                );
            }
        }
        
        return $value;
    }

    private function do_upload($file,$type = TRUE ,$path) {
        
        
        $upload_path='/uploads/'.$path.'/';
        // if (!is_dir($upload_path ))
        // {
        //     mkdir($upload_path , 0777, true);
        // }
        $config['upload_path']          = 'uploads/'.$path;
        $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|bmp';
        //$config['max_size']             = 30000;
        $config['encrypt_name']         = TRUE;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        
        if ( ! $this->upload->do_upload($file))
        {
            $data = array('error' => $this->upload->display_errors());
        }
        else
        {
            $data = array('index' => $this->upload->data());
        }
        
        return $data;
        
    }

}
