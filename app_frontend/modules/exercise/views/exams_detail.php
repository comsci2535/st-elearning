<section class="exams-body">
    <div class="container">
      <div class="row">

        <!-- Course -->
          <div class="col-lg-12">
            <div class="col-xs-12 head">
                <p class="fontLv6"><i class="fa fa-book" aria-hidden="true"></i> <?=$info['title'];?></p>
                <p class="fontLv8 color6"><?=$info['excerpt'];?></p>
                <hr>
            </div>
            <div class="row">
                       
                        <div class="col-xs-11 col-sm-5 col-md-7 box-exam2">  
                             <div id="exam"></div>
            
                             <br>
                            <a href="javascript:;" class="btn btn-secondary fontLv8 Arial prev">ย้อนกลับ</a>
                           <!--  <a href="javascript:;" class="btn btn-warning fontLv8 Arial next">ข้าม</a> -->
                            <a href="javascript:;" class="btn btn-success fontLv8 Arial next"><i class="fa fa-check" aria-hidden="true"></i> ทำข้อถัดไป</a>
                        </div>
                         <div class="col-xs-1 col-sm-1 col-md-1 ">  
                        </div>
                        
                        
                        <div class="col-xs-12 col-sm-6 col-md-4 time-exam text-center ">
                           <div class="container">
                                <p class="color7 fontLv7">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </p>
                                <p class="fontLv7 color7" id="clock"></p>
                                <div class="choice">
                                    <div class="row">
                                         <div class="col-xs-12 nopadding question ">
                                            <?php for ($i = 1; $i<=$examNum; $i++) : ?>
                                            <button  class="btn  fontLv8 quiz-no" id="<?php echo $i; ?>"><?php echo $i; ?></button>
                                            <?php endfor; ?>
                                        </div> 
                                    </div>
                                     <div class="row">
                                        <div class="col-xs-12 text-cnter fontLv8" style="padding-top: 15px;">   
                                           <!--  <span class="badge badge-warning">ข้าม</span> -->
                                            <span class="badge badge-secondary">ยังไม่ทำ</span>
                                            <span class="badge badge-success">ทำแล้ว</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 text-right fontLv8" style="margin-top:15px;">    
                                             <a href="javascript:;" id="submit-quiz" class="btn btn-primary font_level3 Arial">บันทึกแบบทดสอบ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                       

    </div> 
</div>
</div>
</div>
</section>