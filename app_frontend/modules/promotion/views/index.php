<section class="Sec_Courses">

    <div class="container">
        <div class="row">

            <div class="col-sm-12 text-right mt-4 mb-4">
                <a href="<?=site_url('promotion/create/'.$course_id)?>"><button type="button" class="btn btn-success"><i
                            class="fas fa-plus"></i> เพิ่มข้อมูล</button></a>
            </div>
           
            <div class="col-sm-12 mb-5">
                <input type="hidden" id="course_id" value="<?=$course_id?>">
                <div class="row">
                    <table id="data-list" class="table table-striped- table-bordered table-hover table-checkable" width="100%">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รายการ</th>
                            <th>ช่วงเวลาโปรโมชั่น</th>
                            <th>ราคาโปรโมชั่น</th>
                            <th>สร้าง</th>
                            <th>แก้ไข</th>
                            <th>สถานะ</th>
                            <th>จัดการ</th>
                        </tr>
                    </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>