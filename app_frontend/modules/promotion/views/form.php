<section class="Sec_Courses">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 pt-4">
                <h4 class="pt-4">
                    <a href="<?=site_url('promotion/index/'.$courses->course_id);?>">โปรโมชั่นทั้งหมด</a> > <span><?php echo !empty($breadcrumb)? $breadcrumb : '';?></span>
                </h4>
                <hr>
            </div>
            
            <div class="col-sm-12 mb-5">
                <div class="col-sm-12">
                    <form class="form-horizontal frm-create" method="post" action="<?=$frmAction?>" autocomplete="off" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="courseTitle">คอร์สเรียน</label>
                            <div class="col-sm-10">
                                <input value="<?php echo !empty($courses->title) ? $courses->title : NULL ?>" type="text" class="form-control " name="courseTitle" disabled>
                                <input class="form-control" name="course_id" id="course_id" type="hidden" value="<?php echo $courses->course_id;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="price">ราคาปกติ</label>
                            <div class="col-sm-10">
                            <input value="<?php echo !empty($courses->price) ? number_format($courses->price,2) : 0 ?>" type="text" id="price" class="form-control m-input " name="price" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ชื่อโปรโมชั่น <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo !empty($promotion->title) ? $promotion->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title"  required>
                                <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">slug <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo !empty($promotion->slug) ? $promotion->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug" required>
                                <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ประเภท <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <label class="icheck-inline"><input id="display1"  type="radio" name="type" value="1" class="icheck" <?php if(isset($promotion->type) && $promotion->type=="1"){ echo "checked"; }?> <?php if(!isset($info->type)){ echo "checked"; }?> /> ปกติ</label>
                                <label class="icheck-inline"><input id="display2" type="radio" name="type" value="2" class="icheck" <?php if(isset($promotion->type) && $promotion->type=="2"){ echo "checked"; }?>/> กำหนดราคาเอง</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ราคาโปรโมชั่น <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo isset($promotion->discount) ? number_format($promotion->discount) : NULL ?>" type="text" id="input-discount" class="amount form-control m-input " name="discount" >
                                <div id="alert_e" style="color: red;"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ช่วงโปรโมชั่น <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>" />
                                <input type="hidden" name="startDate" value="<?php echo isset($promotion->startDate) ? $promotion->startDate : NULL ?>" />
                                <input type="hidden" name="endDate" value="<?php echo isset($promotion->endDate) ? $promotion->endDate : NULL ?>" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">สถานะ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <label><input <?php if(isset($promotion->active) && $promotion->active== 1){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="1" checked> เปิด</label>
                                <label><input <?php if(isset($promotion->active) && $promotion->active== 0){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd"> <h5 class="block">ข้อมูล SEO</h5></label>
                            <div class="col-sm-10"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ชื่อเรื่อง (Title)</label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo !empty($promotion->metaTitle) ? $promotion->metaTitle : NULL ?>" type="text" id="" class="form-control" name="metaTitle">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">คำอธิบาย (Description)</label>
                            <div class="col-sm-10"> 
                                <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo !empty($promotion->metaDescription) ? $promotion->metaDescription : NULL ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">คำหลัก (Keyword)</label>
                            <div class="col-sm-10"> 
                                <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo !empty($promotion->metaKeyword) ? $promotion->metaKeyword : NULL ?></textarea>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                                <input type="hidden" class="form-control" name="db" id="db" value="promotions">
                                <input type="hidden" name="id" id="input-id" value="<?php echo isset($promotion->promotion_id) ? encode_id($promotion->promotion_id) : 0 ?>">
                                <button type="submit" class="btn btn-success">บันทึก</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? base_url().$info->file : ''; ?>';
    var file_id         = '<?=(isset($info->course_id)) ? $info->course_id : ''; ?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/'+file_id;

</script>