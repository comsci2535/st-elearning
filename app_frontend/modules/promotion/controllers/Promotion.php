<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("promotion_m");
		$this->load->model("courses/courses_m");
	}

    private function seo(){
		$title          = 'ST Developer';
		$robots         = 'ST Developer';
		$description    = 'ST Developer';
		$keywords       = 'ST Developer';
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url()."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
		return $meta;
	}

	public function get_promotion($course_id=""){
		
        $promotion=array();
        $item= $this->promotion_m->getPromotionById($course_id)->row_array();

        if(!empty($item)){
            $promotion=$item;
        }
        return $promotion;
	}

	public function get_promotion_all() {
         
       
        $input['']="";
        $info=$this->promotion_m->get_rows($input)->result();

        if ( !empty($info) ) {
            foreach ($info as $key => $value) {
            	 $value->start=date('F d,Y', strtotime(date('Y-m-d'))).' 00:00:00'; 
                 $value->end=date('F d,Y', strtotime($value->endDate)).' 23:59:59'; 
            }
            $array =  (array) $info;
            echo json_encode($array);
        }
         

	}

	public function index($id= '')
	{
		$data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'index',
            'footer'  => 'footer',
            'function'=>  array('custom','promotion'),
		);
		//loade view
		
		$data['course_id'] = $id;

        $this->load->view('template/body', $data);
	}
	
	public function ajax_data() {
		
		$input = $this->input->post();
		
        $info = $this->promotion_m->get_promotion_all_by_course_id($input);
        $infoCount = $this->promotion_m->count_promotion_all_by_course_id($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->course_id);

            $active = $rs->active ? '<span class="badge badge-success">เปิด</span>' : '<span class="badge badge-danger">ปิด</span>';
            
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $no;
            $column[$key]['course_id']  = $rs->course_id;
			$column[$key]['title']      = $rs->title;
			$column[$key]['date']  		= $rs->startDate.' ถึง '.$rs->endDate;
			$column[$key]['discount']   = number_format($rs->discount,2);
			$column[$key]['active']     = $active;
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['updated_at'] = $rs->updated_at;
            $column[$key]['action']     = '<div class="btn-group">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="'.base_url('promotion/edit/'.$id.'/'.$rs->promotion_id).'"><i class="fa fa-edit"></i> แก้ไข</a>
                                                    <a class="dropdown-item btn-click-delete" href="javascript:void(0)" data-id="'.$rs->promotion_id.'"><i class="fa fa-trash"></i> ลบ</a>
                                                </div>
                                            </div>';
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
	}
	
	public function create($id = ''){
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','promotion/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/storage");

        //loade view
        $data['breadcrumb']     = 'เพิ่มข้อมูล';
        $info = $this->courses_m->get_courses_by_course_id($id)->row();
		$data['courses'] = $info;

		$this->load->view('template/body', $data);
	}

	public function edit($id = '', $promotion_id){
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','promotion/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/update");

        //loade view
        $data['breadcrumb']     = 'เพิ่มข้อมูล';
        $info = $this->courses_m->get_courses_by_course_id($id)->row();
        $data['courses'] = $info;
        
        $infoPromotion = $this->promotion_m->get_promotion_by_id($promotion_id)->row();
        $data['dateRang'] ="";
        if($infoPromotion->startDate!="0000-00-00" && $infoPromotion->endDate!="0000-00-00"){
            $data['dateRang'] = date('d-m-Y', strtotime($infoPromotion->startDate)).' ถึง '.date('d-m-Y', strtotime($infoPromotion->endDate));
        }
        
        $data['promotion'] = $infoPromotion;

		$this->load->view('template/body', $data);
    }
	
	public function storage() {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->promotion_m->insert($value);

        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['course_id']}"));
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->promotion_m->update($id, $value);
        if ( $result ) {
           
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['course_id']}"));
    }
	
	private function _build_data($input) {
        
        $value['title'] 	= $input['title'];
        $value['slug']  	= $input['slug'];
        $value['excerpt'] 	= $input['excerpt'];
        $value['detail'] 	= $input['detail'];
        $value['type'] 		= $input['type'];
        $value['course_id'] = $input['course_id'];
        $value['discount'] 	= $input['discount'];
        $value['startDate'] = $input['startDate'];
        $value['endDate'] 	= $input['endDate'];
        if($input['type']!='1'){
            $value['discount'] = "";
        }else{
            $value['discount'] = str_replace(",","",$input['discount']);  
        }

        if($input['metaTitle']!=""){
            $value['metaTitle'] = $input['metaTitle'];
        }else{
             $value['metaTitle'] = str_replace(","," ",$input['title']);
        }
        if($input['metaDescription']!=""){
            $value['metaDescription'] = $input['metaDescription'];
        }else{
            $value['metaDescription'] = $input['excerpt'];
        }
        if($input['metaKeyword']!=""){
            $value['metaKeyword'] = $input['metaKeyword'];
        }else{
             $value['metaKeyword'] = str_replace(","," ",$input['title']);
        }

        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['updated_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['UID'];
        } else {
            $value['active']     = $input['active'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['UID'];
        }
        return $value;
    }

    public function action(){
        
        $toastr['type']     = 'error';
        $toastr['lineOne']  = config_item('appName');
        $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success']    = false;
        $data['toastr']     = $toastr;

        $input               = $this->input->post();
        $dateTime            = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['UID'];
        $result = false;
        
        if ( $input['type'] == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['UID'];
            $result = $this->promotion_m->update_in($input['id'], $value);
        }
        
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    
    $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));        
} 
}
