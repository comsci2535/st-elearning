<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Promotion_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function getPromotionById($course_id){
        
        $this->db->where('a.course_id', $course_id);
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".date('Y-m-d')."' AND a.endDate >='".date('Y-m-d')."'))");
        $this->db->order_by('a.promotion_id', 'desc');
            
        $query = $this->db
                        ->select('a.promotion_id,a.discount,a.title,a.slug,a.type')
                        ->from('promotions a')
                        ->get();
        return $query;
    }

    public function get_rows($param) 
    {
        $this->_condition($param); 
          
        $query = $this->db
                        ->select('a.*')
                        ->from('promotions a')
                        ->get();
        return $query;
    }

    private function _condition($param) 

    {   
   
        $this->db->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".date('Y-m-d')."' AND a.endDate >='".date('Y-m-d')."'))");
       
       if ( isset($param['course_id']) ) 
            $this->db->where('a.course_id', $param['course_id']);


        $this->db->where('a.active', 1);
               
        $this->db->where('a.recycle',0);

       

    }

    // New Function Query

    public function count_promotion_all_by_course_id($param)
    {
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('title', $param['search']['value'])
                    ->or_like('excerpt', $param['search']['value'])
                    ->or_like('detail', $param['search']['value'])
                    ->group_end();
        }
        
        $this->db->where('course_id', $param['course_id']); 
        $this->db->where('recycle', 0);
        $this->db->get('promotions');
        return $this->db->count_all_results();
    }

    public function get_promotion_all_by_course_id($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('title', $param['search']['value'])
                    ->or_like('excerpt', $param['search']['value'])
                    ->or_like('detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 2) $columnOrder = "title";

            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        $this->db->where('recycle', 0);
        $this->db->where('course_id', $param['course_id']); 
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        $query = $this->db
                        ->select('*')
                        ->from('promotions')
                        ->get();
        return $query;
    }

    public function get_promotion_by_course_id($course_id){
        
        $this->db->where('a.course_id', $course_id);
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".date('Y-m-d')."' AND a.endDate >='".date('Y-m-d')."'))");
        $this->db->order_by('a.promotion_id', 'desc');
            
        $query = $this->db
                        ->select('a.promotion_id,a.discount,a.title,a.slug,a.type')
                        ->from('promotions a')
                        ->get();
        return $query;
    }

    public function get_promotion_by_id($promotion_id){
        
        $this->db->where('a.promotion_id', $promotion_id);

        $query = $this->db
                        ->select('a.*')
                        ->from('promotions a')
                        ->get();
        return $query;
    }

    public function insert($value) {
        $this->db->insert('promotions', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('promotion_id', $id)
                        ->update('promotions', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('promotion_id', $id)
                        ->update('promotions', $value);
        return $query;
    }  
}
