<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Banks_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        // $this->db->last_query();
    }
    
    // new function query
    public function count_banks_all() 
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('banks');
    }

    public function get_banks_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('banks');
        return $query;
    }

    public function get_banks_option_all($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('title', $param['search']);
        endif;

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('banks');
        
        return $query;
    }

}
