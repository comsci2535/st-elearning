<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banks extends MX_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('banks_m');

		// Test Model
		$param = array(
			'length' => '',
			'start' => '',
			'search' => '',
		);
		arr($this->banks_m->get_banks_option_all($param)->result());
		exit();
	}

    private function seo(){
		$title          = 'ST Developer';
		$robots         = 'ST Developer';
		$description    = 'ST Developer';
		$keywords       = 'ST Developer';
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url()."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
		return $meta;
	}

	public function index($slug=''){
		Modules::run('track/front','');
        $data = array(
            'menu'    => 'articles',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'articles',
            'footer'  => 'footer',
            'function'=>  array('custom','articles'),
		);

		// get articles
        if(!empty($slug)){
        	$data['slug']  = $slug;
        	$category = $this->articles_m->get_categoryAll($slug)->row();
        	$input_articles['category_id']	= $category->article_categorie_id;
        	$data['category_name'] = $category->title;
        	$slug_cat =  'category/'.$slug;
        }       

		$input_articles['recycle'] 	= 0;
		$input_articles['active']  	= 1;
		$input_articles['search']	= !empty($this->input->get('search'))? $this->input->get('search') :'';
		$total		= $this->articles_m->count_rows($input_articles);
        $segment	= 3;
		$per_page 	= 12;
		
		if(!empty($this->input->get('view')) && $this->input->get('view')=="list"){
			if(!empty($this->input->get('search')) && $this->input->get('search')!=""){

				$uri = 'articles/'.$slug_cat.'?view=list&search='.$this->input->get('search');
			}else{
				$uri = 'articles/'.$slug_cat.'?view=list';
			}
			
        }else{
			if(!empty($this->input->get('search')) && $this->input->get('search')!=""){
				$uri = 'articles/'.$slug_cat.'view=grid&search='.$this->input->get('search');
			}else{
				$uri = 'articles/'.$slug_cat.'?view=grid';
			}
		}

        $data["pagination"] = $this->pagin($uri, $total, $segment, $per_page);
        
        $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;
 

		$input_articles['length']	= $per_page;
        $input_articles['start']	= $page;
		$input_articles['recycle'] 	= 0;
		$input_articles['active']  	= 1;
		$info_articles  = $this->articles_m->get_rows($input_articles);
		$data['articles'] = $info_articles->result();
		$data['search']  = $input_articles['search'];
		$data['total']    = $total;
		$data['category'] = $this->articles_m->get_categoryAll()->result();
		
		//loade view

        $this->load->view('template/body', $data);
	}
    
    public function detail($slug){

        $data = array(
            'menu'    => 'articles',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'articls_detail',
            'footer'  => 'footer',
            'function'=>  array('custom','articles'),
		);

		// get articles
	
		$input_articles['recycle'] 	= 0;
		$input_articles['active']  	= 1;
		$input_articles['slug']  	= $slug;
		$info_articles  = $this->articles_m->get_rows($input_articles);
		$data['articles'] = $info_articles->row();

		$this->articles_m->plus_view($data['articles']->article_id);
		 

		//loade view

        $this->load->view('template/body', $data);
	}

	public function articles_home(){

		// get articles
		$input['length'] 	= 6;
		$input['start'] 	= 0;
		$input['recycle'] 	= 0;
		$input['active']  	= 1;
		$info_articles  = $this->articles_m->get_rows($input);
		$data['articles'] = $info_articles->result();
		$this->load->view('articles_home', $data); 
	}
}
