<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exercise_manage extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('courses_lesson/courses_lesson_m');
		$this->load->model('exercise_manage_m');
		$this->load->model('courses_instructors/courses_instructors_m');
		
	}

	public function index($id=""){
        $data = array(
            'menu'    => 'exercise',
            'seo'     => "",
            'header'  => 'header',
            'content' => 'index',
            'footer'  => 'footer',
            'function'=>  array('custom','exercise_manage'),
		);

        $data['course_lesson_id'] = $id;

        $this->load->view('template/body', $data);
    }
    
    public function ajax_data_exercise() 
    {
        $input = $this->input->post();
        $info = $this->exercise_manage_m->get_exercise_by_all($input);
        $infoCount = $this->exercise_manage_m->count_exercise_by_all($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->exercise_id);
            
            $manage = '<a href ="'.site_url('exercise_lists/index/'.$rs->exercise_id).'" class="btn btn-primary btn-sm "><i class="fa fa-list"></i>  รายการแบบฝึกหัด</a>';
           
            $active = $rs->active ? '<span class="badge badge-success">เปิด</span>' : '<span class="badge badge-danger">ปิด</span>';
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $no;
            $column[$key]['course_lesson_id']  = $rs->course_lesson_id;
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['list']       = $manage;
            $column[$key]['active']     = $active;
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['updated_at'] = $rs->updated_at;
            $column[$key]['action']     = '<div class="btn-group">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="'.base_url('exercise_manage/edit/'.$rs->course_lesson_id.'/'.$id).'"><i class="fa fa-edit"></i> แก้ไข</a>
                                                    <a class="dropdown-item btn-click-delete" href="javascript:void(0)" data-id="'.$id.'"><i class="fa fa-trash"></i> ลบ</a>
                                                </div>
                                            </div>';
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function create($id)
    {
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => "",
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','exercise_manage/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/storage");

        //loade view
        
        $data['courses_lesson_id']     = $id;

        $lesson = $this->courses_lesson_m->get_courses_lesson_by_id($id)->row();
        $data['breadcrumb'] = $lesson->title;

        $this->load->view('template/body', $data);
    }

    public function edit($courses_lesson_id, $id = 0)
    {
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => '',
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','exercise_manage/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/update");

        //loade view
        
        $data['courses_lesson_id']     = $courses_lesson_id;

        $lesson = $this->courses_lesson_m->get_courses_lesson_by_id($courses_lesson_id)->row();
        $data['breadcrumb'] = $lesson->title;

        $info = $this->exercise_manage_m->get_exercise_by_exercise_id($id);
        if ( $info->num_rows() == 0) {
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        if($info->startDate!="0000-00-00" && $info->endDate!="0000-00-00" ){
            $data['dateRang'] = date('d-m-Y', strtotime($info->startDate)).' ถึง '.date('d-m-Y', strtotime($info->endDate));
        }

        $this->load->view('template/body', $data);
    }

    public function storage()
    {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->exercise_manage_m->insert($value);
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['course_lesson_id']}"));
    }

    public function update()
    {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->exercise_manage_m->update($id, $value);
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['course_lesson_id']}"));
    }

    private function _build_data($input)
    {
        
        $value['title']             = $input['title'];
        $value['slug']              = $input['slug'];
        $value['excerpt']           = $input['excerpt'];
        $value['course_lesson_id']  = $input['course_lesson_id'];
        $value['examTime']          = $input['examTime'];
        $value['examNo']            = $input['examNo'];
        $value['answerRandom']      = $input['answerRandom'];
        $value['examNumber']        = $input['examNumber'];
        $value['pass']              = $input['pass'];
        $value['startDate']         = $input['startDate'];
        $value['endDate']           = $input['endDate'];
        $value['active']            = $input['active'];
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['updated_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['UID'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['UID'];
        }
        return $value;
    }

    public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = base_url().'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function action(){
        
        $toastr['type']     = 'error';
        $toastr['lineOne']  = config_item('appName');
        $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success']    = false;
        $data['toastr']     = $toastr;

        $input               = $this->input->post();
        $dateTime            = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['UID'];
        $result = false;
        
        if ( $input['type'] == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['UID'];
            $result = $this->exercise_manage_m->update_in($input['id'], $value);
        }
        
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));        
    } 

}
