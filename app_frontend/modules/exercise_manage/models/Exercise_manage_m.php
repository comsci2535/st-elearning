<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Exercise_manage_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_exercise_by_all($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";

            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        if ( isset($param['exercise_id']) )
            $this->db->where('a.exercise_id', $param['exercise_id']);
        
        if ( isset($param['course_lesson_id']) )
            $this->db->where('a.course_lesson_id', $param['course_lesson_id']);
        
        $this->db->where('a.recycle', 0);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        $query = $this->db
                        ->select('a.*')
                        ->from('exercise a')
                        ->get();
        return $query;
    }

    public function count_exercise_by_all($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['exercise_id']) )
            $this->db->where('a.exercise_id', $param['exercise_id']);
        
        if ( isset($param['course_lesson_id']) )
            $this->db->where('a.course_lesson_id', $param['course_lesson_id']);
        
        $this->db->where('a.recycle', 0);

        return $this->db->count_all_results('exercise a');
    }

    public function get_exercise_by_exercise_id($exercise_id)
    {
        $this->db->where('a.recycle', 0);
        $this->db->where('a.exercise_id', $exercise_id);
        $query = $this->db
                        ->select('a.*')
                        ->from('exercise a')
                        ->get();
        return $query;
    }

    public function insert($value) 
    {
        $this->db->insert('exercise', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('exercise_id', $id)
                        ->update('exercise', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('exercise_id', $id)
                        ->update('exercise', $value);
        return $query;
    }    
}
