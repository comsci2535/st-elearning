<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupons extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("coupons_m");
	}

    

	public function check(){
        $input = $this->input->post();

        $result = $this->coupons_m->getCoupon($input)->row_array();
        if(!empty($result)){

            if($result['couponType']==1){

                $coupons_courses = $this->db
                                    ->select('a.course_id')
                                    ->from('coupons_courses a')
                                    ->where('a.coupon_id', $result['coupon_id'])
                                    ->where('a.course_id', $input['course_id'])
                                    ->get()->num_rows();
                if($coupons_courses > 0){
                     $c=1;
                }else{
                    
                    $c=2;
                }

            }else{

                if($result['isCourse']==1){
                    $courseNum = $this->db
                                    ->select('a.order_id')
                                    ->from('orders a')
                                    ->join('courses_students b','a.order_id=b.order_id','left')
                                    ->where('b.user_id', $this->session->users['UID'])
                                    ->where('a.coupon_id', $result['coupon_id'])
                                    ->get()->num_rows();

                    if($courseNum < $result['isCourseNum']){
                        $c=1;
                    }else{
                        $c=0;
                    }

                }else{
                    $c=1;
                }

            }
            

            if($result['isMember']==1){
                $memberNum = $this->db
                                ->select('a.order_id')
                                ->from('orders a')
                                ->join('courses_students b','a.order_id=b.order_id','left')
                                ->where('a.coupon_id', $result['coupon_id'])
                                ->get()->num_rows();

                if($memberNum < $result['isMemberNum']){
                    $m=1;
                }else{
                    $m=0;
                }

            }else{
                $m=1;
            }

            if($c==2 && $m==1){
                $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสคูปองไม่ได้อยู่ในคอร์สเรียนนี้','msg2'=>'กรุณาตรวจสอบอีกครั้ง !');
                echo json_encode($resp_msg);
                return false;
            }else if($c==0 && $m==1){
                $resp_msg = array('info_txt'=>"error",'msg'=>'ท่านได้ใช้รหัสคูปองนี้ครบแล้ว','msg2'=>'กรุณาตรวจสอบอีกครั้ง !');
                echo json_encode($resp_msg);
                return false;
            }else if($c==2 && $m==0){
                 $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสคูปองนี้ได้ลงทะเบียนครบ '.$result['isMemberNum'].' ท่านแรกแล้ว','msg2'=>'กรุณาตรวจสอบอีกครั้ง !');
                echo json_encode($resp_msg);
                return false;
            }else if($c==1 && $m==0){
                 $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสคูปองนี้ได้ลงทะเบียนครบ '.$result['isMemberNum'].' ท่านแรกแล้ว','msg2'=>'กรุณาตรวจสอบอีกครั้ง !');
                echo json_encode($resp_msg);
                return false;
            }else if($c==0 && $m==0){
                 $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสคูปองนี้ได้ลงทะเบียนครบ '.$result['isMemberNum'].' ท่านแรกแล้ว','msg2'=>'กรุณาตรวจสอบอีกครั้ง !');
                echo json_encode($resp_msg);
                return false;
            }else{
                $resp_msg = array('info_txt'=>"success",'type_'=>$result['type'],'discount'=>$result['discount'],'coupon_id'=>$result['coupon_id'],'result'=>$result);
                echo json_encode($resp_msg);
                return false;
            } 

            
        } else {
            $input['user_id']=$this->session->users['UID'];
            $result2 = $this->coupons_m->getCouponMember($input)->row_array();
            if(!empty($result2)){

                $resp_msg = array('info_txt'=>"success",'type_'=>1,'discount'=>100,'result'=>$result2,'coupon_id'=>$input['couponCode']);
                echo json_encode($resp_msg);
                return false;

             }else{

                $resp_msg = array('info_txt'=>"error",'msg'=>'รหัสคูปองนี้ไม่สามารถใช้งานได้','msg2'=>'กรุณาตรวจสอบอีกครั้ง !');
                    echo json_encode($resp_msg);
                   return false;
            }
        }
	}

	

}
