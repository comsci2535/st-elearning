<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Coupons_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function getCoupon($param) 
    {
      
        $this->db->where('a.couponCode', $param['couponCode']);
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".date('Y-m-d')."' AND a.endDate >='".date('Y-m-d')."'))");
        $query = $this->db
                        ->select('a.*')
                        ->from('coupons a')
                        ->get();
        return $query;
    }

    public function getCouponMember($param) 
    {
      
        $this->db->where('a.couponCode', $param['couponCode']);
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);

        if ( isset($param['user_id']) ){
           $this->db->where('a.user_id !=', $param['user_id']);
        }
        
        $query = $this->db
                        ->select('a.*')
                        ->from('users a')
                        ->get();
        return $query;
    }
}
