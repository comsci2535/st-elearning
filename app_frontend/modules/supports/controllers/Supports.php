<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supports extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('supports_m');
    }
    
    public function index()
    {
        $input['active']='1';
        $input['recycle']='0';
		// get supports
		$info_supports  = $this->supports_m->get_rows($input);
		$data['supports'] = $info_supports->result();
        
        $this->load->view('index', $data); 
    }
}