<div class="sponcer">
    <div class="container mt-5 mb-5">
        <div class="row text-center">
            <?php
            if(isset($supports) && count($supports) > 0):
                $i = 0;
                
                foreach($supports as $support):
                    $set_div = '';
                    if($i < 1):
                        $set_div = 'offset-sm-1';
                    endif;
            ?>
                <div class="<?=$set_div;?> col-sm-2">
                    <img src="<?=base_url().$support->file;?>" class="ImgFluid" alt="คอสเรียน">
                </div>
            <?php
                $i++;
                endforeach;
            endif;
            ?>
        </div>
    </div>
</div>