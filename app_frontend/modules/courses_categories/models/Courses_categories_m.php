<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Courses_categories_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own Courses_categories_m code
    }
    
    public function get_rows($param) 
    {
        if ( isset($param['length']) ) 
        $this->db->limit($param['length'], $param['start']);
    
        if ( isset($param['course_categorie_id']) ) 
            $this->db->where('a.course_categorie_id', $param['course_categorie_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);

        if ( isset($param['parent_id']) )
            $this->db->where('a.parent_id', $param['parent_id']);
        
        if ( isset($param['slug']) )
            $this->db->where('a.slug', $param['slug']);

        $this->db
            ->order_by('parent_id', 'asc')
            ->order_by('order', 'asc')
            ->order_by('course_categorie_id', 'asc');  

        $query = $this->db
                        ->select('a.*')
                        ->from('courses_categories a')
                        ->get();
        return $query;
    }

    // new function query

    public function count_courses_categories_all() 
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('courses_categories');
    }

    public function count_courses_categories_by_parent_id($id) 
    {
        $this->db->where('parent_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('courses_categories');
    }

    public function get_courses_categories_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('order', 'ASC');
        $this->db->select('*');
        $query = $this->db->get('courses_categories');
        return $query;
    }

    public function get_courses_categories_parent_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('parent_id', 0);
        $this->db->order_by('order', 'ASC');
        $this->db->select('*');
        $query = $this->db->get('courses_categories')->result();
        foreach ($query as $key => $rs) {
           $rs->parent = $this->db->where('recycle', 0)
                                ->where('active', 1)
                                ->where('parent_id', $rs->course_categorie_id)
                                ->order_by('order', 'ASC')
                                ->select('*')
                                ->get('courses_categories')
                                ->result();
        }
        return $query;
    }

    public function get_courses_categories_option_all($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('title', $param['search']);
            $this->db->or_like('excerpt', $param['search']);
            $this->db->or_like('detail', $param['search']);
        endif;

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('order', 'ASC');
        $this->db->select('*');
        $query = $this->db->get('courses_categories');
        
        return $query;
    }

    public function get_courses_categories_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('order', 'ASC');
        $this->db->select('*');
        $query = $this->db->get('courses_categories');
        
        return $query;
    }

    public function get_courses_categories_by_id($id)
    {
        $this->db->where('course_categorie_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('order', 'ASC');
        $this->db->select('*');
        $query = $this->db->get('courses_categories');
        
        return $query;
    }

    public function get_courses_categorie_by_parent_id($id)
    {
        $this->db->where('parent_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('order', 'ASC');
        $this->db->select('*');
        $query = $this->db->get('courses_categories');
        
        return $query;
    }
}
