<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses_categories extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
		$this->load->model('courses_categories_m');
	}
	
	public function index()
	{
		# code...
	}
    
    public function articles_categories_home()
    {
        $input['recycle'] = 0;
		$input['active']  = 1;
        // get articles_categories
		$info_articles_categories  = $this->articles_categories_m->get_rows($input);
		$data['articles_categories'] = $info_articles_categories->result();

		// get courses_categories
		$input_courses_cat['length'] 	= 6;
		$input_courses_cat['start'] 	= 0;
		$input_courses_cat['recycle'] 	= 0;
		$input_courses_cat['active']  	= 1;
		$info_courses_categories  		= $this->courses_categories_m->get_rows($input_courses_cat);
		$info_courses_categories_obj  	= $info_courses_categories->result();
		$data['courses_categories'] 	= $info_courses_categories_obj;
		$arr_parent_cate = array();
		if(isset($info_courses_categories_obj)):
			foreach ($info_courses_categories_obj as $value) :
				$input['course_categorie_id'] = $value->parent_id;
				$info_courses_categories_parent  = $this->courses_categories_m->get_rows($input);
					$cate_parent_obj = $info_courses_categories_parent->row();
				$arr_parent_cate[$value->parent_id] = $cate_parent_obj->title;
			endforeach;
		endif;

		$data['courses_cate_parent'] = $arr_parent_cate;
		
        
        $this->load->view('articles_categories_home', $data); 
	}
	
	public function categories_menu()
	{
		// get courses_categories
		$info  = $this->courses_categories_m->get_courses_categorie_by_parent_id(0)->result();
		if($info):
			foreach($info as $item):
				$item->info_list  = $this->courses_categories_m->get_courses_categorie_by_parent_id($item->course_categorie_id)->result();
			endforeach;
		endif;
		$data['info'] = $info;
        $this->load->view('categories_tab', $data); 
	}

}
