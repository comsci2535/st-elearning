<form action="" method="post">
    <?php
    if(!empty($info)):
        foreach($info as $item):
    ?>
    <div class="form-group">
        <label class="head" for="sel1"><?php echo !empty($item->title) ? $item->title : '';?></label>
        <?php 
            if(!empty($item->info_list)):
                foreach($item->info_list as $list):
        ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkbox-fiter-courses" value="<?php echo !empty($list->course_categorie_id) ? $list->course_categorie_id : '';?>">
                            <span class="cr"><i class="cr-icon fas fa-check"></i></span>
                            <span class="name"><?php echo !empty($list->title) ? $list->title : '';?></span>
                        </label>
                    </div>
        <?php  
                endforeach;
            endif; 
        ?>
    </div>
    <?php
        endforeach;
    endif;
    ?>
</form>