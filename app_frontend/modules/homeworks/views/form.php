<section class="Sec_Courses">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 pt-4">
                <h4 class="pt-4">
                    <a href="<?=site_url('homeworks/index/'.$courses_lesson_id);?>">บทเรียนทั้งหมด</a> > <span><?php echo !empty($breadcrumb)? $breadcrumb : '';?></span>
                </h4>
                <hr>
            </div>
            
            <div class="col-sm-12 mb-12">
                <div class="col-sm-12">
                    <form class="form-horizontal frm-create" method="post" action="<?=$frmAction?>" autocomplete="off" enctype="multipart/form-data">
                        
                        
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">หัวข้อ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="titles" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="detail">รายละเอียด <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <textarea name="detail" rows="3" class="form-control " id="detail" ><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                            </div>
                        </div>
                        
                       
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">สถานะ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <label><input <?php if(isset($info->active) && $info->active== 1){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="1" checked> เปิด</label>
                                <label><input <?php if(isset($info->active) && $info->active== 0){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
                            </div>
                        </div>
                        
                        <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="course_lesson_id" id="input-course_id" value="<?php echo $courses_lesson_id ?>">
                                <input type="hidden" class="form-control" name="db" id="db" value="courses_homeworks">
                                <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                                <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->courses_homework_id) ? encode_id($info->courses_homework_id) : 0 ?>">
                                <button type="submit" class="btn btn-success">บันทึก</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>
