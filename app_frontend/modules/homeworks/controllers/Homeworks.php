<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeworks extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
       
        $this->load->library('uploadfile_library');
        $this->load->model('homeworks_m');
        $this->load->model('courses_lesson/courses_lesson_m');
        
    }

    public function index($id=""){
        $data = array(
            'menu'    => 'exercise',
            'seo'     => "",
            'header'  => 'header',
            'content' => 'index',
            'footer'  => 'footer',
            'function'=>  array('custom','homeworks'),
		);

        $data['course_lesson_id'] = $id;

        $this->load->view('template/body', $data);
    }

    public function ajax_data() 
    {
        $input = $this->input->post();
        $info = $this->homeworks_m->get_courses_homeworks_by_all($input);
        $infoCount = $this->homeworks_m->count_courses_homeworks_by_all($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->courses_homework_id);
            $active = $rs->active ? '<span class="badge badge-success">เปิด</span>' : '<span class="badge badge-danger">ปิด</span>';
            $column[$key]['DT_RowId']           = $id;
            $column[$key]['no']                 = $no;
            $column[$key]['course_lesson_id']   = $rs->course_lesson_id;
            $column[$key]['title']              = $rs->title;
            $column[$key]['detail']             = $rs->detail;
            $column[$key]['active']     = $active;
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['updated_at'] = $rs->updated_at;
            $column[$key]['action']     = '<div class="btn-group">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="'.base_url('homeworks/edit/'.$rs->course_lesson_id.'/'.$id).'"><i class="fa fa-edit"></i> แก้ไข</a>
                                                    <a class="dropdown-item btn-click-delete" href="javascript:void(0)" data-id="'.$id.'"><i class="fa fa-trash"></i> ลบ</a>
                                                </div>
                                            </div>';
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function create($id)
    {
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => "",
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','homeworks/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/storage");

        //loade view
        
        $data['courses_lesson_id']     = $id;

        $lesson = $this->courses_lesson_m->get_courses_lesson_by_id($id)->row();
        $data['breadcrumb'] = $lesson->title;

        $this->load->view('template/body', $data);
    }

    public function edit($courses_lesson_id, $id = 0)
    {
        
        $data = array(
            'menu'    => 'courses',
            'seo'     => '',
            'header'  => 'header',
            'content' => 'form',
            'footer'  => 'footer',
            'function'=>  array('custom','homeworks/form'),
        );

        $data['frmAction'] = site_url("{$this->router->class}/update");

        //loade view
        
        $data['courses_lesson_id']     = $courses_lesson_id;

        $lesson = $this->courses_lesson_m->get_courses_lesson_by_id($courses_lesson_id)->row();
        $data['breadcrumb'] = $lesson->title;

        $info = $this->homeworks_m->get_homeworks_by_id($id);
        if ( $info->num_rows() == 0) {
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
       
        $this->load->view('template/body', $data);
    }

    public function storage()
    {
        $input = $this->input->post();
        $value = $this->_build_homeworks_data($input);
        
        $id = $this->homeworks_m->insert($value);
        if ( $id ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['course_lesson_id']}"));
    }

    public function send()
    {
        $input = $this->input->post();
        $value = $this->_build_data($input);
        
        $query = $this->homeworks_m->insert_send($value);
        if ( $query ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }

        redirect(base_url("courses-lesson/{$input['slug']}"));    
    }
    
    public function update()
    {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_homeworks_data($input);
        $result = $this->homeworks_m->update($id, $value);
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['course_lesson_id']}"));
    }

    private function _build_homeworks_data($input) 
    {
        $value                         = array();
        $value['course_lesson_id']  = $input['course_lesson_id'];
        $value['title']             = $input['title'];
        $value['active']            = $input['active'];
        $value['detail']            = html_escape($input['detail']);
       
        if ($input['mode'] == 'create') {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['UID'];
            $value['updated_at'] = db_datetime_now();
        } else {
            $value['updated_by'] = $this->session->users['UID'];
            $value['updated_at'] = db_datetime_now();
        }

        return $value;
    }

    private function _build_data($input) 
    {
        $value                         = array();
        $value['courses_homework_id']  = $input['courses_homework_id'];
        $value['user_id']              = $this->session->users['UID'];
        $value['detail']               = html_escape($input['detail']);
        
        $path        = 'homeworks';
        $upload      = $this->uploadfile_library->do_upload('file',TRUE,$path);
        $file        = '';
        if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
            $outfile = $input['outfile'];
            if(isset($outfile)){
                $this->load->helper("file");
                unlink($outfile);
            }
            $value['file'] = $file;
        }
       
        if ($input['mode'] == 'create') {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['UID'];
            $value['updated_at'] = db_datetime_now();
        } else {
            $value['updated_by'] = $this->session->users['UID'];
            $value['updated_at'] = db_datetime_now();
        }

        return $value;
    }

    public function action(){
        
        $toastr['type']     = 'error';
        $toastr['lineOne']  = config_item('appName');
        $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success']    = false;
        $data['toastr']     = $toastr;

        $input               = $this->input->post();
        $dateTime            = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['UID'];
        $result = false;
        
        if ( $input['type'] == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['UID'];
            $result = $this->homeworks_m->update_in($input['id'], $value);
        }
        
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));        
    } 
}
