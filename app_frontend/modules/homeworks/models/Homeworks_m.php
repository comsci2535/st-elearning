<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Homeworks_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }   

    public function get_courses_homeworks_by_all($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";

            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        if ( isset($param['courses_homework_id']) )
            $this->db->where('a.courses_homework_id', $param['courses_homework_id']);
        
        if ( isset($param['course_lesson_id']) )
            $this->db->where('a.course_lesson_id', $param['course_lesson_id']);
        
        $this->db->where('a.recycle', 0);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        $query = $this->db
                        ->select('a.*')
                        ->from('courses_homeworks a')
                        ->get();
        return $query;
    }

    public function count_courses_homeworks_by_all($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['courses_homework_id']) )
            $this->db->where('a.courses_homework_id', $param['courses_homework_id']);
        
        if ( isset($param['course_lesson_id']) )
            $this->db->where('a.course_lesson_id', $param['course_lesson_id']);
        
        $this->db->where('a.recycle', 0);

        return $this->db->count_all_results('courses_homeworks a');
    }

    public function get_homeworks_by_id($id)
    {
        $this->db->where('a.courses_homework_id', $id); 
        $query = $this->db->select('a.*')->from('courses_homeworks a')->get();
        return $query;
    }

    public function get_homeworks_by_user_homework_id($id, $user_id)
    {
        $this->db->where('a.course_lesson_id', $id); 
        $query = $this->db->select('a.*')->from('courses_homeworks a')->get();
        if(!empty($query->result())):
            foreach($query->result() as $item):
                $item->qty = $this->count_courses_homeworks_question_by_user_homework_id($item->courses_homework_id, $user_id);
            endforeach;
        endif;
        return $query;
    }

    public function get_courses_homeworks_question_by_courses_homework_id($id, $user_id)
    {
        $this->db->where('a.courses_homework_id', $id);
        $this->db->where('a.user_id', $user_id);
        $query = $this->db
        ->select('a.*')
        ->from('courses_homeworks_question a')
        ->get();
        return $query;
    }

    public function count_courses_homeworks_question_by_user_homework_id($id, $user_id)
    {
        $this->db->where('courses_homework_id', $id);
        $this->db->where('user_id', $user_id);
        return $this->db->count_all_results('courses_homeworks_question');
    }

    public function insert($value)
    {
        $this->db->insert('courses_homeworks', $value);
        return $this->db->insert_id();
    }

    public function insert_send($value)
    {
        return $this->db->insert('courses_homeworks_question', $value);
    }

    public function update($id, $value)
    {
        $query = $this->db
                        ->where('courses_homework_id', $id)
                        ->update('courses_homeworks', $value);
        return $query;
    }

    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('courses_homework_id', $id)
                        ->update('courses_homeworks', $value);
        return $query;
    }    

}
