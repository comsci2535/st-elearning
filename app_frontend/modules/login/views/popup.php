<div class="modal-dialog">
    <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
            <h3 class="modal-title text-center ImgFluid">เข้าสู่ระบบหรือสมัครสมาชิก</h3>
            <button type="button" class="close" data-dismiss="modal">X</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6 left">
                    <form id="LoginForm" class="form-signin" action="<?=site_url('login/login_user');?>" method="post">
                        <div class="form-label-group">
                            <label for="email1">ชื่อผู้ใช้หรืออีเมล</label>
                            <input type="email" id="email1" name="email" class="form-control"
                                placeholder="username email" required autofocus>
                        </div>

                        <div class="form-label-group">
                            <label for="password1">รหัสผ่าน</label>
                            <input type="password" id="password1" name="password" class="form-control"
                                placeholder="Password" required>
                        </div>

                        <div class="custom-control custom-checkbox my-2">
                            <!-- <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Remember password</label> -->
                            <!-- <input type="checkbox" class="custom-control-input" name="remember" value="1"
                                id="rememberme">
                            <label class="custom-control-label" for="rememberme">Remember</label> -->
                        </div>
                        <button class="btn btn-primary btn-block text-uppercase" type="submit" id="btn-login">Sign
                            in</button>
                        <hr class="my-2">
                        <a href="<?php echo $authURL; ?>">
                            <button class="btn btn-facebook btn-block text-uppercase" type="button"><i
                                    class="fab fa-facebook-f mr-2"></i>
                                Sign in with Facebook</button></a>
                    </form>
                </div>
                <div class="col-sm-6">
                    <div class="regis text-center">
                        <h2>หรือ</h2>
                        <a href="<?=site_url('login');?>">
                            <button type="button" class="btn btn-custom ImgFluid">สมัครสมาชิก</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
    
        $("#LoginForm").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 5
                },
            },
            messages: {
                email: {
                    required: ""+please+" email",
                    email:  ""+please+" email ให้ถูกต้อง"
                },
                password: {
                    required: ""+please+" รหัสผ่าน",
                    minlength: ""+Tmin+" 5 คำ"
                },
            },
            errorElement: "em",
        });
        $('form#LoginForm').submit(function(event) {

              if($('#email1').val()==""){
                $('#email1').focus();
                $(".alert_email1").html('<font color=red>* ชื่อผู้ใช้หรืออีเมล</font>'); 
                return false;
              }
              if($('#password1').val()==""){
                $('#password1').focus();
                $(".alert_password1").html('<font color=red>* รหัสผ่าน</font>'); 
                return false;
              }


              $('#form-error-div').html(' ');
              $('#form-success-div').html(' ');

             // var conf = archiveFunction();
              //alert(conf);
              if(confirm("ยืนยันการแจ้งชำระเงินอีกครั้ง !!")){


                event.preventDefault();             
                var formObj = $(this);
                var formURL = formObj.attr("action");
                var formData = new FormData(this);
                $('#form-img-div').html('<img src="'+base_url+'images/loading1.gif" alt="loading" title="loading" style="display:inline">');
                $.ajax({
                  url: formURL,
                  type: 'POST',
                  data:formData,
                  dataType:"json",
                  mimeType:"multipart/form-data",
                  contentType: false,
                  cache: false,
                  processData:false,
                  success: function(data, textStatus, jqXHR){

                   if(data.info_txt == "error")
                   {
                     $('#form-img-div').html(' ');
                     $("#form-error-div").append("<p><strong><li class='text-red'></li>"+data.msg+"</strong>,"+data.msg2+"</p>");
                     $("#form-error-div").slideDown(400);

                   }
                   if(data.info_txt == "success")
                   {
                        
                          setTimeout(function(){
                              $('#form-img-div').html(' ');
                              swal({
                                  title: 'คุณทำรายการเรียบร้อย',
                                  text: "ขอให้สนุกในการเรียนน่ะค่ะ",
                                  type: 'success',
                                  confirmButtonText: 'ตกลง'
                              }).then(function(result) {
                                  
                                  if (result.value) {
                                      window.location=base_url+'profile/my-course';
                                  }
                              });
                            },2000);

                        }

                      },
                      error: function(jqXHR, textStatus, errorThrown){
                       $('#form-img-div').html(' ');
                       $("#form-error-div").append("<p><strong>บันทึกข้อมูลไม่สำเร็จ</strong>,กรุณาลองใหม่อีกครั้ง!</p>");
                       $("#form-error-div").slideDown(400);
                     }

                   });
              }else{
                return false;
              }
        });

    });
</script>