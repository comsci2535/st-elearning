<?php 

/**
 * 
 */
class Users_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function find_users_by_user_facebook($oauth_uid){

		$query = $this->db->where('oauth_uid',$oauth_uid)
		         ->get('users');
		return $query->row();         
	}

	public function find_users_by_user($username){

		$query = $this->db->where('username',$username)
						  ->where('active',1)
		         		  ->get('users');
		return $query->row();         
	}

	public function find_users_by_user_and_salt($username,$salt){

		$query = $this->db->where('username',$username)
						  ->where('salt',$salt)
		                  ->get('users');
		return $query->row();
	}

    public function register_user($value) {
        $this->db->insert('users', $value);
        return $this->db->insert_id();
        //return true;
	}
	
	public function update_user($id, $value)
    {
        $query = $this->db
                        ->where('user_id', $id)
                        ->update('users', $value);
        return $query;
	}
	
	public function check_passwords($param){

		$query = $this->db->where('user_id', $param['user_id'])
		         ->get('users');
		return $query->row();         
	}

	public function get_rows($param)
    {
       $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('users a')
                        ->get();
        return $query;
	}
	private function _condition($param) 
    {   
        
        if ( isset($param['user_id']) ) 
            $this->db->where('a.user_id', $param['user_id']);

        if ( isset($param['username']) ) 
            $this->db->where('a.username', $param['username']);

         if ( isset($param['email']) ) 
            $this->db->where('a.email', $param['email']);

        if ( isset($param['recycle']) ){
            if (is_array($param['recycle'])) {
                $this->db->where_in('a.recycle', $param['recycle']);
            } else {
                $this->db->where('a.recycle', $param['recycle']);
            }
        }
           

    }

    public function get_user_by_id($user_id) 
    {
      
        $this->db->where('a.user_id', $user_id);
        $query = $this->db
                        ->select('a.*')
                        ->from('users a')
                        ->get();
        return $query;
    }

}