<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends MX_Controller {

	function __construct() {
        parent::__construct();
        
        $this->load->model('banners/banners_m');
		$this->load->model('articles_m');
		$this->load->model('articles_categories/articles_categories_m');

		$this->load->model('courses/courses_m');
		$this->load->model('courses_instructors/courses_instructors_m');
		$this->load->model('seos/seos_m');
	}

    

	private function seo(){

		$obj_seo = $this->seos_m->get_seos_by_display_page('articles')->row();
		$title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
		$robots         = !empty($obj_seo->title)? $obj_seo->title : 'คอร์สเรียนออลไลน์';
		$description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : 'คอร์สเรียนออลไลน์';
		$keywords       = !empty($obj_seo->detail)? $obj_seo->detail : 'คอร์สเรียนออลไลน์';
		$img       		= !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url('articles')."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".$img."'/>";
		return $meta;
	}

	private function seo_detail($slug){

        
        $input_articles['recycle'] 	= 0;
		$input_articles['active']  	= 1;
		$input_articles['slug']  	= $slug;
		$obj_seo  = $this->articles_m->get_rows($input_articles)->row();

        $title          = !empty($obj_seo->metaTitle)? config_item('siteTitle').' | '.$obj_seo->metaTitle : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
        $robots         = !empty($obj_seo->metaTitle)? $obj_seo->metaTitle : 'คอร์สเรียนออลไลน์';
        $description    = !empty($obj_seo->metaDescription)? $obj_seo->metaDescription : 'คอร์สเรียนออลไลน์';
        $keywords       = !empty($obj_seo->metaKeyword)? $obj_seo->metaKeyword : 'คอร์สเรียนออลไลน์';
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('articles/detail/'.$slug)."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

	public function index($slug=''){
		Modules::run('track/front','');
        $data = array(
            'menu'    => 'articles',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'articles',
            'footer'  => 'footer',
            'function'=>  array('custom','articles'),
		);
		
		//loade view
        $data['total'] = $this->articles_m->count_article_all();
        $this->load->view('template/body', $data);
	}
    
    public function detail($slug){

        $data = array(
            'menu'    => 'articles',
            'seo'     => $this->seo_detail($slug),
            'header'  => 'header',
            'content' => 'articls_detail',
            'footer'  => 'footer',
            'function'=>  array('custom','articles'),
		);

		// get articles
	
		$input_articles['recycle'] 	= 0;
		$input_articles['active']  	= 1;
		$input_articles['slug']  	= $slug;
		$info_articles  = $this->articles_m->get_rows($input_articles);
		$data['articles'] = $info_articles->row();

		$this->articles_m->plus_view($data['articles']->article_id);
        
        // get data articles_categories 
        $info_articles_categories  = $this->articles_categories_m->get_articles_categories_by_id($data['articles']->article_categorie_id);
		$data['articles_categories'] = $info_articles_categories->row();

		$param['ogUrl'] = 'articles/detail/'.$slug;
        Modules::run('social/set_share', $param);
        Modules::run('track/front',$data['articles']->article_id);
		//loade view
        $this->load->view('template/body', $data);
	}

	public function articles_home(){

		// get articles
		// $input['length'] 	= 6;
		// $input['start'] 	= 0;
		// $input['recycle'] 	= 0;
		// $input['active']  	= 1;
		// $info_articles  = $this->articles_m->get_rows($input);
		// $data['articles'] = $info_articles->result();
		// $this->load->view('articles_home', $data); 

		// get articles new
		$param['length'] = 6;
		$param['start']	 = 0;
		$info = $this->articles_m->get_article_option_all($param)->result();

		if($info):
			foreach($info as $item):
				$item->list_categories = $this->articles_categories_m->get_articles_categories_by_id($item->article_categorie_id)->row();
			endforeach;
		endif;

		$data['info'] = $info;
		$this->load->view('articles_home', $data); 
	}

	
	public function ajax_load_articles()
	{
		$mgs 	= 'แจ้งเตือน';
		$status = 0;
        $html   = '';
        
        $input = $this->input->post();
		$input['length'] = 6;
        $input['start']  = $input['length']*$input['page'];
        
        $fiter_arr = array();
        $article_categories =  array();
        //unset($article_categories);
        if(!empty($input['fiter_arr'])):
            foreach($input['fiter_arr'] as $fiter):
                array_push($fiter_arr, $fiter);
            endforeach;
            $input['article_id_arr'] = array('-');
            foreach ($fiter_arr as $key => $rs) {
                $article_categories=$this->db
                                    ->where('article_categorie_id',$rs)
                                    ->select('article_id')
                                    ->get('articles_categories_map')
                                    ->result();
            }
            //arr($article_categories);
            if(!empty($article_categories)){
                foreach ($article_categories as $key => $rs) {
                    array_push($input['article_id_arr'], $rs->article_id);
                }
            }
        endif;
        //$input['fiter_arr'] = $fiter_arr;

        $info = $this->articles_m->get_article_option_all($input)->result();
        $info_count = $this->articles_m->get_article_count_option_all($input);
        $total='<p>'.$info_count.' บทความ</p>';

        $pageAll=$info_count;
        $pageAll= 0 ? 1 : ceil($pageAll/$input['length']);
        
        if($pageAll==($input['page']+1)){
            $pageAll=0;
        }
		if($info):
			
            foreach($info as $item):
                
            $datatime = explode(',', date_languagefull($item->created_at, true,'th'));
            // $datatime[0] =  25 พฤษภาคม 2562' ,  $datatime[1] =  เวลา  00:22 น.

            $dateshow = explode(',', date_languagefull($item->start_date, true,'th'));
            //  $dateshow[0] =  31 พฤษภาคม 2562  เพราะ start_date เก็บแค่วันที่อย่างเดียว

            $html.='<div class="Item">
						<a href="'.site_url("articles/detail/{$item->slug}").'">
						<div class="boxx">
                            <div class="ribbin">';
                            $html.='<input type="hidden" id="course_id-'.$item->article_id.'" value="'.$item->article_id.'">';
                            $img = "'".base_url('images/Getup_Teacher.jpg')."'";
                            $html.='</div>
                            <div class="img">
                                <img src="'.base_url($item->file).'" class="ImgFluid imgBig" alt="'.$item->title.'" onerror="this.src= '.$img.' ">
                                <div class="hovv">
                                    <div class="text Bold">
                                        <span><i class="fas fa-link" style="font-size: 18px;"></i> รายละเอียดบทความ</span>
                                    </div>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="name Bold">
                                    <p>'.$item->title.'</p>
                                </div>
                                <div class="text">
                                    <span>'.$item->excerpt.'</span>
								</div>
								<div class="dateview">
                                    <div class="date">
                                        <span><i class="far fa-calendar-alt"></i> '.$dateshow[0].'</span>
                                        <span><i class="far fa-clock"></i> เวลา '.$datatime[1].' น.</span>
                                    </div>
                                    <div class="view">
                                        <span><i class="far fa-eye"></i> '.$item->qty_eye.'</span>
                                    </div>
								</div>
                            </div>
                        </div>
						</a>
					</div>';
			endforeach;

			$status = 1;
		endif;
		
		$data = array(
			'data' 		=> $html
			,'mgs' 		=> $mgs
            ,'status' 	=> $status
            ,'test'     => $fiter_arr
            ,'pageAll' => $pageAll
            ,'total' => $total
		);

		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
	}
	
}
