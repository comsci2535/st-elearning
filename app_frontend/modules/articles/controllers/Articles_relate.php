<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles_relate extends MX_Controller {

	function __construct() {
        parent::__construct();
        
        $this->load->model('articles/articles_m');

	}

	function index($category_id,$id){

		
		$articles_categories_map=$this->db
                                    ->where('article_id',$id)
                                    ->select('article_categorie_id')
                                    ->get('articles_categories_map')
                                    ->result();
        $article_categories =  array();
        foreach ($articles_categories_map as $key => $rs) {
            $article_categories=$this->db
                                ->where('article_categorie_id',$rs->article_categorie_id)
                                ->select('article_id')
                                ->get('articles_categories_map')
                                ->result();
        }                            
        $input_articles['article_id_arr'] = array('-');
        if(!empty($article_categories)){
            foreach ($article_categories as $key => $rs) {
                array_push($input_articles['article_id_arr'], $rs->article_id);
            }
        }
                                   
		$input_articles['relate_id']  	= $id;
		$input_articles['category_id']  = $category_id;
		$input_articles['length']  = 4;
		$input_articles['recycle'] 	= 0;
		$input_articles['active']  	= 1;
		$info_articles  = $this->articles_m->get_rows($input_articles);
		$data['articleRelates'] = $info_articles->result();
		
		//loade view

        	$this->load->view('articles/articls_relate', $data);
	}
}