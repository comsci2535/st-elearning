<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Articles_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        // $this->db->last_query();
    }
    
    public function get_rows($param) 
    {
        //  $this->db->where("((a.start_date ='0000-00-00' AND a.end_date ='0000-00-00') OR  (a.start_date <='".date('Y-m-d')."' AND a.end_date >='".date('Y-m-d')."'))");
        $this->db->where("((a.start_date ='0000-00-00') OR (a.start_date <= '".date('Y-m-d')."'))");
        if ( isset($param['search']) && $param['search'] != "" ) {
            $this->db
            ->group_start()
            ->like('a.title', $param['search'])
            ->or_like('a.excerpt', $param['search'])
            ->or_like('a.detail', $param['search'])
            ->group_end();
        }
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        if ( isset($param['article_id']) ) 
            $this->db->where('a.article_id', $param['article_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);
        
        if ( isset($param['recommend']) )
            $this->db->where('a.recommend', $param['recommend']);

        if ( isset($param['slug']) )
            $this->db->where('a.slug', $param['slug']);

        if ( isset($param['relate_id']) )
            $this->db->where_not_in('a.article_id', $param['relate_id']);

        // if(!empty($param['fiter_arr'])):
        //     $this->db->where_in('article_categorie_id', $param['fiter_arr']);
        // endif;
        if(!empty($param['article_id_arr'])):
            $this->db->where_in('article_id', $param['article_id_arr']);
        endif;



        $this->db->order_by('a.recommend', 'DESC');
        $this->db->order_by('a.created_at', 'DESC');
        $query = $this->db
        ->select('a.*')
        ->from('articles a')
        ->get();

        return $query;
    }

    public function count_rows($param) 
    {
        $this->db->where("((a.start_date ='0000-00-00') OR (a.start_date <= '".date('Y-m-d')."'))");
        if ( isset($param['search']) && $param['search'] != "" ) {
            $this->db
            ->group_start()
            ->like('a.title', $param['search'])
            ->or_like('a.excerpt', $param['search'])
            ->or_like('a.detail', $param['search'])
            ->group_end();
        }
        
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);

        return $this->db->count_all_results('articles a');
    }

    public function get_categoryAll($slug = null){

        if ( isset($slug) )
            $this->db->where('c.slug', $slug);

        $this->db->order_by('c.title', 'ASC');
        
        $query = $this->db->select('c.*')
        ->distinct()
        ->from('articles_categories c')
        ->join('articles a', 'c.article_categorie_id = a.article_categorie_id', 'right')
        ->where('c.active', 1)
        ->get();

        /*echo $this->db->last_query();
        exit();*/

        return $query;
    }

    public function plus_view($id)
    {
        $sql = "UPDATE articles SET qty_eye = (qty_eye+1) WHERE article_id=?";
        $this->db->query($sql, array($id));
    }


    // new function query

    public function count_article_all() 
    {
        $this->db->where("((start_date ='0000-00-00') OR (start_date <= '".date('Y-m-d')."'))");
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('articles');
    }

    public function count_article_categorie_by_id($id) 
    {
        $this->db->where("((start_date ='0000-00-00') OR (start_date <= '".date('Y-m-d')."'))");
        $this->db->where('article_categorie_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('articles');
    }

    public function get_article_all()
    {
        $this->db->where("((start_date ='0000-00-00') OR (start_date <= '".date('Y-m-d')."'))");
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('articles');
        return $query;
    }

    public function get_article_option_all($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->group_start();
                $this->db->like('title', $param['search']);
                $this->db->or_like('excerpt', $param['search']);
                $this->db->or_like('detail', $param['search']);
            $this->db->group_end();
        endif;

        // if(!empty($param['fiter_arr'])):
        //     $this->db->where_in('article_categorie_id', $param['fiter_arr']);
        // endif;
        if(!empty($param['article_id_arr'])):
            $this->db->where_in('article_id', $param['article_id_arr']);
        endif;

        $this->db->where("((start_date ='0000-00-00') OR (start_date <= '".date('Y-m-d')."'))");
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('articles');
        
        return $query;
    }

    //ree
    public function get_article_count_option_all($param)
    {
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->group_start();
                $this->db->like('title', $param['search']);
                $this->db->or_like('excerpt', $param['search']);
                $this->db->or_like('detail', $param['search']);
            $this->db->group_end();
        endif;

        // if(!empty($param['fiter_arr'])):
        //     $this->db->where_in('article_categorie_id', $param['fiter_arr']);
        // endif;
        if(!empty($param['article_id_arr'])):
            $this->db->where_in('article_id', $param['article_id_arr']);
        endif;

        // $order_by = !empty($param['sort'])? $param['sort'] : 'DESC'; 
        $this->db->where("((start_date ='0000-00-00') OR (start_date <= '".date('Y-m-d')."'))");
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', $order_by);
        $this->db->order_by('created_at', $order_by);
        $this->db->select('*');
        $query = $this->db->get('articles');
        
        return $query->num_rows();
    }


    public function get_article_by_slug($slug)
    {
        $this->db->where("((start_date ='0000-00-00') OR (start_date <= '".date('Y-m-d')."'))");
        $this->db->where('slug', $slug);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('articles');
        
        return $query;
    }

    public function get_article_categorie_by_id($id)
    {
        $this->db->where("((start_date ='0000-00-00') OR (start_date <= '".date('Y-m-d')."'))");
        $this->db->where('article_categorie_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('articles');
        
        return $query;
    }

    public function get_article_join_categorie_by_id($id)
    {
        $this->db->where("((a.start_date = '0000-00-00') OR (a.start_date <= '".date('Y-m-d')."'))");
        $this->db->where('a.article_categorie_id', $id);
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->order_by('a.recommend', 'DESC');
        $this->db->order_by('a.created_at', 'DESC');
        $this->db->select('a.*, b.*');
        $this->db->join('articles_categories b', 'a.article_categorie_id = b.article_categorie_id');
        $query = $this->db->get('articles a');
        
        return $query;
    }

    public function get_article_join_categorie_by_slug($slug)
    {
        $this->db->where("((a.start_date ='0000-00-00') OR (a.start_date <= '".date('Y-m-d')."'))");
        $this->db->where('b.slug', $slug);
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->order_by('a.recommend', 'DESC');
        $this->db->order_by('a.created_at', 'DESC');
        $this->db->select('a.*, b.*');
        $this->db->join('articles_categories b', 'a.article_categorie_id = b.article_categorie_id');
        $query = $this->db->get('articles a');
        
        return $query;
    }
}
