<?php if(!empty($articleRelates)){ ?>
	<h3 class="mb-2 font-weight-bold text-center"><u>บทความที่เกี่ยวข้อง</u></h3>
<?php }?>

<div class="row">
	<div class="col-md-12">
		<div class="CoursesItem">

		<?php 
			foreach ($articleRelates as $articleRelate) { 
				$datatime = explode(',', date_languagefull($articleRelate->created_at, true,'th'));
				$img = "'".base_url('images/Getup_Teacher.jpg')."'";
		?>

			<!-- <div class="col-md-3 col-sm-6 mb-4">
			<a href="<?=base_url('articles/detail/').$articleRelate->slug?>">
				<img class="img-fluid" src="<?=base_url('').$articleRelate->file?>" alt="">
				<h4><?=$articleRelate->title?></h4>
				<p><?=$articleRelate->excerpt?></p>
			</a>
		</div> -->

			<div class="Item colum-full">
				<a href="<?=site_url("articles/detail/{$articleRelate->slug}")?>">
					<div class="boxx">
						<div class="ribbin">
						</div>
						<div class="img">
							<img src="<?=base_url($articleRelate->file)?>" class="ImgFluid imgBig"
								alt="<?=$articleRelate->title?>" onerror="this.src=<?=$img?> ">
							<div class="hovv">
								<div class="text Bold">
									<span><i class="fas fa-link" style="font-size: 18px;"></i> รายละเอียดบทความ</span>
								</div>
							</div>
						</div>
						<div class="detail">
							<div class="name Bold">
								<p><?=$articleRelate->title?></p>
							</div>
							<div class="text">
								<span><?=$articleRelate->excerpt?></span>
							</div>
							<div class="dateview">
								<div class="date">
									<span><i class="far fa-calendar-alt"></i> <?=$datatime[0]?></span>
									<span><i class="far fa-clock"></i> เวลา <?=$datatime[1]?> น.</span>
								</div>
							</div>
							<div style="font-size: 15px;">
								<span><i class="far fa-eye"></i> <?=$articleRelate->qty_eye?></span>
							</div>
						</div>
					</div>
				</a>
			</div>

			<?php }?>
		</div>
	</div>
</div>