<div class="GETUP_BLOG container">
    <div class="title Bold">
        <h1>GETUP BLOG</h1>
        <div class="text">
            <p>บทความออนไลน์ อ่านฟรี 24 ชั่วโมง ทุกหมวดหมู่ที่คุณสนใจ ถูกรวมไว้แล้ว</p>
        </div>
    </div>
    <div class="row">
        <?php 
            if(!empty($info)):
                foreach($info as $item):
        ?>
        <div class="col-md-4 wow fadeIn" data-wow-delay="0.0s">
            <a href="<?=base_url('articles/detail/').$item->slug?>">
                <div class="boxx">
                        <img src="<?=base_url($item->file)?>" class="ImgFluid imgBig" alt="<?=$item->title? $item->title : ''?>" onerror="this.src='<?=base_url("images/GETUP_BLOG.jpg");?>'">
                    <div class="text">
                        <span><?=$item->list_categories->title? $item->list_categories->title : ''?></span>
                        <p><?=$item->title? $item->title : ''?></p>
                    </div>
                </div>
            </a>
        </div>
        <?php 
                endforeach; 
            endif; 
        ?>
    </div>
    <div class="text-center mb-5">
        <a href="<?=base_url()?>articles" class="btn-custom">บทความทั้งหมด</a>
    </div>
</div>