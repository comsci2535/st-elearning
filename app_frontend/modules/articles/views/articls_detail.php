<section class="Secc_article">

	<?php echo Modules::run('banners/index', 'articles', 'articles'); ?>
	<!-- Page Content -->
	<div class="container">
		<div class="row">
			<div class="col-md-12 mt-5 mb-5">
				<div class="d-flex">
					<a class="m-2" href="<?=base_url('articles')?>">บทความ</a>
					<span class="m-2"> > </span>
					<span class="m-2" href="#"><?=$articles->title;?></span>
				</div>
				<hr style="margin-bottom: 0px; margin-top: 0px;">
				<?php $datatime = explode(',', date_languagefull($articles->created_at, true,'th')); ?>
				<!-- <div class="date-custom font-weight-bold m-2">
					<span class="dates" style="font-size: 16px;"><i class="far fa-clock" style="font-size: 12px;"></i> <?=$datatime[0];?></span>
					<span class="dates" style="font-size: 16px;"> เวลา <?=$datatime[1];?> น.</span>
				</div> -->
			</div>
			<div class="col-sm-9">
				<div class="row">
					<!-- Portfolio Item Heading -->
					<div class="col-md-12">
						<div class="text-center">
							<h1 class="my-1 font-weight-bold text-center"><?=$articles->title;?></h1>
							<img src="<?=base_url().$articles->file;?>" class="img-thumbnail" alt="Responsive image"
								onerror="this.src='<?php echo base_url("images/Getup_Teacher.jpg");?>'">
							<?php echo Modules::run('social/share_button'); ?>
						</div>
						<div class="date">
							<span style="font-size: 16px;"><i class="far fa-calendar-alt" style="font-size: 12px;"></i> <?=$datatime[0];?></span>
							<span style="font-size: 16px;"><i class="far fa-clock" style="font-size: 12px;"></i> เวลา <?=$datatime[1];?> น.</span>
							<span style="font-size: 16px;"><i class="far fa-eye" style="font-size: 12px;"></i> <?=number_format($articles->qty_eye)?></span>
						</div>
					
					</div>
					<!-- Portfolio Item Row -->
					<?php if(!empty($articles->excerpt)){ ?>
					<div class="col-md-12">
						<h4 class="my-4 font-weight-bold"><u>รายละเอียดย่อ</u></h4>
						<p><?=$articles->excerpt;?></p>
					</div>
				    <?php } ?>
				    <?php if(!empty($articles->detail)){ ?>
					<div class="col-md-12 text-break" style="word-wrap: break-word">
						<h4 class="my-4 font-weight-bold"><u>รายละเอียด</u></h4>
						<p><?=html_entity_decode($articles->detail);?></p>
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="col-sm-3">
				<?php echo Modules::run('articles/Articles_relate/index',$articles->article_categorie_id , $articles->article_id) ?>
			</div>
		</div>

		<!-- Related Projects Row -->
		<!-- /.row -->


	</div>
	<!-- /.container -->

</section>