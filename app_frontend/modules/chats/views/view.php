
<div class="LiveChat d-none2">
    <img id="OpenChat" src="<?=base_url('images/chat.png');?>" alt="คอสเรียน" data-user-id="14" data-course-id="3">

    <div class="UserChat">
        <div class="HederUser">
            <div class="profile">
                <img id="text-user-img-chat" src="" class="" alt="คอสเรียน" onerror="this.src='<?php echo base_url('images/user.png');?>'">
                <div id="text-user-name-chat" class="text"><span>Sobree kama</span></div>
                <button id="CloseCaht" type="button" class="btn">X</button>
            </div>
        </div>
        <div class="ListUser" onerror="this.src='<?php echo base_url('images/user.png');?>'">
            <!-- offline, online -->
            <!-- <div class="box online">
                <img src="<?=base_url('images/Great_Teacher.jpg');?>" class="" alt="คอสเรียน">
                <div class="content">
                    <div class="name">ซอบรี(คนหล่อ) กามา</div>
                    <div class="noti">0</div>
                    <i class="fas fa-paper-plane"></i>
                </div>
            </div> -->
            
        </div>
        <div class="ButtomUser">
            <i class="fas fa-comments"></i>
            <span>Live Chat..</span>
        </div>
    </div>


    <div class="MassageChat">
        <div class="HederChat">
            <div class="back">
                <i id="BackCaht" class="fas fa-arrow-left"></i>
                <div class="profile">
                    <img id="text-user-img-chat-by" src="" class="" alt="คอสเรียน" onerror="this.src='<?php echo base_url('images/user.png');?>'">
                    <div id="text-user-name-chat-by" class="text"><span></span></div>
                    <button id="Close" type="button" class="btn">X</button>
                </div>
            </div>
        </div>
        <div class="ListChat">
            <div class="box">
                <img src="<?=base_url('images/Great_Teacher.jpg');?>" class="" alt="คอสเรียน">
                <div class="content">
                    <div class="text">
                        การพัฒนาตัวจนเพื่อสร้างบุคลิกภาพที่ดีเป็นสิ่ง
                        หลักสูตรพัฒนาตนเพื่อการเรียน-รู้ตลอดชีวิต-ด้วยทักษะที่มีใน-4
                    </div>
                </div>
            </div>
            <div class="box Friend">
                <img src="<?=base_url('images/Great_Teacher.jpg');?>" class="" alt="คอสเรียน">
                <div class="content">
                    <div class="text">
                        การพัฒนาตัวจนเพื่อสร้างบุคลิกภาพที่ดีเป็นสิ่ง
                        หลักสูตรพัฒนาตนเพื่อการเรียน-รู้ตลอดชีวิต-ด้วยทักษะที่มีใน-4
                        หลักสูตรพัฒนาตนเพื่อการเรียน-รู้ตลอดชีวิต-ด้วยทักษะที่มีใน-4
                    </div>
                </div>
            </div>
        </div>
        <div class="ButtomChat">
            <form action="" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" id="" placeholder="ข้อความ..." required>
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupPrepend2"><i class="fas fa-paper-plane"></i></span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>