<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chats extends MX_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('courses_instructors/courses_instructors_m');
        $this->load->model('courses_students/courses_students_m');
    }
    
    public function view(){

        $this->load->view('view');

    }    
    
    public function api_instructors()
    {
        $input      = $this->input->post();
        $status     = 0;
        $info       = $this->courses_instructors_m->get_courses_instructors_join_user_by_course_id($input['course_id'])->row();
        if(!empty($info)):
            $data['fullname']       = $info->fullname;
            $data['file']           = base_url().$info->file;
            $status = 1;
        endif;
        
        $data['status'] = $status;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }

    public function api_student()
    {
        $input      = $this->input->post();
        $status     = 0;
        $info       = $this->courses_instructors_m->get_courses_instructors_join_user_id_by_course_id($input['course_id'], $input['user_id'])->row();
        if(!empty($info)):
            $data['fullname']       = $info->fullname;
            $data['file']           = base_url().$info->file;
            $input_['course_id']    = $input['course_id'];
            $input_['status']       = 1;
            $data['student']        = $this->set_courses_students($this->courses_students_m->get_courses_students_join_users_by_id($input_)->result());
            $status = 1;
        endif;
        
        $data['status'] = $status;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }

    public function set_courses_students($result)
    {
        $data = array();
        if(!empty($result)):
            foreach($result as $item):
                if($item->onoff > 0){
                    $onoff = 'online';
                }else{
                    $onoff = 'offline';
                }
                array_push($data, array(
                    'fullname'  => $item->fullname,
                    'file'      => base_url().$item->file,
                    'user_id'   => $item->user_id,
                    'course_id' => $item->course_id,
                    'onoff'     => $onoff
                ));
            endforeach;
        endif;
        return $data;
    }
    
}
