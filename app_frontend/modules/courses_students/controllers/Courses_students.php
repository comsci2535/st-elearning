<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses_students extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('courses_students_m');
        $this->load->model('courses/courses_m');
        $this->load->model('courses_lesson/courses_lesson_m');
        $this->load->model('exams/exams_m');
        $this->load->model('exams_lists/exam_list_m');
        $this->load->model('homeworks/homeworks_m');
        $this->load->model('certificates/certificates_m');
        $this->load->model('login/users_model');
        
    }
    
    private function seo(){
		$title          = 'ST Developer';
		$robots         = 'ST Developer';
		$description    = 'ST Developer';
		$keywords       = 'ST Developer';
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url()."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
		return $meta;
    }
    
    public function index()
    {
    }

    public function lists($id = 0)
    {

        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'lists',
            'footer'  => 'footer',
            'function'=>  array('custom','courses_students'),
		);
        //loade view

        $data['course_id'] = $id;
        $course = $this->courses_m->get_courses_by_course_id($id)->row();
        $data['breadcrumb'] = $course->title;

        $this->load->view('template/body', $data);

    }

    public function ajax_data_courses() {
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->courses_students_m->get_courses_students_join_users_by_id($input);
        $infoCount = $this->courses_students_m->count_courses_students_join_users_by_id($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->user_id);
            
            if(!empty($rs->oauth_picture)):
                $img =  $rs->oauth_picture;
            else:
                $img =  base_url($rs->file);
            endif;

            $active = $rs->status ? '<span class="badge badge-success">นักเรียน</span>' : '<span class="badge badge-danger">รอการตรวจสอบ</span>';
            $img    = '<img src="'.$img.'" class="img-rounded" style="width: 100px;">';
            
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['no']         = $no;
            $column[$key]['course_id']  = $rs->course_id;
            $column[$key]['fullname']   = $rs->fullname;
            $column[$key]['img']        = $img;
            $column[$key]['active']     = $active;
            $column[$key]['email']      = $rs->email;
            $column[$key]['phone']      = $rs->phone;
            $column[$key]['created_at'] = $rs->created_at;
            $column[$key]['updated_at'] = $rs->updated_at;
            $column[$key]['action']     = '<div class="btn-group">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"></button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item btn-click-view" href="javascript:void(0)" data-user-id="'.$id.'" data-course-id="'.$rs->course_id.'"><i class="fa fa-book"></i> ดูข้อมูลการเรียน</a>
                                                    <a class="dropdown-item btn-click-quiz" href="javascript:void(0)" data-user-id="'.$id.'" data-course-id="'.$rs->course_id.'"><i class="fa fa-book"></i> ดูข้อมูลผลการสอบ</a>
                                                    <a class="dropdown-item btn-click-homeworks" href="javascript:void(0)" data-user-id="'.$id.'" data-course-id="'.$rs->course_id.'"><i class="fa fa-edit"></i> การบ้าน</a>
                                                </div>
                                            </div>';
        }

        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function ajax_data_lesson()
    {
        $input      = $this->input->post();
        $infoCourse = $this->courses_m->get_courses_by_course_id($input['course_id'])->row();
        $info       = $this->courses_lesson_m->get_courses_lesson_by_course_id($input['course_id'])->result();
        if($info):
            foreach($info as $item):
                $item->info_lesson  = $this->courses_lesson_m->get_courses_lesson_by_parent_id($item->course_lesson_id)->result();
                if(!empty($item->info_lesson)):
                    foreach($item->info_lesson as $list):
                        $list->lesson  = $this->courses_lesson_m->get_courses_lesson_member_by_id($list->course_lesson_id, $input['user_id'])->result();
                    endforeach;
                endif;
               
            endforeach;
        endif;

        $data['courses'] = $infoCourse;
        $data['data'] = $info;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function ajax_data_quiz()
    {
        $info_arr = array();
        $input = $this->input->post();
        $info = $this->exams_m->get_quiz_by_user_id($input['user_id'], $input['course_id'])->result();
        if(!empty($info)):
            foreach($info as $item):
                $input['exam_id'] = $item->exam_id;
                $input['quiz_id'] = $item->quiz_id;
                $info_arr[$item->type] = array(
                     'score'       => $item->score
                    ,'quiz_id'     => $item->quiz_id
                    ,'exam_id'     => $item->exam_id
                    ,'status'      => $item->status
                    ,'user_id'     => $item->user_id
                    ,'qty'         => $this->exam_list_m->count_exams_list_by_all($input)
                    ,'exam'        => $this->exams_m->get_exam_by_exam_id($item->exam_id)->row()
                    ,'exam_detail' => $this->exams_m->get_question_quiz_detail($input)
                );
                
            endforeach;
        endif;
        $data['data'] = $info_arr;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function ajax_data_homeworks()
    {
        $arr_homeworks = array();
        $input = $this->input->post();
        $info = $this->courses_lesson_m->get_courses_lesson_by_parent_id_course_id($input['course_id'], 0)->result();
        if(!empty($info)):
            foreach($info as $item): 
                $courses_lesson = $this->courses_lesson_m->get_courses_lesson_by_parent_id($item->course_lesson_id)->result();
                if(!empty($courses_lesson)):
                    foreach($courses_lesson as $lesson):
                        $input_['course_lesson_id']  = $lesson->course_lesson_id; 
                        $courses_homeworks = $this->homeworks_m->get_courses_homeworks_by_all($input_)->result(); 
                        foreach($courses_homeworks as $homeworks):
                            $homeworks->info_homeworks = $this->homeworks_m->get_courses_homeworks_question_by_courses_homework_id($homeworks->courses_homework_id, $input['user_id'])->result();
                        endforeach;
                        array_push($arr_homeworks, array( 
                            'title'                 => $lesson->title
                            ,'courses_homeworks'    => $courses_homeworks
                        ));
                    endforeach;
                endif;
                
                unset($courses_homeworks);
                unset($courses_lesson);
                
            endforeach;
        endif;
        $data['data'] = $arr_homeworks;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function print($course_id="", $user_id=""){

        $certificates           = $this->certificates_m->get_certificates_by_coursesid($course_id)->row();
        $data['detail']         = $certificates->detail; 
        $data['file']           = $certificates->file; 
        $infoCourses            = $this->courses_m->get_courses_by_id($course_id)->row();
        $data['course_title']   = $infoCourses->title;
        $infoUsers              = $this->users_model->get_user_by_id($user_id)->row();
        $data['fullname']       =  $infoUsers->fullname; 
        $data['dateTh']           = DateThai(date('d-m-Y'),'full','th'); //set date , get full name , digit
        $data['dateEn']           = DateThai(date('d-m-Y')); //set date , get full name , digit

		$html = $this->load->view("certificate",$data, true);
        //this the the PDF filename that user will get to download
		$pdfFilePath = getcwd()."/uploads/".$course_id."-".$user_id.".pdf";
		$this->load->library('m_pdf');
		$this->m_pdf->pdf->WriteHTML($html);
		$this->m_pdf->pdf->Output($pdfFilePath, "I");
    }
}
