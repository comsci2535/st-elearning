<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Courses_students_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function count_courses_students_by_user_id($user_id, $status = 0) 
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('status', $status);
        return $this->db->count_all_results('courses_students');
    }

    public function count_courses_students_by_course_id($course_id, $user_id, $status = 0) 
    {
        $this->db->where('course_id', $course_id);
        $this->db->where('user_id', $user_id);
        $this->db->where('status', $status);
        return $this->db->count_all_results('courses_students');
    }

    public function count_courses_studentsroom_by_course_id($course_id) 
    {
        $this->db->where('course_id', $course_id);
        return $this->db->count_all_results('courses_students');
    }

    public function get_courses_students_join_courses_by_id($param) 
    {

        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['search']['value'])
                    ->or_like('b.excerpt', $param['search']['value'])
                    ->or_like('b.detail', $param['search']['value'])
                    ->group_end();
        }

        
        $this->db->where('a.status', $param['status']);
        $this->db->where('a.user_id', $param['user_id']);
        $this->db->where('b.recycle', 0);
        $this->db->where('b.active', 1);
        $this->db->order_by('b.recommend', 'DESC');
        $this->db->order_by('b.created_at', 'DESC');
        $this->db->select('b.*');
        $this->db->join('courses b', 'a.course_id = b.course_id');
        $query = $this->db->get('courses_students a');
        return $query;
    }
     
    public function get_courses_students_join_orders_by_id($param) 
    {

        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        $this->db->where('a.status', $param['status']);
        $this->db->where('a.user_id', $param['user_id']);
        $this->db->where('b.recycle', 0);
        $this->db->order_by('b.created_at', 'DESC');
        $this->db->select('a.*,b.*');
        $this->db->join('orders b', 'a.order_id = b.order_id');
        $query = $this->db->get('courses_students a');
        return $query;
    }

    public function get_courses_students_join_promotions_by_id($user_id, $status=0) 
    {  
        $this->db->where('a.status', $status);
        $this->db->where('a.user_id', $user_id);
        $this->db->where('b.recycle', 0);
        $this->db->order_by('a.course_id', 'DESC');
        $this->db->select('a.*,b.*');
        $this->db->join('promotions b', 'a.promotion_id = b.promotion_id', 'left');
        $query = $this->db->get('courses_students a');
        return $query;
    }

    public function get_courses_students_join_users_by_id($param) 
    {

        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.fname', $param['search']['value'])
                    ->or_like('b.lname', $param['search']['value'])
                    ->group_end();
        }
       
        if($param['status'] != ''):
            $this->db->where('a.status', $param['status']);
        endif;
        if(!empty($param['user_id'])):
            $this->db->where('a.user_id', $param['user_id']);
        endif;

        if(!empty($param['course_id'])):
            $this->db->where('a.course_id', $param['course_id']);
        endif;
        
        $this->db->order_by('b.fname', 'DESC');
        $this->db->select('b.*,a.*');
        $this->db->join('users b', 'a.user_id = b.user_id');
        $query = $this->db->get('courses_students a');
        return $query;
    }

    public function count_courses_students_join_users_by_id($param) 
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.fname', $param['search']['value'])
                    ->or_like('b.lname', $param['search']['value'])
                    ->group_end();
        }

        if($param['status'] != ''):
            $this->db->where('a.status', $param['status']);
        endif;
        if(!empty($param['user_id'])):
            $this->db->where('a.user_id', $param['user_id']);
        endif;

        if(!empty($param['course_id'])):
            $this->db->where('a.course_id', $param['course_id']);
        endif;
        
        $this->db->where('b.recycle', 0);
        $this->db->where('b.active', 1);
        $this->db->order_by('b.recommend', 'DESC');
        $this->db->order_by('b.created_at', 'DESC');
        $this->db->select('b.*,a.*');
        $this->db->join('users b', 'a.user_id = b.user_id');
        return $this->db->count_all_results('courses_students a');
    }
}
