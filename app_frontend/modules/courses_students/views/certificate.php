<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>PDF Certificate</title>

	<style type="text/css">
	#container{
        width: 100%;
        height: 100%
	}
	</style>
</head>
<body>

<div id="container" style="background-image:url('<?php echo !empty($file) ? $this->config->item('root_url').$file :'';?>');background-position: center;background-repeat: no-repeat;background-size: cover;">
	<div>
		<?php if(!empty($detail)):

			$arr_data = array(
				'certificate.getCourseName()',
				'certificate.getStudentName()',
				'certificate.getDateTH()',
				'certificate.getDateEN()'
			);

			$arr_detail = array(
				$course_title,
				$fullname,
				$dateTh,
				$dateEn
			);

			$detail = str_replace($arr_data, $arr_detail, $detail);
			echo html_entity_decode($detail);
			
		endif;
		
		?>
	</div>
</div>
	

</body>
</html>
  
