<section class="Sec_Courses">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-4 mb-4">
                <br>
                <h4 class="pt-4">
                    <a href="<?=site_url('instructors/courses');?>">ข้อมูลนักเรียนทั้งหมด</a> > <span><?php echo !empty($breadcrumb)? $breadcrumb : '';?></span>
                </h4>
                <hr>
            </div>
            
            <div class="col-sm-12 mb-5">
                <div class="">
                    <input type="hidden" id="course_id" name="course_id" value="<?=$course_id?>">
                    <table id="data-list" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รูป</th>
                            <th>ชื่อนามสกุล</th>
                            <th>อีเมล</th>
                            <th>เบอร์โทรศัทพ์</th>
                            <th>วันที่ลงทะเบียน</th>
                            <th>สถานะ</th>
                            <th>จัดการ</th>
                        </tr>
                    </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div id="modal-show-lesson" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">รายละเอียดการดูวีดีโอ</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>ชื่อคอร์ส : <span id="text-courses-title"></span></h4>
                    </div>
                    <div class="col-sm-12">
                        <br>
                        <h4>เนื้อหา</h4>
                        <br>
                        <table id="table-text-lesson" class="table">
                            <thead>
                                <tr>
                                    <th style="width: 60%;">หัวข้อ</th>
                                    <th style="width: 40%;">สถานะการดูวีดีโอ</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิดหน้าต่าง</button>
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="modal-show-quiz" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">รายละเอียดการสอบ</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tab-quiz-1">สอบก่อนเรียน</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-quiz-2">สอบหลังเรียน</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-quiz-3">สอบ Final</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="tab-quiz-1" class="container tab-pane active"><br>
                                <div id="div-quiz-1" style="display:none">
                                    <h3>ได้คะแนน (<strong class="text-score-qty">0</strong> ข้อ ถูก <strong class="text-score">0</strong> ข้อ)</h3>
                                    <h4>หัวข้อแบบทดสอบ : <label class="text-courses"></label></h4>
                                    <table id="table-quiz-1" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>คำถาม</th>
                                                <th>คำตอบ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <h4 id="msg-quiz-1" class="text-center" style="display:none">ไม่มีข้อมูล</h4>
                            </div>
                            <div id="tab-quiz-2" class="container tab-pane fade"><br>
                                <div id="div-quiz-2" style="display:none">
                                    <h3>ได้คะแนน (<strong class="text-score-qty">0</strong> ข้อ ถูก <strong class="text-score">0</strong> ข้อ)</h3>
                                    <h4>หัวข้อแบบทดสอบ : <label class="text-courses"></label></h4>
                                    <table id="table-quiz-2" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>คำถาม</th>
                                                <th>คำตอบ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <h4 id="msg-quiz-2" class="text-center" style="display:none">ไม่มีข้อมูล</h4>
                            </div>
                            <div id="tab-quiz-3" class="container tab-pane fade"><br>
                                <div id="div-quiz-3" style="display:none">
                                    <h3>ได้คะแนน (<strong class="text-score-qty">0</strong> ข้อ ถูก <strong class="text-score">0</strong> ข้อ)</h3>
                                    <h4>หัวข้อแบบทดสอบ : <label class="text-courses"></label></h4>
                                    <table id="table-quiz-3" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>คำถาม</th>
                                                <th>คำตอบ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <h4 id="msg-quiz-3" class="text-center" style="display:none">ไม่มีข้อมูล</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิดหน้าต่าง</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modal-show-homeworks" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">การบ้าน</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="display-homeworks" class="col-md-12">
                        
                   </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิดหน้าต่าง</button>
            </div>
        </div>

    </div>
</div>