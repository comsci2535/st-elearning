<section class="Sec_Courses">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 pt-4">
                <h4 class="pt-4">
                    <a href="<?=site_url('exams/lists/'.$info->course_id);?>">ชื่อเนื้อหาทั้งหมด</a> > <span><?php echo !empty($breadcrumb)? $breadcrumb : '';?></span>
                </h4>
                <hr>
            </div>
           
            <div class="col-sm-12 mb-5">
                <div class="col-sm-12">
                    <form class="form-horizontal frm-create" method="post" action="<?=$frmAction?>" autocomplete="off" enctype="multipart/form-data">
                        
                        
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">หัวข้อ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" required>
                                <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="slug">ชื่อเนื้อหา <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug" required>
                                <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="excerpt">รายละเอียดย่อ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <textarea name="excerpt" rows="3" class="form-control summernote" id="excerpt" ><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="examTime">เวลาในการทำข้อสอบ <span class="text-danger"> *</span></label>
                            <div class="col-sm-3"> 
                                <input value="<?php echo isset($info->examTime) ? $info->examTime : NULL ?>" type="number" id="input-examTime" class="form-control" name="examTime" required>
                            </div>
                            <div class="col-sm-3" style="text-align: left;">
                                (นาที)
                            </div>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label class="control-label col-sm-3" for="examNo">จำนวนครั้งที่สอบ (ไม่เกิน) <span class="text-danger"> *</span></label>
                            <div class="col-sm-3"> 
                                <input value="<?php echo isset($info->examNo) ? $info->examNo : 1 ?>" type="number" id="input-examNo" class="form-control" name="examNo" required>
                            </div>
                            <div class="col-sm-3" style="text-align: left;">
                                (ครั้ง)
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="answerRandom">สุ่มข้อ <span class="text-danger"> *</span></label>
                            <div class="col-sm-7"> 
                                <label class="icheck-inline"><input type="radio" name="answerRandom" value="1" class="icheck" <?php if(isset($info->answerRandom) && $info->answerRandom=="1"){?> checked <?php } ?>/> สุ่ม</label>
                                <label class="icheck-inline"><input type="radio" name="answerRandom" value="0" class="icheck" <?php if(isset($info->answerRandom) && $info->answerRandom=="0"){?> checked <?php } ?>/> ไม่สุ่ม</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="examNumber">จำนวนข้อที่ออกสอบ <span class="text-danger"> *</span></label>
                            <div class="col-sm-7"> 
                                <input value="<?php echo isset($info->examNumber) ? $info->examNumber : NULL ?>" type="text" id="input-examNumber" class="form-control" name="examNumber" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pass">ผ่าน <span class="text-danger"> *</span></label>
                            <div class="col-sm-3"> 
                                <input value="<?php echo isset($info->pass) ? $info->pass : NULL ?>" type="number" id="input-pass" class="form-control" name="pass" required>
                            </div>
                            <div class="col-sm-3" style="text-align: left;">
                                (%)
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="dateRange">กำหนดวันสอบ <span class="text-danger"> *</span></label>
                            <div class="col-sm-5"> 
                                <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>" />
                                <input type="hidden" name="startDate" value="<?php echo isset($info->startDate) ? $info->startDate : NULL ?>" />
                                <input type="hidden" name="endDate" value="<?php echo isset($info->endDate) ? $info->endDate : NULL ?>" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">สถานะ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <label><input <?php if(isset($info->active) && $info->active== 1){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="1" checked> เปิด</label>
                                <label><input <?php if(isset($info->active) && $info->active== 0){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
                            </div>
                        </div>
                       
                        <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="course_id" id="input-course_id" value="<?php echo $course_id ?>">
                                <input type="hidden" class="form-control" name="db" id="db" value="exam">
                                <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                                <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->exam_id) ? encode_id($info->exam_id) : 0 ?>">
                                <button type="submit" class="btn btn-success">บันทึก</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->fileUrl)) ? base_url().$info->fileUrl : ''; ?>';
    var file_id         = '<?=(isset($info->course_lesson_id)) ? $info->course_lesson_id : ''; ?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/'+file_id;

</script>