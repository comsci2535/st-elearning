<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Courses_instructors_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function count_courses_instructors_by_user_id($user_id) 
    {
        $this->db->where('user_id', $user_id);
        return $this->db->count_all_results('courses_instructors');
    }

    public function count_courses_instructors_by_course_id($course_id, $user_id) 
    {
        $this->db->where('course_id', $course_id);
        $this->db->where('user_id', $user_id);
        return $this->db->count_all_results('courses_instructors');
    }

    public function get_courses_instructors_join_courses_by_id($param) 
    {

        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('b.title', $param['search']);
            $this->db->or_like('b.excerpt', $param['search']);
            $this->db->or_like('b.detail', $param['search']);
        endif;

        $this->db->where('a.user_id', $param['user_id']);
        $this->db->where('b.recycle', 0);
        $this->db->where('b.active', 1);
        $this->db->order_by('b.recommend', 'DESC');
        $this->db->order_by('b.created_at', 'DESC');
        $this->db->select('b.*');
        $this->db->join('courses b', 'a.course_id = b.course_id');
        $query = $this->db->get('courses_instructors a');
        return $query;
    }

    public function get_courses_instructors_join_user_by_course_id($course_id) 
    {
        $this->db->where('a.course_id', $course_id);
        $this->db->where('b.recycle', 0);
        $this->db->where('b.active', 1);
        $this->db->where('c.private', 0);
        $this->db->select('b.*');
        $this->db->join('users b', 'a.user_id = b.user_id');
        $this->db->join('instructors c', 'c.user_id = b.user_id');
        $query = $this->db->get('courses_instructors a');
        return $query;
    }

    public function get_courses_instructors_join_user_id_by_course_id($course_id, $user_id) 
    {
        $this->db->where('a.course_id', $course_id);
        $this->db->where('a.user_id', $user_id);
        $this->db->where('b.recycle', 0);
        $this->db->where('b.active', 1);
        $this->db->where('c.private', 0);
        $this->db->select('b.*');
        $this->db->join('users b', 'a.user_id = b.user_id');
        $this->db->join('instructors c', 'c.user_id = b.user_id');
        $query = $this->db->get('courses_instructors a');
        return $query;
    }
     
}
