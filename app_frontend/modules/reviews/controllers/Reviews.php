<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reviews extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('reviews_m');
    }

    public function index()
    {
        # code...
    }
    
    public function reviews($page='')
    {
        $input['course_id']= $page;
        // get banners
		$info_reviews  = $this->reviews_m->get_rows($input);
		$data['reviews'] = $info_reviews->result();
        
        $this->load->view('reviews', $data); 
    }

    public function happy_students()
    {
        $input['active']='1';
        $input['recycle']='0';
        $input['length']  = 10;
		$input['start']	  = 0;
        // get banners
		$info  = $this->reviews_m->get_reviews_rows($input);
		$data['info'] = $info->result();
        $this->load->view('happy_students', $data); 
    }
    
    public function reviews_stars_save() 
    {
        $input              = $this->input->post();
        $value['course_id'] = $input['course_id'];
        $value['score']     = $input['score'];
        $value['comment']   = $input['comment'];
        $value['user_id']   = $this->session->users['UID'];
        $value['create_at'] = db_datetime_now();
        $value['update_at'] = db_datetime_now();
        $info=$this->reviews_m->reviews_stars_save($input['course_id'],$this->session->users['UID'],$value);
        
        $status = 0;
        if ($info) {
            $status = 1;
        }
        $data['status'] = $status;

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function get_reviews_stars($course_id="") 
    {
        $input=$this->input->post();
       
        if($course_id!=""){
            $course_id=$course_id;
        }else{
            $course_id=$input['course_id'];
        }
        $info_arr=$this->reviews_m->get_reviews_stars($course_id)->result_array();
        $total=count($info_arr);
        $txt['stars_result_user']="<br>";
        $average=array();
        $v1=0;
        $v2=0;
        $v3=0;
        $v4=0;
        $v5=0;
        if ( !empty($info_arr) ) {
            foreach ($info_arr as $key => $info) {
                $average[]=$info['score'];
                $image = base_url('images/user.png');
                
                $sc1="";
                $sc2="";
                $sc3="";
                $sc4="";
                $sc5="";

                if($info['score']=="1"){
                   $sc1="checked"; 
                   $v1++;

                    $stars_result='<li class="active"><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>';
                }
                if($info['score']=="2"){
                   $sc2="checked"; 
                   $v2++;

                    $stars_result='<li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>';
                }
                if($info['score']=="3"){
                   $sc3="checked"; 
                   $v3++;

                    $stars_result='<li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>';
                }
                if($info['score']=="4"){
                   $sc4="checked"; 
                   $v4++;

                    $stars_result='<li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <span class="fa fa-star"></span>';
                }
                if($info['score']=="5"){
                   $sc5="checked";
                   $v5++; 

                    $stars_result='<li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>';
                }

                

                $txt['stars_result_user'].='<section id="section-team" class="topmargin-lg">
                    <div class="row">
                        <div class="col-sm-2 col-md-2 col-4 text-center">
                            <img src="'.$image.'" alt="" style="width:40px;">
                        </div>
                        <div class="all-review col-sm-4 col-md-3 col-8">
                        <ul class="cours-star">
                            '.$stars_result.'
                        </ul>
                        </div>
                        <div class="col-sm-6 col-md-7 col-12">
                          <div class="c-name"> by '.$info['fullname'].'</div><div class="c-comment">'.$info['comment'].'</div>
                        </div>
                    </div>
                    </section>';
             } 
        }
        if(!empty($average)){
            $txt['v']=$this->average($average);
        }else{
            $txt['v']='0';
        }

        if($txt['v'] < "2" && $txt['v'] >= "1" ){
                  
                    $txt['star_amount']='<ul class="cours-star"><li class="active"><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span></ul><span>'.$total.' Rating</span>';
        }
        if($txt['v'] < "3" && $txt['v'] >= "2" ){
         
            $txt['star_amount']='<ul class="cours-star"><li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span></ul><span>'.$total.' Rating</span>';
        }
        if($txt['v'] < "4" && $txt['v'] >= "3" ){
         
            $txt['star_amount']='<ul class="cours-star"><li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span></ul><span>'.$total.' Rating</span>';
        }
        if($txt['v'] < "5" && $txt['v'] >= "4" ){
          
            $txt['star_amount']='<ul class="cours-star"><li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <span class="fa fa-star"></span></ul><span>'.$total.' Rating</span>';
        }
        if($txt['v'] >= "5" ){
          
             $txt['star_amount']='<ul class="cours-star"><li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li>
                                    <li class="active"><i class="fa fa-star"></i></li></ul><span>'.$total.' Rating</span>';
        }
        
        $txt['v1']=$v1;
        $txt['v2']=$v2;
        $txt['v3']=$v3;
        $txt['v4']=$v4;
        $txt['v5']=$v5;

        if($total<=0){
            $txt['progress1']='<div class="progress-bar progress-bar-yellow" style="width: 0%"></div>';
            $txt['progress2']='<div class="progress-bar progress-bar-yellow" style="width: 0%"></div>';
            $txt['progress3']='<div class="progress-bar progress-bar-yellow" style="width: 0%"></div>';
            $txt['progress4']='<div class="progress-bar progress-bar-yellow" style="width: 0%"></div>';
            $txt['progress5']='<div class="progress-bar progress-bar-yellow" style="width: 0%"></div>';

            $txt['percen1']='<div></div>';
            $txt['percen2']='<div></div>';
            $txt['percen3']='<div></div>';
            $txt['percen4']='<div></div>';
            $txt['percen5']='<div></div>';

        }else{
             $txt['progress1']='<div class="progress-bar progress-bar-yellow" style="width: '.number_format(($v1*100/$total),2).'%"></div>';
            $txt['progress2']='<div class="progress-bar progress-bar-yellow" style="width: '.number_format(($v2*100/$total),2).'%"></div>';
            $txt['progress3']='<div class="progress-bar progress-bar-yellow" style="width: '.number_format(($v3*100/$total),2).'%"></div>';
            $txt['progress4']='<div class="progress-bar progress-bar-yellow" style="width: '.number_format(($v4*100/$total),2).'%"></div>';
            $txt['progress5']='<div class="progress-bar progress-bar-yellow" style="width: '.number_format(($v5*100/$total),2).'%"></div>';

            $txt['percen1']='<div> '.number_format(($v1*100/$total),2).'%</div>';
            $txt['percen2']='<div> '.number_format(($v2*100/$total),2).'%</div>';
            $txt['percen3']='<div> '.number_format(($v3*100/$total),2).'%</div>';
            $txt['percen4']='<div> '.number_format(($v4*100/$total),2).'%</div>';
            $txt['percen5']='<div>'.number_format(($v5*100/$total),2).'%</div>';

        }
       
        
        //return $txt;
        echo json_encode($txt);
         

    }

    public function reviews_stars_home($course_id)
    {
       
        $info_r=$this->reviews_m->get_reviews_stars($course_id)->result_array();
        $total=count($info_r);
        $average=array();
        if ( !empty($info_r) ) {
            foreach ($info_r as $key => $info) {
                $average[]=$info['score'];
            }
        }

        if(!empty($average)){
            $v=$this->average($average);
        }else{
            $v='0';
        }

        if($v < "1" ){
                  
                    $t='<ul class="cours-star"><li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span> <span>('.$v.')</span></ul>';
        }

        if($v < "2" && $v >= "1" ){
                  
                    $t='<ul class="cours-star"><li class="active"><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span> <span>('.$v.')</span></ul>';
        }
        if($v < "3" && $v >= "2" ){
         
            $t='<ul class="cours-star"><li class="active"><i class="fa fa-star"></i></li>
                <li class="active"><i class="fa fa-star"></i></li>
                <li><i class="fa fa-star"></i></li>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span> <span>('.$v.')</span></ul>';
        }
        if($v < "4" && $v >= "3" ){
         
            $t='<ul class="cours-star"><li class="active"><i class="fa fa-star"></i></li>
                    <li class="active"><i class="fa fa-star"></i></li>
                    <li class="active"><i class="fa fa-star"></i></li>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span> <span>('.$v.')</span></ul>';
        }
        if($v < "5" && $v >= "4" ){
          
            $t='<ul class="cours-star"><li class="active"><i class="fa fa-star"></i></li>
                    <li class="active"><i class="fa fa-star"></i></li>
                    <li class="active"><i class="fa fa-star"></i></li>
                    <li class="active"><i class="fa fa-star"></i></li>
                    <span class="fa fa-star"></span> <span>('.$v.')</span></ul>';
        }
        if($v >= "5" ){
          
             $t='<ul class="cours-star"><li class="active"><i class="fa fa-star"></i></li>
                    <li class="active"><i class="fa fa-star"></i></li>
                    <li class="active"><i class="fa fa-star"></i></li>
                    <li class="active"><i class="fa fa-star"></i></li>
                    <li class="active"><i class="fa fa-star"></i></li> <span>('.$v.')</span></ul>';
        }

        return $t;
       
    }

    public function average($arr)
    {
        $array_size = count($arr);

        $total = 0;
        for ($i = 0; $i < $array_size; $i++) {
            $total += $arr[$i];
        }

        $average = (float)($total / $array_size);
        return number_format($average,1);
    }

    public function reviews_stars_courses($course_id = '')
    {
        
        $data['info'] = $this->reviews_m->get_reviews_stars($course_id)->result();
        $this->load->view('reviews_courses', $data); 
        
    }

    public function ajax_reviews_stars() 
    {
        $input      = $this->input->post();
        $course_id  = $input['course_id'];
        $info_arr   = $this->reviews_m->get_reviews_stars($course_id)->result_array();
        $total      = count($info_arr);
        $average    = array();
        $v1=0;
        $v2=0;
        $v3=0;
        $v4=0;
        $v5=0;
        if (!empty($info_arr) ) {
            foreach ($info_arr as $key => $info) {
                $average[] = $info['score'];

                switch($info['score']):
                    case 1:
                        $v1++;
                    break;
                    case 2:
                        $v2++;
                    break;
                    case 3:
                        $v3++;
                    break;
                    case 4:
                        $v4++;
                    break;
                    case 5:
                        $v5++;
                    break;
                endswitch;
             } 
        }
        if(!empty($average)){
            $data['v'] = $this->average($average);
        }else{
            $data['v'] = 0;
        }
        
        $data['percen1']= !empty($total)? number_format(($v1*100/$total)) : 0;
        $data['percen2']= !empty($total)? number_format(($v2*100/$total)) : 0;
        $data['percen3']= !empty($total)? number_format(($v3*100/$total)) : 0;
        $data['percen4']= !empty($total)? number_format(($v4*100/$total)) : 0;
        $data['percen5']= !empty($total)? number_format(($v5*100/$total)) : 0;
        
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));

    }
}
