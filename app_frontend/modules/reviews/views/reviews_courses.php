<div class="Bold my-4" id="scoll4">
    <h2>ความคิดเห็นผู้เรียน</h2>
</div>
<div class="courses-review">
    <div class="all-review">
        <h2 class="rating-type Bold" id="p-value">0</h2>
        <div id="star_amount">
            <ul class="cours-star">
                <li class="active"><i class="fa fa-star"></i></li>
                <li class="active"><i class="fa fa-star"></i></li>
                <li class=""><i class="fa fa-star"></i></li>
                <li class=""><i class="fa fa-star"></i></li>
                <li class=""><i class="fa fa-star"></i></li>
            </ul>
        </div>
        <p>คะแนนหลักสูตร</p>
    </div>
    <div class="review-bar">
        <div class="bar-bx">
            <div class="side">
                <div>5 ดาว</div>
            </div>
            <div class="progress" id="progress-5">
                <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
            </div>
            <div class="percen" id="percen-5">
                <div>0%</div>
            </div>
        </div>
        <div class="bar-bx">
            <div class="side">
                <div>4 ดาว</div>
            </div>
            <div class="progress" id="progress-4">
                <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
            </div>
            <div class="percen" id="percen-4">
                <div>0%</div>
            </div>
        </div>
        <div class="bar-bx">
            <div class="side">
                <div>3 ดาว</div>
            </div>
            <div class="progress" id="progress-3">
                <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
            </div>
            <div class="percen" id="percen-3">
                <div>0%</div>
            </div>
        </div>
        <div class="bar-bx">
            <div class="side">
                <div>2 ดาว</div>
            </div>
            <div class="progress" id="progress-2">
                <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
            </div>
            <div class="percen" id="percen-2">
                <div>0%</div>
            </div>
        </div>
        <div class="bar-bx">
            <div class="side">
                <div>1 ดาว</div>
            </div>
            <div class="progress" id="progress-1">
                <div class="progress-bar progress-bar-yellow" style="width: 0%"></div>
            </div>
            <div class="percen" id="percen-1">
                <div>0%</div>
            </div>
        </div>
    </div>
</div>
 <?php if(!empty($this->session->users['UID'])){ ?>
<div class="Bold my-4">
    <hr>
    <div class="user-review-stars-container">
        <div class="user-review-stars"  >
            <fieldset class="rating">
                <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="gorgeous"></label>
                <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="good"></label>
                <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="regular"></label>
                <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="poor"></label>
                <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="bad"></label>
            </fieldset>
        <span style="margin: 8px;font-size: 22px;">กดเพื่อให้คะแนน</span>
        </div>
    </div>
    <div class="review-form text-right">
        <input type="hidden" id="review-form-title" class="form-control" placeholder="" value="">
        <textarea id="review-form-comment" class="form-control" rows="3" cols="83" placeholder="แสดงความคิดเห็น"></textarea>
        <div class="review-form-btns" style="padding-top: 10px;">
            <button id="review-form-submit-btn" class="btn-smal btn-flat btn-register btn btn-info">ตกลง</button>
            <button id="review-form-cancel-btn" class="btn-smal btn-flat btn-default btn btn-danger">ยกเลิก</button>
        </div>
    </div>
    <hr>
</div>
<?php } ?>


<style>
    .cours-star {
        margin: 0;
        padding: 0;
        list-style-position: outside;
        font-size: 20px;
        color: #ACACAC;
    }
    .cours-star li.active{
        color: #ffa500;
    }
    .cours-star li{
        display: inline-block;
        list-style: none;
    }
</style>
<?php
if(!empty($info)): ?>
<div class="Bold my-4" id="scoll5">
    <h2>รีวิวคอร์สเรียน</h2>
</div>
<?php
    foreach($info as $key => $item):
?>
<div class="courses-teaher2">
    <div class="img">
        <img src="<?=base_url($item->file)?>" class="ImgFluid" alt="คอสเรียน" onerror="this.src='<?php echo base_url("images/Great_Teacher.jpg"); ?>'">
    </div>
    
    <div class="name Bold"><?php echo !empty($item->fullname)? $item->fullname: '';?>
        <ul class="cours-star">
            <?php
            for($i = 1; $i <= 5; $i++):
                $active = '';
                if($i <= $item->score):
                    $active = 'active';
                endif;
            ?>
                <li class="<?=$active?>"><i class="fa fa-star"></i></li>
            <?php
            endfor;
            ?>
        </ul>
    </div>
    <div class="text">
        <div class="pp">
            <div id="text-row-comment-<?=$key?>" class="p">
                <p>
                    <?php echo !empty($item->comment)? $item->comment: '';?>
                </p>
            </div>
            <a href="javascript:void(0)" id="overlate" class="overlate-hide" data-line="text-row-comment-<?=$key?>" data-hide="0">
                <i class="fas fa-plus"></i> ดูเพิ่มเติม
            </a>
        </div>
    </div>
</div>
<hr>
<?php
endforeach;

?>
<div class="text-center mb-5">
    <a href="javascript:void(0)" class="btn-custom btn-click-load-page" data-page="0">ดูรีวิวเพิ่มเติม</a>
</div>
<?php endif; ?>