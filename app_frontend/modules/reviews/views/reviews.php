
<div id="Reviews">
    <div class="title">
        <h2>รีวิวคอร์สเรียน</h2>
    </div>
    <div class="review-bx">
        <div class="all-review">
            <h2 class="rating-type" id="p-value">3</h2>
            <div  id="star_amount">
               
            </div>
            
        </div>
        <div class="review-bar">
            
            <div class="bar-bx">
                <div class="side">
                    <div>5 star</div>
                </div>
                <div class="progress" id="progress-5">
                    
                </div>
                <div class="percen" id="percen-5">
                    
                </div>
            </div>
            <div class="bar-bx">
                <div class="side">
                    <div>4 star</div>
                </div>
                <div class="progress" id="progress-4">
                    
                </div>
                <div class="percen" id="percen-4">
                    
                </div>
            </div>
            <div class="bar-bx">
                <div class="side">
                    <div>3 star</div>
                </div>
                <div class="progress" id="progress-3">
                    
                </div>
                <div class="percen" id="percen-3">
                    
                </div>
            </div>
            <div class="bar-bx">
                <div class="side">
                    <div>2 star</div>
                </div>
                <div class="progress" id="progress-2">
                    
                </div>
                <div class="percen" id="percen-2">
                    
                </div>
            </div>
            <div class="bar-bx">
                <div class="side">
                    <div>1 star</div>
                </div>
                <div class="progress" id="progress-1">
                    
                </div>
                <div class="percen" id="percen-1">
                    
                </div>
            </div>
            
        </div>
    </div>
    <?php if(!empty($this->session->users['UID'])){ ?>
    <div class="review-bx">
        <div class="col-lg-12">
            <div class="user-review">
                <div class="divider"></div>
                <div class="user-review-stars-container">
                    <div class="user-review-stars"  >
                        <fieldset class="rating">
                            <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="gorgeous"></label>
                            <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="good"></label>
                            <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="regular"></label>
                            <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="poor"></label>
                            <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="bad"></label>
                        </fieldset>
                    <span style="margin: 8px;font-size: 22px;">กดเพื่อให้คะแนน</span>
                    </div>
                </div>
            </div>
            <div class="review-form text-right"> 
                <input type="hidden" id="review-form-title" class="form-control" placeholder="" value="">
                <textarea id="review-form-comment" class="form-control" rows="3" cols="83" placeholder="แสดงความคิดเห็น"></textarea>
                <div class="review-form-btns" style="padding-top: 10px;">
                  <button id="review-form-submit-btn" class="btn-smal btn-flat btn-register btn btn-info">ตกลง</button>
                  <button id="review-form-cancel-btn" class="btn-smal btn-flat btn-default btn btn-danger">ยกเลิก</button>
                </div>
            </div>
            <div id="reviews_stars_result"></div>
        </div>
    </div>
    <?php } ?>
</div>
