<div class="Happy_Students">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-5">
                <div class="title">
                    <h1>Happy Students</h1>
                </div>
            </div>
        </div>
        <div id="Happy_Students" class="owl-carousel owl-theme">
            <?php
            if(!empty($info)):
                foreach($info as $item):
            ?>
            <div class="item wow fadeIn" data-wow-delay="0.1s">
                <div class="CoursesItem">
                    <div class="detail-text">
                        <div class="user">
                            <div class="">
                                <div class="left">
                                    <img src="<?php echo !empty($item->file)? base_url().$item->file : '';?>" class="ImgFluid" alt="<?php echo !empty($item->title)? $item->title : '';?>" onerror="this.src='<?php echo base_url("images/user.png"); ?>'">
                                </div>
                                <div class="right">
                                    <h5><?php echo !empty($item->title)? $item->title : '';?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="detail">
                            <p><?php echo !empty($item->excerpt)? $item->excerpt : '';?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                endforeach;
            endif;
            ?>
        </div>
    </div>
</div>