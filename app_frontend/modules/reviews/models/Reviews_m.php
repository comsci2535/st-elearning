<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reviews_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param){
        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);
             
        $query = $this->db
                        ->select('a.*')
                        ->from('reviews_stars a')
                        ->get();
        return $query;
    }

    public function get_reviews_rows($param){
        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);

        if ( isset($param['review_id']) )
            $this->db->where('a.review_id', $param['review_id']);
        
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
       
        if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']); 
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
    
        $this->db->order_by('a.created_at', 'DESC');    

        $query = $this->db
                        ->select('a.*')
                        ->from('reviews a')
                        ->get();
        return $query;
    }

    public function get_reviews_stars($course_id) 
    {
        
        $this->db->where('a.course_id', $course_id);
         $this->db->order_by('a.create_at', 'DESC');
        $query = $this->db
                        ->select('a.*,b.fullname,b.file')
                        ->from('reviews_stars a')
                        ->join('users b', 'a.user_id = b.user_id', 'left')
                        ->get();
        return $query;
    }

    public function reviews_stars_save($course_id,$user_id,$value) 
    {
         $this->db
                ->where('user_id', $user_id)
                ->where('course_id', $course_id)
                ->delete('reviews_stars');

        return $this->db->insert('reviews_stars', $value);

    }

    // new function query

    public function cont_reviews_stars_by_course_id($course_id)
    {
        $this->db->where('course_id', $course_id);
        return $this->db->count_all_results('reviews_stars');
    }

    public function get_reviews_stars_by_course_id($course_id)
    {
        $this->db->where('a.course_id', $course_id);
        $this->db->select('a.*');     
        $query = $this->db->get('reviews_stars a');
        return $query;
    }

    public function get_reviews_by_course_id($course_id, $review_id, $length = 10, $start = 0)
    {
        if(!empty($course_id)):
            $this->db->where('a.course_id', $course_id);
        endif;
        if(!empty($review_id)):
            $this->db->where('a.review_id', $review_id);
        endif;
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1); 
        
        if ( isset($length) ) 
            $this->db->limit($length, $start);

        $this->db->order_by('a.created_at', 'DESC');    
        $this->db->select('a.*');     
        $query = $this->db->get('reviews a');
        return $query;
    }
}
