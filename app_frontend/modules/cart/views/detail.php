<section class="Sec_Courses_Detail">
    <div class="container-fluid">
        <div class="row">
            <div id="slide_banner" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="<?=base_url('images/slide_courses.JPG');?>" class="ImgFluid" alt="">
                </div>
            </div>
        </div>
    </div>

    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-5 mb-4">
                <ul class="nav nav-url">
                    <li class="nav-item">
                        <a class="nav-link" href="#">หน้าแรก</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">คอร์สเรียน</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">ทั้งหมด</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 mb-4 blog-left">
                <div class="course-detail-bx">
                    <div class="course-price">
                        <del>$190</del>
                        <h4 class="price">$120</h4>
                    </div>
                    <div class="text-center">
                        <a href="#" class="btn btn-custom">Buy Now This Courses</a>
                    </div>
                    <div class="teacher-bx">
                        <div class="teacher-info">
                            <div class="teacher-thumb">
                                <img src="<?=base_url('images');?>/detail/pic1.jpg" alt="">
                            </div>
                            <div class="teacher-name">
                                <h5>Hinata Hyuga</h5>
                                <span>Science Teacher</span>
                            </div>
                        </div>
                    </div>
                    <div class="cours-more-info">
                        <div class="review">
                            <span>3 Review</span>
                            <ul class="cours-star">
                                <li class="active"><i class="fa fa-star"></i></li>
                                <li class="active"><i class="fa fa-star"></i></li>
                                <li class="active"><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                        </div>
                        <div class="price categories">
                            <span>Categories</span>
                            <h5>Frontend</h5>
                        </div>
                    </div>
                    <div class="course-info-list scroll-page scroller">
                        <ul class="navbar">
                            <li><a class="nav-link active" href="#Overview"><i class="far fa-file-alt"></i> Overview</a>
                            </li>
                            <li><a class="nav-link" href="#Curriculum"><i class="far fa-file-alt"></i> Curriculum</a>
                            </li>
                            <li><a class="nav-link" href="#Instructor"><i class="far fa-file-alt"></i> Instructor</a>
                            </li>
                            <li><a class="nav-link" href="#reviews"><i class="far fa-file-alt"></i> Reviews</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-9 blog-right">
                <div id="Textdetail">
                    <img src="<?=base_url('images');?>/detail/thum1.jpg" class="ImgFluid" alt="">
                    <br><br>
                    <h1>Nvidia and UE4 Technologies Practice</h1>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book.
                    </p>
                    <br>
                </div>

                <div id="Overview" class="row">
                    <div class="col-sm-5">
                        <div class="table-left">
                            <div class="title">
                                <h2>Overview</h2>
                            </div>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><i class="far fa-file-alt"></i> John</td>
                                        <td>john@example.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="far fa-file-alt"></i> Mary</td>
                                        <td>mary@example.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="far fa-file-alt"></i> July</td>
                                        <td>july@example.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="far fa-file-alt"></i> July</td>
                                        <td>july@example.com</td>
                                    </tr>
                                    <tr>
                                        <td><i class="far fa-file-alt"></i> July</td>
                                        <td>july@example.com</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <h2>Course Description</h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p><br><br>
                        <h2>Course Description</h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p><br><br>
                        <h2>Course Description</h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p>
                    </div>
                </div>

                <div id="Curriculum">
                    <div class="title">
                        <h2>Curriculum</h2>
                    </div>
                    <ul class="curriculum-list">
                        <li>
                            <h4>First Level</h4>
                            <ul>
                                <li>
                                    <div class="curriculum-list-box">
                                        <span>Lesson 1.</span> Introduction to UI Design
                                    </div>
                                    <span>120 minutes</span>
                                </li>
                                <li>
                                    <div class="curriculum-list-box">
                                        <span>Lesson 2.</span> User Research and Design
                                    </div>
                                    <span>60 minutes</span>
                                </li>
                                <li>
                                    <div class="curriculum-list-box">
                                        <span>Lesson 3.</span> Evaluating User Interfaces Part 1
                                    </div>
                                    <span>85 minutes</span>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h4>Second Level</h4>
                            <ul>
                                <li>
                                    <div class="curriculum-list-box">
                                        <span>Lesson 1.</span> Prototyping and Design
                                    </div>
                                    <span>110 minutes</span>
                                </li>
                                <li>
                                    <div class="curriculum-list-box">
                                        <span>Lesson 2.</span> UI Design Capstone
                                    </div>
                                    <span>120 minutes</span>
                                </li>
                                <li>
                                    <div class="curriculum-list-box">
                                        <span>Lesson 3.</span> Evaluating User Interfaces Part 2
                                    </div>
                                    <span>120 minutes</span>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h4>Final</h4>
                            <ul>
                                <li>
                                    <div class="curriculum-list-box">
                                        <span>Part 1.</span> Final Test
                                    </div>
                                    <span>120 minutes</span>
                                </li>
                                <li>
                                    <div class="curriculum-list-box">
                                        <span>Part 2.</span> Online Test
                                    </div>
                                    <span>120 minutes</span>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div id="Instructor">
                    <div class="title">
                        <h2>Instructor</h2>
                    </div>
                    <div class="instructor-bx">
                        <div class="instructor-author">
                            <img src="<?=base_url('images');?>/detail/pic1.jpg" alt="">
                        </div>
                        <div class="instructor-info">
                            <h6>Keny White </h6>
                            <span>Professor</span>
                            <ul class="list-inline m-tb10">
                                <li><a href="#" class="btn sharp-sm facebook"><i class="far fa-file-alt"></i></a></li>
                                <li><a href="#" class="btn sharp-sm twitter"><i class="far fa-file-alt"></i></a></li>
                                <li><a href="#" class="btn sharp-sm linkedin"><i class="far fa-file-alt"></i></a></li>
                                <li><a href="#" class="btn sharp-sm google-plus"><i class="far fa-file-alt"></i></a>
                                </li>
                            </ul>
                            <p class="m-b0">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                unknown printer took a galley of type and scrambled it to make a type specimen book. It
                                has survived not only five centuries</p>
                        </div>
                    </div>
                    <div class="instructor-bx">
                        <div class="instructor-author">
                            <img src="<?=base_url('images');?>/detail/pic2.jpg" alt="">
                        </div>
                        <div class="instructor-info">
                            <h6>Keny White </h6>
                            <span>Professor</span>
                            <ul class="list-inline m-tb10">
                                <li><a href="#" class="btn sharp-sm facebook"><i class="far fa-file-alt"></i></a></li>
                                <li><a href="#" class="btn sharp-sm twitter"><i class="far fa-file-alt"></i></a></li>
                                <li><a href="#" class="btn sharp-sm linkedin"><i class="far fa-file-alt"></i></a></li>
                                <li><a href="#" class="btn sharp-sm google-plus"><i class="far fa-file-alt"></i></a>
                                </li>
                            </ul>
                            <p class="m-b0">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                unknown printer took a galley of type and scrambled it to make a type specimen book. It
                                has survived not only five centuries</p>
                        </div>
                    </div>
                </div>

                <div id="Reviews">
                    <div class="title">
                        <h2>Reviews</h2>
                    </div>
                    <div class="review-bx">
                        <div class="all-review">
                            <h2 class="rating-type">3</h2>
                            <ul class="cours-star">
                                <li class="active"><i class="fa fa-star"></i></li>
                                <li class="active"><i class="fa fa-star"></i></li>
                                <li class="active"><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                            <span>3 Rating</span>
                        </div>
                        <div class="review-bar">
                            <?php for ($i=0; $i < 4; $i++) { ?>
                            <div class="bar-bx">
                                <div class="side">
                                    <div>4 star</div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" style="width:70%"></div>
                                </div>
                                <div class="percen">
                                    <div>70 %</div>
                                </div>
                            </div>
                            <?php  }  ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- container -->
</section>