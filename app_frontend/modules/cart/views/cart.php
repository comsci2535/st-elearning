<section class="seccart">


    <div class="container">
        <div class="background-image">
            <div class="title Bold">
                <h1>สรุปการสั่งซื้อ</h1>
            </div>
            <?php if(!empty($item)){ ?>
            <div class="row mb-5">
                <div class="col-sm-8">
                    <div class="Table-Mycourse">
                        <table class="table table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>รายการคอร์ส</th>
                                    <th>ราคา</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td> <?php foreach ($item as $key => $value) { ?>
                                        <div class="CoursesItem-list">
                                            <input type="hidden" class="form-control course_id-a" name="course_id"
                                                id="course_id"
                                                value="<?php echo !empty($value->course_id)? $value->course_id:''?>">
                                            <a href="javascript:void(0);" id="btn-a-delete"
                                                data-id="<?php echo !empty($value->course_id)? $value->course_id:''?>"
                                                class="close">X</a>
                                            <div class="img">
                                                <img src="<?=base_url($value->file);?>" class="ImgFluid imgBig"
                                                    alt="คอสเรียน"
                                                    onerror="this.src='<?=base_url('images/cose.jpg')?>'">
                                            </div>
                                            <div class="detail-text">
                                                <div class="name Bold">
                                                    <p><?=$value->title;?></p>
                                                </div>
                                                <?php $instructors=Modules::run('instructors/get_instructor_by_courses', $value->course_id);  ?>
                                                <?php if(!empty($instructors)){ 
                                                foreach ($instructors as $key => $rs) {
                                                if(!empty($rs->file)){
                                                    $img_intsr=base_url($rs->file);
                                                }else{
                                                    $img_intsr=$rs->oauth_picture;
                                                }
                                                ?>
                                                <div class="user">
                                                    <div class="left">
                                                        <img src="<?php echo $img_intsr;?>" class="ImgFluid"
                                                            alt="คอสเรียน"
                                                            onerror="this.src='<?=base_url('images/cose.jpg')?>'">
                                                    </div>
                                                    <div class="right">
                                                        <p>โดยคุณครู <?=$rs->fullname?></p>
                                                    </div>
                                                </div>
                                                <?php } } ?>
                                            </div>
                                            <hr>
                                            <div class="text-center" style="width: 100%"><span
                                                    id="coupon-error-div"></span></div>
                                        </div>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <div class="price">
                                            <?php if(!empty($value->promotion) && $value->promotion['discount']=='0' && $value->promotion['type']=='2'){ ?>
                                            <div class="row">
                                                <div class="col-sm-3 col-12">
                                                    ใส่ราคา
                                                </div>
                                                <div class="col-sm-9 col-12">

                                                    <input type="text" name="price-a" id="price-a" value=""
                                                        class="amount"> บาท
                                                    <div class="alert_price-a"></div>
                                                </div>
                                            </div>
                                            <?php }else{ ?>
                                            <div class="form-group text-center">

                                                <?php if(!empty($value->promotion) && $value->promotion['discount']!='0' && $value->promotion['type']=='1'){ ?>
                                                <span
                                                    class="Bold"><?php echo $total=number_format($value->promotion['discount'])?>
                                                    บาท </span>
                                                <del><?php echo number_format($value->price)?> บาท</del>
                                                <?php }else{ ?>
                                                <span class="Bold"><?php echo $total=number_format($value->price)?>
                                                    บาท</span>
                                                <?php } ?>
                                                <label for="code" style="margin: 0;">รหัสคูปองเฉพาะคอร์ส</label>
                                                <input type="text" class="form-control couponCode-a" name="couponCode"
                                                    id="couponCode"
                                                    value="<?php if(!empty($this->session->code_data['codeId'])){ echo $this->session->code_data['codeId']; } ?>"
                                                    autocomplete="off">
                                                <!--  <span id="coupon-error-div"></span> -->
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- สรุปจำนวนเงิน -->
                <input type="hidden" class="price" name="price" id="price" value="<?=$price?>">
                <input type="hidden" class="promotion" name="promotion" id="promotion" value="<?=$promotion?>">
                <input type="hidden" class="discount" name="discount" id="discount" value="<?=$discount?>">
                <input type="hidden" class="price_paypal" name="price_paypal" id="price_paypal" value="<?=$discount?>">
                <input type="hidden" class="coupon_id" name="coupon_id" id="coupon_id">


                <div class="col-sm-4">
                    <div class="panel">
                        <div class="blog-panel">
                            <div class="blog3">
                                <span>ราคา</span>
                            </div>
                        </div>
                        <div class="blog-list">
                            <h3>เลือกช่องทางชำระเงิน</h3>
                            <div class="row" style="display: none;" id="sl1">
                                <div class="col-sm-3 col-12">
                                    ส่วนลด
                                </div>
                                <div class="col-sm-9 col-12">
                                    <font color="green"><strong><span id="sl"></span></strong></font>
                                </div>
                            </div>
                            <h5>ยอดที่ท่านต้องชำระ <span style="color: green; font-size: 24px;font-weight: 600; ">
                                    <span class="total"><?php echo number_format($discount)?></span> บาท
                            </h5>

                            <div style="padding-top: 15px"></div>
                            <div id="discount-0" class="text-center"></div>
                            <div id="discount-1">
                                <div class="col s12 m12 card payment ng-scope" style="padding-top: 15px;display:none;">
                                    <div class="pay-detail"><span>ชำระผ่านบัตรเครดิต</span> (เรียนได้ทันทีหลังชำระเงิน)
                                    </div>
                                    <div id="paypal-button-container"></div>
                                </div>
                                <div style="padding-top: 15px"></div>
                                <div class="col s12 m12 card payment ng-scope" style="padding-top: 15px;">
                                    <a href="" data-toggle="modal" data-target="#myModal">
                                        <div class="pay-detail"><span>ชำระโดยการโอนเงิน</span> (เรียนได้ภายใน 30 นาที.
                                            หลังจากแจ้งโอน)</div>
                                        <img class="pay-logo-large" src="<?php echo base_url("images/bank.png") ?>"
                                            style="width: 100%">
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <?php }else{ ?>
            <div class="row mb-5">
                <div class="col-sm-12 text-center" style="color: red">
                    <h2>ไม่มีรายการสั่งซื้อ</h2>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>

</section>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <?php echo form_open_multipart(site_url('payment/payment_file'), array('class' => '', 'id'=>'frm-save' , 'method' => 'post')) ?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ชำระโดยการโอนเงิน (เรียนได้ภายใน 30 นาที. หลังจากแจ้งโอน)</h4>
            </div>

            <div class="modal-body">
                <center>
                    <h3>ยอดที่ท่านต้องชำระ <span style="color: green; font-size: 24px;font-weight: 600; " class="total">
                            <?php echo number_format($discount)?> บาท</span>
                    </h3>
                </center>
                <br>
                <div id="open_buy">

                    <h4><b>ขั้นตอนที่ 1 โอนเงินผ่านธนาคาร</b></h4>
                    <div class="t-pc">
                        <table class="bank-t" width="100%">
                            <tr>
                                <th>ธนาคาร</th>
                                <th>สาขา</th>
                                <th>เลขที่บัญชี</th>
                                <th>ประเภทบัญชี</th>
                                <th>ชื่อบัญชี</th>
                            </tr>
                            <?php foreach ($banks as $key => $value) { ?>
                            <tr>
                                <td>
                                    <img alt="K SME" src="<?php echo base_url($value->file); ?>" style=" width: 25px">
                                    <?php echo $value->title; ?>
                                </td>
                                <td><?php echo $value->branch; ?></td>
                                <td><?php echo $value->accountNumber; ?></td>
                                <td><?php echo $value->type; ?></td>
                                <td><?php echo $value->name; ?></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="t-mobile">
                        <div class="container">
                            <div class="col-sm-12 mb-5">
                                <?php foreach ($banks as $key => $value) { ?>
                                <div class="row col ">
                                    <div class="col-5 col-md-6" style="text-align: right;">
                                        <span style="font-weight: 700;">ธนาคาร : </span>
                                    </div>
                                    <div class="col-7  col-md-6">
                                        <img alt="K SME" src="<?php echo base_url($value->file); ?>"
                                            style=" width: 25px"> <?php echo $value->title; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5 col-md-6" style="text-align: right;">
                                        <span style="font-weight: 700;">สาขา : </span>
                                    </div>
                                    <div class="col-7 col-md-6">
                                        <?php echo $value->branch; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5 col-md-6" style="text-align: right;">
                                        <span style="font-weight: 700;">เลขที่บัญชี : </span>
                                    </div>
                                    <div class="col-7 col-md-6">
                                        <?php echo $value->accountNumber; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5 col-md-6" style="text-align: right;">
                                        <span style="font-weight: 700;">ประเภทบัญชี : </span>
                                    </div>
                                    <div class="col-7 col-md-6">
                                        <?php echo $value->type; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5 col-md-6" style="text-align: right;">
                                        <span style="font-weight: 700;">ชื่อบัญชี : </span>
                                    </div>
                                    <div class="col-7 col-md-6">
                                        <?php echo $value->name; ?>
                                    </div>
                                </div>
                                <br>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <h4><b>ขั้นตอนที่ 2 แจ้งโอนเงิน</b></h4>
                <p style="color: #000">เมื่อโอนเงินเรียบร้อยแล้ว โปรดแจ้งการชำระเงิน โดยกรอกแบบฟอร์มข้างล่าง</p>
                <div class="row">
                    <span class="col-lg-4 col-md-4 col-12">
                        สลิปหลักฐานการโอน (รูป/PDF)
                        <!-- <span class="required">*</span> -->
                    </span>
                    <span class="col-lg-12 col-md-12 col-12">

                        <!-- <input type="file" name="fileUpload" id="fileUpload" onchange="check_file_img(this);"> -->
                        <input id="file-0" type="file" data-theme="fas" name="fileUpload"
                            onchange="check_file_img(this);">
                        <div class="alert_file"></div>

                    </span>
                </div>
                <div class="payment-b">
                    <center>
                        <input type="hidden" class="price" name="price" id="price" value="<?=$price?>">
                        <input type="hidden" class="promotion" name="promotion" id="promotion" value="<?=$promotion?>">
                        <input type="hidden" class="discount" name="discount" id="discount" value="<?=$discount?>">
                        <input type="hidden" class="coupon_id" name="coupon_id" id="coupon_id">
                        <div class="register-form">
                            <div id="form-success-div" class="text-success"></div>
                            <div id="form-error-div" class="text-danger"></div>
                        </div>
                    </center>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-green"><span id="form-img-div"></span>
                        <font color="#fff">แจ้งชำระเงิน</font>
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>

        <?php echo form_close() ?>
    </div>
</div>