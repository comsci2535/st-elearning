<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model("cart_m");
	}

    private function seo(){
		$title          = 'ST Developer';
		$robots         = 'ST Developer';
		$description    = 'ST Developer';
		$keywords       = 'ST Developer';
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url()."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
		return $meta;
	}

	public function index(){
		$cart_data = $this->session->userdata('cart_data');
        if($cart_data == null || count($cart_data) == 0){
            $cart_data = array();
        }

        if(count($cart_data) > 0){

            $lists_id = array();
            foreach ($cart_data as $key => $value) {
                $lists_id[] = $key;
            }

            $item= $this->cart_m->getByListsID($lists_id)->result();

            $price=0;
            $promotion=0;
            $discount=0;
            foreach ($item as $key => $value) {
                $value->promotion=Modules::run('promotion/get_promotion', $value->course_id); 
                if(!empty($value->promotion)){
                    $price=($price+$value->price);
                    $discount=($discount+$value->promotion['discount']);
                    $promotion=($promotion+$value->promotion['discount']);
                }else{
                    $price=($price+$value->price);
                    $discount=($discount+$value->price);
                    $promotion="";
                }
            }

        } else {
            $item = array();
        }
        //

        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'item'    => $item,
            'price'    => $price,
            'promotion'    => $promotion,
            'discount'    => $discount,
            'banks'     => $this->get_banks(),
            'content' => 'cart',
            'footer'  => 'footer',
            'background'  => 'bg',
            'function'=>  array('custom','cart'),
		);
        //print"<pre>";print_r($data);exit();
		//loade view

        $this->load->view('template/body', $data);
	}



	public function detail(){
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'detail',
            'footer'  => 'footer',
            'function'=>  array('custom'),
		);
		//loade view

        $this->load->view('template/body', $data);
	}

	public function update_cart(){
        $this->session->unset_userdata('cart_data');
        $cart_data = $this->session->userdata('cart_data');
        if($cart_data == null || count($cart_data) == 0){
            $cart_data = array();
        } 

        $post_course_id = $this->input->post('course_id');
        $post_num = 1;

        $cart_data[$post_course_id] = $post_num;
        $this->session->set_userdata('cart_data', $cart_data);
        exit();

    }

    public function delete_cart(){
 
        $cart_data = $this->session->userdata('cart_data');
        if($cart_data == null || count($cart_data) == 0){
            $cart_data = array();
        } 
       
        $post_course_id = $this->input->post('course_id');

        if(count($cart_data) > 0){

            $listDel = array();
            foreach ($cart_data as $key => $value) {
                if($key != $post_course_id){
                    $listDel[$key] = $value;
                }

            }
            $cart_data = $listDel;
        } else {
            $cart_data = array();
        }



        $this->session->set_userdata('cart_data', $cart_data);
        exit();
    }

    public function get_banks(){

        $info = $this->cart_m->getBanks()->result();

        return $info;
    }

    

}
