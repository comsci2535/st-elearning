<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function getByListsID($lists_id){
        
        $this->db->where_in('a.course_id', $lists_id);
            
        $query = $this->db
                        ->select('a.*')
                        ->from('courses a')
                        ->get();
        return $query;
    }
    public function getBanks(){
        
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
            
        $query = $this->db
                        ->select('a.title,a.branch,a.accountNumber,a.name,a.file,a.bank_id')
                        ->from('banks a')
                        ->get();
        return $query;
    }
}
