<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MX_Controller {

	function __construct() {
		parent::__construct();

        $this->load->model('login/users_model');
        $this->load->library('login/users_library');
        $this->load->model('profile_m');
        $this->load->model('courses/courses_m');
        $this->load->model('courses_categories/courses_categories_m');
        $this->load->model('seos/seos_m');
        $this->load->model('exams/exams_m');
        $this->load->model('certificates/certificates_m');
        
        
        $this->status=array('รออนุมัติ','ชำระเงินเรียบร้อยแล้ว','ยกเลิก');
	}

    private function seo(){

        $obj_seo = $this->seos_m->get_seos_by_display_page('courses')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : 'คอร์สเรียนออลไลน์';
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : 'คอร์สเรียนออลไลน์';
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : 'คอร์สเรียนออลไลน์';
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('courses')."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

	public function index(){
        $data = array(
            'menu'    => 'instructor',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'courses',
            'footer'  => 'footer',
            'function'=>  array('custom','courses'),
        );

        $this->load->view('template/body', $data);
    }

    public function mycourses(){
        $data = array(
            'menu'    => 'instructor',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'courses',
            'footer'  => 'footer',
            'function'=>  array('custom','courses','profile'),
        );

        $input['user_id'] 	= $this->session->users['UID'];
        $data['count']      = $this->profile_m->count_courses_by_student($input);
        
        $this->load->view('template/body', $data);
    }

    public function ajax_data_courses() {
        $input = $this->input->post();
        $input['recycle']   = 0;
        $input['user_id'] 	= $this->session->users['UID'];
        $infoCount		    = $this->profile_m->count_get_courses_student_by_id($input);
        $info               = $this->profile_m->get_courses_student_by_id($input);
        $column             = array();
        foreach ($info->result() as $key => $rs) {
            $no = $key+$input['start']+1;
            $id = encode_id($rs->user_id);
            
            if($rs->status == 0 || $rs->status == 2):
                $status = '<span class="label bg-red">'.$this->status[$rs->status].'</span>';
            endif;
            if($rs->status == 1):
                $status = '<span class="label bg-green">'.$this->status[$rs->status].'</span>';
            endif;

            $url = "onerror=\"this.src='".base_url("images/cose.jpg")."';\"";
           
            $file           = '<a href="'.base_url('courses/'.$rs->slug).'">
                        <div class="img">
                            <img src="'.base_url($rs->file).'"
                                class="ImgFluid imgBig" alt="คอสเรียน"
                                '.$url.'>
                        </div>
                        <div class="detail-text">
                            <div class="name Bold">
                                <p>'.$rs->title.'</p>
                            </div>
                            <div class="detail">
                                <p>'.$rs->excerpt.'</p>
                            </div>
                        </div>
                    </a>';

            $info_exams     = $this->exams_m->get_quiz_type_by_user_id($input['user_id'], $rs->course_id, 'final')->row();
            $certifi        = $this->certificates_m->count_certificates_by_courses_id($rs->course_id);
            $print_exams    = '';
            if(!empty($info_exams->result) && $info_exams->result == 'pass' && $certifi > 0):
                $print_exams = '<p><a href="'.base_url('courses_students/print/'.$rs->course_id.'/'.$rs->user_id).'" target="_blank"><i class="fa fa-print"></i> เกียรติบัตร</a></p>';
            endif;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['course_id']  = $rs->course_id;
            $column[$key]['file']       = $file;
            $column[$key]['status']     = $status;
            $column[$key]['discount']   = number_format($rs->discount);
            $column[$key]['updated_at'] = $rs->updated_at;
            $column[$key]['action']     = '<p><a href="javascript:void(0)" class="btn-click-view" data-user-id="'.$rs->user_id.'" data-course-id="'.$rs->course_id.'"><i class="fa fa-book"></i> ดูข้อมูลการเรียน</a></p>
                                          <p><a href="javascript:void(0)" class="btn-click-quiz" data-user-id="'.$rs->user_id.'" data-course-id="'.$rs->course_id.'"><i class="fa fa-book"></i> ดูข้อมูลผลการสอบ</a></p>
                                          <p><a href="javascript:void(0)" class="btn-click-homeworks" data-user-id="'.$rs->user_id.'" data-course-id="'.$rs->course_id.'"><i class="fa fa-edit"></i> การบ้าน</a></p>
                                          '.$print_exams;
        }

        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function get_mycourses($course_id){
        
        $rs=$this->db->select('*')
                        ->from('courses_instructors')
                        ->where('course_id',$course_id)
                        ->where('user_id',$this->session->users['UID'])
                        ->get()
                        ->result();
         
        if(!empty($rs)){
            
            return  count($rs);
        }else{
            $input['course_id']   = $course_id;
            $input['user_id']   = $this->session->users['UID'];
            $input['status']   = 1;
            $total      = $this->profile_m->count_courses_by_student($input);
       
             return  $total;
        }
    }

    public function get_mycourses_array($course_id){
        
        $rs=$this->db->select('*')
                        ->from('courses_instructors')
                        ->where('course_id',$course_id)
                        ->where('user_id',$this->session->users['UID'])
                        ->get()
                        ->row_array();
        
        if(!empty($rs)){
            $result = array();
            $result['status'] = 3;
            return  $result;
        }else{
            $input['course_id']   = $course_id;
            $input['user_id']   = $this->session->users['UID'];
            $result      = $this->profile_m->courses_by_student($input)->row_array();
            return $result;
        }
        
    }

    public function coursesdetail($code=""){
        
		$input['slug']  = $code;
        $data = array(
            'menu'    => 'courses',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'detail',
            'footer'  => 'footer',
            'function'=>  array('custom','courses'),
		);
		//loade view
		$info  = $this->courses_m->get_rows($input)->row();
		if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "Gorra Design");
		$data['courses'] = $info;

		$info_categories  = $this->courses_categories_m->get_rows($info->course_categorie_id)->row();
		$data['categories'] = $info_categories;

        $this->load->view('template/body', $data);
	}

}
