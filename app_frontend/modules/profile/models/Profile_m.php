<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function count_courses_by_student($param)
    {
       // if ( isset($param['user_id']) )
        $this->db->where('a.user_id', $param['user_id']);

       if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);

       if ( isset($param['status']) )
            $this->db->where('a.status', $param['status']);

        $this->db->from('courses_students a')
            ->join('courses b', 'a.course_id = b.course_id', 'inner');
            //->join('courses_instructors f', 'b.course_id = f.course_id', 'inner')
            //->join('instructors c', 'f.user_id = c.user_id', 'inner')
            //->join('users e', 'c.user_id = e.user_id', 'inner');
        return $this->db->count_all_results();
    }
    
    public function courses_by_student($param)
    {
        
        $this->db->where('a.user_id', $param['user_id']);
        //$this->db->where('c.private', 0);

        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);

        if ( isset($param['status']) )
            $this->db->where('a.status', $param['status']);

        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

         $this->db->order_by('a.order_id', 'desc');

        $query = $this->db
                        ->select('a.*, b.*, f.updated_at')
                        ->from('courses_students a')
                        ->join('courses b', 'a.course_id = b.course_id', 'inner')
                        ->join('orders f', 'a.order_id = f.order_id', 'inner')
                       // ->join('instructors c', 'f.user_id = c.user_id', 'inner')
                       // ->join('users e', 'c.user_id = e.user_id', 'inner')
                        ->get();
        return $query;
    }
    
    public function get_courses_student_by_id($param) 
    {

        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['search']['value'])
                    ->or_like('b.excerpt', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 0) $columnOrder = "b.order_id";
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.discount";
            if ($param['order'][0]['column'] == 2) $columnOrder = "f.created_at";
            
            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        } 

        $this->db->where('a.user_id', $param['user_id']);

        if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);

        if ( isset($param['status']) )
            $this->db->where('a.status', $param['status']);

        $query = $this->db
                        ->select('a.*, b.*, f.updated_at')
                        ->from('courses_students a')
                        ->join('courses b', 'a.course_id = b.course_id', 'inner')
                        ->join('orders f', 'a.order_id = f.order_id', 'inner')
                        ->get();
        return $query;
    }

    public function count_get_courses_student_by_id($param) 
    {
       
        $this->db->where('a.user_id', $param['user_id']);

        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['search']['value'])
                    ->or_like('b.excerpt', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 0) $columnOrder = "b.order_id";
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.discount";
            if ($param['order'][0]['column'] == 2) $columnOrder = "f.created_at";
            
            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        }

       if ( isset($param['course_id']) )
            $this->db->where('a.course_id', $param['course_id']);

       if ( isset($param['status']) )
            $this->db->where('a.status', $param['status']);

        $this->db->from('courses_students a');
        $this->db->join('courses b', 'a.course_id = b.course_id', 'inner');
        return $this->db->count_all_results();
    }

    
}
