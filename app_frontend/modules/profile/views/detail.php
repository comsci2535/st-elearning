<section class="Sec_Courses_Detail">
    <!-- <div class="container-fluid">
        <div class="row">
            <div id="slide_banner" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="<?=!empty($categories->file)? base_url().$categories->file : '';?>" class="ImgFluid" alt="">
                </div>
            </div>
        </div>
    </div> -->
    <!-- Modules::run('banners/index', 'type', 'file path') -->
    <?php echo Modules::run('banners/index', 'courses', 'index') ?>

    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-5 mb-4">
                <ul class="nav nav-url">
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url()?>">หน้าแรก</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url('courses')?>">คอร์สเรียน</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><?php echo !empty($categories->title)? $categories->title : '';?></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><?php echo !empty($courses->title)? $courses->title : ''?></a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 mb-4 blog-left">
                <div class="course-detail-bx">
                    <div class="course-price">
                        <del>$<?php echo !empty($courses->price)? number_format($courses->price,2) : '0.0'?></del>
                        <h4 class="price">$<?php echo !empty($courses->price)? number_format($courses->price,2) : '0.0'?></h4>
                    </div>
                    <!-- Modules::run('instructors/instructor_courses', 'course_id') -->
                    <?php echo Modules::run('instructors/instructor_tab', $courses->course_id) ?>

                    <div class="cours-more-info">
                        <div class="review">
                            <span>3 ความคิดเห็น</span>
                            <ul class="cours-star">
                                <li class="active"><i class="fa fa-star"></i></li>
                                <li class="active"><i class="fa fa-star"></i></li>
                                <li class="active"><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                        </div>
                        <div class="price categories">
                            <span>กลุ่มเรียน</span>
                            <h5><?php echo !empty($categories->title)? $categories->title : '';?></h5>
                        </div>
                    </div>
                    <div class="course-info-list scroll-page scroller">
                        <ul class="navbar">
                            <li><a class="nav-link active" href="#Overview"><i class="far fa-file-alt"></i> ภาพรวม</a>
                            </li>
                            <li><a class="nav-link" href="#Curriculum"><i class="far fa-file-alt"></i> หลักสูตร</a>
                            </li>
                            <li><a class="nav-link" href="#Instructor"><i class="far fa-file-alt"></i> อาจารย์ผู้สอน</a>
                            </li>
                            <li><a class="nav-link" href="#reviews"><i class="far fa-file-alt"></i> ความคิดเห็น</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-9 blog-right">
                <div id="Textdetail">
                    <img src="<?=!empty($courses->file)? base_url().$courses->file : '';?>" class="ImgFluid" alt="">
                    <br><br>
                    <h1><?php echo !empty($courses->title)? $courses->title : ''?></h1>
                    <p><?php echo !empty($courses->excerpt)? $courses->excerpt : ''?></p>
                    <br>
                </div>

                <div id="Overview" class="row">
                    <div class="col-sm-12 mb-3">
                        <h2>รายละเอียด</h2>
                        <?php echo !empty($courses->detail)? $courses->detail : ''?>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-left">
                            <div class="title">
                                <h2>สิ่งที่ได้รับ</h2>
                            </div>
                            <div>
                                <?php echo !empty($courses->receipts)? $courses->receipts : ''?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modules::run('courses_lesson/courses_lesson', 'course_lesson_id') -->
                <?php echo Modules::run('courses_lesson/instructor_courses', $courses->course_lesson_id) ?>

                <!-- Modules::run('instructors/instructor_courses', 'course_id') -->
                <?php echo Modules::run('instructors/instructor_courses', $courses->course_id) ?>
                
                <!-- Modules::run('reviews/reviews', 'course_id') -->
                <?php echo Modules::run('reviews/reviews', $courses->course_id) ?>
                
            </div>
        </div>

    </div>
    <!-- container -->
</section>