<?php 

/**
 * Library users
 */
class Notif_library 
{
	public $CI;

	public function __construct()
	{
		$this->CI =& get_instance();

		$this->CI->load->model('notification/notification_model');

    }

    public function noti_insert($data){

        $this->CI->notification_model->noti_save($data);
    }

}

