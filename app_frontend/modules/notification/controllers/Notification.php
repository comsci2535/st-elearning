<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('notification_model');
    }
    
    public function noti_all() 
	{
        $noti_all = $this->notification_model->noti_all(); 
        return $noti_all;         
    }

    public function noti_check($id) 
	{
        $noti_check = $this->notification_model->noti_check($id); 
        return $noti_check->row();       
    }

    public function read_noti() 
	{
        $read_noti = $this->notification_model->read_noti($this->uri->segment(3)); 
        if ($read_noti) {
            $row = $this->db->get_where('notification', array('id'=>$this->uri->segment(3)))->row();
            redirect($row->url);
        }
    }

    public function noti_save($data) 
	{
        $noti_save = $this->notification_model->noti_save($data); 
        // return $noti_check->row();       
    }
}
