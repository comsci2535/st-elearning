<section class="Sec_Courses">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 pt-4">
                <h4 class="pt-4">
                    <a href="<?=site_url('manage_article/courses');?>">บทความทั้งหมด</a> > <span><?php echo !empty($breadcrumb)? $breadcrumb : '';?></span>
                </h4>
                <hr>
            </div>
            <!-- <div class="col-sm-3 mb-5">
                <div class="MenuCourses">
                    <h2 class="text-center">เมนูใช้งาน</h2>
                    <hr>
                    <ul class="sidebar-menu">
                        <li><a href="<?=site_url('instructors/courses');?>"><span><i class="fas fa-th-large"></i>
                                    คอร์ส</span></a></li>
                    </ul>
                    
                </div>
            </div> -->
            <div class="col-sm-12 mb-5">
              
                    <form class="form-horizontal frm-create" method="post" action="<?=$frmAction?>" autocomplete="off" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email"></label>
                            <div class="col-sm-10">
                                <input <?php if(!empty($info->recommend) &&  $info->recommend=='1'){ echo "checked";}?> type="checkbox" name="recommend" valus="1"> รายการแนะนำ
                                <span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">หัวข้อ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" required>
                                <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">slug <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug"  required>
                                <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ประเภทบทความ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <select id="article_categorie_id" name="article_categorie_id" class="form-control m-input  select2" required>
                                    <option value="">เลือก</option>
                                    <?php
                                    if(isset($infotype)):
                                        $selected = '';
                                        foreach($infotype as $item):
                                            if($info->article_categorie_id == $item->article_categorie_id):
                                                $selected = 'selected';
                                            else:
                                                $selected = '';
                                            endif;
                                            
                                        ?>
                                            <option value="<?=$item->article_categorie_id;?>" <?=$selected?>><?=$item->title;?></option>
                                        <?php
                                        endforeach;
                                    
                                    endif;
                                    ?>
                                
                                </select>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label class="col-form-label col-sm-2">วันเวลาแสดงข้อมูล</label>
                            <div class="col-sm-10">
                            <input type="text" name="dateRange" class="form-control" value="<?php echo isset($dateRang) ? $dateRang : NULL ?>"/>
                                <input type="hidden" name="start_date"  value="<?php echo isset($info->start_date) ? $info->start_date : NULL ?>"/>
                                <input type="hidden" name="end_date"  value="<?php echo isset($info->end_date) ? $info->end_date : NULL ?>"/>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ไฟล์ </label>
                            <div class="col-sm-10"> 
                                <input id="file" name="file" type="file"  data-preview-file-type="text">
                                <input type="hidden" name="outfile" value="<?=(!empty($info->file)) ? $info->file : ''; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">รายละเอียดย่อ</label>
                            <div class="col-sm-10"> 
                                <textarea name="excerpt" rows="3" class="form-control" id="excerpt" ><?php echo !empty($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">รายละเอียด</label>
                            <div class="col-sm-10"> 
                                <textarea name="detail" rows="3" class="form-control summernote" id="detail" ><?php echo !empty($info->detail) ? $info->detail : NULL ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ช่วงวันที่แสดงผล</label>
                            <div class="col-sm-10"> 
                                <input type="text" name="dateRange" class="form-control" value="<?php echo !empty($dateRang) ? $dateRang : NULL ?>"/>
                                <input type="hidden" name="startDate"  value="<?php echo !empty($info->startDate) ? $info->startDate : NULL ?>"/>
                                <input type="hidden" name="endDate"  value="<?php echo !empty($info->endDate) ? $info->endDate : NULL ?>"/>
                            </div>
                        </div>
                        <?php
                        if($this->router->method == 'edit_courses'):
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">สถานะ <span class="text-danger"> *</span></label>
                            <div class="col-sm-10"> 
                                <label><input <?php if(isset($info->active) && $info->active== 1){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="1" > เปิด</label>
                                <label><input <?php if(isset($info->active) && $info->active== 0){  echo  "checked"; } ?> type="radio" name="active" class="icheck" value="0"> ปิด</label>
                            </div>
                        </div>
                        <?php
                        endif;
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd"> <h5 class="block"><b>ข้อมูล SEO</b></h5></label>
                            <div class="col-sm-10"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">ชื่อเรื่อง (Title)</label>
                            <div class="col-sm-10"> 
                                <input value="<?php echo !empty($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="" class="form-control" name="metaTitle">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">คำอธิบาย (Description)</label>
                            <div class="col-sm-10"> 
                                <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo !empty($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">คำหลัก (Keyword)</label>
                            <div class="col-sm-10"> 
                                <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo !empty($info->metaKeyword) ? $info->metaKeyword : NULL ?></textarea>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                                <input type="hidden" class="form-control" name="db" id="db" value="courses">
                                <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->course_id) ? encode_id($info->course_id) : 0 ?>">
                                <button type="submit" class="btn btn-success">บันทึก</button>
                            </div>
                        </div>
                    </form>
             
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? base_url().$info->file : ''; ?>';
    var file_id         = '<?=(isset($info->course_id)) ? $info->course_id : ''; ?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/'+file_id;

</script>