<section class="Sec_Courses">
    <div class="container-fluid">
        <div class="row">
            <div id="slide_banner" class="owl-carousel owl-theme">
                <div class="item">
                    <img src="<?=base_url('images/slide_courses.JPG');?>" class="ImgFluid" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row my-5">

            <div class="col-sm-12 mb-5">
                <div class="title">
                    <h1>สมัครสอนกับเรา</h1>
                </div>
            </div>

            <div class="col-sm-6">
                <h1>Award-Winning Customer Support</h1>
                <p>Register.com's award winning support team is here to help you find the online solutions that work
                    best for your business. Our experts are ready to give you the insight you need, so feel free to chat
                    with us if you have any questions.</p><br><br>
                <h1>Award-Winning Customer Support</h1>
                <p>Register.com's award winning support team is here to help you find the online solutions that work
                    best for your business. Our experts are ready to give you the insight you need, so feel free to chat
                    with us if you have any questions.</p><br><br>
                <h1>Award-Winning Customer Support</h1>
                <p>Register.com's award winning support team is here to help you find the online solutions that work
                    best for your business. Our experts are ready to give you the insight you need, so feel free to chat
                    with us if you have any questions.</p><br><br>
            </div>
            <div class="col-sm-6">
                <div class="card card-signin">
                    <div class="card-body">
                        <h5 class="card-title text-center">-- ข้อมูลส่วนตัว --</h5>
                        <form id="UpdateForm" class="" action="<?=site_url('instructors/register_update');?>"
                            method="post">
                            <!-- <div class="form-label-group m-form__group">
                                <label for="name">ชื่อ-นามสกุล</label>
                                <input type="text" id="name" name="name"
                                    value="<?php echo isset($Users->fullname) ? $Users->fullname : NULL ?>"
                                    class="form-control" placeholder="ชื่อ-นามสกุล">
                            </div> -->
                            <div class="form-label-group m-form__group">
                                <label for="name">ชื่อ-นามสกุล</label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" id="fname" name="fname" class="form-control " placeholder="ชื่อ" value="<?php echo isset($Users->fname) ? $Users->fname : NULL ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" id="lname" name="lname" class="form-control" placeholder="นามสกุล" value="<?php echo isset($Users->lname) ? $Users->lname : NULL ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-label-group m-form__group">
                                <label for="phone">เบอร์โทร</label>
                                <input type="text" id="phone" name="phone"
                                    value="<?php echo isset($Users->phone) ? $Users->phone : NULL ?>"
                                    class="form-control" placeholder="เบอร์โทร">
                            </div>

                            <div class="form-label-group m-form__group">
                                <label for="email">Email</label>
                                <input type="email" id="email" name="email"
                                    value="<?php echo isset($Users->email) ? $Users->email : NULL ?>"
                                    class="form-control" placeholder="Email" <?php if ($Users->email) {echo 'disabled';} ?>>
                            </div>

                            <div class="form-group m-form__group row">
                                <input type="hidden" id="datajson" name="datajson"
                                    value="<?=base_url('template/frontend');?>/jquery.Thailand.js/database/db.json">
                                <div class="col-lg-6 m-form__group-sub">
                                    <label class="form-col-form-label">ตำบล<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="district" placeholder="ตำบล"
                                        name="districts_name"
                                        value="<?php echo isset($Users->districts_name) ? $Users->districts_name : NULL ?>">
                                </div>
                                <div class="col-lg-6 m-form__group-sub">
                                    <label class="form-col-form-label">อำเภอ<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="amphoe" placeholder="อำเภอ"
                                        name="amphures_name"
                                        value="<?php echo isset($Users->amphures_name) ? $Users->amphures_name : NULL ?>">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6 m-form__group-sub">
                                    <label class="form-col-form-label">จังหวัด<span class="text-danger">
                                            *</span></label>
                                    <input type="text" class="form-control m-input" id="province" placeholder="จังหวัด"
                                        name="provinces_name"
                                        value="<?php echo isset($Users->provinces_name) ? $Users->provinces_name : NULL ?>">
                                </div>
                                <div class="col-lg-6 m-form__group-sub">
                                    <label class="form-col-form-label">รหัสไปรษณีย์<span class="text-danger">
                                            *</span></label>
                                    <input type="text" class="form-control m-input" id="zipcode"
                                        placeholder="รหัสไปรษณีย์" name="zip_code"
                                        value="<?php echo isset($Users->zip_code) ? $Users->zip_code : NULL ?>">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-6 m-form__group-sub">
                                    <label class="form-col-form-label">facebook<span class="text-danger"></label>
                                    <input type="text" class="form-control m-input" id="facebook" placeholder="facebook"
                                        name="facebook"
                                        value="<?php echo isset($Users->facebook) ? $Users->facebook : NULL ?>">
                                </div>
                                <div class="col-lg-6 m-form__group-sub">
                                    <label class="form-col-form-label">lineID<span class="text-danger"></label>
                                    <input type="text" class="form-control m-input" id="lineID" placeholder="lineID"
                                        name="lineID"
                                        value="<?php echo isset($Users->lineID) ? $Users->lineID : NULL ?>">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-12 m-form__group-sub">
                                    <label class="form-col-form-label">ประสบการณ์<span class="text-danger"></label>
                                    <textarea class="form-control m-input" id="experience" placeholder="ประสบการณ์"
                                        name="experience"><?php echo isset($Users->experience) ? $Users->experience : NULL ?></textarea>
                                </div>
                            </div>

                            <div class="form-group form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="checkbox_s"> ยืนยันการสมัคร
                                </label>
                            </div>

                            <input type="hidden" name="id" id="input-id"
                                value="<?php echo isset($this->session->users['UID']) ? $this->session->users['UID'] : NULL ?>">
                            <button class="btn btn-lg btn-primary btn-block text-uppercase"
                                type="submit">บันทึก</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>