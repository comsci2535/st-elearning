<?php
// arr($info);
// exit();
?>
<div class="teacher-bx">
    <?php
    if(!empty($info)):
        foreach($info as $item):
    ?>
    <div class="teacher-info mb-2">
        <div class="teacher-thumb">
            <img src="<?php echo !empty($item->file)? base_url().$item->file : '';?>" alt="<?php echo !($item->fullname)? $item->fullname : '';?>">
        </div>
        <div class="teacher-name">
            <h5><?php echo !empty($item->fullname)? $item->fullname : '';?></h5>
            <span><?php echo !empty($item->type)? $item->type: ''?></span>
        </div>
    </div>
    <?php
        endforeach;
    endif;
    ?>
</div>