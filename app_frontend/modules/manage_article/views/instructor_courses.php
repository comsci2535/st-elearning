<div class="Bold my-4">
    <h2>อาจารย์ผู้สอน</h2>
</div>
<?php

if(!empty($info)):
    foreach($info as $item):
?>
<div class="courses-teaher">
    <div class="img">
        <img src="<?php echo base_url($item->file)?>" class="ImgFluid" alt="คอสเรียน" onerror="this.src='<?php echo base_url("images/Great_Teacher.jpg"); ?>'">
    </div>
    <div class="text">
        <p class="name Bold"><?php echo !empty($item->fullname)? $item->fullname : '';?></p>
        <div class="pp">
            <div id="text-row-teaher-<?=$key?>" class="p">
                <p><?php echo !empty($item->experience)? $item->experience : '';?></p>
                <p>
                <?php
                if(!empty($item->skills)):
                    foreach($item->skills as $key => $skill):
                        $text = '';
                        if($key > 0):
                            $text = ', ';
                        endif;
                        echo $text.$skill->skill_name;
                    endforeach;
                endif;
                ?>
                </p>
            </div>
            <a href="javascript:void(0)" id="overlate" class="overlate-hide" data-line="text-row-teaher-<?=$key?>" data-hide="0">
                <i class="fas fa-plus"></i> ดูเพิ่มเติม
            </a>
        </div>
    </div>
</div>
<?php
endforeach;
endif;
?>