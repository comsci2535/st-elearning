<section class="Secc_article" style="font-size:20px !important">
	<!-- Page Content -->
	<div class="container">
		<div class="col-sm-12 mt-5">
			<div class="d-flex">
				<a class="m-2" href="<?=base_url('ourteacher')?>">คุณครูทั้งหมด</a>
				<span class="m-2"> > </span>
				<span class="m-2" href="#"><?=$ourteacher->fullname;?></span>
			</div>
		</div>
		<hr>

		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-xl-3 col-md-4">
					<div class="m-portlet m-portlet--full-height">
						<div class="m-portlet__body">
							<div class="m-card-profile">
								<div class="m-card-profile__title m--hide">
									Your Profile
								</div>
								<div class="m-card-profile__pic">
									<div class="m-card-profile__pic-wrapper">
										<?php

											 if(!empty($ourteacher->file)){
                                                $img_intsr=base_url($ourteacher->file);
                                            }else{
                                                $img_intsr=$ourteacher->oauth_picture;
                                            }

										 ?>
										<img src="<?php echo $img_intsr;?>" alt=""
											onerror="this.src='<?php echo base_url("images/user.png");?>'" />
									</div>
								</div>
								<div class="m-card-profile__details">
									<span class="m-card-profile__name"><?=$ourteacher->fullname?></span>
									<div class="line-buttom mx-auto"></div>
									<a href="mailto:<?=$ourteacher->email?>"
										class="m-card-profile__email m-link"><?=$ourteacher->email?></a>
								</div>
							</div>
							<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
								<li class="m-nav__separator m-nav__separator--fit"></li>
								<li class="m-nav__item <?=empty($ourteacher->facebook)? 'd-none' : '' ?>">
									<a href="https://www.facebook.com/<?=$ourteacher->facebook?>" class="m-nav__link"
										target="_blank">
										<i class="m-nav__link-icon fab fa-facebook"></i>
										<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-nav__link-text">Facebook</span>
												<!-- <span class="m-nav__link-badge"><span class="m-badge m-badge--success">2</span></span> -->
											</span>
										</span>
									</a>
								</li>
								<li class="m-nav__item <?=empty($ourteacher->instagram)? 'd-none' : '' ?>">
									<a href="https://www.instagram.com/<?=$ourteacher->instagram?>" class="m-nav__link"
										target="_blank">
										<i class="m-nav__link-icon fab fa-instagram"></i>
										<span class="m-nav__link-text">Instagram</span>
									</a>
								</li>
								<li class="m-nav__item <?=empty($ourteacher->lineID)? 'd-none' : '' ?>">
									<a href="http://line.me/ti/p/~<?=$ourteacher->lineID?>" class="m-nav__link"
										target="_blank">
										<i class="m-nav__link-icon fab fa-line"></i>
										<span class="m-nav__link-text">Line ID</span>
									</a>
								</li>
								<li class="m-nav__item <?=empty($ourteacher->twitter)? 'd-none' : '' ?>">
									<a href="https://twitter.com/<?=$ourteacher->twitter?>" class="m-nav__link"
										target="_blank">
										<i class="m-nav__link-icon fab fa-twitter"></i>
										<span class="m-nav__link-text">Twitter</span>
									</a>
								</li>
							</ul>
							<div class="m-portlet__body-separator"></div>
						</div>
					</div>
				</div>
				<div class="col-xl-9 col-md-8">
					<div class="m-portlet m-portlet--full-height m-portlet--tabs">
						<div class="m-portlet__head">
							<div class="m-portlet__head-tools">
								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
									role="tablist">
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link active Bold" data-toggle="tab"
											href="#m_user_profile_tab_1" role="tab">
											<i class="flaticon-share m--hide"></i>
											ข้อมูล
										</a>
									</li>
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link Bold" data-toggle="tab"
											href="#m_user_profile_tab_2" role="tab">
											ความชำนาญ
										</a>
									</li>
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link Bold" data-toggle="tab"
											href="#m_user_profile_tab_3" role="tab">
											ประสบการณ์
										</a>
									</li>
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link Bold couuse-list" data-toggle="tab"
											href="#m_user_profile_tab_4" role="tab">
											คอร์สเรียน
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="tab-content">
							<div class="tab-pane active p-5" id="m_user_profile_tab_1">

								<div class="form-group m-form__group row">
									<label class="col-sm-2 Bold">ชื่อ</label>
									<div class="col-sm-7">
										: <?=$ourteacher->fname;?>
									</div>
								</div>

								<div class="form-group m-form__group row">
									<label class="col-sm-2 Bold">นามสกุล</label>
									<div class="col-sm-7">
										: <?=$ourteacher->lname;?>
									</div>
								</div>

								<div
									class="form-group m-form__group row <?=empty($ourteacher->phone)? 'd-none' : '' ?>">
									<label class="col-sm-2 Bold">เบอร์โทร</label>
									<div class="col-sm-7">
										: <?=$ourteacher->phone;?>
									</div>
								</div>

								<div class="form-group m-form__group row">
									<label class="col-sm-2 Bold">อีเมล์</label>
									<div class="col-sm-7">
										: <?=$ourteacher->email;?>
									</div>
								</div>
							</div>

							<div class="tab-pane " id="m_user_profile_tab_2">
								<div class="m-alert__text m-4">
									<?php if(empty($ourteacher->skill_long)){ ?>
										<div class="text-center mt-5">
											ไม่มีข้อมูล
										</div>
									<?php }?>
									<?=$ourteacher->skill_long?>
								</div>

								<!-- <?php if(count($skill) === 0){ ?>
									<div class="m-alert__text m-4">
										<div class="text-center mt-5">
											ไม่มีข้อมูล
										</div>
									</div>
								<?php }?>
								<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
									<div class="m-demo__preview">
										<div class="m-nav-grid">
											<?php 
												$_count = count($skill)-1;
												if($_count === 0){
													echo '<div class="m-nav-grid__row"><span href="#" class="m-nav-grid__item">';
													echo '<i class="m-nav-grid__icon flaticon-app"></i>';
													echo '<span class="m-nav-grid__text">'.$skill->skill_name.'</span>';
													echo '</span></div>';
												}else{
												foreach($skill as $key=>$item){ 
													if($key%3 == 0 ||  $key == 0 ) 
														echo '<div class="m-nav-grid__row">';	
											?>
											<span href="#" class="m-nav-grid__item">
												<i class="m-nav-grid__icon flaticon-app"></i>
												<span class="m-nav-grid__text"><?=$item->skill_name?></span>
											</span>
											<?php 
													if($key%2 == 0 && $key != 0) 
														echo '</div>';

													}
												
													if($_count == $key && $key%2 != 0) 
														echo '</div>';
												}
											?>
										</div>
									</div>
								</div> -->
							</div>

							<div class="tab-pane " id="m_user_profile_tab_3">
								<div class="m-alert__text m-4">
									<?php if(empty($ourteacher->experience)){ ?>
										<div class="text-center mt-5">
											ไม่มีข้อมูล
										</div>
									<?php }?>
									<?=$ourteacher->experience?>
								</div>
							</div>

							<div class="tab-pane mt-3" id="m_user_profile_tab_4">
								<?php if($num_row === 0){ ?>
									<div class="m-alert__text m-4">
										<div class="text-center mt-5">
											ไม่มีข้อมูล
										</div>
									</div>
								<?php }?>

								<input type="hidden" name="num_row" id="num_row" value="<?=$num_row?>">
								<input type="hidden" name="prepage" id="prepage" value="<?=$per_page?>">
								<input id="user_id" type="hidden" value="<?=$ourteacher->user_id;?>">
								<div id="post-data-courses" class="CoursesItem">
									<?php 

									if($info):
										foreach($info as $item):
											//arr($info); 
											$infoUser = $this->courses_instructors_m->get_courses_instructors_join_user_by_course_id($item->course_id)->row();	
											$promotion = Modules::run('promotion/get_promotion', $item->course_id);
											$Today = date("Y-m-d H:i:s");
											$NewDate = date ("Y-m-d H:i:s", strtotime("+7 day", strtotime($item->created_at)));
											$myCourse   = Modules::run('profile/get_mycourses_array', $item->course_id);
									?>

									<div class="Item">
										<?php if(!empty($myCourse) && $myCourse['status']==1):?>
											<a href="<?=site_url("courses-lesson/{$item->slug}")?>">
										<?php else: ?>
											<a href="<?=site_url("courses/{$item->slug}")?>">
										<?php endif; ?>

											<div class="boxx">
												<div class="ribbin">
													<input type="hidden" id="course_id-<?=$item->course_id?>"
														value="<?=$item->course_id?>">
													<div id="showRemain-mobile-<?=$item->course_id?>" class=" hidden">
													</div>

													<?php if($Today < $NewDate){?>
													<div class="box new">
														<span class="a1">New</span>
													</div>
													<?php } ?>

													<?php if(!empty($promotion)){?>
													<div class="box pro protext-<?=$item->course_id?>">
														<span class="a1">Promotion</span>
													</div>
													<?php }?>

													<?php $img = "'".base_url('images/cose.jpg')."'"; ?>
												</div>
												<div class="img">

													<img src="<?=base_url($item->file)?>" class="ImgFluid imgBig"
														alt="'.$item->title.'" onerror="this.src= <?=$img?> ">
													<div class="hovv">
														<div class="text Bold">
															<span><i class="fas fa-link" style="font-size: 18px;"></i>
																รายละเอียดคอร์สเรียน</span>
														</div>
													</div>
												</div>
												<div class="detail">
													<div class="name Bold">
														<p><?=$item->title?></p>
													</div>
													<div class="teacher">
														<span class="Bold">โดยคุณครู</span>
														<span><?=$infoUser->fullname.' ('.$infoUser->fname.' )'?></span>
													</div>
													<div class="price">
														<?php if ($infoUser->user_id===$this->session->users['UID']): ?>
															<!-- ++++++++++ กรณีคอสของตัวเอง ++++++++++++ -->
															<div class="free">
																<span class="Bold">คอร์สของฉัน</span>
															</div>
														<?php else: ?>
															<!-- ++++++++++ กรณีคอสของคนอื่น ++++++++++++ -->
															<!-- ------------------ promotion -------------------------- -->
															<?php if($item->price > 0 && !empty($promotion)){ 
																		if(!empty($myCourse) && $myCourse['status']==0):?>
																			<div class="confirm">
																				<span class="Bold ">รอการอนุมัติ</span>
																				<div id="showRemain-<?=$item->course_id?>" style="display: none;"></div>
																			</div>
																<?php	elseif(!empty($myCourse) && $myCourse['status']==1):?>
																			<div class="free">
																				<span class="Bold">คลิกเพื่อเข้าเรียน</span>
																				<div id="showRemain-<?=$item->course_id?>" style="display: none;"></div>
																			</div>
																<?php  	else:?>
																			<div class="promotion protext-<?=$item->course_id?>">
																				<div class="left Bold">
																					<div id="showRemain-<?=$item->course_id?>"></div>
																					<p class="ket"><?=number_format($item->price)?></p>
																				</div>
																				<div class="right">
																					<span
																						class="Bold"><?=number_format($promotion['discount']).' ฿'?></span>
																				</div>
																			</div>
																			<div class="normal protext2-<?=$item->course_id?>"
																				style="display: none;">
																				<span
																					class="Bold"><?=number_format($item->price).' ฿'?></span>
																			</div>
																<?php   endif;?>
															<!-- ------------------ free -------------------------- -->
															<?php }else if($item->price==0){ ?>
																<div class="free">
																	<span class="Bold">FREE</span>
																</div>
															<!-- ------------------ normal ------------------------ -->
															<?php }else{ 
																		if(!empty($myCourse) && $myCourse['status']==0):?>
																			<div class="confirm">
																				<span class="Bold">รอการอนุมัติ</span>
																			</div>
																<?php	elseif(!empty($myCourse) && $myCourse['status']==1):?>
																	<div class="free">
																		<span class="Bold">คลิกเพื่อเข้าเรียน</span>
																	</div>
																<?php   else:?>
																	<div class="normal">
																		<span class="Bold"><?=number_format($item->price).' ฿'?></span>
																	</div>
																<?php   endif;?>
															<?php } ?>

														<?php   endif;?>
													</div>
												</div>
											</div>
										</a>
									</div>

									<?php 
											endforeach;
										endif;
									?>
								
								</div>
									<?php 
										if($num_row != 0){
											$this->load->view('ourteacher_holder'); 
										}
									?> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- /.container -->
</section>