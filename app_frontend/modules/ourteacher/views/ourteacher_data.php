<?php
if($info):
    foreach($info as $item):
        $infoUser = $this->courses_instructors_m->get_courses_instructors_join_user_by_course_id($item->course_id)->row();
        
        $promotion = Modules::run('promotion/get_promotion', $item->course_id);
        $Today = date("Y-m-d H:i:s");
        $NewDate = date ("Y-m-d H:i:s", strtotime("+7 day", strtotime($item->created_at)));
        $myCourse   = Modules::run('profile/get_mycourses_array', $item->course_id);
?>

<div class="Item">
    <?php if(!empty($myCourse) && $myCourse['status']==1):?>
        <a href="<?=site_url("courses-lesson/{$item->slug}")?>">
    <?php else: ?>
        <a href="<?=site_url("courses/{$item->slug}")?>">
    <?php endif; ?>

        <div class="boxx">
            <div class="ribbin">
                <input type="hidden" id="course_id-<?=$item->course_id?>" value="<?=$item->course_id?>">
                <div id="showRemain-mobile-<?=$item->course_id?>" class=" hidden">
                </div>

                <?php if($Today < $NewDate){?>
                <div class="box new">
                    <span class="a1">New</span>
                </div>
                <?php } ?>

                <?php if(!empty($promotion)){?>
                <div class="box pro protext-<?=$item->course_id?>">
                    <span class="a1">Promotion</span>
                </div>
                <?php }?>

                <?php $img = "'".base_url('images/cose.jpg')."'"; ?>
            </div>
            <div class="img">

                <img src="<?=base_url($item->file)?>" class="ImgFluid imgBig" alt="'.$item->title.'"
                    onerror="this.src= <?=$img?> ">
                <div class="hovv">
                    <div class="text Bold">
                        <span><i class="fas fa-link" style="font-size: 18px;"></i>
                            รายละเอียดคอร์สเรียน</span>
                    </div>
                </div>
            </div>
            <div class="detail">
                <div class="name Bold">
                    <p><?=$item->title?></p>
                </div>
                <div class="teacher">
                    <span class="Bold">โดยคุณครู</span>
                    <span><?=$infoUser->fullname.' ('.$infoUser->fname.' )'?></span>
                </div>
                <div class="price">
                    <?php if ($infoUser->user_id===$this->session->users['UID']): ?>
                        <!-- ++++++++++ กรณีคอสของตัวเอง ++++++++++++ -->
                        <div class="free">
                            <span class="Bold">คอร์สของฉัน</span>
                        </div>
                    <?php else: ?>
                        <!-- ++++++++++ กรณีคอสของคนอื่น ++++++++++++ -->
                        <!-- ------------------ promotion -------------------------- -->
                        <?php if($item->price > 0 && !empty($promotion)){ 
                                if(!empty($myCourse) && $myCourse['status']==0):?>
                                    <div class="confirm">
                                        <span class="Bold ">รอการอนุมัติ</span>
                                        <div id="showRemain-<?=$item->course_id?>" style="display: none;"></div>
                                    </div>
                        <?php	elseif(!empty($myCourse) && $myCourse['status']==1):?>
                                    <div class="free">
                                        <span class="Bold">คลิกเพื่อเข้าเรียน</span>
                                        <div id="showRemain-<?=$item->course_id?>" style="display: none;"></div>
                                    </div>
                        <?php  	else:?>
                                    <div class="promotion protext-<?=$item->course_id?>">
                                        <div class="left Bold">
                                            <div id="showRemain-<?=$item->course_id?>"></div>
                                            <p class="ket"><?=number_format($item->price)?></p>
                                        </div>
                                        <div class="right">
                                            <span class="Bold"><?=number_format($promotion['discount']).' ฿'?></span>
                                        </div>
                                    </div>
                                    <div class="normal protext2-<?=$item->course_id?>" style="display: none;">
                                        <span class="Bold"><?=number_format($item->price).' ฿'?></span>
                                    </div>
                        <?php   endif;?>
                        <!-- ------------------ free -------------------------- -->
                        <?php }else if($item->price==0){ ?>
                                <div class="free">
                                    <span class="Bold">FREE</span>
                                </div>
                        <!-- ------------------ normal ------------------------ -->
                        <?php }else{ 
                                if(!empty($myCourse) && $myCourse['status']==0):?>
                                    <div class="confirm">
                                        <span class="Bold">รอการอนุมัติ</span>
                                    </div>
                        <?php	elseif(!empty($myCourse) && $myCourse['status']==1):?>
                                    <div class="free">
                                        <span class="Bold">คลิกเพื่อเข้าเรียน</span>
                                    </div>
                        <?php   else:?>
                                    <div class="normal">
                                        <span class="Bold"><?=number_format($item->price).' ฿'?></span>
                                    </div>
                        <?php   endif;?>
                        <?php } ?>

                    <?php   endif;?>
                </div>
            </div>
        </div>
    </a>
</div>

<?php 
        endforeach;
    endif;
?>