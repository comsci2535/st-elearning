<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ourteacher extends MX_Controller {
    private $perPage = 3;

	function __construct() {
		parent::__construct();
		$this->load->model('activities/activities_m');
        $this->load->model('ourteacher_m');

        $this->load->model('courses/courses_m');
        $this->load->model('courses_instructors/courses_instructors_m');
        
	}

    private function seo(){
		$title          = 'ST Developer';
		$robots         = 'ST Developer';
		$description    = 'ST Developer';
		$keywords       = 'ST Developer';
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		$meta 		   .= "<meta property='og:url' content='".site_url()."'/>";
		$meta 		   .= "<meta property='og:type' content='web'/>";
		$meta 		   .= "<meta property='og:title' content='".$title."'/>";
		$meta 		   .= "<meta property='og:description' content='".$description."'/>";
		$meta 		   .= "<meta property='og:image' content='".site_url('images/logo/logo.png')."'/>";
		return $meta;
	}

	public function index(){
		Modules::run('track/front','');
        $data = array(
            'menu'    => 'ourteacher',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'ourteacher',
            'footer'  => 'footer',
            'function'=>  array('custom','ourteacher'),
		);
		
		//loade view
        $data['total'] = $this->ourteacher_m->count_ourteacher_all_join_instructors();
        $this->load->view('template/body', $data);
    }
    
    public function detail($user_id){

        $data = array(
            'menu'    => 'ourteacher',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'ourteacher_detail',
            'footer'  => 'footer',
            'function'=>  array('custom','ourteacher'),
		);

		// get ourteacher
		$info_ourteacher  = $this->ourteacher_m->get_ourteacher_by_id_join_instructors($user_id);
        $data['ourteacher'] = $info_ourteacher->row();
        
		// get instructors
		$info_instructors = $this->ourteacher_m->get_instructors_by_id($user_id);
        $data['instructors'] = $info_instructors->row();
        
        //get instructors
        $skill_arr = explode(",",$data['instructors']->skill);
		$info_skill = $this->ourteacher_m->get_instructors_skill_by_skill_id($skill_arr);
        $data['skill'] = $info_skill->result();

        // get courses
        $param = array();
        $param['length'] = 6;
        $param['start'] = 0;
        $param['user_id'] = $user_id;

        $data['num_row'] = $this->ourteacher_m->count_courses_by_user_created($param);
        $data['per_page'] = $this->perPage;

        $data['info'] = $this->ourteacher_m->get_courses_by_user_created($param)->result();
        
        // arr($data['info']);exit();
		// $this->activities_m->plus_view($data['activities']->activitie_id);

		//loade view
        $this->load->view('template/body', $data);
	}

	public function ajax_load_ourteacher()
	{
		$mgs 	= 'แจ้งเตือน';
		$status = 0;
        $html   = '';
        
        $input = $this->input->post();
		$input['length'] = 8;
        $input['start']  = $input['length']*$input['page'];
        
      
        $input['fiter_arr'] = $fiter_arr;
        $info = $this->ourteacher_m->get_ourteacher_option_all_join_instructors($input)->result();
        $info_count = $this->ourteacher_m->get_ourteacher_count_option_all_join_instructors($input);
        $total='<p>'.$info_count.' ราย</p>';

        $pageAll=$info_count;
        $pageAll= 0 ? 1 : ceil($pageAll/$input['length']);

        if($pageAll==($input['page']+1)){
            $pageAll=0;
        }

		if($info):
            foreach($info as $item):
             if(!empty($item->file)){
                $img_intsr=base_url($item->file);
            }else{
                $img_intsr=$item->oauth_picture;
            }   
            $html.='<div class="Item colum4">
                        <a href="'.site_url("ourteacher/detail/{$item->user_id}").'">
						<div class="boxx">
                            <div class="ribbin">';
                            $html.='<input type="hidden" id="course_id-'.$item->user_id.'" value="'.$item->user_id.'">';
                            $img = "'".base_url('images/user.png')."'";
                            $html.='</div>
                            <div class="img text-center" style="padding-top: 8px;">
                                <img src="'.$img_intsr.'" class="ImgFluid imgBig" alt="'.$item->title.'" onerror="this.src= '.$img.' " style="width: 160px;object-fit: cover;">
                                <div class="hovv">
                                    <div class="text Bold">
                                        <span><i class="fas fa-link" style="font-size: 18px;"></i> รายละเอียดคุณครู</span>
                                    </div>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="name Bold">
                                    <p>'.$item->fullname.'</p>
                                </div>
                                <div class="line-buttom mx-auto"></div>
                                <div class="text-center">
                                    <a href="mailto:'.$item->username.'" class="m-card-profile__email">'.$item->email.'</a>
                                </div>
								
                                <!-- <div class="text">
                                    <span>'.$item->excerpt.'</span>
								</div> -->
								<div class="dateview d-flex justify-content-between align-items-end">
                                    <div class="">
                                        <!-- <span style="font-size: 16px;" data-original-title="" title="">คะแนน</span>
                                        <ul class="d-flex" style="padding: 0;margin: 0;list-style-type: none;">
                                            <li class="active" style="padding: 2px;"><i class="fa fa-star"></i></li>
                                            <li class="active" style="padding: 2px;"><i class="fa fa-star"></i></li>
                                            <li class="" style="padding: 2px;"><i class="fa fa-star"></i></li>
                                            <li class="" style="padding: 2px;"><i class="fa fa-star"></i></li>
                                            <li class="" style="padding: 2px;"><i class="fa fa-star"></i></li>
                                        </ul> -->
                                    </div>
                                    <!-- <div class="">
                                        <span><i class="far fa-eye"></i> 999</span>
                                    </div> -->
								</div>
                            </div>
                        </div>
						</a>
					</div>';
			endforeach;

			$status = 1;
		endif;
		
		$data = array(
			'data' 		=> $html
			,'mgs' 		=> $mgs
            ,'status' 	=> $status
            ,'test'     => $fiter_arr
            ,'pageAll' 	=> $pageAll
            ,'total' 	=> $total
		);

		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    

    public function ajax_load_courses(){
        $data = array();
        $param = array();
        
        $param['user_id'] = $this->input->get("user_id");
		// $courses = $this->ourteacher_m->get_courses_by_user_created($param);
        // $data['info'] = $courses->result();

        // $data['num_row'] = $this->ourteacher_m->count_courses_by_user_created($param);
        // $data['per_page'] = $this->perPage;
        // $data['info'] = $this->ourteacher_m->get_courses_by_user_created($param);
        // arr($data);exit();
        // $data['info'] = $this->courses_m->get_courses_option_all($input)->result();
        // $result = $this->load->view('ourteacher_data', $data);
        // json_encode($result);

        if(!empty($this->input->get("page"))){
            $param['start'] = ceil($this->input->get("page") * $this->perPage);
            $param['length'] = $this->perPage;
            
            $data['info'] = $this->ourteacher_m->get_courses_by_user_created($param)->result();

            $result = $this->load->view('ourteacher_data', $data);
            json_encode($result);  
        }else{    
            // $data['info'] = $this->ourteacher_m->get_courses_by_user_created($param)->result();
            // $result = $this->load->view('ourteacher_data', $data);
            // json_encode($result);
            
        } 
    }

}
