<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ourteacher_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        // $this->db->last_query();
    }
    
    // new function query
    public function count_ourteacher_all() 
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('type', 'instructor');
        return $this->db->count_all_results('users');
    }

    public function get_activities_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('activities');
        return $query;
    }

    public function get_ourteacher_option_all($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('fullname', $param['search']);
            // $this->db->or_like('excerpt', $param['search']);
            // $this->db->or_like('detail', $param['search']);
        endif;

        $order_by = !empty($param['sort'])? $param['sort'] : 'DESC'; 

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('type', 'instructor');
        // $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('users');
        
        return $query;
    }

    //ree
    public function get_ourteacher_count_option_all($param)
    {
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('fullname', $param['search']);
            // $this->db->or_like('excerpt', $param['search']);
            // $this->db->or_like('detail', $param['search']);
        endif;

        $order_by = !empty($param['sort'])? $param['sort'] : 'DESC'; 

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->where('type', 'instructor');
        // $this->db->order_by('recommend', $order_by);
        $this->db->order_by('created_at', $order_by);
        $this->db->select('*');
        $query = $this->db->get('users');
        
        return $query->num_rows();
    }

    public function plus_view($id)
    {
        $sql = "UPDATE activities SET qty_eye = (qty_eye+1) WHERE activitie_id=?";
        $this->db->query($sql, array($id));
    }

    public function get_ourteacher_by_id($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        // $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('users');
        
        return $query;
    }

    public function get_instructors_by_id($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('approve', 1);
        $this->db->select('*');
        $query = $this->db->get('instructors');
        
        return $query;
    }

    public function get_instructors_skill_by_skill_id($skill_id)
    {
        $this->db->where_in('skill_id', $skill_id);
        $this->db->select('*');
        $query = $this->db->get('instructors_skill');
        
        return $query;
    }

    public function count_ourteacher_all_join_instructors() 
    {
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->where('a.type', 'instructor');
        $this->db->where('b.approve', 1);
        $this->db->where('b.private', 0);
        $this->db->join('instructors b', 'a.user_id = b.user_id');
        return $this->db->count_all_results('users a');
    }

    public function get_ourteacher_by_id_join_instructors($user_id)
    {
        $this->db->where('a.user_id', $user_id);
        $this->db->select('a.*, b.*');
        $this->db->join('instructors b', 'a.user_id = b.user_id');
        $query = $this->db->get('users a');

        return $query;
    }

    public function get_ourteacher_option_all_join_instructors($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('fullname', $param['search']);
            // $this->db->or_like('excerpt', $param['search']);
            // $this->db->or_like('detail', $param['search']);
        endif;

        $order_by = !empty($param['sort'])? $param['sort'] : 'DESC'; 

        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->where('a.type', 'instructor');
        $this->db->where('b.approve', 1);
        $this->db->where('b.private', 0);
        // $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('a.created_at', 'DESC');
        $this->db->select('a.*, b.*');
        $this->db->join('instructors b', 'a.user_id = b.user_id');
        $query = $this->db->get('users a');
        
        return $query;
    }

    public function get_ourteacher_count_option_all_join_instructors($param)
    {
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->like('fullname', $param['search']);
            // $this->db->or_like('excerpt', $param['search']);
            // $this->db->or_like('detail', $param['search']);
        endif;

        $order_by = !empty($param['sort'])? $param['sort'] : 'DESC'; 

        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->where('a.type', 'instructor');
        $this->db->where('b.approve', 1);
        $this->db->where('b.private', 0);
        // $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('a.created_at', 'DESC');
        $this->db->select('a.*, b.*');
        $this->db->join('instructors b', 'a.user_id = b.user_id');
        $query = $this->db->get('users a');
        
        return $query->num_rows();
    }

    // courses --------------------------
    // public function get_courses_by_user_created($user_id) 
    // {
    //     $this->db->where('created_by', $user_id);
    //     $query = $this->db->get('courses');
    //     return $query;
    // }

    public function get_courses_by_user_created($param) 
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;

        $this->db->where('b.user_id', $param['user_id']);
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->join('courses_instructors b', 'a.course_id = b.course_id');
        $query = $this->db->get('courses a');
        return $query;
    }



    public function count_courses_by_user_created($param) 
    {
        $this->db->where('b.user_id', $param['user_id']);
        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        $this->db->join('courses_instructors b', 'a.course_id = b.course_id');
        return $this->db->count_all_results('courses a');
    }


}
