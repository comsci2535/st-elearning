<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Static_page extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('seos/seos_m');
    }

     private function seo(){

        $obj_seo = $this->seos_m->get_seos_by_display_page('aboutus')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : config_item('siteTitle');
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : config_item('metaDescription');
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('activities')."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

	public function index($page){
    	
    	

        switch ($page) {
        	case 'ข้อกำหนดและเงื่อนไขในการใช้บริการ':
        		$data = [
		            'menu'          => 'aboutus',
		            'seo'           => $this->seo(),
		            'header'        => 'header',
		            'content'       => 'conditions',
		            'footer'        => 'footer',
		            'background'    => 'bg',
		            'function'      =>  array('custom'),
				];
        		$this->load->view('template/body', $data);
        		break;
        		
        	case 'นโยบายการคืนเงิน':
        		$data = [
		            'menu'          => 'aboutus',
		            'seo'           => $this->seo(),
		            'header'        => 'header',
		            'content'       => 'refund_policy',
		            'footer'        => 'footer',
		            'background'    => 'bg',
		            'function'      =>  array('custom'),
				];
        		$this->load->view('template/body', $data);
        		break;

        	case 'นโยบายคุ้มครองข้อมูลส่วนบุคคล':
        		$data = [
		            'menu'          => 'aboutus',
		            'seo'           => $this->seo(),
		            'header'        => 'header',
		            'content'       => 'protection_policy',
		            'footer'        => 'footer',
		            'background'    => 'bg',
		            'function'      =>  array('custom'),
				];
        		$this->load->view('template/body', $data);
        		break;

        	case 'วิธีการสมัคร':
        		$data = [
		            'menu'          => 'aboutus',
		            'seo'           => $this->seo(),
		            'header'        => 'header',
		            'content'       => 'application_method',
		            'footer'        => 'footer',
		            'background'    => 'bg',
		            'function'      =>  array('custom'),
				];
        		$this->load->view('template/body', $data);
        		break;				
        	
        	default:
        		show_404();
        		break;
        }
    }

}