<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activities extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('activities_m');
        $this->load->model('seos/seos_m');
	}

    private function seo(){

        $obj_seo = $this->seos_m->get_seos_by_display_page('activities')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : 'คอร์สเรียนออลไลน์';
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : 'คอร์สเรียนออลไลน์';
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : 'คอร์สเรียนออลไลน์';
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('activities')."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

    private function seo_detail($slug){
        
        $obj_seo  = $this->activities_m->get_activities_by_slug($slug)->row();

        $title          = !empty($obj_seo->metaTitle)? config_item('siteTitle').' | '.$obj_seo->metaTitle : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
        $robots         = !empty($obj_seo->metaTitle)? $obj_seo->metaTitle : 'คอร์สเรียนออลไลน์';
        $description    = !empty($obj_seo->metaDescription)? $obj_seo->metaDescription : 'คอร์สเรียนออลไลน์';
        $keywords       = !empty($obj_seo->metaKeyword)? $obj_seo->metaKeyword : 'คอร์สเรียนออลไลน์';
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('activities/detail/'.$slug)."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

	public function index(){
		Modules::run('track/front','');
        $data = array(
            'menu'    => 'activities',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'activities',
            'footer'  => 'footer',
            'function'=>  array('custom','activities'),
		);
		
		//loade view
        $data['total'] = $this->activities_m->count_activities_all();
        $this->load->view('template/body', $data);
    }
    
    public function detail($slug){

        $data = array(
            'menu'    => 'activities',
            'seo'     => $this->seo_detail($slug),
            'header'  => 'header',
            'content' => 'activities_detail',
            'footer'  => 'footer',
            'function'=>  array('custom','activities'),
		);

		// get activities
		$info_activities  = $this->activities_m->get_activities_by_slug($slug);
		$data['activities'] = $info_activities->row();

		$this->activities_m->plus_view($data['activities']->activitie_id);

        $param['ogUrl'] = 'activities/detail/'.$slug;
        Modules::run('social/set_share', $param);
        Modules::run('track/front',$data['activities']->activitie_id);

		//loade view
        $this->load->view('template/body', $data);
	}

	public function ajax_load_activities()
	{
		$mgs 	= 'แจ้งเตือน';
		$status = 0;
        $html   = '';
        
        $input = $this->input->post();
		$input['length'] = 8;
        $input['start']  = $input['length']*$input['page'];
        
      
        $input['fiter_arr'] = $fiter_arr;
        $info = $this->activities_m->get_activities_option_all($input)->result();
        $info_count = $this->activities_m->get_activities_count_option_all($input);
        $total='<p>'.$info_count.' ข่าวสาร</p>';

        $pageAll=$info_count;
        $pageAll= 0 ? 1 : ceil($pageAll/$input['length']);
        
        if($pageAll==($input['page']+1)){
            $pageAll=0;
        }
		if($info):
			
            foreach($info as $item):
                
                $datatime = explode(',', date_languagefull($item->created_at, true,'th'));
                // $datatime[0] =  25 พฤษภาคม 2562' ,  $datatime[1] =  เวลา  00:22 น.
    
                $dateshow = explode(',', date_languagefull($item->start_date, true,'th'));
                //  $dateshow[0] =  31 พฤษภาคม 2562  เพราะ start_date เก็บแค่วันที่อย่างเดียว

            $html.='<div class="Item colum4">
						<a href="'.site_url("activities/detail/{$item->slug}").'">
						<div class="boxx">
                            <div class="ribbin">';
                            $html.='<input type="hidden" id="course_id-'.$item->activitie_id.'" value="'.$item->activitie_id.'">';
                            $img = "'".base_url('images/Getup_Teacher.jpg')."'";
                            $html.='</div>
                            <div class="img">
                                <img src="'.base_url($item->file).'" class="ImgFluid imgBig" alt="'.$item->title.'" onerror="this.src= '.$img.' ">
                                <div class="hovv">
                                    <div class="text Bold">
                                        <span><i class="fas fa-link" style="font-size: 18px;"></i> รายละเอียดข่าวสาร</span>
                                    </div>
                                </div>
                            </div>
                            <div class="detail">
                                <div class="name Bold">
                                    <p>'.$item->title.'</p>
                                </div>
                                <div class="text">
                                    <span>'.$item->excerpt.'</span>
								</div>
								<div class="dateview">
                                    <div class="date">
                                        <span><i class="far fa-calendar-alt"></i> '.$dateshow[0].'</span>
                                        <span><i class="far fa-clock"></i> เวลา '.$datatime[1].' น.</span>
                                    </div>
                                    <div class="view">
                                        <span><i class="far fa-eye"></i> '.$item->qty_eye.'</span>
                                    </div>
								</div>
                            </div>
                        </div>
						</a>
					</div>';
			endforeach;

			$status = 1;
		endif;
		
		$data = array(
			'data' 		=> $html
			,'mgs' 		=> $mgs
            ,'status' 	=> $status
            ,'test'     => $fiter_arr
            ,'pageAll' 	=> $pageAll
            ,'total' 	=> $total
		);

		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
	}

}
