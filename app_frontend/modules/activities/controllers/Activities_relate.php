<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activities_relate extends MX_Controller {

	function __construct() {
        parent::__construct();
        
        $this->load->model('activities_m');

	}

	function index($id){

		// get data activities gallery
		$info_activities_gallery  = $this->activities_m->get_activities_gallery_by_activitie_id($id);
		$data['activities_gallery'] = $info_activities_gallery->result();
		
		//loade view
        	$this->load->view('activities/activities_relate', $data);
	}
}