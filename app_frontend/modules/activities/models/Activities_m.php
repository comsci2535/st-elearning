<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Activities_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        // $this->db->last_query();
    }
    
    // new function query
    public function count_activities_all() 
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('activities');
    }

    public function get_activities_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('activities');
        return $query;
    }

    public function get_activities_option_all($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
       
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->group_start();
                $this->db->like('title', $param['search']);
                $this->db->or_like('excerpt', $param['search']);
                $this->db->or_like('detail', $param['search']);
            $this->db->group_end();
        endif;

        $order_by = !empty($param['sort'])? $param['sort'] : 'DESC'; 

        $this->db->where("((start_date ='0000-00-00') OR (start_date <= '".date('Y-m-d')."'))");
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('activities');
        
        return $query;
    }

    //ree
    public function get_activities_count_option_all($param)
    {
        if (isset($param['search']) && $param['search'] != "" ):
            $this->db->group_start();
                $this->db->like('title', $param['search']);
                $this->db->or_like('excerpt', $param['search']);
                $this->db->or_like('detail', $param['search']);
            $this->db->group_end();
        endif;

        $order_by = !empty($param['sort'])? $param['sort'] : 'DESC'; 

        $this->db->where("((start_date ='0000-00-00') OR (start_date <= '".date('Y-m-d')."'))");
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', $order_by);
        $this->db->order_by('created_at', $order_by);
        $this->db->select('*');
        $query = $this->db->get('activities');
        
        return $query->num_rows();
    }

    public function plus_view($id)
    {
        $sql = "UPDATE activities SET qty_eye = (qty_eye+1) WHERE activitie_id=?";
        $this->db->query($sql, array($id));
    }

    public function get_activities_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('activities');
        
        return $query;
    }

    public function get_activities_gallery_by_activitie_id($id)
    {
        $this->db->where('activitie_id', $id);
        $this->db->select('*');
        $query = $this->db->get('activities_gallery');
        
        return $query;
    }
}
