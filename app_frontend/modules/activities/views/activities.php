<section class="courses article">
    <div class="container">
        <div class="title Bold">
            <h1>ข่าวสารทั้งหมด</h1>
            <div class="text" id="total">
                <p><?php echo !empty($total)? $total : 0;?> หลักสูตร</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="menu_left no_shadow Bold float-right">
                    <form action="" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" id="text-search" name="search" placeholder="ค้นหาข่าวสาร..." autocomplete="off">
                                <i class="fas fa-search"></i>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 pb-5">
                <div id="CoursesItem" class="CoursesItem"></div>
                <input type="hidden" name="page" id="page" value="0">
                <div class="text-center mb-5" style="display: none" id="views-more-1">

                    <a href="javascript:void(0)" class="btn-custom btn-click-load-page" data-page="0"> <span id="form-img-div"></span> แสดงข่าวสารเพิ่มเติม</a>
                </div>
            </div>
        </div>

    </div>
</section>