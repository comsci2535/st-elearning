<section class="Secc_article">

	<?php
	// echo Modules::run('banners/index', 'articles', 'articles'); 
	?>
	<!-- Page Content -->
	<div class="container">

		<div class="mt-5">
			<div class="d-flex">
				<a class="m-2" href="<?=base_url('activities')?>">ข่าวสาร</a>
				<span class="m-2"> > </span>
				<span class="m-2" href="#"><?=$activities->title;?></span>
			</div>
		</div>
		<hr style="margin-bottom: 0px; margin-top: 0px;">
		<?php $datatime = explode(',', date_languagefull($activities->created_at, true,'th')); ?>
		<!-- <div class="date-custom font-weight-bold m-2">
			<span class="dates" style="font-size: 16px;"><i class="far fa-clock" style="font-size: 12px;"></i> <?=$datatime[0];?></span>
			<span class="dates" style="font-size: 16px;"> เวลา <?=$datatime[1];?> น.</span>
		</div> -->
		
		<!-- Portfolio Item Heading -->
		<h1 class="my-4 font-weight-bold text-center"><?=$activities->title;?></h1>
		<div class="row justify-content-center">
			<div class="col-md-12 text-center">
			<img src="<?=base_url().$activities->file;?>" class="img-thumbnail" width="70%" alt="Responsive image" onerror="this.src='<?php echo base_url("images/Getup_Teacher.jpg");?>'">

			</div>
			<?php echo Modules::run('social/share_button'); ?>
		</div>

		<div class="date">
			<span style="font-size: 16px;"><i class="far fa-calendar-alt" style="font-size: 12px;"></i> <?=$datatime[0];?></span>
			<span style="font-size: 16px;"><i class="far fa-clock" style="font-size: 12px;"></i> เวลา <?=$datatime[1];?> น.</span>
			<span style="font-size: 16px;"><i class="far fa-eye" style="font-size: 12px;"></i> <?=number_format($activities->qty_eye)?></span>
		</div>
		<!-- Portfolio Item Row -->
		<div class="row">
			<div class="col-md-12">
				<?php if(!empty($activities->excerpt)){?>
					<h4 class="my-4 font-weight-bold"><u>รายละเอียดย่อ</u></h4>
				<?php }?>
				<p><?=$activities->excerpt;?></p>
			</div>
			<div class="col-md-12">
				<?php if(!empty(html_entity_decode($activities->detail))){?>
					<h4 class="my-4 font-weight-bold"><u>รายละเอียด</u></h4>
				<?php }?>
				<div><?=html_entity_decode($activities->detail);?></div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 text-center">
				<h2 class="my-4 font-weight-bold">รูปภาพแกลเลอรี่</h2>
			</div>
		</div>
		<!-- Related Projects Row -->
		<?php echo Modules::run('activities/Activities_relate/index',$activities->activitie_id) ?>
		<!-- /.row -->


	</div>
	<!-- /.container -->

</section>