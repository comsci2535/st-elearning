<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exams extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('courses/courses_m');
		$this->load->model('exams_m');
		$this->load->model('courses_instructors/courses_instructors_m');
        $this->load->model('seos/seos_m');
		
	}

    private function seo(){

        $obj_seo = $this->seos_m->get_seos_by_display_page('exams')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | คอร์สเรียนออลไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : 'คอร์สเรียนออลไลน์';
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : 'คอร์สเรียนออลไลน์';
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : 'คอร์สเรียนออลไลน์';
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('articles')."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

	public function index($code,$id=""){


        $data = array(
            'menu'    => 'exams',
            'seo'     => $this->seo(),
            'header'  => 'no_header',
            'content' => 'exams',
            'footer'  => 'no_footer',
            'function'=>  array('custom','exams'),
		);
        $info_courses = $this->courses_m->get_courses_by_slug($code)->row_array();

        $myCourse=Modules::run('profile/get_mycourses', $info_courses['course_id']); 
        if($myCourse == 0  &&  $info_courses['price'] != 0 ){ 
            redirect_back(); exit();
        }

		$input_e['course_id']=$info_courses['course_id'];
        $input_e['exam_id']=$id;
        $exam = $this->exams_m->get_rows($input_e)->row_array();
        $data['exam'] = $exam;
        $data['info_courses'] = $info_courses;
        
        $info_s = $this->exams_m->get_course_by_id($exam['exam_id']);
       //arr($info_s);exit();
        $exam_s = $this->_question($info_s);
        // 
        // if($info_s['examNumber']!="" && count($exam_s['exam']) > $info_s['examNumber']){ 
        //     $data['examNum'] = $info_s['examNumber'] ; 
        // }else{
            $data['examNum'] = count($exam_s['exam']);
        //}

        $data['examAmount'] = $info_s['examNo'];
        $data['examAmountUser'] = $info_s['amount'];

        $this->load->view('template/no_body', $data);
	}

    public function exam_detail($code,$id)
    {
        
       $data = array(
            'menu'    => 'exams',
            'seo'     => $this->seo(),
            'header'  => 'no_header',
            'content' => 'exams_detail',
            'footer'  => 'no_footer',
            'function'=>  array('custom','exams'),
        );

        $info = $this->exams_m->get_course_by_id($id);
        $myCourse=Modules::run('profile/get_mycourses', $info['course_id']); 
        if($myCourse == 0  &&  $info['price'] != 0 ){ 
            redirect_back(); exit();
        }

      

        if($info['amount']>=$info['examNo']){ 
            redirect_back(); exit();
        }

        if ( !empty($info) && $info['answerActive'] == 1 ) {
            $now = strtotime(date("Y-m-d H:i"));
            $start = strtotime($info['lessonAnswersStart']);
            $end = strtotime($info['lessonAnswersEnd']);
            if ( $now >= $start and $now <= $end) {
                $info['answerActive'] = 1;
            } else {
                $info['answerActive'] = 0;
            }
        }

        $data['info'] = $info;
       
        $exam = $this->_question($info);
        // 
        // if($info['examNumber']!="" && count($exam['exam']) > $info['examNumber']){ 
        //     $data['examNum'] = $info['examNumber'] ; 
        // }else{
        $data['examNum'] = count($exam['exam']);
        //}
        $ee=array();
        foreach ($exam['exam'] as $key_ => $value_) {
           $ee[$key_+1]=$value_;
        }
        //arr($ee);exit();
        $data['exam'] = json_encode($ee);
        $data['answer'] = json_encode($exam['answer']);
        $data['timeUp'] = "false";

        //
       
        $value['start'] = db_datetime_now();
        $value['exam_id'] = $id;
        $value['user_id'] = $this->session->users['UID'];
        $value['status'] = 0;
        $value['type'] = 'final';
        //$value['exam_group_id'] = 0;
        $this->db->insert('quiz', $value);
        $data['quizId'] = $this->db->insert_id();
        
        // if ( !empty($info) && $info['answerActive'] == 0 ) {
        //     $data['contentView'] = "media/exam/exam_detail";
        // } else {
        //     redirect(site_url("exam/lesson_slove/{$id}"));
        // }

        $this->load->view('template/no_body', $data);
       
    }

    public function index_pretest($code,$id=""){


        $data = array(
            'menu'    => 'exams',
            'seo'     => $this->seo(),
            'header'  => 'no_header',
            'content' => 'index_pretest',
            'footer'  => 'no_footer',
            'function'=>  array('custom','exams'),
        );
        $info_courses = $this->courses_m->get_courses_by_slug($code)->row_array();

        $myCourse=Modules::run('profile/get_mycourses', $info_courses['course_id']); 
        if($myCourse == 0  &&  $info_courses['price'] != 0 ){ 
            redirect_back(); exit();
        }

        $input_e['course_id']=$info_courses['course_id'];
        $input_e['exam_id']=$id;
        $exam = $this->exams_m->get_rows($input_e)->row_array();
        $data['exam'] = $exam;
        $data['info_courses'] = $info_courses;
        
        $info_s = $this->exams_m->get_course_by_id($id);
       //arr($info_s);exit();
        $exam_s = $this->_question($info_s);
        // 
        // if($info_s['examNumber']!="" && count($exam_s['exam']) > $info_s['examNumber']){ 
        //     $data['examNum'] = $info_s['examNumber'] ; 
        // }else{
        $data['examNum'] = count($exam_s['exam']);
        //}

        $data['examAmount'] = $info_s['examNo'];
        $data['examAmountUser'] = $info_s['amount'];

        $this->load->view('template/no_body', $data);
    }

    public function exam_pretest($code,$id)
    {
        
       $data = array(
            'menu'    => 'exams',
            'seo'     => $this->seo(),
            'header'  => 'no_header',
            'content' => 'exams_detail',
            'footer'  => 'no_footer',
            'function'=>  array('custom','exams'),
        );

        $info = $this->exams_m->get_course_by_id($id);
        $myCourse=Modules::run('profile/get_mycourses', $info['course_id']); 
        if($myCourse == 0  &&  $info['price'] != 0 ){ 
            redirect_back(); exit();
        }

      

        if($info['amount']>=$info['examNo']){ 
            redirect_back(); exit();
        }

        if ( !empty($info) && $info['answerActive'] == 1 ) {
            $now = strtotime(date("Y-m-d H:i"));
            $start = strtotime($info['lessonAnswersStart']);
            $end = strtotime($info['lessonAnswersEnd']);
            if ( $now >= $start and $now <= $end) {
                $info['answerActive'] = 1;
            } else {
                $info['answerActive'] = 0;
            }
        }

        $data['info'] = $info;
       
        $exam = $this->_question($info);
        // 
        // if($info['examNumber']!="" && count($exam['exam']) > $info['examNumber']){ 
        //     $data['examNum'] = $info['examNumber'] ; 
        // }else{
        $data['examNum'] = count($exam['exam']);
        //}
        $ee=array();
        foreach ($exam['exam'] as $key_ => $value_) {
           $ee[$key_+1]=$value_;
        }
        //arr($ee);exit();
        $data['exam'] = json_encode($ee);
        $data['answer'] = json_encode($exam['answer']);
        $data['timeUp'] = "false";

        //
       
        $value['start'] = db_datetime_now();
        $value['exam_id'] = $id;
        $value['user_id'] = $this->session->users['UID'];
        $value['status'] = 0;
        $value['type'] = 'pretest';
        $this->db->insert('quiz', $value);
        $data['quizId'] = $this->db->insert_id();
        
        // if ( !empty($info) && $info['answerActive'] == 0 ) {
        //     $data['contentView'] = "media/exam/exam_detail";
        // } else {
        //     redirect(site_url("exam/lesson_slove/{$id}"));
        // }

        $this->load->view('template/no_body', $data);
       
    }

    public function index_posttest($code,$id=""){


        $data = array(
            'menu'    => 'exams',
            'seo'     => $this->seo(),
            'header'  => 'no_header',
            'content' => 'index_posttest',
            'footer'  => 'no_footer',
            'function'=>  array('custom','exams'),
        );
        $info_courses = $this->courses_m->get_courses_by_slug($code)->row_array();

        $myCourse=Modules::run('profile/get_mycourses', $info_courses['course_id']); 
        if($myCourse == 0  &&  $info_courses['price'] != 0 ){ 
            redirect_back(); exit();
        }

        $input_e['course_id']=$info_courses['course_id'];
         $input_e['exam_id']=$id;
        $exam = $this->exams_m->get_rows($input_e)->row_array();
        $data['exam'] = $exam;
        $data['info_courses'] = $info_courses;
        
        $info_s = $this->exams_m->get_course_by_id($id);
       //arr($info_s);exit();
        $exam_s = $this->_question($info_s);
        // 
        // if($info_s['examNumber']!="" && count($exam_s['exam']) > $info_s['examNumber']){ 
        //     $data['examNum'] = $info_s['examNumber'] ; 
        // }else{
        $data['examNum'] = count($exam_s['exam']);
        //}

        $data['examAmount'] = $info_s['examNo'];
        $data['examAmountUser'] = $info_s['amount'];

        $this->load->view('template/no_body', $data);
    }

    public function exam_posttest($code,$id)
    {
        
       $data = array(
            'menu'    => 'exams',
            'seo'     => $this->seo(),
            'header'  => 'no_header',
            'content' => 'exams_detail',
            'footer'  => 'no_footer',
            'function'=>  array('custom','exams'),
        );

        $info = $this->exams_m->get_course_by_id($id);
        $myCourse=Modules::run('profile/get_mycourses', $info['course_id']); 
        if($myCourse == 0  &&  $info['price'] != 0 ){ 
            redirect_back(); exit();
        }

      

        if($info['amount']>=$info['examNo']){ 
            redirect_back(); exit();
        }

        if ( !empty($info) && $info['answerActive'] == 1 ) {
            $now = strtotime(date("Y-m-d H:i"));
            $start = strtotime($info['lessonAnswersStart']);
            $end = strtotime($info['lessonAnswersEnd']);
            if ( $now >= $start and $now <= $end) {
                $info['answerActive'] = 1;
            } else {
                $info['answerActive'] = 0;
            }
        }

        $data['info'] = $info;
       
        $exam = $this->_question($info);
        // 
        // if($info['examNumber']!="" && count($exam['exam']) > $info['examNumber']){ 
        //     $data['examNum'] = $info['examNumber'] ; 
        // }else{
        $data['examNum'] = count($exam['exam']);
        //}
        $ee=array();
        foreach ($exam['exam'] as $key_ => $value_) {
           $ee[$key_+1]=$value_;
        }
        //arr($ee);exit();
        $data['exam'] = json_encode($ee);
        $data['answer'] = json_encode($exam['answer']);
        $data['timeUp'] = "false";

        //
       
        $value['start'] = db_datetime_now();
        $value['exam_id'] = $id;
        $value['user_id'] = $this->session->users['UID'];
        $value['status'] = 0;
        $value['type'] = 'posttest';
        $this->db->insert('quiz', $value);
        $data['quizId'] = $this->db->insert_id();
        
        // if ( !empty($info) && $info['answerActive'] == 0 ) {
        //     $data['contentView'] = "media/exam/exam_detail";
        // } else {
        //     redirect(site_url("exam/lesson_slove/{$id}"));
        // }

        $this->load->view('template/no_body', $data);
       
    }

    private function _question($param, $showPoint = FALSE)
    {
        $info =  $this->exams_m->get_question($param);
        $topicId = 0;
        $n = 0;
      

       //arrx($info);
        //shuffle($info);
        foreach ( $info as $key => $rs ) {
            
            $point = NULL;
            if ( $showPoint ){ $point = $rs['point']; }
            
            if ( $topicId != $rs['exam_topic_id']) {

                // $input_u['contentId'] = $rs['exam_topic_id'];
                // $input_u['grpContent'] = "exam_list";
                // $uplode = $this->media_m->get_uplode($input_u)->result_array();
                // //$rs->image=base_url("assets/images/no_image2.png");
                // if (!empty($uplode)){
                //     foreach ($uplode as $i => $value_) {
                //         if($value_['grpType']=='coverImage'){
                //             $coverImage_path=$value_['path'];
                //             $coverImage_filename=$value_['filename'];
                //              //$image = base_url($coverImage_path.$coverImage_filename);
                //              $image = '<img class="img-responsive" width="50%" style="margin:20px 10px" src="'.base_url($coverImage_path.$coverImage_filename).'"/>';
                //         }
                //     }
                // } else {
                    $image = "";
                //}

                $n = $n+1;
                $topicId = $rs['exam_topic_id'];
                $exam[] = array (
                    'questionId' => $rs['exam_topic_id'],
                    'exam_type_id' => $rs['exam_type_id'],
                    'question' => $rs['exam_topic_name']."<br/>".$image,
                );
                $choice[$topicId][$rs['exam_answer_id']] = $rs['exam_answer_name'];
                $solve[$topicId][$rs['exam_answer_id']] = $point;
            } else {
                $choice[$topicId][$rs['exam_answer_id']] = $rs['exam_answer_name'];
                $solve[$topicId][$rs['exam_answer_id']] = $point;
            }
            $n++;
        }

     //arrx($info);exit();

        if($param['answerRandom']=='1'){ shuffle($exam); }
        foreach ( $exam as $key => &$rs ) {
            $rs['choice'] = $choice[$rs['questionId']];
            if ( $showPoint ) $rs['solve'] = $solve[$rs['questionId']];
            $rs['answer'] = NULL;
            $answer[$key+1] = array('questionId'=>$rs['questionId'],'answer'=>0);
            if ( $rs['exam_type_id'] == 1 ) {
               $rs['answer'] = "";
               $answer[$key+1] = array('questionId'=>$rs['questionId'],'answer'=>"");
           }
            
        }
        
        $data['exam'] = $exam;
        $data['answer'] = $answer;
       //arrx($data);
        return $data;
        
        
    } 
    
    public function submit_quiz()
    {
        $input = $this->input->post();
       
        //arr($input);exit();
        $value = array();
        $score = 0;
        // ปรนัย
        $answer=array();
        $answerText2 = 0;
        foreach ( $input['exam'] as $key=>$rs) {
            $answerText = NULL;
            $answerChoice = NULL;
            
            if ( $rs['answer'] != NULL ) {
                $answer[] = $rs['answer'];
                $answerChoice = (int)$rs['answer'];
            }
                      
            $value[] = array(
                'quiz_id' => $input['quizId'],
                'exam_topic_id' => $rs['questionId'],
                'exam_answer_id' => $answerChoice,
                //'answerText' => $answerText,
            );
        }

       
        $this->db->insert_batch('quiz_detail', $value);
        
        //if ( $input['exam'] == 2 && !empty($answer) ) { 
        
        if(!empty($answer)){
            $score = $this->exams_m->objective_score($answer);
        }
        
        $info = $this->exams_m->get_course_by_id($input['exam_id']);

        $exam = $this->_question($input);
        $exam_c=count($exam['exam']);
        if(!empty($score)){
            //$criteria=($score*100)/$info['examNumber'];
            $criteria=($score*100)/$exam_c;

            if($criteria >= $info['pass']){ 
                $result='pass'; 
            }else{ 
                $result='fail'; 
            }
        }

        
       
        //} 
        //$score = "";
        
        $valueQuiz['result'] = $result;
        $valueQuiz['score'] = $score;
        
        $valueQuiz['end'] = db_datetime_now();
        $valueQuiz['status'] = $input['isObjective'] == 2 ? 1 : 2;
        //$valueQuiz['status'] = "";
        $this->db->where('quiz_id', $input['quizId'])->update('quiz', $valueQuiz);
        
        $data['showSlove'] = true;
        $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
        
    }
    
    public function exam_solve($id=0,$quiz_id=0)
    {
       
        $data = array(
            'menu'    => 'exams',
            'seo'     => $this->seo(),
            'header'  => 'no_header',
            'content' => 'exams_solve',
            'footer'  => 'no_footer',
            'function'=>  array('custom','exams'),
        );

       

        $data['exam_id'] = $id;
        $info = $this->exams_m->get_course_by_id($id);

        $myCourse=Modules::run('profile/get_mycourses', $info['course_id']); 
        if($myCourse == 0  &&  $info['price'] != 0 ){ 
            redirect_back(); exit();
        }
        
        $input['exam_id'] = $id;
        $exam = $this->_question($input);
        $data['exam_c']=count($exam['exam']);

        $quiz = $this->exams_m->get_quiz_by_id($quiz_id);

        $data['info'] = $info;
        $data['quiz'] = $quiz;
        // print "<pre>";
        //        print_r($data['info']);
        //        exit();
        $this->load->view('template/no_body', $data);
    }

    public function exam_history($code,$id=""){


        $data = array(
            'menu'    => 'exams',
            'seo'     => $this->seo(),
            'header'  => 'no_header',
            'content' => 'exam_history',
            'footer'  => 'no_footer',
            'function'=>  array('custom','exams'),
        );
        $info_courses = $this->courses_m->get_courses_by_slug($code)->row_array();

        $myCourse=Modules::run('profile/get_mycourses', $info_courses['course_id']); 
        if($myCourse == 0  &&  $info_courses['price'] != 0 ){ 
            redirect_back(); exit();
        }

        $input_e['course_id']=$info_courses['course_id'];
        $input_e['exam_id']=$id;
        $exam = $this->exams_m->get_rows($input_e)->row_array();
        $data['exam'] = $exam;
        $data['info_courses'] = $info_courses;
        
        $info_s = $this->exams_m->get_course_by_id($exam['exam_id']);
       //arr($info_courses);exit();
        // $exam_s = $this->_question($info_s);
        // // 
        // if($info_s['examNumber']!="" && count($exam_s['exam']) > $info_s['examNumber']){ 
        //     $data['examNum'] = $info_s['examNumber'] ; 
        // }else{
        //     $data['examNum'] = count($exam_s['exam']);
        // }

        $history = $this->exams_m->get_quiz_by_examid($id);
        $data['history'] = $history;
        $data['examAmount'] = $info_s['examNo'];
        $data['examAmountUser'] = $info_s['amount'];

        $this->load->view('template/no_body', $data);
    }
    
}
