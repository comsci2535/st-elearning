<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Exams_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('exam a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('exam a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {
        
        $this->db->where('a.recycle',0);
        $this->db->where('a.active',1);


        // $this->db->where("( (CONVERT(datetime,a.startDate) IS NULL AND CONVERT(datetime,a.endDate) IS NULL) OR (CONVERT(datetime,a.startDate) <= convert(datetime,'".date('Y-m-d')."') AND CONVERT(datetime,a.endDate) >= convert(datetime,'".date('Y-m-d')."')) )");

        if (isset($param['exam_id'])){ 
           if ($param['exam_id']!=""){ 
                $this->db->where('a.exam_id', $param['exam_id']);
            }
        }
       
        if ( isset($param['course_id']) ){
            $this->db->where('a.course_id', $param['course_id']);
        }

        if ( isset($param['pretest_posttest']) ){
            $this->db->where('a.pretest_posttest', $param['pretest_posttest']);  
        }

        $this->db->order_by('a.active', 'DESC');
        $this->db->order_by('a.created_at', 'DESC');
                  

    }

    
    
    public function get_course_by_id($id)
    {
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title as course_name , b.slug as course_slug , b.price as price')
                        ->from('exam a')   
                        ->join('courses b', "a.course_id = b.course_id", 'left')           
                        ->where('a.exam_id', $id)
                        ->get()
                        ->row_array();
                        
                        foreach ($query as $key => $value) {
                            $query['amount']=$this->db
                                            ->select('*')
                                            ->from('quiz')
                                            ->where('exam_id', $id)
                                            ->where('user_id', $this->session->users['UID'])
                                            ->get()
                                            ->num_rows();
                        }
        return $query;
    }
    
    public function get_quiz_by_id($id)
    {
        $query = $this->db
                        ->from('quiz a')
                        ->where('a.quiz_id', $id)
                        ->where('a.user_id', $this->session->users['UID'])
                        ->get()
                        ->row_array();
        return $query;
    }

    public function get_exam_by_exam_id($id)
    {
        $query = $this->db
                        ->from('exam a')
                        ->where('a.exam_id', $id)
                        ->get();
        return $query;
    }
    
    public function get_quiz_by_user_id($user_id, $course_id)
    {
        $query = $this->db
                        ->from('quiz a')
                        ->join('exam b', 'a.exam_id = b.exam_id')
                        ->where('a.user_id', $user_id)
                        ->where('b.course_id', $course_id)
                        ->get();
        return $query;
    }

    public function get_quiz_detail_by_id($id)
    {
        $query = $this->db
                        ->from('quiz_detail a')
                        ->where('a.quiz_id', $id)
                        ->get()
                        ->result_array();
        return $query;
    } 
    
    public function get_quiz_detail_by_exam_answer_id($id, $quiz_id)
    {
        $query = $this->db
                        ->from('quiz_detail a')
                        ->where('a.quiz_id', $quiz_id)
                        ->where('a.exam_answer_id', $id)
                        ->get()
                        ->result_array();
        return $query;
    }

    public function get_quiz_by_examid($id)
    {
        $query = $this->db
                        ->from('quiz a')
                        ->where('a.exam_id', $id)
                        ->where('a.user_id', $this->session->users['UID'])
                        ->get()
                        ->result_array();
        return $query;
    }
     
    public function get_question($param)
    {
       $query = $this->db
            ->select('a.exam_topic_id, a.exam_topic_name, a.exam_type_id, a.examPath, a.examFile,a.active')
            ->select('b.exam_answer_id, b.exam_answer_name, b.point')
            ->from('exam_topic a')
            ->join('exam_answer b', 'a.exam_topic_id = b.exam_topic_id', 'left')
            ->where('a.active', 1)
            ->where('a.exam_id', $param['exam_id'])
            ->order_by('a.exam_topic_id', 'ASC')
            ->get()
            ->result_array();
        return $query;
    }

    public function get_detail_by_exam_topic_id($id)
    {
        $query = $this->db
                        ->from('exam_answer a')
                        ->where('a.exam_topic_id', $id)
                        ->get()
                        ->result();
        return $query;
    }

    public function get_question_quiz_detail($param)
    {
        $query = $this->db
            ->select('a.exam_topic_id, a.exam_topic_name, a.exam_type_id, a.examPath, a.examFile,a.active')
            ->from('exam_topic a')
            ->where('a.active', 1)
            ->where('a.exam_id', $param['exam_id'])
            ->order_by('a.exam_topic_id', 'ASC')
            ->get()->result();

            foreach($query as $value):
                $value->exam_answer = $this->get_detail_by_exam_topic_id($value->exam_topic_id);
                if(!empty($value->exam_answer)):
                    foreach($value->exam_answer as $answer):
                        $answer->quiz_select = $this->get_quiz_detail_by_exam_answer_id($answer->exam_answer_id, $param['quiz_id']);
                    endforeach;
                endif;
                
            endforeach;
        return $query;
    }
    
    
    public function objective_score($answer)
    {
        $query = $this->db
                        ->select("SUM(point) point")
                        ->from('exam_answer')
                        ->where_in('exam_answer_id', $answer)
                        ->get()
                        ->row_array();
        return $query['point'];
    }
    
    public function all_child_category($table="", $categoryId="", $parentField="", $parentId=0 )
    {
        $sql = "SELECT GROUP_CONCAT(lv SEPARATOR ',') AS allChild FROM (
            SELECT @pv:=(SELECT GROUP_CONCAT({$categoryId} SEPARATOR ',') FROM {$table} WHERE
            FIND_IN_SET({$parentField}, @pv)) AS lv FROM {$table}
            JOIN (SELECT @pv:=?)tmp WHERE {$categoryId} IN (@pv)) a;";
        $query = $this->db
                        ->query($sql, array($parentId))
                        ->row_array();
        if ($query['allChild'] == "") {
           $category = $parentId;
        } else {
           $category = $query['allChild'].','.$parentId; 
        }
        $category = explode(',', $category);
        return $category;
    }    
    
    public function all_parent_category($table="", $categoryId="", $parentField="", $currentId=0 )
    {
        $sql = "SELECT GROUP_CONCAT(T2.categoryId SEPARATOR ',') AS allParent
                FROM (
                    SELECT
                        @r AS _id,
                        (SELECT @r := {$parentField} FROM {$table} WHERE {$categoryId} = _id) AS parent_id,
                        @l := @l + 1 AS lvl
                    FROM
                        (SELECT @r := ?, @l := 0) vars,
                        {$table} h
                    WHERE @r <> 0) T1
                JOIN {$table} T2
                ON T1._id = T2.{$categoryId}
                ORDER BY T1.lvl DESC";
        $query = $this->db
                        ->query($sql, array($currentId))
                        ->row_array();
        $category = explode(',', $query['allParent']);
        return $category;
    }

    public function get_quiz_final($id)
    {
        $query = $this->db
                        ->from('quiz a')
                        ->where('a.exam_id', $id)
                        ->where('a.user_id', $this->session->users['UID'])
                        ->get()
                        ->row_array();
        return $query;
    }

    public function get_quiz_pretest_posttest($id,$type)
    {
        $query = $this->db
                        ->from('quiz a')
                        ->where('a.exam_id', $id)
                        ->where('a.user_id', $this->session->users['UID'])
                         ->where('a.type', $type)
                        ->get()
                        ->row_array();
        return $query;
    }

    public function get_lesson_persen($slug)
    {
         $query = $this->db
                        ->select('a.course_lesson_id')
                        ->from('courses_lesson a')
                        ->join('courses b', 'a.course_id = b.course_id', 'left')
                        ->where('a.active', 1)
                        ->where('a.parent_id !=', 0)
                        ->where('b.slug', $slug)
                        ->get()
                        ->num_rows();
     
        return $query;
        
    }
    public function get_lesson_member_persen($slug)
    {
         $query = $this->db
                        ->select('sum(a.persen) as persen_all')
                        ->from('courses_lesson_member a')
                        ->join('courses_lesson b', 'a.course_lesson_id = b.course_lesson_id', 'left')
                        ->join('courses c', 'b.course_id = c.course_id', 'left')
                        ->where('c.slug', $slug)
                        ->where('a.user_id', $this->session->users['UID'])
                        ->get()
                        ->row_array();
     
        return $query;
        
    }

    public function get_quiz_type_by_user_id($user_id, $course_id, $type)
    {
        $query = $this->db
                        ->from('quiz a')
                        ->join('exam b', 'a.exam_id = b.exam_id')
                        ->where('a.user_id', $user_id)
                        ->where('a.type', $type)
                        ->where('b.course_id', $course_id)
                        ->order_by('a.quiz_id', 'desc')
                        ->get();
        return $query;
    }
}
