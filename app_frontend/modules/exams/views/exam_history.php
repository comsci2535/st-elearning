<section class="exams-body">
    <div class="container">
      <div class="row">

        <!-- Course -->
        <div class="col-lg-12">
          
          <div class="course_container-2">
             <div class="col-xs-12 head">
                <p class="fontLv6"><i class="fa fa-book" aria-hidden="true"></i> <?=$exam['title'];?> : <?php echo $info_courses['title'];?></p>
              
                <hr>
            </div>
            <div class="course_info-2 d-flex  align-items-lg-center align-items-start justify-content-start">

              <table>
                <tr>
                  <th style="text-align: center;">ครั้งที่</th>
                  <th style="text-align: center;">วันและเวลาเริ่ม</th>
                  <th style="text-align: center;">วันและเวลาสิ้นสุด</th>
                  <th style="text-align: center;">สถานะ</th>
                  <th style="text-align: center;">คะแนนที่ได้</th>
                  <th style="text-align: center;">ผลการสอบ</th>
                </tr>
                <?php foreach ($history as $key => $rs) { ?>
               
                <tr>
                  <td style="text-align: center;"><?php echo $key+1 ?></td>
                  <td style="text-align: center;"><?php if($rs['start']!=""){ echo date_language($rs['start'],true,'th'); } ?></td>
                  <td style="text-align: center;"><?php if($rs['end']!=""){ echo date_language($rs['end'],true,'th'); }else{ echo "ไม่ได้ส่งข้อสอบ"; } ?></td>
                  <td style="text-align: center;"><?php if($rs['status']=="2"){ echo "ส่งข้อสอบ"; }else{  echo "ไม่ได้ส่งข้อสอบ"; } ?></td>
                  <td style="text-align: center;"><?php echo $rs['score'] ?></td>
                  <td style="text-align: center;"><?php if($rs['result']=="pass"){ echo "ผ่าน"; }else{  echo "ไม่ผ่าน"; } ?></td>
                </tr>
                <?php } ?>
              </table>


            </div>
             <div class="col-12 text-center" style="padding-top: 20px;" >
               ท่านได้ทำข้อสอบ <?php echo $examAmountUser;?>/<?php echo $examAmount;?> ครั้ง
            </div>
             <div class="col-12 text-center" style="padding-top: 10px;" >
                <!--  <a  class="btn btn-warning " href="<?php echo site_url("courses-lesson/{$info_courses['slug']}");?>">กลับไปหน้าคอร์สเรียน</a> -->
                  <a  class="btn btn-success " href="<?php echo site_url("exams/index/{$info_courses['slug']}/{$exam['exam_id']}");?>">กลับไปหน้าแบบทดสอบ</a>
              <?php if($examAmountUser < $examAmount ){ ?>
<a  class="btn btn-primary Bold" href="<?php echo site_url('exams/exam_detail/'.$info_courses['slug'].'/'.$exam['exam_id']);?>"><span id="form-img-div"></span> ทำข้อสอบอีกครั้ง</a>
             <?php } ?>
              <!--  <?php if($examAmountUser >= 1){ ?>
<a  class="btn btn-warning Bold" href="<?php echo site_url('exams/exam_history/'.$info_courses['slug'].'/'.$exam['exam_id']);?>"><span id="form-img-div"></span> ประวัติการสอบ</a>
             <?php } ?> -->
            </div>


            
          </div>
        </div>

        
      </div>
    </div>
  </section>