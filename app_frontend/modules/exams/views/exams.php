<section class="exams-body">
    <div class="container">
      <div class="row">

        <!-- Course -->
        <div class="col-lg-12">
          
          <div class="course_container-2">
             <div class="col-xs-12 head">
                <p class="fontLv6"><i class="fa fa-book" aria-hidden="true"></i> <?=$exam['title'];?></p>
                <p class="fontLv8 color6"><?=$exam['excerpt'];?></p>
                <hr>
            </div>
            <div class="course_info-2 d-flex  align-items-lg-center align-items-start justify-content-start">

              <!-- Course Info Item -->
              <div class="course_info_item-2  col-lg-4">
                <div class="Bold">หลักสูตร/คลังสื่อ :</div>
                <div class="course_info_text-2"><?php echo $info_courses['title'];?></div>
              </div>

              <!-- Course Info Item -->
              <div class="course_info_item-2  col-lg-2 ">
                <div class="Bold">เวลาในการทำข้อสอบ :</div>
               <div class="course_info_text-2"><?php echo $exam['examTime'];?> (นาที)</div>
              </div>

              <!-- Course Info Item -->
              <div class="course_info_item-2  col-lg-2 ">
                <div class="Bold">สอบได้ไม่เกิน :</div>
                <div class="course_info_text-2"><?php echo $exam['examNo'];?> (ครั้ง)</div>
              </div>

              <div class="course_info_item-2  col-lg-2 ">
                <div class="Bold">จำนวนข้อสอบ :</div>
               <div class="course_info_text-2"><?php echo $examNum;?> (ข้อ)</div>
              </div>

              <!-- Course Info Item -->
              <div class="course_info_item-2  col-lg-2 ">
                <div class="Bold">เกณฑ์การผ่าน :</div>
                <div class="course_info_text-2"><?php echo $exam['pass'];?> (%)</div>
              </div>


            </div>
             <div class="col-12 text-center" style="padding-top: 20px;" >
               ท่านได้ทำข้อสอบ <?php echo $examAmountUser;?>/<?php echo $examAmount;?> ครั้ง
            </div>
             <div class="col-12 text-center" style="padding-top: 10px;" >
               <!--  <a  class="btn btn-warning " href="<?php echo site_url("courses-lesson/{$info_courses['slug']}");?>">กลับไปหน้าคอร์สเรียน</a> -->
              <?php if($examAmountUser < $examAmount ){ ?>
<a  class="btn btn-primary Bold" href="<?php echo site_url('exams/exam_detail/'.$info_courses['slug'].'/'.$exam['exam_id']);?>"><span id="form-img-div"></span> เริ่มทำข้อสอบ</a>
             <?php } ?>
               <?php if($examAmountUser >= 1){ ?>
<a  class="btn btn-info Bold" href="<?php echo site_url('exams/exam_history/'.$info_courses['slug'].'/'.$exam['exam_id']);?>"><span id="form-img-div"></span> ประวัติการสอบ</a>
             <?php } ?>
            </div>


            
          </div>
        </div>

        
      </div>
    </div>
  </section>